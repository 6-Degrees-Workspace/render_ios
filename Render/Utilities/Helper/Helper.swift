import Foundation
import UIKit
//import Reachability
import CoreLocation
import AwaitToast


let sWidth = UIScreen.main.bounds.width
let sHeight = UIScreen.main.bounds.height

@available(iOS 13.0, *)
class Helper: NSObject {
    
    static let sharedInstance = Helper()
    
    internal static let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    var isLoaderDisplay = false
    var isNetworkConnected = true
    var networkAlert:UIAlertController?
    var isLocationServiceAlertDisplay = false
    var isAppOpenFromNotification = false

    // Reachability instance for Network status monitoring
   // let reachbility = try? Reachability()
    var locationAlert : UIAlertController!
    fileprivate var dictButtonHolderForMultipleTap:NSMutableDictionary = [:]
    fileprivate var homeNavigationController:UINavigationController?
    
    static func BG(_ block: @escaping ()->Void) {
        DispatchQueue.global(qos: .default).async(execute: block)
    }
    
    static func UI(_ block: @escaping ()->Void) {
        DispatchQueue.main.async(execute: block)
    }
    
    func showLogOutAlertNativeDialog(title:String, message:String,completion:@escaping ((String)->Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: .default){ (action) in
            completion("Yes")
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .destructive){ (action) in
            completion("No")
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        Helper.appDelegate?.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showTwoButtonAlertNativeDialog(title:String, message:String,completion:@escaping ((String)->Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: .default){ (action) in
            completion("Yes")
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .destructive){ (action) in
            completion("No")
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        Helper.appDelegate?.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showNotificationBanner(title:String?, message:String, buttonTitle:String?, controller:UIViewController?){
        
    }
 
 
    func showAlertViewControllerWith(title:String?, message:String, buttonTitle:String?, controller:UIViewController?){
        
    }
  

    func showProgressHudViewWithTitle(title:String?){
//        if(isLoaderDisplay){
//            hideProgressHudView()
//        }
        isLoaderDisplay = true
        //if(( try? Reachability().isReachable) != nil){
            Spinner.show("")
        //}
        Helper.appDelegate!.window?.isUserInteractionEnabled = false
    }
   
    
    func hideProgressHudView(){
        Spinner.hide()
        Helper.appDelegate!.window?.isUserInteractionEnabled = true
    }
 
    func getViewController<T>(storyboardName:String, viewControllerName:String) -> T {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerName) as! T
        return viewController
    }
    

    func removeOptionalString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
    
    func randomString(length: Int, isUDIDAllow:Bool = false) -> String {
        
        var letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        if(isUDIDAllow){
            letters = "\(NSUUID().uuidString)" as NSString
        }
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    var controller:UIViewController?
    
    func showDeeplinkInfo() {
       /*
        guard let token = ApplicationPreference.getAccessToken() else {
            return
        }
        
//        if token != nil  {
            if let viewType = UserDefaults.standard.value(forKey:"type") as? String {
                if let rootVC = getVisibleViewController(nil){
                    if rootVC is AddLocationVC || rootVC is OrderDetailsVC || rootVC is SignOutVC || rootVC is WifiStepsVC || rootVC is DeviceSetupPopup || rootVC is CancelSubscriptionVC || rootVC is ManageSubscriptionVC || rootVC is ChooseAvailableSubscriptionVC || rootVC is DeviceFilterVC || rootVC is ConfirmationVC || rootVC is EditDeviceVC || rootVC is EditDeviceReadyVC || rootVC is UpdateDeviceVC || rootVC is FactoryResetVC || rootVC is SacnDeviceVC || rootVC is ScanProcessingStep_1VC || rootVC is ScanProcessingStep_2VC || rootVC is EditWSDDeviceVC || rootVC is EditWSDDeviceReadyVC || rootVC is FactoryReset_1VC || rootVC is HomeFilterVC || rootVC is ReportVC || rootVC is ForgotPasswordVC{
                        Spinner.show("")
                        if let popupVC = rootVC as? BaseVC{
                            popupVC.dismissController()
                        }
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                    Spinner.hide()
                    if token.trim().count > 0 && viewType == Deeplink.home{
                        if let vc: BaseVC = getVisibleViewController(nil) as? BaseVC{
                            vc.actionHome(UIButton())
                        }
                    }else if token.trim().count > 0 && viewType == Deeplink.signin {
                        if let vc: BaseVC = getVisibleViewController(nil) as? BaseVC{
                            vc.actionHome(UIButton())
                        }
                    }else if token.trim().count == 0 && viewType == Deeplink.signin {
//                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
//                            appDelegate.navigationLoginVC()
//                        }
                    }else if token.trim().count > 0 && viewType == Deeplink.device {
                        if let vc: BaseVC = getVisibleViewController(nil) as? BaseVC{
                            let objVC : DevicesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: DevicesVC.nameOfClass)
                            vc.navigationController?.pushViewController(objVC, animated: true)
                        }
                    }
                }
            }
        
//                guard let chatvc = UIStoryboard.init(name: "Message", bundle:nil).instantiateViewController(withIdentifier: "ChatViewC") as? ChatViewC else { return }
//                chatvc.arrProjectData = self.selectedProject
//                chatvc.selectedFreelancer = self.selectedUser
//                let navi = UINavigationController(rootViewController: chatvc)
//                if #available(iOS 13.0, *) {
//                    navi.modalPresentationStyle = .overFullScreen
//                    navi.isModalInPresentation = true
//                }
//                self.controller?.present(navi, animated: true)
            
            
        */
    }
    
}

@available(iOS 13.0, *)
func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIViewController().window()?.rootViewController
        }
        if rootVC?.presentedViewController == nil {
            if rootVC is UINavigationController {
                let navigationController = rootVC as! UINavigationController
                return navigationController.viewControllers.last!
            }else{
                return rootVC
            }
        }
        if let presented = rootVC?.presentedViewController {
            if presented is UINavigationController {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            if presented is UITabBarController {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            return getVisibleViewController(presented)
        }
        return nil
    }

//MARK:- Network
/*@available(iOS 13.0, *)
extension Helper {
    
    // Called whenever there is a change in NetworkReachibility Status
    // — parameter notification: Notification with the Reachability instance
    @objc func handleNetworkChangeEvent(_ notification: Foundation.Notification!) -> Void {
        let reachability = notification.object as! Reachability
        switch reachability.connection {
        case .none:
            debugPrint("Network became unreachable")
            isNetworkConnected = false
            showNetworkNotAvailableAlertController()
        case .wifi:
            debugPrint("Network reachable through WiFi")
            isNetworkConnected = true
            showScreenInfo()
           Helper.appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
        case .cellular:
            debugPrint("Network reachable through Cellular Data")
            isNetworkConnected = true
            showScreenInfo()
            Helper.appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
        case .unavailable:
            print("unavailable")
        }
    }
    
 
    
    //MARK:- Check Network status
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleNetworkChangeEvent),
                                               name: Notification.Name.reachabilityChanged,
                                               object: try? Reachability())
        do{
            try? Reachability().startNotifier()
            // reconnect socket
            if((try? Reachability().connection == .wifi ||  ((try? Reachability().connection == .cellular) != nil)) != nil){
               // self.connectSocket()
            }
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    /// Stops monitoring the network availability status
    func stopMonitoring(){
       // self.disconnectSocket()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleNetworkChangeEvent),
                                               name: Notification.Name.reachabilityChanged,
                                               object: try? Reachability())
        do{
            try? Reachability().startNotifier()
            // reconnect socket
            if((try? Reachability().connection == .wifi || ((try? Reachability().connection == .cellular) != nil)) != nil){
                
            }
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
} */

//MARK: Degree to Radian and Radian to degree converstion
@available(iOS 13.0, *)
extension Helper {

    func radiansToDegrees (_ radians: Double)->Double {
        return radians * 180 / Double.pi
    }

     
    func degreesToRadians (_ degrees: Double)->Double {
        return degrees / 180 * Double.pi
    }
    
    
    func removeOptionalWordFromString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
        
}

@available(iOS 13.0, *)
extension Helper {
    
    func showToast(isError: Bool?, title: String?){    
        
        // Get singleton appearance object
        let defaultAppearance = ToastAppearanceManager.default
        
        // Update singletone appearance properties
        defaultAppearance.numberOfLines = 0
        defaultAppearance.textAlignment = .left
        defaultAppearance.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 16, bottom: 5, right: 16)

        ToastAppearanceManager.default.height = AutomaticDimension
        defaultAppearance.backgroundColor = isError == true ? UIColor.red : .red
        let toast: Toast = Toast.default(text: title ?? "", direction: .top)
        
        toast.show()
    }
 
}


@available(iOS 13.0, *)
extension Helper{
    //Check Location service
    func isDeviceLocationServiceOn() -> Bool{
        var isFound = false
        
        if(CLLocationManager.locationServicesEnabled() == true && (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways)){
            isFound = true
        }
        
        return isFound
    }
        
}


public extension NSObject{
    class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
extension UIViewController{
    func showTwoButtonAlertNativeDialog(title:String, message:String,completion:@escaping ((String)->Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: .destructive){ (action) in
            completion("Yes")
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .default){ (action) in
            completion("No")
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}


//var users: [Contact] = [Contact]()// processJSONData(filename: "UsersData.json")
func processJSONData<T: Decodable>(filename: String) -> T {
  let data: Data
  guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
      else {
          fatalError("Couldn't find \(filename) in main bundle.")
  }
  
  do {
      data = try Data(contentsOf: file)
  } catch {
      fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
  }
  
  do {
      let decoder = JSONDecoder()
      return try decoder.decode(T.self, from: data)
  } catch {
      fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
  }
}
