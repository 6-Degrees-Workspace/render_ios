import UIKit


class DashboardTabBarController: UITabBarController {
    let gradientlayer = CAGradientLayer()
    
    //@IBOutlet weak var tabBar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //tabBar.sizeThatFits(CGSize(width: 100, height: 100))
        //setGradientBackground(colorOne: .yellow, colorTwo: .red)
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 2
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.3
        
        if #available(iOS 13, *) {
            // iOS 13:
            let appearance = tabBar.standardAppearance
            appearance.configureWithOpaqueBackground()
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            tabBar.standardAppearance = appearance
        } else {
            // iOS 12 and below:
            tabBar.shadowImage = UIImage()
            tabBar.backgroundImage = UIImage()
        }
        
        func setGradientBackground(colorOne: UIColor, colorTwo: UIColor)  {
            gradientlayer.frame = tabBar.bounds
            gradientlayer.colors = [colorOne.cgColor, colorTwo.cgColor]
            gradientlayer.locations = [0, 1]
            gradientlayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientlayer.endPoint = CGPoint(x: 0.0, y: 1.0)
            self.tabBar.layer.insertSublayer(gradientlayer, at: 0)
        }
        
    }
    

//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        tabBar.frame.size.height = self.view.frame.height * 0.12
//        tabBar.frame.origin.y = view.frame.height - self.view.frame.height * 0.12
//    }
    
}
