import UIKit

extension UIViewController {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func addDoneButtonOnKeyboard(textField:UITextField){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        textField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        NotificationCenter.default.post(name: Notification.Name("DoneButtonForPostalCode"), object: nil)
        self.dismissKeyboard()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlert(message:String){
        let alertController = UIAlertController(title: "Render", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion:{
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    func alertWithAction(_ title:String, _ message:String,_ completionHandler:@escaping(UIAlertAction)->()) -> Void {
        let titleString = title
        let messageString = message
        let alertController = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
        let okString = "OK"
        let cancelAction = UIAlertAction(title: okString, style: .cancel, handler:completionHandler)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true,  completion:{
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
        
    }
    
    func conditionalAlert(_ title:String?, _ message:String?,_ acceptHandler:@escaping(UIAlertAction)->(),_ declineHandler:@escaping(UIAlertAction)->()) -> Void {
        
        let alertController = UIAlertController(title: title , message: message , preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "No" , style: UIAlertAction.Style.default, handler: declineHandler)
        
        let OKAction = UIAlertAction.init(title: "Yes" , style: .default, handler:acceptHandler)
        
        alertController.addAction(OKAction)
        
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true,  completion:{
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    func showPhotoOptionAlert(_ acceptHandler:@escaping(UIAlertAction)->(),_ declineHandler:@escaping(UIAlertAction)->()) -> Void {
        
        let alertController = UIAlertController(title: "Render", message: "From where you want to select photo.", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Camera", style: .default, handler: declineHandler)
        
        let OKAction = UIAlertAction.init(title: "Gallery", style: .default, handler:acceptHandler)
        
        alertController.addAction(OKAction)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion:{
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
        
    }
    
    @objc func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showLoader(){
        self.view.activityStartAnimating(activityColor: UIColor.white, backgroundColor: UIColor(white: 0.0/255, alpha: 0.5))
    }
    
    func showCustomLoader() {
        //self.view.customActivator(newView: self.view)
    }
    
    func removeCustomLoader() {
        DispatchQueue.main.async { () -> Void in
            self.view.activityStopCustomAnimation()
        }
    }
    
    func removLoader(){
         DispatchQueue.main.async { () -> Void in
        self.view.activityStopAnimating()
        }
    }
    
    func showToast(message : String, color: UIColor) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-140, width: 200, height: 35))
            toastLabel.backgroundColor =  UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = color
        toastLabel.font = UIFont(name: "Lato-Bold", size: 22)
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 10.0, delay: 0.2, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
