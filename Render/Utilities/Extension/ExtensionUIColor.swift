import Foundation
import UIKit

extension UIColor {

    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func appBlueBackground() -> UIColor {
        return hexStringToUIColor(hex: "#3E9DD7")
    }
    
    class func appRedBackground() -> UIColor {
        return hexStringToUIColor(hex: "#D73E3E")
    }
    
    class func appBlackBackground() -> UIColor {
        return hexStringToUIColor(hex: "#000000")
    }

    class func appGrayBackground() -> UIColor {
        return hexStringToUIColor(hex: "#E0E0E0")
    }
    
    class func appOrnageBackground() -> UIColor {
        return hexStringToUIColor(hex: "#EC6933")
    }
    
    static var appDefault: UIColor {
        return UIColor(red: 0.2, green: 0.4, blue: 1, alpha: 1)
    }
}
 




