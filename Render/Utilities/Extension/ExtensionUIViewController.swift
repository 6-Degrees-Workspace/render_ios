import Foundation
import UIKit

//@available(iOS 13.0, *)
extension UIViewController {
    func tapToDismiss() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissController))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissController() {
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @discardableResult func present(viewController: UIViewController?, parent: UIViewController) -> Self? {
        guard let presentedVC = viewController else { return nil }
        presentedVC.modalPresentationStyle = .overCurrentContext
        presentedVC.modalPresentationCapturesStatusBarAppearance = true
        parent.present(presentedVC, animated: true, completion: nil)
        return presentedVC as? Self
    }
    
    func animateLineView(view:UIView, onButton: UIButton, withConstraint: NSLayoutConstraint) {
        UIView.animate(withDuration: 0.15) {
            var frame = view.frame
            frame.origin.x = onButton.frame.origin.x
            withConstraint.constant = onButton.frame.origin.x
            view.frame = frame
        }
    }
    func window() -> UIWindow? {
        struct Static {
            /** @abstract   Save keyWindow object for reuse.
             @discussion Sometimes [[UIApplication sharedApplication] keyWindow] is returning nil between the app.   */
            static weak var keyWindow: UIWindow?
        }
        if let originalKeyWindow = UIApplication.shared.keyWindow,
           (Static.keyWindow == nil || Static.keyWindow != originalKeyWindow) {
            Static.keyWindow = originalKeyWindow
        }
        //Return KeyWindow
        return Static.keyWindow
    }
}

extension UINavigationController {
    func presentViewAnimated(viewController:UIViewController) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = .moveIn
        transition.subtype = .fromTop
        self.view.layer.add(transition, forKey: nil)
        self.pushViewController(viewController, animated: true)
    }
    
    func dismissViewController() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = .reveal
        transition.subtype = .fromTop
        self.view.layer.add(transition, forKey: nil)
        self.popViewController(animated: true)
    }
    
}


extension NSNotification.Name{
    static let kNotificationReloadUserData = Notification.Name("reloadUserData")
}
