import Foundation


extension Date {
    
    static let DIFF_YEAR = "y"
    static let DIFF_MONTH = "M"
    static let DIFF_WEEK = "w"
    static let DIFF_DAY = "d"
    static let DIFF_HOUR = "h"
    static let DIFF_MIN = "m"
    static let DIFF_SEC = "s"
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
    func daysBetween(date: Date) -> Int {
        return Date.daysBetween(start: self, end: date)
    }
    
    static func daysBetween(start: Date, end: Date) -> Int {
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        
        let a = calendar.dateComponents([.day], from: date1, to: date2)
        return a.value(for: .day)!
    }
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    func adding(months: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
    
    func adding(years: Int) -> Date {
        return Calendar.current.date(byAdding: .year, value: years, to: self)!
    }
    
}


//MARK:- DateTimeUtils
@available(iOS 13.0, *)
class DateTimeUtils {
    
    static let sharedInstance = DateTimeUtils()
    
    static let MILLI_SECOND_IN_HOUR:Double = 1000*60*60 //long
    static let MILLI_SECOND_IN_TWO_HOUR:Double = MILLI_SECOND_IN_HOUR * 2
    static let MILLI_SECOND_IN_DAY:Double = MILLI_SECOND_IN_HOUR*24;
    
    private var DF_TIME_hh_mm_aa:DateFormatter?
    var DF_TIME_yyyy_MM_dd:DateFormatter?
    private var DF_TIME_dd_MMM_yy:DateFormatter?
    private var DF_TIME_dd_MMMM_yyyy:DateFormatter?
    private var DF_TIME_dd_MMM_yyyy:DateFormatter?
    private var DF_TIME_dd_MMM:DateFormatter?
    private var DF_dd_MM_yyyy:DateFormatter?
    private var DF_DAY_MMM_dd_yyyy:DateFormatter?
    private var DF_MMMM:DateFormatter?
    private var DF_FULL_DAY_DD_MM_YYYY:DateFormatter?
    private var DF_dd_MM_yyyy_TIME:DateFormatter?
    private var DF_dd_MM_yyyy_TIME_12_HOURS:DateFormatter?
    private var DF_dd_MM_TIME_12_HOURS:DateFormatter?
    
    private var DF_MMM_dd_yyyy:DateFormatter?
    
    
    //MARK:- Time Like yesterday, today
    private var msgTimeTextAnHourAgo:String?
    private var msgTimeTextTodayAt:String?
    private var msgTimeTextYesterdayAt:String?
    
    private var msgTimeTextTodayAtHeader:String?
    private var msgTimeTextYesterdayAtHeader:String?
    private var msgTimeTextLastSeen:String?
    
    
    init(){
        loadFormatter()
    }
    
    func loadFormatter() {
        
        DF_TIME_hh_mm_aa = DateFormatter()
        if(DF_TIME_hh_mm_aa != nil){
            DF_TIME_hh_mm_aa?.dateFormat = "hh:mm a"
            DF_TIME_hh_mm_aa?.timeZone = TimeZone.current
            DF_TIME_hh_mm_aa?.locale = Locale.current
        }
        
        
        DF_TIME_dd_MMMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMMM_yyyy != nil){
            DF_TIME_dd_MMMM_yyyy?.dateFormat = "dd-MMMM-yyyy"
            DF_TIME_dd_MMMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMMM_yyyy?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMM_yyyy != nil){
            DF_TIME_dd_MMM_yyyy?.dateFormat = "dd-MMM-yyyy"
            DF_TIME_dd_MMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yyyy?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM = DateFormatter()
        if(DF_TIME_dd_MMM != nil){
            DF_TIME_dd_MMM?.dateFormat = "dd-MMMM"
            DF_TIME_dd_MMM?.timeZone = TimeZone.current
            DF_TIME_dd_MMM?.locale = Locale.current
        }
        
        DF_TIME_yyyy_MM_dd = DateFormatter()
        if(DF_TIME_yyyy_MM_dd != nil){
            DF_TIME_yyyy_MM_dd?.dateFormat = "yyyy/MM/dd HH:mm:ss z"
            DF_TIME_yyyy_MM_dd?.timeZone = TimeZone.current
            DF_TIME_yyyy_MM_dd?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM_yy = DateFormatter()
        if(DF_TIME_dd_MMM_yy != nil){
            DF_TIME_dd_MMM_yy?.dateFormat = "dd/MMM/yy"
            DF_TIME_dd_MMM_yy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yy?.locale = Locale.current
        }
        
        DF_DAY_MMM_dd_yyyy = DateFormatter()
        if(DF_DAY_MMM_dd_yyyy != nil){
            DF_DAY_MMM_dd_yyyy?.dateFormat = "EEE, dd MMM, yyyy"
            DF_DAY_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_DAY_MMM_dd_yyyy?.locale = Locale.current
        }
        
        DF_MMMM = DateFormatter()
        if(DF_MMMM != nil){
            DF_MMMM?.dateFormat = "MMMM yyyy"
            DF_MMMM?.timeZone = TimeZone.current
            DF_MMMM?.locale = Locale.current
        }
        
        DF_FULL_DAY_DD_MM_YYYY = DateFormatter()
        if(DF_FULL_DAY_DD_MM_YYYY != nil){
            DF_FULL_DAY_DD_MM_YYYY?.dateFormat = "EEEE, dd MMMM, yyyy"
            DF_FULL_DAY_DD_MM_YYYY?.timeZone = TimeZone.current
            DF_FULL_DAY_DD_MM_YYYY?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy = DateFormatter()
        if(DF_dd_MM_yyyy != nil){
            DF_dd_MM_yyyy?.dateFormat = "dd/MM/yyyy"
            DF_dd_MM_yyyy?.timeZone = TimeZone.current
            DF_dd_MM_yyyy?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy_TIME  = DateFormatter()
        if(DF_dd_MM_yyyy_TIME != nil){
            DF_dd_MM_yyyy_TIME?.dateFormat = "dd/MM/yyyy HH:mm:ss z"
            DF_dd_MM_yyyy_TIME?.timeZone = TimeZone.current
            DF_dd_MM_yyyy_TIME?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy_TIME_12_HOURS = DateFormatter()
        if(DF_dd_MM_yyyy_TIME_12_HOURS != nil){
            //DF_dd_MM_yyyy_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm a"
            DF_dd_MM_yyyy_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm"
            DF_dd_MM_yyyy_TIME_12_HOURS?.timeZone = TimeZone.current
            DF_dd_MM_yyyy_TIME_12_HOURS?.locale = Locale.current
        }
        
        DF_dd_MM_TIME_12_HOURS = DateFormatter()
        if(DF_dd_MM_TIME_12_HOURS != nil){
            //DF_dd_MM_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm a"
            DF_dd_MM_TIME_12_HOURS?.dateFormat = "dd-MMM HH:mm"
            DF_dd_MM_TIME_12_HOURS?.timeZone = TimeZone.current
            DF_dd_MM_TIME_12_HOURS?.locale = Locale.current
        }
        
        DF_MMM_dd_yyyy = DateFormatter()
        if(DF_MMM_dd_yyyy != nil){
            DF_MMM_dd_yyyy?.dateFormat = "MMM dd, yyyy"
            DF_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_MMM_dd_yyyy?.locale = Locale.current
        }
    }

    
    func currentDateForConfirRX(date:Date) ->String? {
        return (DF_MMM_dd_yyyy?.string(from: date))!
    }
    
    func formatSimpleTime(date:Date) ->String? {
        return (DF_TIME_hh_mm_aa?.string(from: date))!
    }
    
    func formatFancyDate(date:Date) ->String? {
        return (DF_TIME_dd_MMMM_yyyy?.string(from: date))!
    }
    
    func formatFancyDate1(date:Date) ->String? {
        return (DF_TIME_dd_MMM_yy?.string(from: date))!
    }
    
    func formatFancyDate2(date:Date) ->String? {
        return (DF_MMM_dd_yyyy?.string(from: date))!
    }
    
    func formatDatePicker(date:Date) ->String? {
        return (DF_MMM_dd_yyyy?.string(from: date))!
    }
    
    func formatFancyShortDate(date:Date) ->String? {
        return (DF_TIME_dd_MMM_yyyy?.string(from: date))!
    }
    
    func formatFancyDateForHistory(date:Date) ->String? {
        return (DF_TIME_dd_MMM?.string(from: date))!
    }
    func  formatSimpleDate(date:Date) -> String?{
        return (DF_TIME_dd_MMM_yy?.string(from: date))!
    }
    
    func formatDateToString(date:Date) -> String{
        return (DF_TIME_yyyy_MM_dd?.string(from: date))!
    }
    
    func formateDateToMonthString(date:Date) -> String{
        return (DF_MMMM?.string(from: date))!
    }
    
    func formateFullDateStr(date:Date) -> String{
        return (DF_FULL_DAY_DD_MM_YYYY?.string(from:date))!
    }
    
    func formateDateTimeToString(date:Date) -> String{
        return (DF_dd_MM_yyyy_TIME?.string(from:date))!
    }
    
    func formateDateTimeTo12HourTimeString(date:Date) -> String{
        return (DF_dd_MM_yyyy_TIME_12_HOURS?.string(from:date))!
    }
    
    func formateShortDateTimeTo12HourTimeString(date:Date) -> String{
        return (DF_dd_MM_TIME_12_HOURS?.string(from: date))!
    }
       
    func getCurrentsDate() -> String{
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, yyyy"
        let result = formatter.string(from: date)
        print("result",result )
        return result
    }
    
    func thirtyDaysBeforeToday() -> String{
        let today = Date()
        let thirtyDaysBeforeToday = Calendar.current.date(byAdding: .day, value: -30, to: today)!
        print("thirtyDaysBeforeToday",thirtyDaysBeforeToday)
        
        let df = DateFormatter()
        df.dateFormat = "MMM dd, yyyy"
        let dateString = df.string(from: thirtyDaysBeforeToday)
        print("dateString",dateString)
        return dateString
        
    }
    
    func describeComparison(startDate: Date, endDate: Date) -> String {
        
        var descriptionArray: [String] = []
        
        if startDate < endDate {
            descriptionArray.append("date1 < date2")
        }
        
        if startDate <= endDate {
            descriptionArray.append("date1 <= date2")
        }
        
        if startDate > endDate {
            descriptionArray.append("FromDateGreater")
            Helper.sharedInstance.showToast(isError: false, title: "To date should be greater than from date. Dates are set to default")
        }
        
        return descriptionArray.joined(separator: ",  ")
    }
    
    func convertDateFormat(inputDate: String) -> String {

         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = "MMM dd, yyyy"

         let oldDate = olDateFormatter.date(from: inputDate)

         let convertDateFormatter = DateFormatter()
         convertDateFormatter.dateFormat = "MM/dd/yyyy"

         return convertDateFormatter.string(from: oldDate!)
    }
    
    func convertDate(inputDate: String) -> Date {

         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = "MMM dd, yyyy"
         let oldDate = olDateFormatter.date(from: inputDate)
        return oldDate!
    }

    
    func getCurrentMonthName() -> String{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    func getCurrentDate() -> Date {
        let now = Date()//.addingTimeInterval(TimeInterval(-2.0 * 60.0))

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-DD HH:mm:ss"
        let strDate = dateFormatter.string(from: now)
        let currentDate = dateFormatter.date(from: strDate)
        return currentDate!
    }
    
    //MARK:- To Get Current Time Stamp
    func getCurrentTimeInterval() -> TimeInterval{
       // return (getCurrentDate().timeIntervalSince1970)*1000
        return (getCurrentDate().timeIntervalSince1970)
    }
    
    func getDateFrom(strTimeinterval : String?)-> Date?{
        let doubleTimeInterval = strTimeinterval?.toNSNumber()?.doubleValue
        let timeinterval = TimeInterval(exactly: doubleTimeInterval!)
        let date = Date(timeIntervalSince1970: timeinterval!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date_ = dateFormatter.date(from: date.stringFromFormat("yyyy-MM-dd HH:mm:ss"))?.toLocalTime()
        return date_
    }
    
    
    func getTimeIntervalFromDate(date:Date) -> TimeInterval{
        //return (date.timeIntervalSince1970)*1000
        return (date.timeIntervalSince1970)
    }
        
    func getDateFromTimeInterval(timeInterval:TimeInterval) -> Date{
        //return Date(timeIntervalSince1970: timeInterval / 1000.0)
        return Date(timeIntervalSince1970: timeInterval)
    }
    

    func getimeIntervalFromDate(timeInterval:Date) -> String{
        return String(describing: Date.timeIntervalSince(timeInterval))
    }
    
   
    func getSeenAtDateFromString(strDate:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let myDate = dateFormatter.date(from: strDate)!
        dateFormatter.dateFormat = "MMM dd, EEEE"
        let somedateString = dateFormatter.string(from: myDate)
        return somedateString
    }
    
    
    func dateFromExpirdDateString(strDate:String) -> Date? {
        // "2019-08-06T23:43:00.000Z"
        
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        //        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: strDate)?.toLocalTime()
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //        dateFormatter.timeZone = NSTimeZone.init(name: "UTC")  as TimeZone?
        let timeStamp = dateFormatter.string(from: date!)
        let newDate = dateFormatter.date(from: timeStamp)?.toLocalTime()
        
        return newDate
    }
    
    func calculateDaysBetweenTwoDates(start: Date, end: Date) -> Int {

        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: start) else {
            return 0
        }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: end) else {
            return 0
        }
        return end - start
    }
    
    
    // let strUTCOpenTime = DateTimeUtils.sharedInstance.localToUTC(date: strStartTime, fromFormat: "hh:mm a", toFormat: "hh:mm a")
    // let strUTCCloseTime = DateTimeUtils.sharedInstance.UTCToLocal(date: strEndTime, fromFormat: "hh:mm a", toFormat: "hh:mm a")
    
    func getDateFromString(strDate:String,strDateFormatter:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strDateFormatter
        let convertedDate = dateFormatter.date(from:strDate)
        return convertedDate
    }
    
    //MARK:- ⏰ Time From Date Time String ⏰
    //MARK:-
    func getTimeFromDateTimeString(strDate:String) -> String? {
        
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: strDate)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    //TODO:- "yyyy-MM-DD HH:mm:ss"
    func getOnlyDate(strDate: String?)->Date? {
        //TODO:- "2019-09-04 13:23:34"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let doubleTimeInterval = strDate?.toNSNumber()?.doubleValue
        let timeinterval = TimeInterval(exactly: doubleTimeInterval!)
        let date_ = Date(timeIntervalSince1970: timeinterval!).stringFromDateFormatter("yyyy-MM-dd HH:mm:ss")
        
        let date = dateFormatter.date(from: date_)//?.toLocalTime()
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //                dateFormatter.timeZone = TimeZone.current//NSTimeZone.init(name: "UTC")  as TimeZone?
        //        dateFormatter.locale = Locale.current
        let timeStamp = dateFormatter.string(from: date!)
        let newDate = dateFormatter.date(from: timeStamp)?.toLocalTime()
        
        return newDate
    }
    
    //TODO:- "yyyy-MM-DD HH:mm:ss"
    /*
     String Date to Date Object
     */
    func getDateFromStringDateWithDateFormate(strDate: String?)->Date? {
        //TODO:- "2019-09-04 13:23:34"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let doubleTimeInterval = strDate?.toNSNumber()?.doubleValue
        let timeinterval = TimeInterval(exactly: doubleTimeInterval!)
        let date_ = Date(timeIntervalSince1970: timeinterval!).stringFromDateFormatter("yyyy-MM-dd HH:mm:ss")
        
        
        let date = dateFormatter.date(from: date_)?.toLocalTime()
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                dateFormatter.timeZone = TimeZone.current//NSTimeZone.init(name: "UTC")  as TimeZone?
//        dateFormatter.locale = Locale.current
        let timeStamp = dateFormatter.string(from: date!)
        let newDate = dateFormatter.date(from: timeStamp)
        
        return newDate
    }
    
    //MARK:- String Date To Date
    func getDateFromStringDate(strDate:String) -> Date? {
        // "2019-08-06T23:43:00.000Z"
        
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: strDate)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd"
//        dateFormatter.timeZone = NSTimeZone.init(name: "UTC")  as TimeZone?
        let timeStamp = dateFormatter.string(from: date!)
        let newDate = dateFormatter.date(from: timeStamp)
        
        return newDate
    }
    
    func getLocalDateFromString(_ withDateString : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "yyyy-MM-dd"
        dateFormatter.timeZone = .current
        return dateFormatter.date(from: withDateString)!
    }
    
    func getTimeOnlyForChatUI(timeInterval:String) -> String? {
        
        let doubleTimeInterval = timeInterval.toNSNumber()?.doubleValue
        let timeinterval = TimeInterval(exactly: doubleTimeInterval!)
        var date_ = Date(timeIntervalSince1970: timeinterval!)
//        date_.addTimeInterval(TimeInterval(2.0 * 60.0))
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: date_.stringFromFormat("yyyy-MM-dd HH:mm:ss"))
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    func getDateForTransactionFilterUI(strTimeStamp: String?)->String? {
        //TODO:- "2019-09-04 13:23:34"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
//
        let doubleTimeInterval = strTimeStamp!.toNSNumber()!.doubleValue
        let timeinterval = TimeInterval(doubleTimeInterval)
        let date_ = Date(timeIntervalSince1970: timeinterval)
        let strDate = dateFormatter.string(from: date_)
    
        return strDate
    }
}

extension Date {
    

    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
        return targetDay
    }
    
    /// Returns a Date with the specified days subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        let inverseYears = -1 * years
        let inverseMonths = -1 * months
        let inverseDays = -1 * days
        let inverseHours = -1 * hours
        let inverseMinutes = -1 * minutes
        let inverseSeconds = -1 * seconds
        return add(years: inverseYears, months: inverseMonths, days: inverseDays, hours: inverseHours, minutes: inverseMinutes, seconds: inverseSeconds)
    }
    
    // MARK: - Format dates
    
    func stringFromFormat(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func stringFromDateFormatter(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.calendar = NSCalendar.current
        formatter.timeZone = TimeZone.current
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.locale = Locale.current
        return formatter.string(from: self)
    }
    
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
}

@available(iOS 13.0, *)
extension Date {
    
    func getChatMessageHeader() -> String {
        let sourceDate = self
        
        let currentDate = DateTimeUtils.sharedInstance.getCurrentDate()
        var secondsAgo = Double(currentDate.timeIntervalSince(sourceDate))
        if secondsAgo < 0 {
            secondsAgo = secondsAgo * (-1)
        }
        
        let minute = 60.0
        let hour = 60.0 * minute
        let day = 24 * hour
        let week = 7 * day
        
        if secondsAgo < minute  {
            //            if secondsAgo < 2 {
            //                return "just now"
            //            }else{
            //                //                return "\(secondsAgo) secs ago"
            //                return "just now"
            //            }
            return "Today"
        } else if secondsAgo < hour {
            //            let min = Double(secondsAgo/minute)
            //
            //            let roundedMin = round(min)
            //            let roundedMinInt = Int(roundedMin)
            //
            //            let strMin = roundedMinInt.toString()//formatter.string(from: (min) as NSNumber)
            //            if roundedMinInt == 1{
            //                return "\(strMin ?? "") min ago"
            //            }else{
            //                return "\(strMin ?? "") min ago"
            //            }
            return "Today"
        } else if secondsAgo < day {
            return "Today"
            //            let hr = Double(secondsAgo/hour)
            //
            //            let roundedHr = round(hr)
            //            let roundedHrInt = Int(roundedHr)
            //
            //            let strHr = roundedHrInt.toString()//formatter.string(from: (hr) as NSNumber)
            //            if roundedHrInt == 1{
            //                return "\(strHr ?? "") hr ago"
            //            } else {
            //                return "\(strHr ?? "") hrs ago"
            //            }
        } else if secondsAgo < week {
            let days = Double(secondsAgo/day)
            let rounded = Double(round(days))
            let roundedDays = Int(rounded)
            //let strDays = formatter.string(from: (days) as NSNumber)
            if roundedDays == 1{
                return "Yesterday"//"\(day) day ago"
            }else{
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMM yyyy"
                formatter.locale = Locale(identifier: "en_US")
                let strDate: String = formatter.string(from: sourceDate)
                return strDate
            }
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM yyyy"
            formatter.locale = Locale(identifier: "en_US")
            let strDate: String = formatter.string(from: sourceDate)
            return strDate
        }
    }
    
    func timeAgoSinceDate(numericDates:Bool = true) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date().toLocalTime()
        let earliest = now < self ? now : self
        let latest = (earliest == now) ? self : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
//                latest
                
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
//            return "\(components.hour!) hours ago"
            return "Today"
        } else if (components.hour! >= 1){
            if (numericDates){
//                return "1 hour ago"
                return "Today"
            } else {
//                return "An hour ago"
                return "Today"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
//            return "Today"
        } else if (components.minute! >= 1){
            if (numericDates){
//                return "1 minute ago"
                return "Today"
            } else {
//                return "A minute ago"
                return "Today"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
//            return "Today"
        } else {
            return "Just now"
//            return "Today"
        }
        
    }
    
    func seenAtAgo(numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date().toLocalTime()
        let earliest = now < self ? now : self
        let latest = (earliest == now) ? self : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
                
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
            
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
                
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
            
        } else {
            return "Just now"
        }
        
    }
    

}

@available(iOS 13.0, *)
extension DateTimeUtils {
    
    func getDeviceTimeZoneDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMM yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        //Current Date
        let currentdate = Date()
        var currentStr = dateFormatter.string(from: currentdate)
        
        // YesterDay Date
        var dateComponents = DateComponents()
        dateComponents.setValue(-1, for: .day) // -1 day
        let yesterday = Calendar.current.date(byAdding: dateComponents, to: currentdate)
        
        var yesterdayStr = dateFormatter.string(from: yesterday!)
        
        if strDate == currentStr {
            currentStr = "Today"
            return currentStr
        }
        if strDate == yesterdayStr {
            yesterdayStr = "Yesterday"
            return yesterdayStr
        }
        
        return strDate
   }
    
    func getChatSectionDeviceTimeZoneDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let convertedDate = self.convertTimeToEasyWithoutDate(sourceDate: date, sourceDateDouble: dateUnixFormate)
        print("Convert date: \(convertedDate ?? ""))")
        return convertedDate ?? ""
    }
 
    func convertTimeToEasyWithoutDate(sourceDate:Date, sourceDateDouble:Double) -> String?{
        
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date().toLocalTime()
        let earliest = now < sourceDate ? now : sourceDate
        let latest = (earliest == now) ? sourceDate : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        
        var easyDateTimeString = ""
        let currentDate = Date()
        let difference = currentDate.offset(from: sourceDate)
        var sameDay = false
        if(difference.hasSuffix(Date.DIFF_HOUR) || difference.hasSuffix(Date.DIFF_MIN) || difference.hasSuffix(Date.DIFF_SEC)){
            sameDay = true
        }
        
        let currentTimeInterval:Double = currentDate.timeIntervalSinceNow
        let messageTimeInterval:Double = Date().timeIntervalSince(sourceDate)
        
        let dayCount = sourceDate.daysBetween(date: currentDate)
        
        if(sameDay && dayCount < 1){
            if (currentTimeInterval - messageTimeInterval < DateTimeUtils.MILLI_SECOND_IN_HOUR) {
                easyDateTimeString = String(format: "\(components.minute!) " +  "\(msgTimeTextTodayAtHeader!)" )
            }
            if ((currentTimeInterval - messageTimeInterval > DateTimeUtils.MILLI_SECOND_IN_HOUR)
                && (currentTimeInterval - messageTimeInterval < DateTimeUtils.MILLI_SECOND_IN_TWO_HOUR)) {
                easyDateTimeString =  String(format: "\(components.hour!) " +  "\(msgTimeTextAnHourAgo!)" )
               
            }
            if ((currentTimeInterval - messageTimeInterval > DateTimeUtils.MILLI_SECOND_IN_TWO_HOUR)) {
                easyDateTimeString = String(format: msgTimeTextTodayAtHeader ?? "")
            }
        }else{
            if (currentTimeInterval - messageTimeInterval < DateTimeUtils.MILLI_SECOND_IN_DAY && dayCount < 2) {
                easyDateTimeString = String(format: msgTimeTextYesterdayAtHeader ?? "")
            } else {
                easyDateTimeString = getDeviceTimeZoneDateStrFromUnixFormat(sourceDateDouble) ?? ""
            }
        }
        return easyDateTimeString
    }
    
}


//MARK:-
@available(iOS 13.0, *)
extension DateTimeUtils {
//Sandeep code here..
    
    func getPastTime(timeInterval : String) -> String {
        
        
        let formatter: NumberFormatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        
        let sourceDate = Date(timeIntervalSince1970: timeInterval.toNSNumber()!.doubleValue)
        
        let currentDate = DateTimeUtils.sharedInstance.getCurrentDate()
        var secondsAgo = Double(currentDate.timeIntervalSince(sourceDate))
        if secondsAgo < 0 {
            secondsAgo = secondsAgo * (-1)
        }
        
        let minute = 60.0
        let hour = 60.0 * minute
        let day = 24 * hour
        let week = 7 * day
        
        if secondsAgo < minute  {
            if secondsAgo < 2 {
                return "just now"
            }else{
//                return "\(secondsAgo) secs ago"
                return "just now"
            }
        } else if secondsAgo < hour {
            let min = Double(secondsAgo/minute)
            
            let roundedMin = round(min)
            let roundedMinInt = Int(roundedMin)
            
            let strMin = roundedMinInt.toString()//formatter.string(from: (min) as NSNumber)
            if roundedMinInt == 1{
                return "\(strMin ?? "") min ago"
            }else{
                return "\(strMin ?? "") min ago"
            }
        } else if secondsAgo < day {
            let hr = Double(secondsAgo/hour)
            
            let roundedHr = round(hr)
            let roundedHrInt = Int(roundedHr)
            
            let strHr = roundedHrInt.toString()//formatter.string(from: (hr) as NSNumber)
            if roundedHrInt == 1{
                return "\(strHr ?? "") hr ago"
            } else {
                return "\(strHr ?? "") hrs ago"
            }
        } else if secondsAgo < week {
            let days = Double(secondsAgo/day)
            let rounded = Double(round(days))
            let roundedDays = Int(rounded)
            //let strDays = formatter.string(from: (days) as NSNumber)
            if roundedDays == 1{
                return "Yesterday"//"\(day) day ago"
            }else{
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                formatter.locale = Locale(identifier: "en_US")
                let strDate: String = formatter.string(from: sourceDate)
                return strDate
            }
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            formatter.locale = Locale(identifier: "en_US")
            let strDate: String = formatter.string(from: sourceDate)
            return strDate
        }
    }
    func getPastTimeForDashboardOnly(timeInterval : String) -> String {

        let formatter: NumberFormatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        
        let sourceDate = Date(timeIntervalSince1970: timeInterval.toNSNumber()!.doubleValue).toLocalTime()
        
        
        let currentDate = DateTimeUtils.sharedInstance.getCurrentDate().toLocalTime()
      
        debugPrint(currentDate)
        
        
        var secondsAgo = Double(currentDate.timeIntervalSince(sourceDate))
        if secondsAgo < 0 {
            secondsAgo = secondsAgo * (-1)
        }
        
        let minute = 60.0
        let hour = 60.0 * minute
        let day = 24 * hour
        let week = 7 * day

        if secondsAgo < minute  {
            if secondsAgo < 2 {
                return "just now"
            }else{
//                return "\(secondsAgo) secs ago"
                return "just now"
            }
        } else if secondsAgo < hour {
            let min = Double(secondsAgo/minute)
            
            let roundedMin = round(min)
            let roundedMinInt = Int(roundedMin)
            
            let strMin = roundedMinInt.toString()//formatter.string(from: (min) as NSNumber)
            if roundedMinInt == 1{
                return "\(strMin ?? "") min ago"
            }else{
                return "\(strMin ?? "") min ago"
            }
        } else if secondsAgo < day {
            let hr = Double(secondsAgo/hour)
            
            let roundedHr = round(hr)
            let roundedHrInt = Int(roundedHr)
            
            let strHr = roundedHrInt.toString()//formatter.string(from: (hr) as NSNumber)
            if roundedHrInt == 1{
                return "\(strHr ?? "") hr ago"
            } else {
                return "\(strHr ?? "") hrs ago"
            }
        } else if secondsAgo < week {
            let days = Double(secondsAgo/day)
            let rounded = round(days)
            let roundedDays = Int(rounded)
            //let strDays = formatter.string(from: (days) as NSNumber)
            if roundedDays == 1{
                return "Yesterday"//"\(day) day ago"
            }else{
                //                return "\(day) days ago"
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mma  dd MMM yyyy"
                formatter.locale = Locale(identifier: "en_US")
                let strDate: String = formatter.string(from: sourceDate)
                return strDate
            }
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "h:mma  dd MMM yyyy"
            formatter.locale = Locale(identifier: "en_US")
            let strDate: String = formatter.string(from: sourceDate)
            return strDate
        }
    }
    
    func getDateTimeForNotificationList(timeInterval : String) -> String {
        let sourceDate = Date(timeIntervalSince1970: timeInterval.toNSNumber()!.doubleValue).toLocalTime()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy h:mma"
        formatter.locale = Locale(identifier: "en_US")
        let strDate: String = formatter.string(from: sourceDate)
        return strDate
    }


}
