import Foundation
import UIKit

extension String {
    var length: Int {
        return self.count
    }
    
    // Returns true if the string has at least one character in common with matchCharacters.
    func containsCharactersIn(matchCharacters: String) -> Bool {
        let characterSet = NSCharacterSet(charactersIn: matchCharacters)
        return self.rangeOfCharacter(from: characterSet as CharacterSet) != nil
    }
    
    // Returns true if the string contains only characters found in matchCharacters.
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return self.rangeOfCharacter(from: disallowedCharacterSet) == nil
        //return self.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
    }
    
    // Returns true if the string has no characters in common with matchCharacters.
    func doesNotContainCharactersIn(matchCharacters: String) -> Bool {
        let characterSet = NSCharacterSet(charactersIn: matchCharacters)
        return self.rangeOfCharacter(from: characterSet as CharacterSet) == nil
    }
    
    // Returns true if the string represents a proper numeric value.
    // This method uses the device's current locale setting to determine
    // which decimal separator it will accept.
    func isNumeric() -> Bool
    {
        let scanner = Scanner(string: self)
        
        // A newly-created scanner has no locale by default.
        // We'll set our scanner's locale to the user's locale
        // so that it recognizes the decimal separator that
        // the user expects (for example, in North America,
        // "." is the decimal separator, while in many parts
        // of Europe, "," is used).
        scanner.locale = NSLocale.current
        
        return scanner.scanDecimal(nil) && scanner.isAtEnd
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    
    func isValidPassword() -> Bool {
        
        if (self.components(separatedBy:(NSCharacterSet.whitespacesAndNewlines)).count > 1) {
            return false
        }
        return true
    }
    
    func isValidFullName() -> Bool {
        let regularExpression = "^([a-zA-Z]{1,}\\s[a-zA-z]{0,}'?-?[a-zA-Z]{1,}\\s?([a-zA-Z]{0,})?)"
        let fullnameValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return fullnameValidation.evaluate(with: self)
    }
    
    /*var password = "@Abcd011" //string from UITextField (Password)
     password.isValidPassword() // -> true*/
    func isContainValidPassword() -> Bool {
        let regularExpression = /*"^{5,}$"*/"((?=.*[0-9])(?=.*[!@#$&*])(?=.*[a-z])(?=.*[A-Z]).{8,12})"
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: self)
    }
    //    func isPasswordValid(_ password : String) -> Bool{
    //        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].0*[a-z].*[a-z]).{8}$")
    //        return passwordTest.evaluate(with: password)
    //    }
    func isMatchConfirmPassword(password : String) -> Bool {
        return (password == self)
    }
    
    func trimmedString() -> String? {
        return (self.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines)))
    }
    
    func isValidURL() -> Bool {
        let urlRegEx = "(www).((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let urlText = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        return urlText.evaluate(with: self)
    }
    
    func isPasswordValidate() -> Bool{
     
        let regexPasswdPattrn  =   "(?=.*[A-Z])(?=.*[@#$%])(?=.*[0-9])(?=.*[a-z]).{8,20}"
        let texttest = NSPredicate(format:"SELF MATCHES %@", regexPasswdPattrn)
        let isValidate = texttest.evaluate(with: self)
        
        return isValidate
        
    }
    
    func isValidName() -> Bool {
        let nameRegEx = "[a-zA-z.\\s]+([ '-][a-zA-Z]+)*$"
        
        let nameText = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameText.evaluate(with: self)
    }
    
    func isValidUserName() -> Bool {
        let usernameRegEx = "^(?=.{1,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
        
        let userTest = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
        return userTest.evaluate(with: self)
    }
    
    func isValidZipcode() -> Bool {
        let zipRegEx = "([0-9]{6}|[0-9]{3}\\s[0-9]{3})"
        
        let zipText = NSPredicate(format:"SELF MATCHES %@", zipRegEx)
        return zipText.evaluate(with: self)
    }
    
    func isValidPhoneNumber() -> Bool {
        let phoneRegEx = "^+(?:[0-9] ?){6,14}[0-9]$"
        
        let phoneText = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneText.evaluate(with: self)
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat(Float.greatestFiniteMagnitude))
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.width
    }
    
    var byWords: [String] {
        var result:[String] = []
        enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) {
            guard let word = $0 else { return }
            print($1,$2,$3)
            result.append(word)
        }
        return result
    }
    var lastWord: String {
        return byWords.last ?? ""
    }
    func lastWords(_ max: Int) -> [String] {
        return Array(byWords.suffix(max))
    }

    
    func append(lastName: String) -> String {
        return self + " " + lastName
    }
    
    //MARK:- String to Int Conversion
     func toInt()->Int? {
            let val = NSString(format: "%@", self)
            return val.integerValue
    }
    
    func toNSNumber()->NSNumber? {
    
    if let myInteger = Int(self) {
        let myNumber = NSNumber(value:myInteger)
        return myNumber
      }
        return 0
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    

    func removeOptionalString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
}

//MARK: - ↓↓ Dictionary ↓↓
//MARK: -
extension Dictionary where Value: Equatable {
    func containsValue(value : Value) -> Bool {
        return self.contains { $0.1 == value }
    }
    
    
}


//MARK:- Bool to NSNumber Conversion
extension Bool {
    func toNumber() -> NSNumber {
        let num = NSNumber(booleanLiteral: self)
        return num
    }
}


//MARK:- Int to String Conversion
extension Int {
    func toString()->String? {
        let val = NSNumber(integerLiteral: self)
        return val.stringValue
    }
    
}


extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}




extension String {
    
    static func findTimeDifference(time1Str: String, time2Str: String) {
        //let time1 = time1Str
        //let time2 = time2Str
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        let date1 = formatter.date(from: time1Str)!
        let date2 = formatter.date(from: time2Str)!
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "hh:mm a"
        
        if let startDate = dateFormatter.date(from: time1Str),
           let endDate = dateFormatter.date(from: time2Str) {
            let hr = Calendar.current.dateComponents([.hour], from: startDate, to: endDate < startDate ? Calendar.current.date(byAdding: .day, value: 1, to: endDate) ?? endDate : endDate).hour ?? 0
            let elapsedTime = date2.timeIntervalSince(date1)
            // convert from seconds to hours, rounding down to the nearest hour
            let hours = floor(elapsedTime / 60 / 60)
            let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
            if Int(hours) == 0 && Int(hours) == 0 {
                //  lblTrespassingDuration.text = "Trespassing duration set off"
            }else {
                //  lblTrespassingDuration.text = "Trespassing duration set for \(Int(hr)) Hrs and  \(Int(minutes)) Mins (Everyday)"
            }
            
        }
    }
    
    static func convertUTCToLocal(timeString: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let timeUTC = dateFormatter.date(from: timeString)
        
        if timeUTC != nil {
            dateFormatter.timeZone = NSTimeZone.local
            
            let localTime = dateFormatter.string(from: timeUTC!)
            return localTime
        }
        
        return nil
    }
    
    static func timeFormat (time: String) -> String? {
        
        let dateAsString = time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        print("12 hour formatted Date:",Date12)
        return Date12
    }
    
    static func convertToUTCFromLocalDate(dateStr : String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        let utc = NSTimeZone(abbreviation: "UTC")
        formatter.timeZone = utc as TimeZone?
        formatter.dateFormat = "h:mm a"
        let localDate: Date? = formatter.date(from: dateStr)
        let timeZoneOffset: TimeInterval = TimeInterval(NSTimeZone.default.secondsFromGMT())
        let utcTimeInterval: TimeInterval? = (localDate?.timeIntervalSinceReferenceDate)! - timeZoneOffset
        let utcCurrentDate = Date(timeIntervalSinceReferenceDate: utcTimeInterval!)
        // print(UTC time %@",utcCurrentDate)
        return formatter.string(from: utcCurrentDate)
    }
    
    static func convertToUTC(dateToConvert: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let timeUTC = dateFormatter.date(from: dateToConvert)
        
        if timeUTC != nil {
            dateFormatter.timeZone = NSTimeZone.local
            
            let localTime = dateFormatter.string(from: timeUTC!)
            return localTime
        }
        
        return nil
    }
    
    
}



extension String{
    func convertDoubleToCurrency() -> String{
        guard let amount1 = Double(self) else { return "" }
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale(identifier: "en_US")
        return numberFormatter.string(from: NSNumber(value: amount1))!
    }
}

extension String {
    // formatting text for currency textField
    func currencyFormatting() -> String {
        if let value = Double(self) {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            if let str = formatter.string(for: value) {
                return str
            }
        }
        return ""
    }
}


extension Int {
    var roundedWithAbbreviations: String {
        let number = Double(self)
        let thousand = number / 1000
        let million = number / 1000000
        if million >= 1.0 {
            return "\(round(million*10)/10)M"
        }
        else if thousand >= 1.0 {
            return "\(round(thousand*10)/10)K"
        }
        else {
            return "\(self)"
        }
    }
}
