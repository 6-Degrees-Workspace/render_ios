//
//  ExtensionIBInspectable.swift
//  Impact
//
//  Created by Apple on 30/04/21.
//

import Foundation
import UIKit


extension UIView {

    @IBInspectable var cornerRadius: CGFloat {
     get{
          return layer.cornerRadius
      }
      set {
          layer.cornerRadius = newValue
          layer.masksToBounds = newValue > 0
      }
    }

    @IBInspectable var borderWidth: CGFloat {
      get {
          return layer.borderWidth
      }
      set {
          layer.borderWidth = newValue
      }
    }
    
    @IBInspectable var borderColor: UIColor? {
      get {
          return UIColor(cgColor: layer.borderColor!)
      }
      set {
          layer.borderColor = newValue?.cgColor
      }
    }
    
    @IBInspectable var masksToBounds: Bool {
      get {
         return layer.masksToBounds
      }
      set {
         layer.masksToBounds = newValue
      }
    }
    
    //For Shadow
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
            layer.masksToBounds = false
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get{
            return layer.shadowOffset
        }set{
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowColor : UIColor {
        get{
            return UIColor.init(cgColor: layer.shadowColor!)
        }
        set {
            layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat {
        get{
            return CGFloat(layer.shadowOpacity)
        }
        set {
            layer.shadowOpacity = Float(newValue)
        }
    }

}



@available(iOS 13.4, *)
extension UITextField {
    @IBInspectable var doneAccessory: Bool{
            get{
                return self.doneAccessory
            }
            set (hasDone) {
                if hasDone{
                    addDoneButtonOnKeyboard()
                }
            }
        }
        
        func addDoneButtonOnKeyboard() {
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            doneToolbar.barStyle = .default
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
            
            let items = [flexSpace, done]
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            
            self.inputAccessoryView = doneToolbar
        }
        
        @objc func doneButtonAction() {
            self.resignFirstResponder()
        }
    
    
    func datePicker<T>(target: T, doneAction: Selector, cancelACtion: Selector, datePickerMode: UIDatePicker.Mode = .date, setDate: String? ) {
        let screenWidth = UIScreen.main.bounds.width
        
        func buttonItem(withSystemItemStyle style: UIBarButtonItem.SystemItem) -> UIBarButtonItem {
            let buttonTarget = style == .flexibleSpace ? nil : target
            let action: Selector? = {
                switch style {
                case .cancel:
                    return cancelACtion
                case .done:
                    return doneAction
                default:
                    return nil
                }
            }()
            
            let barButtonItem = UIBarButtonItem(barButtonSystemItem: style, target: buttonTarget, action: action)
            barButtonItem.tintColor = UIColor(named: "primaryBlue")
            
            
            return barButtonItem
        }
        
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.setDate(from: setDate ?? "", format: "MMM dd, yyyy")
        self.inputView = datePicker
        
        
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        toolBar.setItems([buttonItem(withSystemItemStyle: .cancel), buttonItem(withSystemItemStyle: .flexibleSpace), buttonItem(withSystemItemStyle: .done)], animated: true)
        self.inputAccessoryView = toolBar
    }
        
    
}

extension UIDatePicker {
    
    func setDate(from string: String, format: String, animated: Bool = true) {
        
        let formater = DateFormatter()
        
        formater.dateFormat = format
        
        let date = formater.date(from: string) ?? Date()
        
        setDate(date, animated: animated)
    }
}
