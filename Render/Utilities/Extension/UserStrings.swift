//
//  UserStrings.swift
//  Hotspod
//
//  Created by Apple on 12/03/21.
//

import Foundation


struct UserString {
    
    // MARK: Button Titles
    static let ok = "Ok"
    
    // MARK: Login
    static let enterName = "Please enter name"
    static let enterEmail = "Please enter email"
    static let enterValidEmail = "Please enter a valid email"
    static let enterPassword = "Please enter password"
    static let enterValidPassword = "Please enter a valid password"
    static let enterDOB = "Please select date-of-birth"
    static let selectGender = "Please select gender"
    
    
    
}


