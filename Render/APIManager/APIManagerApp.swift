import UIKit
import Foundation


let POST : String = "POST"
let GET  : String = "GET"
let DELETE  : String = "DELETE"

@available(iOS 13.0, *)
class APIManagerApp: NSObject {
    
    static let shared : APIManagerApp = APIManagerApp()
    
    func makeAPIRequest(ofType : String, withEndPoint : String, andParam : [ String : Any ], showHud : Bool, changeBaseUrl:Bool = false , completionHandler: @escaping ( [ String : Any ] ) -> Void ) {
        
        if showHud {
            Spinner.show("")
        }
        
        let token = ApplicationPreference.getAccessToken() ?? ""
        
        var BaseUrl = ""
        if token != "" && changeBaseUrl == false {
            BaseUrl = SERVICE_URL
        }else{
            
            BaseUrl = SERVICE_URL//SERVICE_URL_ONBOARD
            
        }
        
        let strFinalURL = "\(BaseUrl)\(withEndPoint)"
        let finalURL = URL.init(string: strFinalURL)
        
        let request = NSMutableURLRequest(url: finalURL! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 60.0)
        request.httpMethod = ofType
        let bearerToken: String = "Bearer " + (token)
        
        var headers = [String:String]()
        if token != ""{
            headers = [
                "version"               : "1.0",
                "Content-Type"          : "application/json",
                "deviceid":"123456",
                "devicetype":"iPhone",
                "device-fcm-id":"1234567"
            ]
        }else{
            headers = [
                "version"               : "1",
                "Content-Type"          : "application/json",
                "deviceid":"123456",
                "devicetype":"iPhone",
                "device-fcm-id":"1234567"
            ]
        }
        
      
        request.allHTTPHeaderFields = headers
        
        if ofType != GET {
            let bodyData : Data = try! JSONSerialization.data(withJSONObject: andParam, options: .prettyPrinted)
            request.httpBody = bodyData
        }
        else {
            
        }
        
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!.localizedDescription)
                print("Server Call -- >\(ofType)\n finalURL --> \(strFinalURL) \n header --> \(request.allHTTPHeaderFields!) \n Params --> \(andParam) \n Error == \(error?.localizedDescription ?? "")")
                Helper.UI({
                    Spinner.hide()
                    Helper.sharedInstance.showToast(isError: true, title: error?.localizedDescription)
                })
            } else {
                
                var json = [ String : Any ]()
                
                if let isJSON = try? JSONSerialization.jsonObject(with: data!, options: []) as? [ String : Any ] {
                    json = isJSON
                }
                
                print("Server Call -- >\(ofType)\n finalURL --> \(strFinalURL) \n header --> \(request.allHTTPHeaderFields!) \n Params --> \(andParam) \n responce == \(json)")
                
                if let data = data, json.count == 0 {
                    let html = String(decoding: data, as: UTF8.self)
                    json["html"] = html
                }
                
                //                    let let json = try! JSONSerialization.jsonObject(with: data!, options: []) as? [ String : Any ]
                print("response json -> \(json)")
                
                Helper.UI({
                    Spinner.hide()
                })
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode == 200 {
                    if json["success"] as? Bool == false {
                        Helper.UI({
                            Helper.sharedInstance.showToast(isError: true, title: json["message"] as? String)
                        })
                        
                    } else {
                        completionHandler(json)
                    }
                }else  if httpResponse?.statusCode == 404 {
                    
                    var json = [ String : Any ]()
                    json["errorMessage"] = ""
                    completionHandler(json)
                    
                }else  if httpResponse?.statusCode == 401 {
                    //session expire
                    var json = [ String : Any ]()
                    json["errorMessage"] = "Session expiration time out"
                    completionHandler(json)
                    ApplicationPreference.clearAllData()
                }else {
                    
                    Helper.UI({
                        //                        var errorMessage:String = "Internal server error"
                        //                        if let message = json["message"] as? String { errorMessage = message }
                        //                        if let message = json["errorMessage"] as? String { errorMessage = message }
                        //                        json["errorMessage"] = "Internal server error"
                        //                        completionHandler(json)
                        //                        Helper.sharedInstance.showToast(isError: true, title: errorMessage)
                        completionHandler(json)
                        
                    })
                }
            }
        })
        dataTask.resume()
    }
    
    
    func makeAPIRequestWithAllResponse(ofType : String, withEndPoint : String, andParam : [ String : Any ], showHud : Bool, completionHandler: @escaping ( [ String : Any ] ) -> Void ) {
        
        if showHud {
            Spinner.show("")
        }
        
        let token = ApplicationPreference.getAccessToken() ?? ""
        
        var BaseUrl = ""
        if token != ""{
            BaseUrl = SERVICE_URL
        }else{
            BaseUrl = SERVICE_URL//SERVICE_URL_ONBOARD
        }
        
        let strFinalURL = "\(BaseUrl)\(withEndPoint)"
        let finalURL = URL.init(string: strFinalURL)
        
        let request = NSMutableURLRequest(url: finalURL! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = ofType
        let bearerToken: String = "Bearer " + (token)
        
        var headers = [String:String]()
        if token != ""{
            headers = [
                "version"               : "1",
                "Content-Type"          : "application/json",
                "Authorization"         : bearerToken
            ]
        }else{
            headers = [
                "version"               : "1",
                "Content-Type"          : "application/json",
            ]
        }
        request.allHTTPHeaderFields = headers
        
        //        let currentLocation = SSLocationManager.shared.currentLocation
        //        if currentLocation != nil {
        //
        //            let latitude = "\(currentLocation!.coordinate.latitude)"
        //            let longitude = "\(currentLocation!.coordinate.longitude)"
        //            request.addValue(latitude, forHTTPHeaderField: "lastKnownLocation_lat")
        //            request.addValue(longitude, forHTTPHeaderField: "lastKnownLocation_long")
        //        }
        
        if ofType != GET {
            let bodyData : Data = try! JSONSerialization.data(withJSONObject: andParam, options: .prettyPrinted)
            request.httpBody = bodyData
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil) {
                Helper.UI({
                    Spinner.hide()
                    Helper.sharedInstance.showToast(isError: true, title: error?.localizedDescription)
                    
                })
            }
            else {
                
                let json = try! JSONSerialization.jsonObject(with: data!, options: []) as? [ String : Any ]
                print("response json -> \(json!)")
                Helper.UI({
                    Spinner.hide()
                })
                completionHandler(json!)
            }
        })
        
        dataTask.resume()
    }
    
    
    func checkPincode(pincode : String, completionHandler: @escaping ( [ String : Any ] ) -> Void ) {
        
        Spinner.show("")
        let strFinalURL = "\(validatePincodeURL)\(pincode)"
        let finalURL = URL.init(string: strFinalURL)
        let request = NSMutableURLRequest(url: finalURL! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                Helper.UI({
                    Spinner.hide()
                    Helper.sharedInstance.showToast(isError: true, title: error?.localizedDescription)
                    
                })
            }
            else {
                
                var json = [ String : Any ]()
                
                if let isJSON = try? JSONSerialization.jsonObject(with: data!, options: []) as? [[ String : Any ]] {
                    if let dict = isJSON.first{
                        json = dict
                    }
                }
                
                print("PINCODE API Call response-- >\(String(describing: response))\n finalURL --> \(strFinalURL) \n responce == \(json)")
                
                if let data = data, json.count == 0 {
                    let html = String(decoding: data, as: UTF8.self)
                    json["html"] = html
                }
                print("response json -> \(json)")
                Helper.UI({
                    Spinner.hide()
                })
                completionHandler(json)
            }
        })
        
        dataTask.resume()
    }
}
