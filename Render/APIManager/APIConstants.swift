import Foundation

struct BASEURLs {
    static let production = "https://renderweb.6degreesit.com/"
    static let testing = "https://renderweb.6degreesit.com/"
    static let development = "https://renderweb.6degreesit.com/API/customers/"//"http://renderweb.wsisrdev.com/"//"http://dev.rendr-app.com/"
    static let baseURL = development
    //http://renderweb.wsisrdev.com
}


let SERVICE_URL : String = "https://renderweb.6degreesit.com/" // Development
//let SERVICE_URL_ONBOARD = "https://mms-identity-staging.azurewebsites.net/api" //Development
let validatePincodeURL = "https://api.postalpincode.in/pincode/"

struct APIEndPoint {
    // onBoard
    static let login : String = "login_with_mobile"
    static let forgotPassword = "/password/EmailResetLink"
    static let userDetails = "/user/UserDetails"
    static let NotificationDeviceSetup = "/Notification/deviceSetup"
    static let register = "/user/register"
    static let resendConfirmationEmail = "/user/resendConfirmationEmail?email="
    
    // home
    static let getAllAlerts : String = "/v1/Alerts"
    static let getAlertType : String = "/v1/Alerts/types?devices="
    static let getLocation : String = "/v1/Location"
    static let bookmarkAlert : String = "/v1/Alerts/bookmark/"
    static let reportIncorrect : String = "/v1/Alerts/reportIncorrect"
    static let getAcknowledge : String = "/v1/Alerts/acknowledge/"
    
}


