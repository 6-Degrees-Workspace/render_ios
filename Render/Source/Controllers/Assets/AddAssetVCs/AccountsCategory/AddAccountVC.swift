//
//  AccountsVC.swift
//  Render
//
//  Created by Anis Agwan on 28/12/21.
//

import UIKit

class AddAccountVC: BaseVC {
    
    var accountTypes: [String] = ["Savings", "Checking"]
    var selectedType: String = ""
    var picker: UIPickerView!
    var asset: AssetDatum?
    var update: Bool = false
    var frameSize: CGRect?
    var safe: CGRect?
    //IBOutlets
    @IBOutlet weak var assetNameLbl: UITextField!
    @IBOutlet weak var institutionNameLbl: UITextField!
    @IBOutlet weak var accountNoLbl: UITextField!
    @IBOutlet weak var amtLbl: UITextField!
    @IBOutlet weak var descLbl: UITextField!
    @IBOutlet weak var assetTypeLbl: UITextField!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var btnAddAsset: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //safe = self.view.safeAreaLayoutGuide.layoutFrame
        //
        if update {
            self.assetNameLbl.text = self.asset?.assetName
            
            if self.asset?.assetCurrentValue?.length ?? 0 > 9{
                self.amtLbl.text = self.asset?.assetCurrentValue?.replacingOccurrences(of: ".00", with: "")
            }else{
                self.amtLbl.text = self.asset?.assetCurrentValue
            }
            
            self.assetTypeLbl.text = self.asset?.assetType
            self.institutionNameLbl.text = self.asset?.institutionName
            self.accountNoLbl.text = self.asset?.accountNumber
            self.btnAddAsset.setTitle("Update Asset", for: .normal)
            assetTypeLbl.isEnabled = false
            assetTypeLbl.textColor = UIColor(named: "textGrayColor")
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            //self.bottomConst.constant = 0
        }else{
            self.bottomConst.constant = 110
        }
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.assetNameLbl.text = assetNameLbl.text?.trim()
        self.institutionNameLbl.text = self.institutionNameLbl.text?.trim()
        self.accountNoLbl.text = self.accountNoLbl.text?.trim()
        self.amtLbl.text = self.amtLbl.text?.trim()
        //self.descLbl.text = self.descLbl.text?.trim()
        self.assetTypeLbl.text = self.assetTypeLbl.text?.trim()
        //let assetNameLblLength = assetNameLbl.text?.length ?? 0
        
        if (assetNameLbl.text?.isEmpty)! {
            message = "Please Enter Asset Name"
            isFound = false
        }else if (assetNameLbl.text?.length ?? 0 < 3){
            message = "The Asset Name must be at least 3 characters in length"
            isFound = false
        }else if (institutionNameLbl.text?.isEmpty)! {
            message = "Please Enter Institution Name"
            isFound = false
        }else if (institutionNameLbl.text?.length ?? 0 < 3){
            message = "The Institution Name must be at least 3 characters in length"
            isFound = false
        } else if (accountNoLbl.text?.isEmpty)! {
            message = "Please Enter Account Number"
            isFound = false
        }else if (accountNoLbl.text?.length ?? 0 < 10){
            message = "The Account Number must be at least 10 digits in length"
            isFound = false
        } else if (amtLbl.text?.isEmpty)! {
            message = "Please Enter Amount"
            isFound = false
        } else if (assetTypeLbl.text?.isEmpty)! {
            message = "Please Select Asset Type"
            isFound = false
        }else if (amtLbl.text == "0" || amtLbl.text == "00" || amtLbl.text == "000" || amtLbl.text == "0000" || amtLbl.text == "00000" || amtLbl.text == "000000") {
            message = "Please Enter Vaild Amount"
            isFound = false
        } else if (amtLbl.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        }
        
        
        //        else if (descLbl.text?.isEmpty)! {
        //            message = "Please enter the desc."
        //            isFound = false
        //        }
        
        if !isFound {
           // Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func actionAddAccount(_ sender: Any) {
        self.view.endEditing(true)
        
        if self.btnAddAsset.titleLabel?.text == "Update Asset"{
            self.updateAssetServiceCall()
        }else{
            if self.isValidFields() {
                self.addAssetServiceCall()
            }
        }
    }
}


extension AddAccountVC: UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == amtLbl {
            
            // Allow to remove character (Backspace)
            if string == "" {
                return true
            }
            
            // Block multiple dot
            if (textField.text?.contains("."))! && string == "." {
                return false
            }
            
            if (textField.text?.contains("."))! {
                let limitDecimalPlace = 2
                let decimalPlace = textField.text?.components(separatedBy: ".").last
                if (decimalPlace?.count)! < limitDecimalPlace {
                    return true
                }
                else {
                    return false
                }
            }
        }
      
        let allowedCharacters : CharacterSet
        if textField == assetNameLbl || textField == institutionNameLbl{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_letter).inverted
        }else{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_special_letter).inverted
        }
        
        let components = string.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        
        
        if textField == assetNameLbl {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = assetNameLbl.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
            
        }
        
        if textField == institutionNameLbl {
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = institutionNameLbl.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }

        }
        
        if textField == accountNoLbl {
            let maxLength = 16
            let currentString: NSString = accountNoLbl.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == amtLbl {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = amtLbl.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        
        return true
    }
    
    func pickUP(_ textField: UITextField) {
        self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.selectRow(0, inComponent: 0, animated: true)
        self.selectedType = accountTypes[0]
        //        self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
        //        self.selectedCategory = self.mainCategoryModel?.data[0].categoryName ?? ""
        assetTypeLbl.inputView = self.picker
        //
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        //
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        assetTypeLbl.inputAccessoryView = toolBar
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUP(assetTypeLbl)
    }
    
    @objc func selectClick() {
        
        self.assetTypeLbl.text = self.selectedType
        assetTypeLbl.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        assetTypeLbl.resignFirstResponder()
    }
}

//MARK: - UIPicker Delegates
extension AddAccountVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.text = accountTypes[row]
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedType = accountTypes[row]
    }
}

//MARK: - Service Calls
extension AddAccountVC {
    func addAssetServiceCall() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
        
        self.assetNameLbl.text = assetNameLbl.text?.trim()
        self.institutionNameLbl.text = self.institutionNameLbl.text?.trim()
        self.accountNoLbl.text = self.accountNoLbl.text?.trim()
        self.amtLbl.text = self.amtLbl.text?.trim()
        //self.descLbl.text = self.descLbl.text?.trim()
        self.assetTypeLbl.text = self.assetTypeLbl.text?.trim()
        
        NotificationCenter.default.post(name: Notification.Name("AddLoaderForAsset"), object: nil, userInfo: nil)
        
        let parameter:[String:Any] = [
            
            "asset_name": self.assetNameLbl.text ?? "",
            "institution_name": self.institutionNameLbl.text ?? "",
            "account_number": self.accountNoLbl.text ?? "",
            "asset_current_value": self.amtLbl.text ?? "",
            "asset_type": "1",
            "category_code": "SAVINGANDCHECKINGACCOUNTS"
            
            
        ]
        let assetService = AssetService()
        assetService.addAssetSavingAccount(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
            
        }
    }
    
    
    func updateAssetServiceCall() {
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
        
        self.assetNameLbl.text = assetNameLbl.text?.trim()
        self.institutionNameLbl.text = self.institutionNameLbl.text?.trim()
        self.accountNoLbl.text = self.accountNoLbl.text?.trim()
        self.amtLbl.text = self.amtLbl.text?.trim()
        self.assetTypeLbl.text = self.assetTypeLbl.text?.trim()
        
        NotificationCenter.default.post(name: Notification.Name("AddLoaderForAsset"), object: nil, userInfo: nil)
        
        let parameter:[String:Any] = [
            
            "category_code": "SAVINGANDCHECKINGACCOUNTS",
            "asset_id": asset?.assetID ?? "",
            "asset_name": self.assetNameLbl.text ?? "",
            "institution_name": self.institutionNameLbl.text ?? "",
            "account_number": self.accountNoLbl.text ?? "",
            "asset_current_value": self.amtLbl.text ?? ""
        ]
        
        let netWorthService = NetWorthService()
        netWorthService.updateAsset(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
    }
}



