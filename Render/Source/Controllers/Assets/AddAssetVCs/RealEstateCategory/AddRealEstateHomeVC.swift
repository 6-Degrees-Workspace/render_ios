import UIKit

class AddRealEstateHomeVC: UIViewController {
    
    var mode: String = ""
    var frameSize: CGRect?
    var safe: CGRect?
    var update: Bool = false
    var asset: AssetDatum?
    
    //IBOutlets
    @IBOutlet weak var btnHomeEquity: UIButton!
    @IBOutlet weak var btnHomeValuepaid: UIButton!
    @IBOutlet weak var homePurchaseTxtFld: UITextField!
    @IBOutlet weak var purchaseDivider: UIView!
    @IBOutlet weak var homeCurrentvaluetxtFld: UITextField!
    @IBOutlet weak var priceDivider: UIView!
    @IBOutlet weak var valueDivider: UIView!
    @IBOutlet weak var homeValuePaidTxtFld: UITextField!
    @IBOutlet weak var differencePriceLbl: UILabel!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var btnAddAsset: UIButton!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if update {
//            btnHomeEquity.isEnabled = false
//            btnHomeValuepaid.isEnabled = false
            btnHomeEquity.isUserInteractionEnabled = false
            btnHomeValuepaid.isUserInteractionEnabled = false

            self.btnAddAsset.setTitle("Update Asset", for: .normal)

            if asset?.assetName == "REAL ESTATE (HOME equity)"{
                self.homeEquity()
                btnHomeValuepaid.setTitleColor(UIColor(named: "textGrayColor"), for: .normal)
                if asset?.assetPurchasePrice?.length ?? 0 > 9{
                    self.homeCurrentvaluetxtFld.text = self.asset?.assetPurchasePrice?.replacingOccurrences(of: ".00", with: "")
                }else{
                    self.homeCurrentvaluetxtFld.text = self.asset?.assetPurchasePrice
                }
                
                if asset?.assetCurrentValue?.length ?? 0 > 9{
                    self.homePurchaseTxtFld.text = self.asset?.assetCurrentValue?.replacingOccurrences(of: ".00", with: "")
                }else{
                    self.homePurchaseTxtFld.text = self.asset?.assetCurrentValue
                }
                
                //homeCurrentvaluetxtFld.text = asset?.assetPurchasePrice
                //homePurchaseTxtFld.text = asset?.assetCurrentValue
                if let valueOne = self.homeCurrentvaluetxtFld.text, let valueTwo = self.homePurchaseTxtFld.text {
                    let difference = (Double(valueTwo) ?? 0.0) - (Double(valueOne) ?? 0.0)
                    if let assetCurrentValue = "\(difference)".first {
                        if assetCurrentValue == "-"{
                            self.differencePriceLbl.text = "Estimated Home Equity: \(difference)".replacingOccurrences(of: "-", with: "-$")
                        }else{
                            self.differencePriceLbl.text = "Estimated Home Equity: $\(difference)"
                        }
                    }
                }
                
            }else{
                self.valuePaid()
                homeValuePaidTxtFld.text = asset?.assetCurrentValue
                btnHomeEquity.setTitleColor(UIColor(named: "textGrayColor"), for: .normal)
            }
            
//             btnHomeEquity.setTitleColor(UIColor(named: "textGrayColor"), for: .normal)
//             btnHomeValuepaid.setTitleColor(UIColor(named: "textGrayColor"), for: .normal)
            
        }
        
        self.homeCurrentvaluetxtFld.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            self.bottomConst.constant = 50
        }else{
            self.bottomConst.constant = 110
        }
    }
    
    func isValidFields()->Bool {
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.homePurchaseTxtFld.text = homePurchaseTxtFld.text?.trim()
        self.homeCurrentvaluetxtFld.text = self.homeCurrentvaluetxtFld.text?.trim()
        // self.landValueTxtFld.text = self.landValueTxtFld.text?.trim()
        if self.mode == "Home equity" {
            if (homePurchaseTxtFld.text?.isEmpty)! {
                message = "Please Enter Estimated Home Value"
                isFound = false
            } else if (homeCurrentvaluetxtFld.text?.isEmpty)! {
                message = "Please Enter Mortgage Balance."
                isFound = false
            }else if (homePurchaseTxtFld.text == "0" || homePurchaseTxtFld.text == "00" || homePurchaseTxtFld.text == "000" || homePurchaseTxtFld.text == "0000" || homePurchaseTxtFld.text == "00000" || homePurchaseTxtFld.text == "000000") {
                message = "Please Enter Vaild Amount"
                isFound = false
            }else if (homeCurrentvaluetxtFld.text == "0" || homeCurrentvaluetxtFld.text == "00" || homeCurrentvaluetxtFld.text == "000" || homeCurrentvaluetxtFld.text == "0000" || homeCurrentvaluetxtFld.text == "00000" || homeCurrentvaluetxtFld.text == "000000") {
                message = "Please Enter Vaild Amount"
                isFound = false
            } else if (homePurchaseTxtFld.text?.toInt() ?? 0 <= 0) {
                message = "Please Enter Vaild Amount"
                isFound = false
            } else if (homeCurrentvaluetxtFld.text?.toInt() ?? 0 <= 0) {
                message = "Please Enter Vaild Amount"
                isFound = false
            }
            //Please Enter Estimated Home value
            if !isFound {
                //Helper.sharedInstance.showToast(isError: true, title: message)
                self.showAlert(message: message)
            }
            return isFound
            
        } else {
            
            if (homeValuePaidTxtFld.text?.isEmpty)! {
                message = "Please Enter Home Value"
                isFound = false
            }else if (homeValuePaidTxtFld.text == "0" || homeValuePaidTxtFld.text == "00" || homeValuePaidTxtFld.text == "000" || homeValuePaidTxtFld.text == "0000" || homeValuePaidTxtFld.text == "00000" || homeValuePaidTxtFld.text == "000000") {
                message = "Please Enter Vaild Amount"
                isFound = false
            }else if (homeValuePaidTxtFld.text?.toInt() ?? 0 <= 0) {
                message = "Please Enter Vaild Amount"
                isFound = false
            }
            if !isFound {
                //Helper.sharedInstance.showToast(isError: true, title: message)
                self.showAlert(message: message)
            }
            return isFound
            
        }
        return isFound
    }
    
    @IBAction func actionHomeEquity(_ sender: Any) {
        self.homeEquity()
    }
    
    func homeEquity() {
        self.mode = "Home equity"
        self.btnHomeEquity.setImage(UIImage.init(named: "radio_select"), for: .normal)
        self.btnHomeEquity.setTitle("Home Equity", for: .normal)
        self.btnHomeEquity.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
        self.btnHomeEquity.tintColor = UIColor.blue
        //self.btnHomeEquity.setImage(UIImage(systemName: "circle.inset.filled"), for: .normal)
        //self.btnHomeEquity.tintColor = UIColor(named: "primaryBlue")
        //self.btnHomeValuepaid.setImage(UIImage(systemName: "circle"), for: .normal)
        self.btnHomeValuepaid.setImage(UIImage.init(named: "radio_unselect"), for: .normal)
        self.btnHomeValuepaid.setTitle("Home Value (Paid off)", for: .normal)
        self.btnHomeValuepaid.setTitleColor(UIColor.black, for: .normal)
        self.btnHomeValuepaid.tintColor = .label
        self.homePurchaseTxtFld.isHidden = false
        self.differencePriceLbl.isHidden = false
        self.purchaseDivider.isHidden = false
        self.homeCurrentvaluetxtFld.isHidden = false
        self.priceDivider.isHidden = false
        self.homeValuePaidTxtFld.isHidden = true
        self.valueDivider.isHidden = true
        
    }
    
    
    @IBAction func actionValueHome(_ sender: Any) {
        self.valuePaid()
    }
    
    func valuePaid() {
        
        self.mode = "Value paid"
        self.btnHomeValuepaid.setImage(UIImage.init(named: "radio_select"), for: .normal)
        self.btnHomeValuepaid.setTitle("Home Value (Paid off)", for: .normal)
        self.btnHomeValuepaid.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
        self.btnHomeValuepaid.tintColor = UIColor.blue
        //self.btnHomeValuepaid.setImage(UIImage(systemName: "circle.inset.filled"), for: .normal)
        //self.btnHomeValuepaid.tintColor = UIColor(named: "primaryBlue")
        //self.btnHomeEquity.setImage(UIImage(systemName: "circle"), for: .normal)
        self.btnHomeEquity.setImage(UIImage.init(named: "radio_unselect"), for: .normal)
        self.btnHomeEquity.setTitle("Home Equity", for: .normal)
        self.btnHomeEquity.setTitleColor(UIColor.black, for: .normal)
        self.btnHomeEquity.tintColor = .label
        self.homePurchaseTxtFld.isHidden = true
        self.differencePriceLbl.isHidden = true
        self.purchaseDivider.isHidden = true
        self.homeCurrentvaluetxtFld.isHidden = true
        self.priceDivider.isHidden = true
        self.homeValuePaidTxtFld.isHidden = false
        self.valueDivider.isHidden = false
        
    }
    
    @IBAction func addAssetAction(_ sender: Any) {
        self.view.endEditing(true)
        if self.btnAddAsset.titleLabel?.text == "Update Asset"{
            if self.isValidFields() {
            self.updateAssetServiceCall()
            }
        }else{
            if self.isValidFields() {
            self.addAssetServiceCall()
            }
        }
    }
}

//MARK: - UITextField delegates
extension AddRealEstateHomeVC: UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // Allow to remove character (Backspace)
           if string == "" {
               return true
           }

          // Block multiple dot
           if (textField.text?.contains("."))! && string == "." {
               return false
           }
        
        
        if (textField.text?.contains("."))! {
            let limitDecimalPlace = 2
            let decimalPlace = textField.text?.components(separatedBy: ".").last
            if (decimalPlace?.count)! < limitDecimalPlace {
                return true
            }
            else {
                return false
            }
        }
        
        if textField == homePurchaseTxtFld {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = homePurchaseTxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == homeCurrentvaluetxtFld {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = homeCurrentvaluetxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == homeValuePaidTxtFld {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = homeValuePaidTxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        return true
    }
    
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        if let valueOne = textfield.text, let valueTwo = self.homePurchaseTxtFld.text {
            let difference = (Double(valueTwo) ?? 0.0) - (Double(valueOne) ?? 0.0)
            if let assetCurrentValue = "\(difference)".first {
                if assetCurrentValue == "-"{
                    self.differencePriceLbl.text = "Estimated Home Equity: \(difference)".replacingOccurrences(of: "-", with: "-$")
                }else{
                    self.differencePriceLbl.text = "Estimated Home Equity: $\(difference)"
                }
            }
        }
    }
}


//MARK: - Service Calls
extension AddRealEstateHomeVC {
    func addAssetServiceCall() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
        
        self.homePurchaseTxtFld.text = homePurchaseTxtFld.text?.trim()
        self.homeCurrentvaluetxtFld.text = self.homeCurrentvaluetxtFld.text?.trim()
        
        NotificationCenter.default.post(name: Notification.Name("AddLoaderForAsset"), object: nil, userInfo: nil)
        
        let parameter:[String:Any]
        
        if self.mode == "Home equity" {
            parameter = [
                "asset_type": "equity",// (equity/paidoff)
                "asset_current_value": self.homePurchaseTxtFld.text ?? "",
                "asset_purchase_price": self.homeCurrentvaluetxtFld.text ?? "",
                "category_code": "HOME"
            ]
        } else {
            parameter = [
                "asset_type": "paidoff",// (equity/paidoff)
                "asset_current_value":self.homeValuePaidTxtFld.text ?? "",
                //"asset_purchase_price":self.homeCurrentvaluetxtFld.text ?? "",
                "category_code": "HOME"
            ]
        }
        
        let assetService = AssetService()
        if self.mode == "Home equity" {
            assetService.addAssetHomeEquity(parameters: parameter){ (result,code) in
                switch result{
                case .success(let responseData):
                    if code == 200 {
                        if responseData.status == 200{
                            self.navigationController?.popViewController(animated: false)
                        }
                        self.showAlert(message: responseData.message)
                    }else{
                        self.showAlert(message: responseData.message)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                }
                self.removLoader()
                NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)

            }
        } else {
            assetService.addAssetHomePaidoff(parameters: parameter) { (result, code) in
                switch result {
                case .success(let responseData):
                    if code == 200 {
                        if responseData.status == 200{
                            self.navigationController?.popViewController(animated: false)
                        }
                        self.showAlert(message: responseData.message)
                    }else{
                        self.showAlert(message: responseData.message)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                    
                }
                self.removLoader()
                NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)

            }
        }
    }
    
    func updateAssetServiceCall() {
        
        self.homePurchaseTxtFld.text = homePurchaseTxtFld.text?.trim()
        self.homeCurrentvaluetxtFld.text = self.homeCurrentvaluetxtFld.text?.trim()
        
        let parameter:[String:Any]
        if self.mode == "Home equity" {
            parameter = [
                "category_code": "HOME",
                "asset_id":asset?.assetID ?? "",
                 "asset_type": "equity",
                 "asset_current_value":self.homePurchaseTxtFld.text ?? "",
                 "asset_purchase_price":self.homeCurrentvaluetxtFld.text ?? "",
            ]
        } else {
            parameter = [
                "category_code": "HOME",
                "asset_id":asset?.assetID ?? "",
                "asset_type": "paidoff",
                "asset_current_value":self.homeValuePaidTxtFld.text ?? ""
            ]
        }
        
        let assetService = AssetService()
        if self.mode == "Home equity" {
            assetService.updateAssetHomeEquity(parameters: parameter){ (result,code) in
                switch result{
                case .success(let responseData):
                    if code == 200 {
                        if responseData.status == 200{
                            self.navigationController?.popViewController(animated: false)
                        }
                        self.showAlert(message: responseData.message)
                    }else{
                        self.showAlert(message: responseData.message)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                }
                self.removLoader()
            }
        } else {
            assetService.updateAssetHomePaidoff(parameters: parameter) { (result, code) in
                switch result {
                case .success(let responseData):
                    if code == 200 {
                        if responseData.status == 200{
                            self.navigationController?.popViewController(animated: false)
                        }
                        self.showAlert(message: responseData.message)
                    }else{
                        self.showAlert(message: responseData.message)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                }
                self.removLoader()
            }
        }
    }
}
