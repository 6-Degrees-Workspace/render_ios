import UIKit

class AddRealEstateLandVC: UIViewController {
    
    var mode: String = ""
    var frameSize: CGRect?
    var safe: CGRect?
    var update: Bool = false
    var asset: AssetDatum?
    
    //IBOutlets
    @IBOutlet weak var btnLandEquity: UIButton!
    @IBOutlet weak var btnLandValuePaid: UIButton!
    @IBOutlet weak var landPurchaseTxtFld: UITextField!
    @IBOutlet weak var purchaseDivider: UIView!
    @IBOutlet weak var landCurrentValuetxtFld: UITextField!
    @IBOutlet weak var priceDivider: UIView!
    @IBOutlet weak var valueDivider: UIView!
    @IBOutlet weak var landValuePaidTxtFld: UITextField!
    @IBOutlet weak var differenceLbl: UILabel!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var btnAddAsset: UIButton!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if update {
//            btnLandEquity.isEnabled = false
//            btnLandValuePaid.isEnabled = false
            
            btnLandEquity.isUserInteractionEnabled = false
            btnLandValuePaid.isUserInteractionEnabled = false

            
            self.btnAddAsset.setTitle("Update Asset", for: .normal)
            if asset?.assetName == "REAL ESTATE (LAND equity)"{
                self.landEquity()
                btnLandValuePaid.setTitleColor(UIColor(named: "textGrayColor"), for: .normal)
                if asset?.assetCurrentValue?.length ?? 0 > 9{
                    self.landCurrentValuetxtFld.text = self.asset?.assetCurrentValue?.replacingOccurrences(of: ".00", with: "")
                }else{
                    self.landCurrentValuetxtFld.text = self.asset?.assetCurrentValue
                }
                
                if asset?.assetPurchasePrice?.length ?? 0 > 9{
                    self.landPurchaseTxtFld.text = self.asset?.assetPurchasePrice?.replacingOccurrences(of: ".00", with: "")
                }else{
                    self.landPurchaseTxtFld.text = self.asset?.assetPurchasePrice
                }
                
                //landCurrentValuetxtFld.text = asset?.assetCurrentValue
                //landPurchaseTxtFld.text = asset?.assetPurchasePrice
                
                if let valueOne = self.landCurrentValuetxtFld.text, let valueTwo = self.landPurchaseTxtFld.text {
                    let difference = (Double(valueTwo) ?? 0.0) - (Double(valueOne) ?? 0.0)
                    
                    if let assetCurrentValue = "\(difference)".first {
                        if assetCurrentValue == "-"{
                            self.differenceLbl.text = "Estimated Home Equity: \(difference)".replacingOccurrences(of: "-", with: "-$")
                        }else{
                            self.differenceLbl.text = "Estimated Home Equity: $\(difference)"
                        }
                    }
                }

            }else{
                self.valuePaid()
                landValuePaidTxtFld.text = asset?.assetCurrentValue
                btnLandEquity.setTitleColor(UIColor(named: "textGrayColor"), for: .normal)
            }
            
//            btnLandEquity.setTitleColor(UIColor(named: "textGrayColor"), for: .normal)
//            btnLandValuePaid.setTitleColor(UIColor(named: "textGrayColor"), for: .normal)
            
        }
        
        self.landCurrentValuetxtFld.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            self.bottomConst.constant = 50
        }else{
            self.bottomConst.constant = 110
        }
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        
        self.landValuePaidTxtFld.text = self.landValuePaidTxtFld.text?.trim()
        self.landPurchaseTxtFld.text = self.landPurchaseTxtFld.text?.trim()
        self.landCurrentValuetxtFld.text = self.landCurrentValuetxtFld.text?.trim()
        
        if self.mode == "Land Equity" {
            
            if (landPurchaseTxtFld.text?.isEmpty)! {
                message = "Please Enter Estimated Land Value"
                isFound = false
            } else if (landCurrentValuetxtFld.text?.isEmpty)! {
                message = "Please Enter Land Loan Balance"
                isFound = false
            } else if (landPurchaseTxtFld.text == "0" || landPurchaseTxtFld.text == "00" || landPurchaseTxtFld.text == "000" || landPurchaseTxtFld.text == "0000" || landPurchaseTxtFld.text == "00000" || landPurchaseTxtFld.text == "000000") {
                message = "Please Enter Vaild Amount"
                isFound = false
            } else if (landCurrentValuetxtFld.text == "0" || landCurrentValuetxtFld.text == "00" || landCurrentValuetxtFld.text == "000" || landCurrentValuetxtFld.text == "0000" || landCurrentValuetxtFld.text == "00000" || landCurrentValuetxtFld.text == "000000") {
                message = "Please Enter Vaild Amount"
                isFound = false
            } else if (landPurchaseTxtFld.text?.toInt() ?? 0 <= 0) {
                message = "Please Enter Vaild Amount"
                isFound = false
            } else if (landCurrentValuetxtFld.text?.toInt() ?? 0 <= 0) {
                message = "Please Enter Vaild Amount"
                isFound = false
            }
            //Please Enter Estimated Home value
            if !isFound {
                Helper.sharedInstance.showToast(isError: true, title: message)
            }
            return isFound
            
        }else{
            if (landValuePaidTxtFld.text?.isEmpty)! {
                message = "Please Enter Land Value"
                isFound = false
            }else if (landValuePaidTxtFld.text == "0" || landValuePaidTxtFld.text == "00" || landValuePaidTxtFld.text == "000" || landValuePaidTxtFld.text == "0000" || landValuePaidTxtFld.text == "00000" || landValuePaidTxtFld.text == "000000") {
                message = "Please Enter Vaild Amount"
                isFound = false
            }else if (landValuePaidTxtFld.text?.toInt() ?? 0 <= 0) {
                message = "Please Enter Vaild Amount"
                isFound = false
            }
            if !isFound {
                //Helper.sharedInstance.showToast(isError: true, title: message)
                self.showAlert(message: message)
            }
            return isFound
        }
        
        if (landValuePaidTxtFld.text?.isEmpty)! {
            message = "Please Enter Estimated Land Value."
            isFound = false
        }else if (landValuePaidTxtFld.text == "0" || landValuePaidTxtFld.text == "00" || landValuePaidTxtFld.text == "000" || landValuePaidTxtFld.text == "0000" || landValuePaidTxtFld.text == "00000" || landValuePaidTxtFld.text == "000000") {
            message = "Please Enter Vaild Amount"
            isFound = false
        }else if (landValuePaidTxtFld.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func landEquityAction(_ sender: Any) {
        self.landEquity()
    }
    
    func landEquity(){
        self.mode = "Land Equity"
        
        self.btnLandEquity.setImage(UIImage.init(named: "fillCircle"), for: .normal)
        self.btnLandEquity.setTitle("Land Equity", for: .normal)
        self.btnLandEquity.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
        self.btnLandEquity.tintColor = UIColor.blue
        //self.btnLandEquity.setImage(UIImage(systemName: "circle.inset.filled"), for: .normal)
        //self.btnLandEquity.tintColor = UIColor(named: "primaryBlue")
        //self.btnLandValuePaid.setImage(UIImage(systemName: "circle"), for: .normal)
        self.btnLandValuePaid.setImage(UIImage.init(named: "unfiledCircle"), for: .normal)
        self.btnLandValuePaid.setTitle("Land Value (Paid off)", for: .normal)
        self.btnLandValuePaid.setTitleColor(UIColor.black, for: .normal)
        // self.btnLandValuePaid.tintColor = .label
        self.landPurchaseTxtFld.isHidden = false
        self.purchaseDivider.isHidden = false
        self.differenceLbl.isHidden = false
        self.landCurrentValuetxtFld.isHidden = false
        self.priceDivider.isHidden = false
        self.landValuePaidTxtFld.isHidden = true
        self.valueDivider.isHidden = true
    }
    
    @IBAction func valueLandAction(_ sender: Any) {
        self.valuePaid()
    }
    
    func valuePaid(){
        self.mode = "Value paid"
        //self.btnLandValuePaid.setImage(UIImage(systemName: "circle.inset.filled"), for: .normal)
        self.btnLandValuePaid.tintColor = UIColor.blue
        self.btnLandValuePaid.setImage(UIImage.init(named: "fillCircle"), for: .normal)
        self.btnLandValuePaid.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
        self.btnLandValuePaid.setTitle("Land Value (Paid off)", for: .normal)
        //self.btnLandValuePaid.tintColor = UIColor(named: "primaryBlue")
        //self.btnLandEquity.setImage(UIImage(systemName: "circle"), for: .normal)
        self.btnLandEquity.setImage(UIImage.init(named: "unfiledCircle"), for: .normal)
        self.btnLandEquity.setTitle("Land Equity", for: .normal)
        self.btnLandEquity.setTitleColor(UIColor.black, for: .normal)
        //self.btnLandEquity.tintColor = .label
        self.landPurchaseTxtFld.isHidden = true
        self.purchaseDivider.isHidden = true
        self.differenceLbl.isHidden = true
        self.landCurrentValuetxtFld.isHidden = true
        self.priceDivider.isHidden = true
        self.landValuePaidTxtFld.isHidden = false
        self.valueDivider.isHidden = false
    }
    
    @IBAction func addAssetAction(_ sender: Any) {
        self.view.endEditing(true)
        //if self.isValidFields() {
        //self.addAssetServiceCall()
        //}
        
        if self.btnAddAsset.titleLabel?.text == "Update Asset"{
            if self.isValidFields() {
            self.updateAssetServiceCall()
            }
        }else{
            if self.isValidFields() {
            self.addAssetServiceCall()
            }
        }
    }
    
    func addAssetServiceCall() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
        
        self.landValuePaidTxtFld.text = self.landValuePaidTxtFld.text?.trim()
        
        NotificationCenter.default.post(name: Notification.Name("AddLoaderForAsset"), object: nil, userInfo: nil)

        let parameters: [String: Any]
        
        if self.mode == "Land Equity" {
            parameters = [
                "asset_type": "equity",
                "asset_current_value":self.landCurrentValuetxtFld.text ?? "",
                "asset_purchase_price":self.landPurchaseTxtFld.text ?? "",
                "category_code": "LAND"
            ]
        } else {
            print("Trying paid off")
            parameters = [
                "asset_type": "paidoff",
                "asset_current_value":self.landValuePaidTxtFld.text ?? "",
                "category_code": "LAND"
            ]
        }
        
        let assetService = AssetService()
        assetService.addAssetHomePaidoff(parameters: parameters){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            //self.removLoader()
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
    }
    
    func updateAssetServiceCall() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
        
        self.landValuePaidTxtFld.text = self.landValuePaidTxtFld.text?.trim()
        NotificationCenter.default.post(name: Notification.Name("AddLoaderForAsset"), object: nil, userInfo: nil)
        
        let parameter:[String:Any]
        if self.mode == "Land Equity" {
            parameter = [
                "category_code": "LAND",
                "asset_id":asset?.assetID ?? "",
                "asset_type": "equity",
                "asset_current_value":self.landCurrentValuetxtFld.text ?? "",
                "asset_purchase_price":self.landPurchaseTxtFld.text ?? "",
            ]
        } else {
            parameter = [
                "category_code": "LAND",
                "asset_id":asset?.assetID ?? "",
                "asset_type": "paidoff",
                "asset_current_value":self.landValuePaidTxtFld.text ?? ""
            ]
        }
        
        let assetService = AssetService()
        if self.mode == "Land Equity" {
            assetService.updateAssetHomeEquity(parameters: parameter){ (result,code) in
                switch result{
                case .success(let responseData):
                    if code == 200 {
                        if responseData.status == 200{
                            self.navigationController?.popViewController(animated: false)
                        }
                        self.showAlert(message: responseData.message)
                    }else{
                        self.showAlert(message: responseData.message)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                }
                self.removLoader()
                NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
            }
        } else {
            assetService.updateAssetHomePaidoff(parameters: parameter) { (result, code) in
                switch result {
                case .success(let responseData):
                    if code == 200 {
                        if responseData.status == 200{
                            self.navigationController?.popViewController(animated: false)
                        }
                        self.showAlert(message: responseData.message)
                    }else{
                        self.showAlert(message: responseData.message)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                }
                self.removLoader()
                NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
            }
        }
    }
    
}


extension AddRealEstateLandVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        // Allow to remove character (Backspace)
           if string == "" {
               return true
           }

          // Block multiple dot
           if (textField.text?.contains("."))! && string == "." {
               return false
           }
        
        
        if (textField.text?.contains("."))! {
            let limitDecimalPlace = 2
            let decimalPlace = textField.text?.components(separatedBy: ".").last
            if (decimalPlace?.count)! < limitDecimalPlace {
                return true
            }
            else {
                return false
            }
        }
        
        if textField == landPurchaseTxtFld {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = landPurchaseTxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == landCurrentValuetxtFld {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = landCurrentValuetxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == landValuePaidTxtFld {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = landValuePaidTxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        return true
    }
    
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        if let valueOne = textfield.text, let valueTwo = self.landPurchaseTxtFld.text {
            let difference = (Double(valueTwo) ?? 0.0) - (Double(valueOne) ?? 0.0)
            
            if let assetCurrentValue = "\(difference)".first {
                if assetCurrentValue == "-"{
                    self.differenceLbl.text = "Estimated Home Equity: \(difference)".replacingOccurrences(of: "-", with: "-$")
                }else{
                    self.differenceLbl.text = "Estimated Home Equity: $\(difference)"
                }
            }
        }
    }
}
