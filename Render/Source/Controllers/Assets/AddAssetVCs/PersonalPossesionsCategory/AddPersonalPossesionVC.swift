//
//  AddPersonalPossesionVC.swift
//  Render
//
//  Created by Anis Agwan on 31/12/21.
//

import UIKit

class AddPersonalPossesionVC: UIViewController {
    
    
    var picker: UIPickerView!
//    var possessions: [String] = [
//        "Automobiles",
//        "Jewellery",
//        "Collectibles",
//        "Antiques",
//        "Boats",
//        "Other"
//    ]
    var selectedType: String = ""
    var selectedAssetTypeId: String = ""
    var asset: AssetDatum?
    var update: Bool = false
    var assetTypes: AssetAllTypeModel?
    var frameSize: CGRect?
    var safe: CGRect?
    
    @IBOutlet weak var assetName: UITextField!
    @IBOutlet weak var assetType: UITextField!
    @IBOutlet weak var currentValue: UITextField!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var btnAddAsset: UIButton!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAssetTypes()
        if update {
            
            if asset?.assetCurrentValue?.length ?? 0 > 9{
                self.currentValue.text = self.asset?.assetCurrentValue?.replacingOccurrences(of: ".00", with: "")
            }else{
                self.currentValue.text = self.asset?.assetCurrentValue
            }
            
            self.assetName.text = self.asset?.assetName
            //self.assetName.isUserInteractionEnabled = !update
            //self.currentValue.text = self.asset?.assetCurrentValue
            self.assetType.text = self.asset?.assetType
            self.assetType.isEnabled = false
            assetType.textColor = UIColor(named: "textGrayColor")
            self.btnAddAsset.setTitle("Update Asset", for: .normal)
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            self.bottomConst.constant = 50
        }else{
            self.bottomConst.constant = 110
        }
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        
        self.assetName.text = assetName.text?.trim()
        self.assetType.text = self.assetType.text?.trim()
        self.currentValue.text = self.currentValue.text?.trim()
        
        if (assetName.text?.isEmpty)! {
            message = "Please Enter Asset Name"
            isFound = false
        } else if (assetName.text?.length ?? 0 < 3){
            message = "The Asset Name must be at least 3 characters in length"
            isFound = false
        } else if (assetType.text?.isEmpty)! {
            message = "Please Select Asset Type"
            isFound = false
        }else if (currentValue.text?.isEmpty)! {
            message = "Please Enter Current Value"
            isFound = false
        }else if (currentValue.text == "0" || currentValue.text == "00" || currentValue.text == "000" || currentValue.text == "0000" || currentValue.text == "00000" || currentValue.text == "000000") {
            message = "Please Enter Vaild Amount"
            isFound = false
        }else if (currentValue.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func addAssetAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if self.btnAddAsset.titleLabel?.text == "Update Asset"{
            self.updateAssetServiceCall()
        }else{
            if self.isValidFields() {
                self.addAssetServiceCall()
            }
        }
    }
    
    
    func addAssetServiceCall() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
        
        self.assetName.text = assetName.text?.trim()
        self.assetType.text = self.assetType.text?.trim()
        self.currentValue.text = self.currentValue.text?.trim()
        
        NotificationCenter.default.post(name: Notification.Name("AddLoaderForAsset"), object: nil, userInfo: nil)
        let parameter:[String:Any] = [
            
            "asset_name": self.assetName.text ?? "",
            "asset_type":self.selectedAssetTypeId,
            "asset_current_value": self.currentValue.text ?? "",
            "category_code":"PERSONALPOSSESION"
            
        ]
        let assetService = AssetService()
        assetService.addAssetPersonal(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
    }
    
    func getAssetTypes() {
        self.showLoader()
        let parameter: [String: Any] = [
            "category_id": "3"
        ]
        let assetService = AssetService()
        assetService.getAllAssetType(parameters: parameter) { (result, code) in
            switch result {
            case .success(let responseData):
                if code == 200 {
                    self.assetTypes = responseData
                    if let arr = self.assetTypes?.data.assetTypes {
                        if arr.count > 0 {
                            self.selectedType = arr[0].assetTypeName
                            self.selectedAssetTypeId = arr[0].assetTypeID
                        } else {
                            print("Nothing selected")
                        }
                    }
                    print(responseData)
                } else {
                    print("ERROR")
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
        }
        self.removLoader()
    }
    
    func updateAssetServiceCall() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
        
        self.assetName.text = assetName.text?.trim()
        self.assetType.text = self.assetType.text?.trim()
        self.currentValue.text = self.currentValue.text?.trim()
        NotificationCenter.default.post(name: Notification.Name("AddLoaderForAsset"), object: nil, userInfo: nil)
        
        let parameter:[String:Any] = [
            "asset_id": asset?.assetID ?? "",
            "asset_name": self.assetName.text ?? "",
            "asset_current_value":self.currentValue.text ?? "",
            "category_code":"PERSONALPOSSESION"
        ]
        
        let netWorthService = NetWorthService()
        netWorthService.updateAsset(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
            NotificationCenter.default.post(name: Notification.Name("RemoveLoaderForAsset"), object: nil, userInfo: nil)
        }
    }
    
}


extension AddPersonalPossesionVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == currentValue {
            // Allow to remove character (Backspace)
               if string == "" {
                   return true
               }

              // Block multiple dot
               if (textField.text?.contains("."))! && string == "." {
                   return false
               }
            
            
            if (textField.text?.contains("."))! {
                let limitDecimalPlace = 2
                let decimalPlace = textField.text?.components(separatedBy: ".").last
                if (decimalPlace?.count)! < limitDecimalPlace {
                    return true
                }
                else {
                    return false
                }
            }
        }
       
        let allowedCharacters : CharacterSet
        if textField == assetName {
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_letter).inverted
        }else{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_special_letter).inverted
        }
        
        let components = string.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        
        if textField == assetName {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = assetName.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
           
            
        }
        
        if textField == currentValue {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = currentValue.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        
        
        return true
    }
    
    func pickUP(_ textField: UITextField) {
        self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.selectRow(0, inComponent: 0, animated: true)
        if let arr = assetTypes?.data.assetTypes {
            if arr.count > 0 {
                self.selectedType = arr[0].assetTypeName
            }
        }
        //        self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
        //        self.selectedCategory = self.mainCategoryModel?.data[0].categoryName ?? ""
        assetType.inputView = self.picker
        //
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        //
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        assetType.inputAccessoryView = toolBar
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUP(assetType)
    }
    
    @objc func selectClick() {
        
        self.assetType.text = self.selectedType
        assetType.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        assetType.resignFirstResponder()
    }
}


extension AddPersonalPossesionVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return assetTypes?.data.assetTypes.count ?? 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        if let arr = assetTypes?.data.assetTypes {
            if arr.count > 0 {
                label.text = arr[row].assetTypeName
            } else {
                label.text = "No Asset Types"
            }
        }
        
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if let arr = assetTypes?.data.assetTypes {
            if arr.count > 0 {
                self.selectedType = arr[row].assetTypeName
                self.selectedAssetTypeId = arr[row].assetTypeID
            } else {
                print("Nothing selected")
            }
        }
    }
}
