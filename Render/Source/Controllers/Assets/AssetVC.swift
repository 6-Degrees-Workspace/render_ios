import UIKit

class AssetVC: BaseVC {
    
    var navTitle: String = ""
    var categoryId: String = ""
    var assetDetailByCategoryModel : AssetDetailByCategoryModel?
    var assetSubCategory: [String] = [ ]
    var assetCategory: String = ""
    var catDatum: CatDatum?
    var dataArray = NSMutableArray()
    var DicAsset = [String:[AssetDatum]]()
    var DicPlaidAsset = [String:[AssetPlaidData]]()
    var getAllPlaidAssetDetailByCategoryModel : GetAllPlaidAssetDetailByCategoryModel?
    var data: String = "Manual"
    // IBOutlets
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var lblAssetAmt: UILabel!
    @IBOutlet weak var lblAssetdifference: UILabel!
    @IBOutlet weak var assetTable: UITableView!
    @IBOutlet weak var lblAssetSmallHdr: UILabel!
    // @IBOutlet weak var assetCollection: UICollectionView!
    @IBOutlet weak var lblCentreConst: NSLayoutConstraint!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tblTopConst: NSLayoutConstraint!
    @IBOutlet weak var btnAddView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assetTable.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lbl_NavigationTitle.text = navTitle
        switch navTitle {
        case "Cash and Financial Account Balances":
            lblCentreConst.constant = 10
            self.getAllAssetDetailByCategoryServiceCall()
            assetTable.isHidden = false
            //assetCollection.isHidden = true
        case "Real Estate Assets":
            tblTopConst.constant = 35
            segmentedControl.isHidden = true
            self.getAllAssetDetailByCategoryServiceCall()
            assetTable.isHidden = false
            //assetCollection.isHidden = true
        case "Personal Possessions":
            tblTopConst.constant = 35
            segmentedControl.isHidden = true
            self.getAllAssetDetailByCategoryServiceCall()
            assetTable.isHidden = false
            //assetCollection.isHidden = true
            
        default:
            assetTable.isHidden = true
            // assetCollection.isHidden = true
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    @IBAction func actionAddAsset(_ sender: Any) {
        let objVC: AddAssetViewController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.assets, viewControllerName: AddAssetViewController.nameOfClass)
        objVC.selectedCategory = assetCategory
        objVC.categoryId = categoryId
        objVC.addDefault = true
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func dataChanged(sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            btnAddView.isHidden = false
            data = "Manual"
            NSLog("manual selected")
            switch navTitle {
            case "Cash and Financial Account Balances":
                lblCentreConst.constant = 10
                self.getAllAssetDetailByCategoryServiceCall()
                assetTable.isHidden = false
                //assetCollection.isHidden = true
            case "Real Estate Assets":
                self.getAllAssetDetailByCategoryServiceCall()
                assetTable.isHidden = false
                //assetCollection.isHidden = true
            case "Personal Possessions":
                self.getAllAssetDetailByCategoryServiceCall()
                assetTable.isHidden = false
                //assetCollection.isHidden = true
                
            default:
                assetTable.isHidden = true
                // assetCollection.isHidden = true
            }
        case 1:
            data = "Plaid"
            btnAddView.isHidden = true
            NSLog("Plaid selected")
            switch navTitle {
            case "Cash and Financial Account Balances":
                lblCentreConst.constant = 10
                self.getAllPlaidAssetDetailByCategory()
                //assetTable.isHidden = false
            case "Real Estate Assets":
                self.getAllPlaidAssetDetailByCategory()
                assetTable.isHidden = false
            case "Personal Possessions":
                self.getAllPlaidAssetDetailByCategory()
                assetTable.isHidden = false
            default:
                assetTable.isHidden = true
                // assetCollection.isHidden = true
            }
        default:
            break;
        }
    }
    
    
}

//MARK: - TableView for Stocks and more
extension AssetVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //let systemVersion = UIDevice.current.systemVersion
        
        if #available(iOS 14.0, *) {
            let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
            let label = UILabel(frame: CGRect(x:10, y:10, width:tableView.frame.size.width, height:18))
            label.font = UIFont.systemFont(ofSize: 18)
            label.text = assetSubCategory[section]
            view.addSubview(label);
            view.backgroundColor = UIColor.white;
            return view
        } else {
            let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
            let label = UILabel(frame: CGRect(x:10, y:0, width:tableView.frame.size.width, height:18))
            label.font = UIFont.systemFont(ofSize: 18)
            label.text = assetSubCategory[section]
            view.addSubview(label);
            view.backgroundColor = UIColor.white;
            return view
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if data == "Manual"{
            return self.DicAsset.keys.count
        }else{
            return self.DicPlaidAsset.keys.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if data == "Manual"{
            let cate = assetSubCategory[section]
            return self.DicAsset[cate]?.count ?? 0
        }else{
            let cate = assetSubCategory[section]
            return self.DicPlaidAsset[cate]?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = assetTable.dequeueReusableCell(withIdentifier: "assetCell", for: indexPath) as! AssetTableCell
        
        let key = assetSubCategory[indexPath.section]
        
        if data == "Manual"{
            
            cell.btnDelete.isHidden = false
            cell.assetDate.isHidden = false
            cell.btnShowDelete.isHidden = false
            
            let category = self.DicAsset[key]
            let objDict = category?[indexPath.row]
            
            cell.assetName.text = objDict?.assetName
            cell.assetDate.text = DateTimeUtils.sharedInstance.getSeenAtDateFromString(strDate: objDict!.assetDate)//objDict?.assetDate
            cell.assetImg.sd_setImage(with: URL(string: objDict?.subcategoryImageUrl ?? ""), placeholderImage: UIImage(named: "placeholder"))
            
            switch navTitle {
            case "Cash and Financial Account Balances":
                cell.assetAmt.text = "\(objDict!.assetCurrentValue ?? "")".convertDoubleToCurrency()
            case "Real Estate Assets":
                if objDict?.assetName == "REAL ESTATE (HOME paidoff)"{
                    if let assetCurrentValue = "\(objDict!.assetCurrentValue ?? "")".first {
                        if assetCurrentValue == "-"{
                            cell.assetAmt.text = "\(objDict!.assetCurrentValue ?? "")".replacingOccurrences(of: "-", with: "-$")
                        }else{
                            cell.assetAmt.text = "$"+"\(objDict!.assetCurrentValue ?? "")"
                        }
                    }
                }else if objDict?.assetName == "REAL ESTATE (HOME equity)"{
                    if let equity = "\(objDict!.equity ?? "")".first {
                        if equity == "-"{
                            cell.assetAmt.text = "\(objDict!.equity ?? "")".replacingOccurrences(of: "-", with: "-$")
                        }else{
                            cell.assetAmt.text = "$"+"\(objDict!.equity ?? "")"
                        }
                    }
                }else if objDict?.assetName == "REAL ESTATE (LAND paidoff)"{
                    if let assetCurrentValue = "\(objDict!.assetCurrentValue ?? "")".first {
                        if assetCurrentValue == "-"{
                            cell.assetAmt.text = "\(objDict!.assetCurrentValue ?? "")".replacingOccurrences(of: "-", with: "-$")
                        }else{
                            cell.assetAmt.text = "$"+"\(objDict!.assetCurrentValue ?? "")"
                        }
                    }
                }else{
                    
                    if let equity = "\(objDict!.equity ?? "")".first {
                        if equity == "-"{
                            cell.assetAmt.text = "\(objDict!.equity ?? "")".replacingOccurrences(of: "-", with: "-$")
                        }else{
                            cell.assetAmt.text = "$"+"\(objDict!.equity ?? "")"
                        }
                    }
                }
                
            case "Personal Possessions":
                cell.assetAmt.text = "\(objDict!.assetCurrentValue ?? "")".convertDoubleToCurrency()
            default: break
                
            }
            
            cell.assetDifference.isHidden = true
            let assetId : Int = Int (objDict?.assetID ?? "") ?? 0
            cell.btnDelete.tag =  assetId
            cell.btnDelete.addTarget(self, action: #selector(self.actionDelete(_:)), for: UIControl.Event.touchUpInside)
            return cell
            
        }else{
            cell.btnDelete.isHidden = true
            cell.assetDate.isHidden = true
            cell.btnShowDelete.isHidden = true
            let category = self.DicPlaidAsset[key]
            let objDict = category?[indexPath.row]
            cell.assetName.text = objDict?.assetName
            cell.assetImg.sd_setImage(with: URL(string: objDict?.subcategoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
            switch navTitle {
            case "Cash and Financial Account Balances":
                cell.assetAmt.text = "\(objDict!.assetCurrentValue)".convertDoubleToCurrency()
            case "Real Estate Assets": break
            case "Personal Possessions": break
            default: break
                
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if data == "Manual"{
            let key = assetSubCategory[indexPath.section]
            let category = self.DicAsset[key]
            let objDict = category?[indexPath.row]
            
            let objVC: AddAssetViewController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.assets, viewControllerName: AddAssetViewController.nameOfClass)
            objVC.update = true
            objVC.selectedCategory = assetCategory
            objVC.asset = objDict
            objVC.categoryId = categoryId
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
    
    @IBAction func actionDelete(_ sender: UIButton){
        
        let alert = UIAlertController(title: "Render", message: "Are you sure to delete this item?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {_ in
            
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: {_ in
            self.deleteAssetServiceCall(assetId: String(sender.tag))
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension AssetVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        let objDict = self.assetDetailByCategoryModel?.data.assetData[indexPath.row]
        //        let objVC: AddAssetsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.assets, viewControllerName: AddAssetsVC.nameOfClass)
        //        objVC.categoryId = self.categoryId
        //        objVC.navTitle = "Update Asset"
        //        self.navigationController?.pushViewController(objVC, animated: true)
    }
}

extension AssetVC {
    
    func getAllAssetDetailByCategoryServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id": categoryId
        ]
        
        let netWorthService = NetWorthService()
        netWorthService.getAllAssetDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.assetSubCategory.removeAll()
                    self.lblAssetAmt.text = "\(responseData.data.plaidAndManual)".convertDoubleToCurrency()
                    
                    self.assetDetailByCategoryModel = responseData
                    self.DicAsset = Dictionary(grouping: ((self.assetDetailByCategoryModel?.data.assetData)!)) { $0.subcategoryName }
                    self.assetDetailByCategoryModel?.data.assetData.forEach { subcategoryName in
                        self.assetSubCategory.append(subcategoryName.subcategoryName)
                    }
                    
                    self.assetSubCategory = NSOrderedSet(array: self.assetSubCategory).map({ $0 as! String })
                    self.assetTable.reloadData()
                    
                    switch self.navTitle {
                    case "Cash and Financial Account Balances":
                        self.assetTable.reloadData()
                    case "Real Estate Assets":
                        self.assetTable.reloadData()
                    case "Personal Possessions":
                        self.assetTable.reloadData()
                    default: break
                    }
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func deleteAssetServiceCall(assetId: String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "asset_id": assetId
        ]
        
        let assetService = AssetService()
        assetService.deleteAsset(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.getAllAssetDetailByCategoryServiceCall()
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}

//Plaid Data
extension AssetVC {
    
    func getAllPlaidAssetDetailByCategory(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id": categoryId
        ]
        
        let netWorthService = NetWorthService()
        netWorthService.getAllPlaidAssetDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    print(responseData)
                    self.assetSubCategory.removeAll()
                    self.lblAssetAmt.text = "\(responseData.data.totalAssets)".convertDoubleToCurrency()
                    self.getAllPlaidAssetDetailByCategoryModel = responseData
                    
                    self.DicPlaidAsset = Dictionary(grouping: ((self.getAllPlaidAssetDetailByCategoryModel?.data.assetData)!)) { $0.subcategoryName }
                   
                    self.getAllPlaidAssetDetailByCategoryModel?.data.assetData.forEach { subcategoryName in
                        self.assetSubCategory.append(subcategoryName.subcategoryName)
                    }
                    
                    self.assetSubCategory = NSOrderedSet(array: self.assetSubCategory).map({ $0 as! String })
                   // self.assetTable.reloadData()
                    
                    switch self.navTitle {
                    case "Cash and Financial Account Balances":
                        self.assetTable.reloadData()
                    case "Real Estate Assets":
                        self.assetTable.reloadData()
                    case "Personal Possessions":
                        self.assetTable.reloadData()
                    default: break
                        
                    }
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
}

extension Float {
    var asLocaleCurrency:String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = NSLocale.current
        return formatter.string(from: NSNumber(value: self))!
    }
}
