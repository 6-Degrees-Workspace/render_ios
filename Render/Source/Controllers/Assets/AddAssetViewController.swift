//
//  AddAssetViewController.swift
//  Render
//
//  Created by Anis Agwan on 27/12/21.
//

import UIKit

class AddAssetViewController: BaseVC {
    //IBOutlets
    @IBOutlet weak var assetCategoryPicker: UIPickerView!
    @IBOutlet weak var subCategoryCollection: UICollectionView!
    @IBOutlet weak var addAccountContainer: UIView!
    @IBOutlet weak var addCDsContainer: UIView!
    @IBOutlet weak var addInvestmentContainer: UIView!
    @IBOutlet weak var cattxtFld: UITextField!
    @IBOutlet weak var addPersonalContainer: UIView!
    @IBOutlet weak var addRealEstateHomeContainer: UIView!
    @IBOutlet weak var addRealEstateLandContainer: UIView!
    @IBOutlet weak var scroller: UIScrollView!
   
    //Variables
    var height: CGFloat!
    var selectedCategory: String = "Select Category"
    var assetCategory: [String] = [
        "Select Category",
        "Cash and Financial Account Balances",
        "Real Estate Assets",
        "Personal Possesions"
    ]
    var accounts: [String] = [
        "Savings & Checking Accounts",
        "CD",
        "Investments"
    ]
    var realEstates: [String] = [
        "Home",
        "Land"
    ]
    var frameSize: CGRect = UIScreen.main.bounds
    var picker: UIPickerView!
    var mainCategoryModel : MainCategoryModel?
    var mainCategoryID: String = ""
    var subCategoryModel : SubCategoryModel?
    var update: Bool = false
    var addDefault: Bool = false
    var asset: AssetDatum?
    var categoryId: String = ""
    
    //View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfAddLoader(notification:)), name: Notification.Name("AddLoaderForAsset"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfRemoveLoader(notification:)), name: Notification.Name("RemoveLoaderForAsset"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.subCategoryCollection.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getAllCategoriesServiceCall()
        self.subCategoryCollection.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        if addDefault {
            self.mainCategoryID = self.categoryId
            self.cattxtFld.text = self.selectedCategory
            self.cattxtFld.isEnabled = false
            self.getSubCategoriesServiceCall(categoryId: self.categoryId)
            self.cattxtFld.text = selectedCategory
            self.hideContainers()
            self.addPersonalContainer.isHidden = true
            cattxtFld.resignFirstResponder()
        }
        
        if update {
            subCategoryCollection.isUserInteractionEnabled = false
            cattxtFld.isUserInteractionEnabled = false
            cattxtFld.textColor = UIColor(named: "textGrayColor")
            self.cattxtFld.text = self.selectedCategory
            self.mainCategoryID = self.categoryId
            self.subCategoryCollection.isHidden = false
            self.getSubCategoriesServiceCall(categoryId: self.mainCategoryID)
            
            if mainCategoryID != "3" {
                switch asset?.subcategoryName {
                case "Savings & Checking Accounts":
                    addAccountContainer.isHidden = false
                    addCDsContainer.isHidden = true
                    addInvestmentContainer.isHidden = true
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = true
                    addRealEstateHomeContainer.isHidden = true
                case "CD":
                    addAccountContainer.isHidden = true
                    addCDsContainer.isHidden = false
                    addInvestmentContainer.isHidden = true
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = true
                    addRealEstateHomeContainer.isHidden = true
                case "Investments":
                    addAccountContainer.isHidden = true
                    addCDsContainer.isHidden = true
                    addInvestmentContainer.isHidden = false
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = true
                    addRealEstateHomeContainer.isHidden = true
                case "Home":
                    addAccountContainer.isHidden = true
                    addCDsContainer.isHidden = true
                    addInvestmentContainer.isHidden = true
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = true
                    addRealEstateHomeContainer.isHidden = false
                case "Land":
                    addAccountContainer.isHidden = true
                    addCDsContainer.isHidden = true
                    addInvestmentContainer.isHidden = true
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = false
                    addRealEstateHomeContainer.isHidden = true
                default:
                    self.hideContainers()
                    addPersonalContainer.isHidden = true
                }
            } else {
                addAccountContainer.isHidden = true
                addCDsContainer.isHidden = true
                addInvestmentContainer.isHidden = true
                subCategoryCollection.isHidden = true
                addPersonalContainer.isHidden = false
                addRealEstateLandContainer.isHidden = true
                addRealEstateHomeContainer.isHidden = true
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            scroller.contentSize = CGSize(width: 325,height:800)
        }else{
        }
        scroller.isScrollEnabled = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "toAccounts":
            if let accountVC = segue.destination as? AddAccountVC {
                accountVC.asset = asset
                accountVC.update = update
                accountVC.frameSize = self.frameSize
                accountVC.safe = self.view.safeAreaLayoutGuide.layoutFrame
            }
        case "toCDS":
            if let cdVC = segue.destination as? AddCDs {
                cdVC.asset = asset
                cdVC.update = update
                cdVC.frameSize = frameSize
            }
        case "toInvestments":
            if let investmentVC = segue.destination as? AddInvestmentsVC {
                investmentVC.asset = asset
                investmentVC.update = update
                investmentVC.frameSize = frameSize
            }
        case "toHomeEstate":
            if let homeVC = segue.destination as? AddRealEstateHomeVC {
                homeVC.frameSize = frameSize
                homeVC.asset = asset
                homeVC.update = update
            }
        case "toLandEstate":
            if let landVC = segue.destination as? AddRealEstateLandVC {
                landVC.frameSize = frameSize
                landVC.asset = asset
                landVC.update = update
            }
        case "toPersonalAsset":
            if let personalVC = segue.destination as? AddPersonalPossesionVC {
                personalVC.asset = asset
                personalVC.update = update
                personalVC.frameSize = self.frameSize
            }
        default:
            print("None")
        }
        
        if segue.identifier == "toAccounts" {
            
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let newHeight: CGFloat = self.subCategoryCollection.collectionViewLayout.collectionViewContentSize.height
        
        var frame : CGRect = self.subCategoryCollection.frame
        frame.size.height = newHeight
        self.subCategoryCollection.frame = frame
    }
    
    @objc func methodOfAddLoader(notification: Notification) {
        self.showLoader()
    }
    
    @objc func methodOfRemoveLoader(notification: Notification) {
        self.removLoader()
    }
    
    
}

//MARK: - UITextField Delegates
extension AddAssetViewController: UITextFieldDelegate {
    
    func pickUP(_ textField: UITextField) {
        self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.selectRow(0, inComponent: 0, animated: true)
        self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
        self.selectedCategory = self.mainCategoryModel?.data[0].categoryName ?? ""
        cattxtFld.inputView = self.picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        cattxtFld.inputAccessoryView = toolBar
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUP(cattxtFld)
    }
    
    @objc func selectClick() {
        self.getSubCategoriesServiceCall(categoryId: self.mainCategoryID)
        self.cattxtFld.text = selectedCategory
        self.hideContainers()
        self.addPersonalContainer.isHidden = true
        cattxtFld.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        cattxtFld.resignFirstResponder()
    }
}


//MARK: - UIPicker delegates
extension AddAssetViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.mainCategoryModel?.data.count ?? 0//assetCategory.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        let objDict = self.mainCategoryModel?.data[row]
        label.text = objDict?.categoryName//assetCategory[row]
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 16)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let objDict = self.mainCategoryModel?.data[row]
        print(objDict?.categoryID ?? "")
        self.mainCategoryID = objDict?.categoryID ?? ""
        self.selectedCategory = objDict?.categoryName ?? ""
        //self.getSubCategoriesServiceCall(categoryId: objDict?.categoryID ?? "")
    }
}


//MARK: - UICollection Delegates
extension AddAssetViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subCategoryModel?.data.subcatData.count ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = subCategoryCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "categoryHeader", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let objDict = self.subCategoryModel?.data.subcatData[indexPath.row]
        
        let cell = subCategoryCollection.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionCell
        cell.categoryLbl.text = objDict?.subCategoryName
        if objDict?.subCategoryName == "Savings & Checking Accounts"{
            cell.categoryImg.image = UIImage(named: "saving")
        }else if objDict?.subCategoryName == "CD"{
            cell.categoryImg.image = UIImage(named: "CD_icon")
        }else  if objDict?.subCategoryName == "Investments"{
            cell.categoryImg.image = UIImage(named: "Investment")
        }else  if objDict?.subCategoryName == "Land"{
            cell.categoryImg.image = UIImage(named: "land")
        }else  if objDict?.subCategoryName == "Home"{
            cell.categoryImg.image = UIImage(named: "home")
        }else  if objDict?.subCategoryName == "Property"{
            cell.categoryImg.image = UIImage(named: "propertyImage")
        }else  if objDict?.subCategoryName == "Jewellery"{
            cell.categoryImg.image = UIImage(named: "jewelleryImage")
        } else {
            //cell.categoryImg.image = UIImage(named: "otherActive")
            cell.categoryImg.sd_setImage(with: URL(string: objDict?.subCategoryImageUrl ?? ""), placeholderImage: UIImage(named: "placeholder"))
        }
        if update {
            if objDict?.subCategoryName == asset?.subcategoryName {
                cell.isSelected = true
               // cell.isDisableActive()
                cell.isActive()
            }else{
                cell.isDisableActive()
            }
        } else {
            cell.isInactive()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = subCategoryCollection.cellForItem(at: indexPath) as? CategoryCollectionCell {
            
            if mainCategoryID != "3" {
                switch subCategoryModel?.data.subcatData[indexPath.item].subCategoryName {
                case "Savings & Checking Accounts":
                    addAccountContainer.isHidden = false
                    addCDsContainer.isHidden = true
                    addInvestmentContainer.isHidden = true
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = true
                    addRealEstateHomeContainer.isHidden = true
                case "CD":
                    addAccountContainer.isHidden = true
                    addCDsContainer.isHidden = false
                    addInvestmentContainer.isHidden = true
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = true
                    addRealEstateHomeContainer.isHidden = true
                case "Investments":
                    
                    addAccountContainer.isHidden = true
                    addCDsContainer.isHidden = true
                    addInvestmentContainer.isHidden = false
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = true
                    addRealEstateHomeContainer.isHidden = true
                case "Home":
                    addAccountContainer.isHidden = true
                    addCDsContainer.isHidden = true
                    addInvestmentContainer.isHidden = true
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = true
                    addRealEstateHomeContainer.isHidden = false
                case "Land":
                    addAccountContainer.isHidden = true
                    addCDsContainer.isHidden = true
                    addInvestmentContainer.isHidden = true
                    addPersonalContainer.isHidden = true
                    addRealEstateLandContainer.isHidden = false
                    addRealEstateHomeContainer.isHidden = true
                default:
                    self.hideContainers()
                    addPersonalContainer.isHidden = true
                }
            } else {
                
                addAccountContainer.isHidden = true
                addCDsContainer.isHidden = true
                addInvestmentContainer.isHidden = true
                subCategoryCollection.isHidden = true
                addPersonalContainer.isHidden = false
                addRealEstateLandContainer.isHidden = true
                addRealEstateHomeContainer.isHidden = true
            }
            
            cell.isActive()
            //let objDict = self.subCategoryCollection?.data[indexPath.row]
            //categorySelected = objDict?.categoryID ?? ""
            //print(categorySelected)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = subCategoryCollection.cellForItem(at: indexPath) as? CategoryCollectionCell {
            cell.isInactive()
            //print(categorySelected)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let edgePadding = subCategoryCollection.frame.width * 0.05
        let interSpacing = subCategoryCollection.frame.width * 0.05
        let cellWidth = (subCategoryCollection.frame.width - 2*edgePadding - 2*interSpacing) / 3
        
        return .init(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let edgePadding = subCategoryCollection.frame.width * 0.05
        //let interSpacing = numpadCollection.frame.width * 0.1
        return .init(top: 10, left: edgePadding, bottom: 10, right: edgePadding)
    }
    
    
}

extension AddAssetViewController {
    // MARK: - Get All Categories Service Call
    func getAllCategoriesServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "type": "ASSET",
        ]
        let netWorthService = NetWorthService()
        netWorthService.getMasterCategories(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.mainCategoryModel = responseData
                    //self.assetCategoryPicker.reloadAllComponents()
                    //print(responseData.data[1].categoryID)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func getSubCategoriesServiceCall(categoryId:String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id": categoryId,
        ]
        let netWorthService = NetWorthService()
        netWorthService.getAllSubcategories(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.subCategoryModel = responseData
                    //self.assetCategoryPicker.reloadAllComponents()
                    if self.subCategoryModel?.data.categoryID == self.mainCategoryID && self.mainCategoryID != "3" {
                        self.addPersonalContainer.isHidden = true
                        self.subCategoryCollection.isHidden = false
                        self.subCategoryCollection.reloadData()
                    } else if self.mainCategoryID == "3" {
                        self.subCategoryCollection.isHidden = true
                        self.addPersonalContainer.isHidden = false
                        self.hideContainers()
                    }
                    print(responseData)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
    func hideContainers() {
        self.addAccountContainer.isHidden = true
        self.addCDsContainer.isHidden = true
        self.addInvestmentContainer.isHidden = true
        self.addRealEstateHomeContainer.isHidden = true
        self.addRealEstateLandContainer.isHidden = true
        
    }
}



extension UIDevice {
    var iPhoneX: Bool { UIScreen.main.nativeBounds.height == 2436 }
    var iPhone: Bool { UIDevice.current.userInterfaceIdiom == .phone }
    var iPad: Bool { UIDevice().userInterfaceIdiom == .pad }
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_6Plus_6sPlus_7Plus_8Plus_Simulators = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus Simulators"
        case iPhones_X_XS_12MiniSimulator = "iPhone X or iPhone XS or iPhone 12 Mini Simulator"
        case iPhone_XR_11 = "iPhone XR or iPhone 11"
        case iPhone_XSMax_ProMax = "iPhone XS Max or iPhone Pro Max"
        case iPhone_11Pro = "iPhone 11 Pro"
        case iPhone_12Mini = "iPhone 12 Mini"
        case iPhone_12_12Pro = "iPhone 12 or iPhone 12 Pro"
        case iPhone_12ProMax = "iPhone 12 Pro Max"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 1136: return .iPhones_5_5s_5c_SE
        case 1334: return .iPhones_6_6s_7_8
        case 1792: return .iPhone_XR_11
        case 1920: return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2208: return .iPhones_6Plus_6sPlus_7Plus_8Plus_Simulators
        case 2340: return .iPhone_12Mini
        case 2426: return .iPhone_11Pro
        case 2436: return .iPhones_X_XS_12MiniSimulator
        case 2532: return .iPhone_12_12Pro
        case 2688: return .iPhone_XSMax_ProMax
        case 2778: return .iPhone_12ProMax
        default: return .unknown
        }
    }
}


extension UIScreen {
    
    enum SizeType: CGFloat {
        case Unknown = 0.0
        case iPhone4 = 960.0
        case iPhone5 = 1136.0
        case iPhone6 = 1334.0
        case iPhone6Plus = 1920.0
    }
    
    var sizeType: SizeType {
        let height = nativeBounds.height
        guard let sizeType = SizeType(rawValue: height) else { return .Unknown }
        return sizeType
    }
}
