//
//  HomeVC.swift
//  Render
//
//  Created by Anis Agwan on 02/12/21.
//

import UIKit

class HomeVC: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var suggestionTableView: UITableView!
    @IBOutlet weak var suggestionTableHeight: NSLayoutConstraint!
    @IBOutlet weak var lblUserNetWorth: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblNetWorthPrice: UILabel!
    @IBOutlet weak var lblAssetPrice: UILabel!
    @IBOutlet weak var lblLiabPrice: UILabel!
    @IBOutlet weak var lblNetIncomePrice: UILabel!
    @IBOutlet weak var lblIncomePrice: UILabel!
    @IBOutlet weak var lblExpencePrice: UILabel!
    @IBOutlet weak var imageExpense: UIImageView!
    @IBOutlet weak var imageIncome: UIImageView!
    
    //View Lifecyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "SavedPerson") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(UserData.self, from: savedPerson) {
                //print(loadedPerson.name)
                lblUserName.text = loadedPerson.fullName
                self.imageUser.sd_setImage(with: URL(string: loadedPerson.userProfilePic ), placeholderImage: UIImage(named: "placeholder"))
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileServiceCall()
        self.getDashboardDataServiceCall()
        self.suggestionTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        self.suggestionTableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.suggestionTableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newvalue = change?[.newKey] {
                let newsize = newvalue as! CGSize
                self.suggestionTableHeight.constant = newsize.height
            }
        }
    }
    
    @IBAction func actionNetIncome(_ sender: UIButton){
        let objVC: DashboardTabBarController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.dashboard, viewControllerName: DashboardTabBarController.nameOfClass)
        objVC.selectedIndex = 2
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    @IBAction func actionNetWorth(_ sender: UIButton){
        let objVC: DashboardTabBarController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.dashboard, viewControllerName: DashboardTabBarController.nameOfClass)
        objVC.selectedIndex = 1
        self.navigationController?.pushViewController(objVC, animated: false)
    }
}


//MARK: - UITableView
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = suggestionTableView.dequeueReusableCell(withIdentifier: "suggestionCell", for: indexPath) as! SuggestionTableViewCell
        
        return cell
    }
    
    
    func getDashboardDataServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "": ""
        ]
        
        let homeService = HomeService()
        homeService.getDashboardData(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.message.length > 0 {
                        self.showAlert(message: responseData.message)
                    }
                    if let netWorthPriceObj = "\(responseData.data.netWorth)".first {
                        if netWorthPriceObj == "-"{
                            self.lblNetWorthPrice.text = responseData.data.netWorth.replacingOccurrences(of: "-", with: "-$") //"\(String(format: "%.2f", responseData.data.netWorth))".replacingOccurrences(of: "-", with: "-$")
                        }else{
                            self.lblNetWorthPrice.text = "$"+"\(responseData.data.netWorth)"//"$"+"\(String(format: "%.2f", responseData.data.netWorth))"
                        }
                    }
                    
                    if let netIncomePriceObj = "\(responseData.data.netIncome)".first {
                        if netIncomePriceObj == "-"{
                            self.lblNetIncomePrice.text = responseData.data.netIncome.replacingOccurrences(of: "-", with: "-$")
                        }else{
                            self.lblNetIncomePrice.text = "$"+"\(responseData.data.netIncome)"
                        }
                    }
                    
                    
                    if let totalAssetValue = responseData.data.totalAssetValue.first {
                        if totalAssetValue == "-"{
                            self.lblAssetPrice.text = responseData.data.totalAssetValue.replacingOccurrences(of: "-", with: "-$")
                        }else{
                            self.lblAssetPrice.text = "$"+"\(responseData.data.totalAssetValue)"
                        }
                    }
                    
                    if let totalLiabilityValue = responseData.data.totalLiabilityValue.first {
                        if totalLiabilityValue == "-"{
                            self.lblLiabPrice.text = responseData.data.totalLiabilityValue.replacingOccurrences(of: "-", with: "-$")
                        }else{
                            self.lblLiabPrice.text = "$"+"\(responseData.data.totalLiabilityValue)"
                        }
                    }
                    
                    if let totalIncomeValue = responseData.data.totalIncomeValue.first {
                        if totalIncomeValue == "-"{
                            self.lblIncomePrice.text = responseData.data.totalIncomeValue.replacingOccurrences(of: "-", with: "-$")
                        }else{
                            self.lblIncomePrice.text = "$"+"\(responseData.data.totalIncomeValue)"
                        }
                    }
                    
                    if let totalExpenseValue = responseData.data.totalExpenseValue.first {
                        if totalExpenseValue == "-"{
                            self.lblExpencePrice.text = responseData.data.totalExpenseValue.replacingOccurrences(of: "-", with: "-$")
                        }else{
                            self.lblExpencePrice.text = "$"+"\(responseData.data.totalExpenseValue)"
                        }
                    }
                    
                    if responseData.data.totalIncomeValue == "0.00" && responseData.data.totalExpenseValue == "0.00" {
                        self.imageExpense.isHidden = true
                        self.imageIncome.isHidden = true
                    }else{
                        if responseData.data.downArrow == "Expense"{
                            self.imageExpense.image =  UIImage(named: "ArrowDown")
                        }else{
                            self.imageExpense.image =  UIImage(named: "ArrowUp")
                        }
                        
                        if responseData.data.downArrow == "Income"{
                            self.imageIncome.image =  UIImage(named: "ArrowDown")
                        }else{
                            self.imageIncome.image =  UIImage(named: "ArrowUp")
                        }
                    }
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func getProfileServiceCall() {
        self.showLoader()
        let parameter:[String:Any] = [
            "":""
        ]
        
        let loginService = LoginService()
        loginService.getUserDetail(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    if responseData.data.gender == "female"{
                        self.imageUser.sd_setImage(with: URL(string: responseData.data.userProfilePic), placeholderImage: UIImage(named: "FemaleProfileAvatar"))
                    }else{
                        self.imageUser.sd_setImage(with: URL(string: responseData.data.userProfilePic), placeholderImage: UIImage(named: "profileAvatar"))
                    }
                    
                    self.lblUserName.text = responseData.data.fullName
                    
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
}


