//
//  EditProfileVC.swift
//  Render
//
//  Created by Anis Agwan on 24/12/21.
//

import UIKit

class EditProfileVC: BaseVC {
    
    //Variables
    var gender = ""
    var firstName = ""
    var lastName = ""
    var email = ""
    var haveVehicle = ""
    var haveProperty = ""
    var accountTypes: [String] = ["Married", "Unmarried"]
    var selectedType: String = ""
    var picker: UIPickerView!
    var userDetails : UserDetailsModel?
    
    //IBOutlets
    @IBOutlet weak var firstNameTxtFld: UITextField!
    @IBOutlet weak var lastNametxtFld: UITextField!
    @IBOutlet weak var emailAddresstxtFld: UITextField!
    @IBOutlet weak var occupationTxtFld: UITextField!
    @IBOutlet weak var employerTxtFld: UITextField!
    @IBOutlet weak var address1TxtFld: UITextField!
    @IBOutlet weak var address2txtFld: UITextField!
    @IBOutlet weak var cityTxtFld: UITextField!
    @IBOutlet weak var stateTxtFld: UITextField!
    @IBOutlet weak var zipCodeTxtFld: UITextField!
    @IBOutlet weak var maritalTxtFld: UITextField!
    @IBOutlet weak var childrenTxtFld: UITextField!
    @IBOutlet weak var citizenshipTxtFld: UITextField!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var noPropertyBtn: UIButton!
    @IBOutlet weak var havePropertyBtn: UIButton!
    @IBOutlet weak var noVehicleBtn: UIButton!
    @IBOutlet weak var haveVehicleBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    func initialSetup() {
        print("FIRST NAME \(firstName), EMAIL: \(email), \(gender)")
        print("userDetailsModel",userDetails?.data)
        self.firstNameTxtFld.text = userDetails?.data.firstName//firstName
        self.lastNametxtFld.text = userDetails?.data.lastName//lastName
        self.emailAddresstxtFld.text = userDetails?.data.userEmail//email
        self.occupationTxtFld.text = userDetails?.data.occupation
        self.employerTxtFld.text = userDetails?.data.employer
        self.address1TxtFld.text = userDetails?.data.addressLine1
        self.address2txtFld.text = userDetails?.data.addressLine2
        self.cityTxtFld.text = userDetails?.data.city
        self.stateTxtFld.text = userDetails?.data.state
        self.zipCodeTxtFld.text = userDetails?.data.zipCode
        self.maritalTxtFld.text = userDetails?.data.maritalStatus
        self.childrenTxtFld.text = userDetails?.data.children
        self.citizenshipTxtFld.text = userDetails?.data.citizenship
        
        
        if userDetails?.data.gender == "male" {
            self.maleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
            self.maleBtn.titleLabel?.textColor = UIColor.white
            
            self.femaleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
            self.femaleBtn.titleLabel?.textColor = UIColor.lightGray
        } else {
            self.femaleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
            self.femaleBtn.titleLabel?.textColor = UIColor.white
            
            self.maleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
            self.maleBtn.titleLabel?.textColor = UIColor.lightGray
        }
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.firstNameTxtFld.text = firstNameTxtFld.text?.trim()
        self.lastNametxtFld.text = lastNametxtFld.text?.trim()
        self.occupationTxtFld.text = self.occupationTxtFld.text?.trim()
        self.employerTxtFld.text = self.employerTxtFld.text?.trim()
        self.citizenshipTxtFld.text = self.citizenshipTxtFld.text?.trim()
        self.emailAddresstxtFld.text = self.emailAddresstxtFld.text?.trim()
        self.address1TxtFld.text = self.address1TxtFld.text?.trim()
        self.address2txtFld.text = self.address2txtFld.text?.trim()
        self.cityTxtFld.text = self.cityTxtFld.text?.trim()
        self.stateTxtFld.text = self.stateTxtFld.text?.trim()
        self.zipCodeTxtFld.text = self.zipCodeTxtFld.text?.trim()
        self.maritalTxtFld.text = self.maritalTxtFld.text?.trim()
        self.childrenTxtFld.text = self.childrenTxtFld.text?.trim()
        
        
        if (firstNameTxtFld.text?.isEmpty)! {
            message = "Please enter first name."
            isFound = false
        }else if (lastNametxtFld.text?.isEmpty)! {
            message = "Please enter last name"
            isFound = false
        }else if (emailAddresstxtFld.text?.isEmpty)! {
            message = "Please enter email address"
            isFound = false
        } else if (occupationTxtFld.text?.isEmpty)! {
            message = "Please enter occupation name"
            isFound = false
        }else if (employerTxtFld.text?.isEmpty)! {
            message = "Please enter employer name"
            isFound = false
        }else if (citizenshipTxtFld.text?.isEmpty)! {
            message = "Please enter citizenship"
            isFound = false
        } else if (address1TxtFld.text?.isEmpty)! {
            message = "Please enter address1"
            isFound = false
        }else if (address2txtFld.text?.isEmpty)! {
            message = "Please enter address2"
            isFound = false
        }else if (cityTxtFld.text?.isEmpty)! {
            message = "Please enter city"
            isFound = false
        }else if (stateTxtFld.text?.isEmpty)! {
            message = "Please enter state"
            isFound = false
        }else if (zipCodeTxtFld.text?.isEmpty)! {
            message = "Please enter zip code"
            isFound = false
        }else if (maritalTxtFld.text?.isEmpty)! {
            message = "Please enter marital status"
            isFound = false
        }else if (childrenTxtFld.text?.isEmpty)! {
            message = "Please enter children"
            isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        
        if isValidFields(){
            self.updateProfileServiceCall()
            //self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func actionMale(_ sender: Any) {
        self.gender = "male"
        self.maleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        self.maleBtn.titleLabel?.textColor = UIColor.white
        
        self.femaleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        self.femaleBtn.titleLabel?.textColor = UIColor.lightGray
    }
    
    
    @IBAction func actionFemale(_ sender: Any) {
        self.gender = "female"
        self.femaleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        self.femaleBtn.titleLabel?.textColor = UIColor.white
        
        self.maleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        self.maleBtn.titleLabel?.textColor = UIColor.lightGray
    }
    
    @IBAction func actionHaveVehicle(_ sender: Any) {
        self.haveVehicle = "Yes"
        self.haveVehicleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        self.haveVehicleBtn.titleLabel?.textColor = UIColor.white
        
        self.noVehicleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        self.noVehicleBtn.titleLabel?.textColor = UIColor.lightGray
    }
    
    @IBAction func actionHaveNotVehicle(_ sender: Any) {
        self.haveVehicle = "No"
        self.noVehicleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        self.noVehicleBtn.titleLabel?.textColor = UIColor.white
        
        self.haveVehicleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        self.haveVehicleBtn.titleLabel?.textColor = UIColor.lightGray
    }
    
    @IBAction func actionHaveProperty(_ sender: Any) {
        self.haveProperty = "OWN"
        //self.gender = "female"
        self.havePropertyBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        self.havePropertyBtn.titleLabel?.textColor = UIColor.white
        
        self.noPropertyBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        self.noPropertyBtn.titleLabel?.textColor = UIColor.lightGray
        
    }
    
    @IBAction func actionHaveNotProperty(_ sender: Any) {
        self.haveProperty = "Rent"
        self.noPropertyBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        self.noPropertyBtn.titleLabel?.textColor = UIColor.white
        
        self.havePropertyBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        self.havePropertyBtn.titleLabel?.textColor = UIColor.lightGray
    }
}

extension EditProfileVC {
    // MARK: - Service Calls
    func updateProfileServiceCall() {
        self.showLoader()
        let parameter:[String:Any] = [
            
            "update_profile":1,
            "first_name":self.firstNameTxtFld.text ?? "",
            "last_name":self.lastNametxtFld.text ?? "",
            "gender":self.gender,
            "occupation":self.occupationTxtFld.text ?? "",
            "employer":self.employerTxtFld.text ?? "",
            "citizenship":self.citizenshipTxtFld.text ?? "",
            "email":self.emailAddresstxtFld.text ?? "",
            "address_line1":self.address1TxtFld.text ?? "",
            "address_line2":self.address2txtFld.text ?? "",
            "city":self.cityTxtFld.text ?? "",
            "state":self.stateTxtFld.text ?? "",
            "zipcode":self.zipCodeTxtFld.text ?? "",
            "marital_status":self.maritalTxtFld.text ?? "",
            "children":self.childrenTxtFld.text ?? "",
            "ownhome_or_rent":self.haveProperty,
            "owncar_or_not":self.haveVehicle
        ]
        let loginService = LoginService()
        loginService.updateProfile(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    print("SUCCESSFULLY CHANGED THE INFO")
                    self.dismiss(animated: true, completion: nil)
                    //self.navigationController?.pushViewController(objVC, animated: true)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                // self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                let objVC : VerifyOTPVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: VerifyOTPVC.nameOfClass)
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            self.removLoader()
        }
    }
    
    
}


//MARK: - UIPicker Delegates
extension EditProfileVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.text = accountTypes[row]
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedType = accountTypes[row]
    }
}


extension EditProfileVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // Allow to remove character (Backspace)
        if string == "" {
            return true
        }
        
        // Block multiple dot
        if (textField.text?.contains("."))! && string == "." {
            return false
        }
        
        
        if (textField.text?.contains("."))! {
            let limitDecimalPlace = 2
            let decimalPlace = textField.text?.components(separatedBy: ".").last
            if (decimalPlace?.count)! < limitDecimalPlace {
                return true
            }
            else {
                return false
            }
        }
        
        if textField == maritalTxtFld {
            let maxLength = 10
            let currentString: NSString = maritalTxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }
    
    func pickUP(_ textField: UITextField) {
        self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.selectRow(0, inComponent: 0, animated: true)
        self.selectedType = accountTypes[0]
        //        self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
        //        self.selectedCategory = self.mainCategoryModel?.data[0].categoryName ?? ""
        maritalTxtFld.inputView = self.picker
        //
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        //
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        maritalTxtFld.inputAccessoryView = toolBar
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUP(maritalTxtFld)
    }
    
    @objc func selectClick() {
        
        self.maritalTxtFld.text = self.selectedType
        maritalTxtFld.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        maritalTxtFld.resignFirstResponder()
    }
}
