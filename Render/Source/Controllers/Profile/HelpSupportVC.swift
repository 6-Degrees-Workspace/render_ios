//
//  HelpSupportVC.swift
//  Render
//
//  Created by Anis Agwan on 23/12/21.
//

import UIKit

class HelpSupportVC: BaseVC {
 
    @IBOutlet weak var helpTableView: UITableView!
    
    
    var getAllStaticPage : HelpSupportModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllStaticPagesServiceCall()
    }
    
}


extension HelpSupportVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getAllStaticPage?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = helpTableView.dequeueReusableCell(withIdentifier: "helpCell", for: indexPath) as! HelpSupportTableCell
        cell.helpLabel.text = self.getAllStaticPage?.data?[indexPath.row].pagesTitle
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let obj = self.getAllStaticPage?.data?[indexPath.row]
        let objVC: StaticPageVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: StaticPageVC.nameOfClass)
        objVC.strTtitle = obj?.pagesTitle
        objVC.arrFAQs.append(GetAllFAQs(faqId: "", faqTitle: "", faqDesc: obj?.pagesDesc ?? ""))
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}

extension HelpSupportVC{

    func getAllStaticPagesServiceCall() {
        self.showLoader()
        let helpSupportService = HelpSupportService()
        helpSupportService.getAllStaticPages(){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.getAllStaticPage = responseData
                    self.getAllStaticPage?.data?.append(GetAllStaticPage(pagesId: "", pagesTitle: "FAQ's", pagesCode: "", pagesDesc: ""))
                    self.helpTableView.reloadData()
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}
