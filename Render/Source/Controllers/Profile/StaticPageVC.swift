//
//  StaticPageVC.swift
//  Render
//
//  Created by Apple on 28/04/22.
//

import UIKit

class StaticPageVC: BaseVC {
    
    @IBOutlet weak var faqTableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!

    var strTtitle: String?
    var arrFAQs = [GetAllFAQs]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.text = strTtitle
        if self.strTtitle == "FAQ's"{
            self.getAllFaqsServiceCall()
        }
    }

}


extension StaticPageVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFAQs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = faqTableView.dequeueReusableCell(withIdentifier: "FAQsCell", for: indexPath) as? FAQsCell
        cell?.lblFAQsTitle.text = self.arrFAQs[indexPath.row].faqTitle
        cell?.lblFAQsDes.attributedText = self.arrFAQs[indexPath.row].faqDesc.convertHtmlToAttributedStringWithCSS(font: UIFont(name: "FilsonPro-Regular", size: 15), csscolor: "gray", lineheight: 4, csstextalign: "left")
        return cell ?? UITableViewCell()
    }
}

extension StaticPageVC{

    func getAllFaqsServiceCall() {
        self.showLoader()
        let helpSupportService = HelpSupportService()
        helpSupportService.getAllFaqs(){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.arrFAQs = responseData.data ?? []
                    self.faqTableView.reloadData()
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}

extension String {
    private var convertHtmlToNSAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }

    public func convertHtmlToAttributedStringWithCSS(font: UIFont?, csscolor: String, lineheight: Int, csstextalign: String) -> NSAttributedString? {
        guard let font = font else {
            return convertHtmlToNSAttributedString
        }
        let modifiedString = "<style>body{font-family: '\(font.fontName)'; font-size:\(font.pointSize)px; color: \(csscolor); line-height: \(lineheight)px; text-align: \(csstextalign); }</style>\(self)"
        guard let data = modifiedString.data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error)
            return nil
        }
    }
}
