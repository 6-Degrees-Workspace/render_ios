import UIKit

class VerifyEmailOTPVC: BaseVC {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var otp1TextField: UITextField!
    @IBOutlet weak var otp2TextField: UITextField!
    @IBOutlet weak var otp3TextField: UITextField!
    @IBOutlet weak var otp4TextField: UITextField!
    @IBOutlet weak var requestAgainButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    var otp = ""
    var email = ""
    var passScreen = ""
    var counter = 60
    var timer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialLoad()
        // Do any additional setup after loading the view.
    }
    
    func initialLoad() {
        self.lblTitle.text = "We sent a code to \(email). Enter there code to verify your email"
        //self.lblTitle.text = "We sent a code to ******\(phone.suffix(3)). Enter the code to verify the number"//"Code has been sent to *******\(phone.suffix(3))"
        self.lblTimer.text = "00:\(counter)"
        self.hideKeyboardWhenTappedAround()
        self.requestAgainButton.isEnabled = false
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        
    }
    
    @objc func updateCounter() {
        //example functionality
        if counter > 0 {
            counter -= 1
            let digit = self.timeFormatted(counter)
            //print("digit",digit)
            self.lblTimer.text = "00:\(digit)"
        }else{
            self.timer?.invalidate()
            self.requestAgainButton.isEnabled = true
            self.lblTimer.text = "00:00"
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        //  let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d", seconds)
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        if otp1TextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" && otp2TextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" && otp3TextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" && otp4TextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.emailVerificationServiceCall()
        }else{
            self.showAlert(message: "Enter 4 digit OTP.")
        }
    }
    
    @IBAction func actionreSendOTP(_ sender: UIButton){
        
        self.resendOTPForEmailServiceCall()
    }
    
    @IBAction func actionNext(_ sender: UIButton){
        //Check mobile number for user send Dashboard or sign up VC
        if otp1TextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" && otp2TextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" && otp3TextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" && otp4TextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.emailVerificationServiceCall()
        }else{
            self.showAlert(message: "Enter 4 digit OTP.")
        }
    }
}


extension VerifyEmailOTPVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print(textField.text)
        if otp1TextField.text != "" && otp2TextField.text != "" && otp3TextField.text != "" && otp4TextField.text != "" {
            self.nextButton.isEnabled = true
            self.nextButton.backgroundColor = UIColor(named: "buttonColor")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == otp1TextField || textField == otp2TextField || textField == otp3TextField || textField == otp4TextField  {
            var shouldProcess = false
            var shouldMoveToNextField = false
            var shouldMoveToPrevField = false
            let stringLength =  string.count
            if stringLength == 0 { //backspace
                shouldProcess = true //Process if the backspace character was pressed
            }
            else {
                if(textField.text?.count == 0) {
                    shouldProcess = true //Process if there is only 1 character right now
                }
            }
            //here we deal with the UITextField on our own
            if shouldProcess {
                //grab a mutable copy of what's currently in the UITextField
                var mstring = textField.text
                if(mstring?.count == 0){
                    //nothing in the field yet so append the replacement string
                    mstring?.append(string)
                    shouldMoveToNextField = true
                    shouldMoveToPrevField = false
                }
                else{
                    mstring = ""
                    shouldMoveToNextField = false
                    shouldMoveToPrevField = true
                }
                
                //set the text now
                textField.text = mstring
                
                if (shouldMoveToNextField) {
                    //
                    //MOVE TO NEXT INPUT FIELD HERE
                    //
                    switch textField {
                    case otp1TextField:
                        otp2TextField.becomeFirstResponder()
                    case otp2TextField:
                        otp3TextField.becomeFirstResponder()
                    case otp3TextField:
                        otp4TextField.becomeFirstResponder()
                    case otp4TextField:
                        textField.resignFirstResponder()
                    default:
                        textField.resignFirstResponder()
                    }
                }
                if (shouldMoveToPrevField) {
                    //
                    //MOVE TO PREVIOUS INPUT FIELD HERE
                    //
                    switch textField {
                    case otp1TextField:
                        otp1TextField.becomeFirstResponder()
                    case otp2TextField:
                        otp1TextField.becomeFirstResponder()
                    case otp3TextField:
                        otp2TextField.becomeFirstResponder()
                    case otp4TextField:
                        otp3TextField.becomeFirstResponder()
                    default:
                        textField.resignFirstResponder()
                    }
                }
            }
            //always return no since we are manually changing the text field
            return false;
        }
        return true
    }
}

extension VerifyEmailOTPVC {
    
    
    // MARK: - Service Calls
    func emailVerificationServiceCall() {
        
        let otpString = "\(otp1TextField.text ?? "")\(otp2TextField.text ?? "")\(otp3TextField.text ?? "" )\(otp4TextField.text ?? "")"
        
        self.showLoader()
        
        let parameter:[String:Any] = [
            "verification_code":otpString
        ]
        
        let loginService = LoginService()
        loginService.emailVerification(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    //Helper.sharedInstance.showToast(isError: true, title: responseData.message)
                    self.navigationController?.dismissViewController()
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func resendOTPForEmailServiceCall(){
        self.showLoader()
        
        let loginService = LoginService()
        let parameter:[String:Any] = [
            "email":email
        ]
        loginService.shootEmailforVerification(parameters: parameter){ (result,code) in
            switch result{
                
            case .success(let responseData):
                if code == 200 {
                    self.counter = 60
                    self.initialLoad()
                    print("responseData",responseData)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}
