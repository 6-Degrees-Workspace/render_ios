import UIKit

class UpcomingDuesVC: BaseVC {

    @IBOutlet weak var dueTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}


extension UpcomingDuesVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Credit card"
        } else if section == 1 {
            return "Loan"
        } else if section == 2 {
            return "Insurance"
        } else {
            return "Header"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = dueTable.dequeueReusableCell(withIdentifier: "dueCell", for: indexPath) as! UpcomingDueTableCell
            
            cell.cellName.text = "Credit Card"
            
            return cell
        } else if indexPath.section == 1 {
            let cell = dueTable.dequeueReusableCell(withIdentifier: "dueCell", for: indexPath) as! UpcomingDueTableCell
            
            cell.cellName.text = "Home Loan"
            
            return cell
        } else {
            let cell = dueTable.dequeueReusableCell(withIdentifier: "dueCell", for: indexPath) as! UpcomingDueTableCell
            
            cell.cellName.text = "Car Loan"
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
}
