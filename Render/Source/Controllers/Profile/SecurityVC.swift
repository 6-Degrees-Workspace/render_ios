//
//  SecurityVC.swift
//  Render
//
//  Created by Anis Agwan on 21/12/21.
//

import UIKit

class SecurityVC: BaseVC {
    
    // Variables
    var viewStatus = "Old PIN"
    var oldPin : [String] = []
    var gettingNewPin: [String]?
    var newPin: [String] = []
    var confirmPin: [String] = []
    //IBOutlets
    var headerTitle: String = "Enter Security PIN"
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    // Black dot view
    @IBOutlet weak var pinDot1: UIView!
    @IBOutlet weak var pinDot2: UIView!
    @IBOutlet weak var pinDot3: UIView!
    @IBOutlet weak var pinDot4: UIView!
    // Text Pin
    @IBOutlet weak var pinTxt1: UITextField!
    @IBOutlet weak var pinTxt2: UITextField!
    @IBOutlet weak var pinTxt3: UITextField!
    @IBOutlet weak var pinTxt4: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinTxt1.becomeFirstResponder()
        if self.viewStatus == "Old PIN" {
            self.titleLabel.text = headerTitle
            //self.resetButton.isHidden = false
        } else if self.viewStatus == "New PIN" {
            self.titleLabel.text = headerTitle
            //self.resetButton.isHidden = true
        } else {
            if let pin = gettingNewPin {
                newPin = pin
                print("NEW PIN \(newPin)")
            }
            self.titleLabel.text = headerTitle
           // self.resetButton.isHidden = true
        }
    }
    
    @IBAction func resetBtnPressed(_ sender: Any) {
        let obj: SecurityVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: SecurityVC.nameOfClass)
        obj.viewStatus = "New Pin"
        obj.headerTitle = "Enter New Security PIN"
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func matchingPins(pin: [String], confirm: [String]) {
        if pin != confirm {
            print("PINS dont match")
            
            let objVC: SecurityVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: SecurityVC.nameOfClass)
            objVC.gettingNewPin = newPin
            objVC.viewStatus = "Confirm PIN"
            objVC.headerTitle = "Confirm Security PIN"
            self.navigationController?.pushViewController(objVC, animated: true)
            
            pinTxt1.becomeFirstResponder()
            self.showAlert(message: "PIN do not match")
            //donotSharePINLabel.text = "Pins do not match"
            //donotSharePINLabel.textColor = .red
        } else {
            // CALL THE API HERE
            print("PINS match")
            self.createPinServiceCall(pin: confirmPin)
            //self.ConfirmPinServiceCall(pin: confirm)
        }
    }
    
}


extension SecurityVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                print("Backspace was pressed")
            }
        }
        
        
        //    if textField == pinTxt1 || textField == pinTxt2 || textField == pinTxt3 || textField == pinTxt4  {
        var shouldProcess = false
        var shouldMoveToNextField = false
        var shouldMoveToPrevField = false
        let stringLength =  string.count
        if stringLength == 0 { //backspace
            shouldProcess = true //Process if the backspace character was pressed
        }
        else {
            if(textField.text?.count == 0) {
                shouldProcess = true //Process if there is only 1 character right now
            }
        }
        //here we deal with the UITextField on our own
        if shouldProcess {
            //grab a mutable copy of what's currently in the UITextField
            var mstring = textField.text
            if(mstring?.count == 0){
                //nothing in the field yet so append the replacement string
                mstring?.append(string)
                shouldMoveToNextField = true
                shouldMoveToPrevField = false
            }
            else{
                mstring = ""
                shouldMoveToNextField = false
                shouldMoveToPrevField = true
            }
            
            //set the text now
            textField.text = mstring
            
            if (shouldMoveToNextField) {
                //
                //MOVE TO NEXT INPUT FIELD HERE
                //
                switch textField {
                case pinTxt1:
                    // Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.showPin), userInfo: nil, repeats: false)
                    pinDot1.backgroundColor = .clear
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        textField.textColor = .clear
                        self.pinDot1.backgroundColor = .black
                    }
                    
                    if viewStatus == "Old PIN" {
                        if let pin = textField.text {
                            oldPin.insert(pin, at: 0)
                            print("PURANA : \(oldPin)")
                        }
                    } else if viewStatus == "New PIN" {
                        if let pin = textField.text {
                            newPin.insert(pin, at: 0)
                            print("PURANA : \(newPin)")
                        }
                    } else if viewStatus == "Confirm PIN" {
                        if let pin = textField.text {
                            confirmPin.insert(pin, at: 0)
                            print("PURANA : \(confirmPin)")
                        }
                    }
                    
                    pinTxt2.becomeFirstResponder()
                case pinTxt2:
                    pinDot2.backgroundColor = .clear
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        textField.textColor = .clear
                        self.pinDot2.backgroundColor = .black
                    }
                    if viewStatus == "Old PIN" {
                        if let pin = textField.text {
                            oldPin.insert(pin, at: 1)
                            print("PURANA : \(oldPin)")
                            
                        }
                    } else if viewStatus == "New PIN" {
                        if let pin = textField.text {
                            newPin.insert(pin, at: 1)
                            print("PURANA : \(newPin)")
                        }
                    } else if viewStatus == "Confirm PIN" {
                        if let pin = textField.text {
                            confirmPin.insert(pin, at: 1)
                            print("PURANA : \(confirmPin)")
                        }
                    }
                    pinTxt3.becomeFirstResponder()
                case pinTxt3:
                    pinDot3.backgroundColor = .clear
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        textField.textColor = .clear
                        self.pinDot3.backgroundColor = .black
                    }
                    
                    if viewStatus == "Old PIN" {
                        if let pin = textField.text {
                            oldPin.insert(pin, at: 2)
                            print("PURANA : \(oldPin)")
                        }
                    } else if viewStatus == "New PIN" {
                        if let pin = textField.text {
                            newPin.insert(pin, at: 2)
                            print("PURANA : \(newPin)")
                        }
                    } else if viewStatus == "Confirm PIN" {
                        if let pin = textField.text {
                            confirmPin.insert(pin, at: 2)
                            print("PURANA : \(confirmPin)")
                        }
                    }
                    
                    pinTxt4.becomeFirstResponder()
                case pinTxt4:
                    pinDot4.backgroundColor = .clear
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        textField.textColor = .clear
                        self.pinDot4.backgroundColor = .black
                    }
                    
                    if viewStatus == "Old PIN" {
                        if let pin = textField.text {
                            oldPin.insert(pin, at: 3)
                            print("PURANA : \(oldPin)")
                        }
                    } else if viewStatus == "New PIN" {
                        if let pin = textField.text {
                            newPin.insert(pin, at: 3)
                            print("PURANA : \(newPin)")
                        }
                    } else if viewStatus == "Confirm PIN" {
                        if let pin = textField.text {
                            confirmPin.insert(pin, at: 3)
                            print("PURANA : \(confirmPin)")
                        }
                    }
                    
                    textField.resignFirstResponder()
                    
                    if oldPin.count == 4 {
                        self.ConfirmPinServiceCall(pin: oldPin)
                    } else if confirmPin.count == 4 {
                        self.matchingPins(pin: newPin, confirm: confirmPin)
                        
                    } else if newPin.count == 4 {
                        let objVC: SecurityVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: SecurityVC.nameOfClass)
                        objVC.gettingNewPin = newPin
                        objVC.viewStatus = "Confirm PIN"
                        objVC.headerTitle = "Confirm Security PIN"
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    //                        else if newPin.count == 4 {
                    //                            let objVC: SecurityVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: SecurityVC.nameOfClass)
                    //                            objVC.gettingNewPin = newPin
                    //                            objVC.viewStatus = "Confirm PIN"
                    //                            objVC.headerTitle = "Confirm Security PIN"
                    //                            self.navigationController?.pushViewController(objVC, animated: true)
                    //                        } else if confirmPin.count == 4 {
                    //                            self.matchingPins(pin: newPin, confirm: confirmPin)
                    //                        }
                    
                default:
                    textField.resignFirstResponder()
                }
            }
            
            if (shouldMoveToPrevField) {
                //
                //MOVE TO PREVIOUS INPUT FIELD HERE
                //
                if viewStatus == "Old PIN" {
                    switch textField {
                    case pinTxt1:
                        pinDot1.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt1.textColor = .label
                        //oldPin.remove(at: 0)
                        if !(oldPin[0].isEmpty) {
                            oldPin.remove(at: 0)
                            print(oldPin)
                        }
                        pinTxt1.becomeFirstResponder()
                    case pinTxt2:
                        pinDot2.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt2.textColor = .label
                        if !(oldPin[1].isEmpty) {
                            oldPin.remove(at: 1)
                            print(oldPin)
                        }
                        pinTxt1.becomeFirstResponder()
                    case pinTxt3:
                        pinDot3.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt3.textColor = .label
                        if !(oldPin[2].isEmpty) {
                            // DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.oldPin.remove(at: 2)
                            //}
                            print(oldPin)
                        }
                        pinTxt2.becomeFirstResponder()
                    case pinTxt4:
                        pinDot4.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt4.textColor = .label
                        if !(oldPin[3].isEmpty) {
                            oldPin.remove(at: 3)
                            print(oldPin)
                        }
                        pinTxt3.becomeFirstResponder()
                    default:
                        textField.resignFirstResponder()
                    }
                    
                } else if viewStatus == "New PIN" {
                    switch textField {
                    case pinTxt1:
                        pinDot1.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt1.textColor = .label
                        self.newPin.removeLast()
                        //oldPin.remove(at: 0)
                        // if !(newPin[0].isEmpty) {
                        //   newPin.remove(at: 0)
                        print(newPin)
                        //}
                        pinTxt1.becomeFirstResponder()
                    case pinTxt2:
                        pinDot2.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt2.textColor = .label
                        self.newPin.removeLast()
                        // if !(newPin[1].isEmpty) {
                        //   newPin.remove(at: 1)
                        print(newPin)
                        //}
                        pinTxt1.becomeFirstResponder()
                    case pinTxt3:
                        pinDot3.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt3.textColor = .label
                        self.newPin.removeLast()
                        // if !(newPin[2].isEmpty) {
                        // DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        //      self.newPin.remove(at: 2)
                        // }
                        print(newPin)
                        //}
                        pinTxt2.becomeFirstResponder()
                    case pinTxt4:
                        pinDot4.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt4.textColor = .label
                        // if !(newPin[3].isEmpty) {
                        //   newPin.remove(at: 3)
                        self.newPin.removeLast()
                        print(newPin)
                        //}
                        pinTxt3.becomeFirstResponder()
                    default:
                        textField.resignFirstResponder()
                    }
                    
                } else if viewStatus == "Confirm PIN" {
                    switch textField {
                    case pinTxt1:
                        pinDot1.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt1.textColor = .label
                        //oldPin.remove(at: 0)
                        //if !(confirmPin[0].isEmpty) {
                        //  confirmPin.remove(at: 0)
                        self.confirmPin.removeLast()
                        print(confirmPin)
                        //}
                        pinTxt1.becomeFirstResponder()
                    case pinTxt2:
                        pinDot2.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt2.textColor = .label
                        // if !(confirmPin[1].isEmpty) {
                        // confirmPin.remove(at: 1)
                        self.confirmPin.removeLast()
                        print(confirmPin)
                        //}
                        pinTxt1.becomeFirstResponder()
                    case pinTxt3:
                        pinDot3.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt3.textColor = .label
                        // if !(confirmPin[2].isEmpty) {
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.confirmPin.removeLast()
                        //}
                        print(confirmPin)
                        // }
                        pinTxt2.becomeFirstResponder()
                    case pinTxt4:
                        pinDot4.backgroundColor = UIColor(named: "emptyPinColor")
                        pinTxt4.textColor = .label
                        // if !(confirmPin[3].isEmpty) {
                        //   confirmPin.remove(at: 3)
                        self.confirmPin.removeLast()
                        print(confirmPin)
                        //}
                        pinTxt3.becomeFirstResponder()
                    default:
                        textField.resignFirstResponder()
                    }
                }
            }
        }
        //always return no since we are manually changing the text field
        
        if string.isEmpty {
            
            if viewStatus == "Old PIN" {
                pinTxt1.becomeFirstResponder()
            } else if viewStatus == "New PIN" {
                pinTxt1.becomeFirstResponder()
            } else if viewStatus == "Confirm PIN" {
                pinTxt1.becomeFirstResponder()
            }
        }
        
        return false;
        //  }
        //   return true
    }
    
    @objc func showPin() {
        self.pinTxt1.isHidden = true
    }
}


extension SecurityVC {
    
    //MARK: - Service Calls
    func ConfirmPinServiceCall(pin: [String]) {
        
        //let securityCodeArray = pin.map { String($0) }
        let pinCode = pin.joined(separator: "")
        print("pinCode",pinCode)
        
        self.showLoader()
        let parameter:[String:Any] = [
            "security_code": pinCode
        ]
        let signupService = SignupService()
        signupService.verifyPin(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    //ApplicationPreference.saveUsercompleteProfile(userInfo: true)
                    if responseData.status == 200 {
                        let objVC: SecurityVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: SecurityVC.nameOfClass)
                        objVC.viewStatus = "New PIN"
                        objVC.headerTitle = "Enter New Security PIN"
                        self.navigationController?.pushViewController(objVC, animated: true)
                        print("CORRECT PIN")
                    }else{
                        
                        self.oldPin.removeAll()
                        
                        self.pinTxt1.text = ""
                        self.pinTxt2.text = ""
                        self.pinTxt3.text = ""
                        self.pinTxt4.text = ""
                        
                        self.pinTxt1.textColor = .clear
                        self.pinTxt2.textColor = .clear
                        self.pinTxt3.textColor = .clear
                        self.pinTxt4.textColor = .clear
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.pinDot1.backgroundColor = UIColor(named: "emptyPinColor")
                            self.pinDot2.backgroundColor = UIColor(named: "emptyPinColor")
                            self.pinDot3.backgroundColor = UIColor(named: "emptyPinColor")
                            self.pinDot4.backgroundColor = UIColor(named: "emptyPinColor")
                        }
                        self.pinTxt1.becomeFirstResponder()
                        self.showAlert(message: responseData.message)
                    }
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func createPinServiceCall(pin: [String]) {
        
        //let securityCodeArray = pin.map { String($0) }
        let pinCode = pin.joined(separator: "")
        print("pinCode",pinCode)
        
        self.showLoader()
        let parameter:[String:Any] = [
            "security_code": pinCode
        ]
        let signupService = SignupService()
        signupService.createPin(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200 {
                        let objVC: DashboardTabBarController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.dashboard, viewControllerName: DashboardTabBarController.nameOfClass)
                        objVC.selectedIndex = 3
                        self.navigationController?.pushViewController(objVC, animated: false)
                        NotificationCenter.default.post(name: Notification.Name("Profile"), object: nil, userInfo: nil)
                    }
                    self.showAlert(message: responseData.message)
                    
                    /*
                     //                    let objVC : SecurityVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: SecurityVC.nameOfClass)
                     //                    //objVC.gettingNewPin = pin
                     //                    objVC.viewStatus = "Confirm PIN"
                     //                    objVC.headerTitle = "Confirm Security PIN"
                     //self.navigationController?.pushViewController(objVC, animated: true)
                     if let vcs = self.navigationController?.viewControllers {
                     let alertController = UIAlertController(title: "Successfully changed the PIN", message: "", preferredStyle: .alert)
                     let okAction = UIAlertAction(title: "OK", style: .default) { alertController in
                     self.navigationController?.popToViewController(vcs[vcs.count - 4], animated: true)
                     }
                     alertController.addAction(okAction)
                     self.present(alertController, animated: true, completion: nil)
                     }*/
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}
