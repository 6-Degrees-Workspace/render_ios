import UIKit

// <!-- SMARTDOWN_IMPORT_LINKKIT -->
import LinkKit
// <!-- SMARTDOWN_IMPORT_LINKKIT -->

protocol LinkOAuthHandling {
    var linkHandler: Handler? { get }
}

class MyProfileVC: BaseVC, LinkOAuthHandling {
    
    
    @IBOutlet weak var btnLogout : UIButton!
    @IBOutlet weak var btnAccountInfo: UIButton!
    @IBOutlet weak var btnSecurity: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var btnverify: UIButton!
    @IBOutlet weak var profileTableView: UITableView!
    
    var linkHandler: Handler?
    let oauthNonce = UUID().uuidString
#warning("Replace YOUR_OAUTH_REDIRECT_URI below with your oauthRedirectUri, which should be a universal link and must be configured in the Plaid developer dashboard")
    let oauthRedirectURI =  URL(string: "YOUR_OAUTH_REDIRECT_URI")    
    var profileTypes: [String] = ["Account Information", "Security", "Help & Support", "Link Bank Account"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Profile"), object: nil)
        self.profileTableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileServiceCall()
    }
    
    @IBAction func actionLogout(_ sender: UIButton){
        print("logout")
        
        let alert = UIAlertController(title: "Render", message: "Are your sure you want to Logout?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {_ in
            
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: {_ in
            self.logoutServiceCall()
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func actionAccountInfo(_ sender: Any) {
        let objVC: AccountInfoVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: AccountInfoVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func actionSecurity(_ sender: Any) {
        let objVC: SecurityVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: SecurityVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func actionUpcomingDues(_ sender: Any) {
        
        let objVC: UpcomingDuesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: UpcomingDuesVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    @IBAction func actionHelpSupport(_ sender: Any) {
        let objVC: HelpSupportVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: HelpSupportVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func actionVerify(_ sender: UIButton){
        self.shootEmailforVerification()
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        //
    }
    
    // MARK: - Service Calls
    func logoutServiceCall() {
        self.showLoader()
        
        let loginService = LoginService()
        loginService.logout(){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    ApplicationPreference.removeToken()
                    ApplicationPreference.removeUsercompleteProfile()
                    
                    let objVC : LoginVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: LoginVC.nameOfClass)
                    self.navigationController?.pushViewController(objVC, animated: false)
                    
                    print("responseData",responseData)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func shootEmailforVerification() {
        self.showLoader()
        
        let loginService = LoginService()
        let parameter:[String:Any] = [
            "email":lblEmail.text ?? ""
        ]
        loginService.shootEmailforVerification(parameters: parameter){ (result,code) in
            switch result{
                
            case .success(let responseData):
                if code == 200 {
                    let objVC: VerifyEmailOTPVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: VerifyEmailOTPVC.nameOfClass)
                    self.navigationController?.pushViewController(objVC, animated: true)
                    print("responseData",responseData)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}



//MARK: - UITableView
extension MyProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = profileTableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! NetWorthTableViewCell
        cell.profileName.text = profileTypes[indexPath.row]
        
        if indexPath.row == 0 {
            cell.profileLogo.image = UIImage(named: "settingsAccount")
        }else if indexPath.row == 1 {
            cell.profileLogo.image = UIImage(named: "settingsSecurity")
            
        }/*else if indexPath.row == 2 {
          cell.profileLogo.image = UIImage(named: "settingsDues")
          
          }*/else if indexPath.row == 2 {
              cell.profileLogo.image = UIImage(named: "settingsHelpDesk")
              
          }else{
              cell.profileLogo.image = UIImage(named: "addbank")
              
          }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let objVC: AccountInfoDetailsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: AccountInfoDetailsVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        }else if indexPath.row == 1 {
            let objVC: SecurityVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: SecurityVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        }/*else if indexPath.row == 2 {
          let objVC: UpcomingDuesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: UpcomingDuesVC.nameOfClass)
          self.navigationController?.pushViewController(objVC, animated: true)
          }*/
        else if indexPath.row == 2{
            let objVC: HelpSupportVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: HelpSupportVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        }else{
            self.createLinkTokenServiceCall()
        }
    }
    
    
    func plaidCall(){
        self.presentPlaidLinkUsingLinkToken()
    }
    
}

extension MyProfileVC {
    
    func getProfileServiceCall() {
        //self.showLoader()
        let parameter:[String:Any] = [
            "":""
        ]
        
        let loginService = LoginService()
        loginService.getUserDetail(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    self.lblName.text = responseData.data.fullName.capitalized
                    self.lblEmail.text = responseData.data.userEmail
                    
                    DispatchQueue.global(qos: .background).async {
                        if responseData.data.gender == "female"{
                            self.imageProfile.sd_setImage(with: URL(string: responseData.data.userProfilePic), placeholderImage: UIImage(named: "FemaleProfileAvatar"))
                        }else{
                            self.imageProfile.sd_setImage(with: URL(string: responseData.data.userProfilePic), placeholderImage: UIImage(named: "profileAvatar"))
                        }
                    }
                    
                    if responseData.data.isVerifyEmail == "0" && responseData.data.userEmail.length > 0{
                        self.btnverify.isHidden = false
                    }else{
                        self.btnverify.isHidden = true
                    }
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    // MARK: - Service Calls
    func createLinkTokenServiceCall() {
        self.showLoader()
        
        let plaidService = PlaidService()
        plaidService.createLinkToken(){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    if responseData.status == 200 {
                        AppDelegate.sharedApp().token = responseData.data.linkToken
                        self.plaidCall()
                    }else{
                        self.showAlert(message: responseData.message)
                    }
                    print("responseData",responseData)
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}
