import UIKit
import Foundation

extension UIButton {
    func centerTextAndImage(spacing: CGFloat) {
           let insetAmount = spacing / 2
           let writingDirection = UIApplication.shared.userInterfaceLayoutDirection
           let factor: CGFloat = writingDirection == .leftToRight ? 1 : -1

           self.imageEdgeInsets = UIEdgeInsets(top: -insetAmount*factor, left: 70, bottom: insetAmount*factor, right: 0)
           self.titleEdgeInsets = UIEdgeInsets(top: insetAmount*factor, left: 0, bottom: -insetAmount*factor, right: 0)
           self.contentEdgeInsets = UIEdgeInsets(top: insetAmount, left: -10, bottom: insetAmount, right:5)
       }
}

class OtherDetailsVC: UIViewController {
    
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtChild: UITextField!
    @IBOutlet weak var btnOwnHouse: UIButton!
    @IBOutlet weak var btnHouseRent: UIButton!
    @IBOutlet weak var btnOwnCar: UIButton!
    @IBOutlet weak var btnRentCar: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var iconLabelFA: UILabel!
    
    var picker: UIPickerView!
    var haveVehicle = ""
    var haveProperty = ""
    var accountTypes: [String] = ["Single", "Married","Widowed","Divorced"] //SINGLE/ MARRIED/ WIDOWED/ DIVORCED
    var selectedType: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("updateOtherInfo"), object: nil)
        
        if #available(iOS 14, *) {
            btnOwnHouse.centerTextAndImage(spacing: 30.0)
            btnHouseRent.centerTextAndImage(spacing: 30.0)
            btnOwnCar.centerTextAndImage(spacing: 30.0)
            btnRentCar.centerTextAndImage(spacing: 30.0)
        } else {
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileServiceCall()
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            //self.bottomConst.constant = -35
        }else{
            // self.bottomConst.constant = 40
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        self.updateOtherInfo()
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtStatus.text = txtStatus.text?.trim()
        self.txtChild.text = self.txtChild.text?.trim()
        
        if (txtStatus.text?.isEmpty)! {
            message = "Please Select Marital Status"
            isFound = false
        } else if (txtChild.text?.isEmpty)! {
            message = "Please Enter Children"
            isFound = false
        } else if (haveProperty == "") {
            message = "Please Select Property"
            isFound = false
        } else if (haveVehicle == "") {
            message = "Please Select Vehicle"
            isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    
    @IBAction func actionProperty(_ sender: UIButton){
        
        if sender.tag == 0 {
            self.haveProperty = "OWN"
            self.btnOwnHouse.backgroundColor = .white
            self.btnOwnHouse.cornerRadius = 8.0
            self.btnOwnHouse.borderWidth = 1.0
            self.btnOwnHouse.borderColor = UIColor(named: "primaryBlue")
            self.btnOwnHouse.masksToBounds = true
            self.btnOwnHouse.shadowColor = UIColor(named: "primaryBlue")!
            self.btnOwnHouse.shadowRadius = 2.0
            self.btnOwnHouse.setImage(UIImage.init(named: "ownActive"), for: .normal)
            self.btnOwnHouse.shadowOffset = CGSize(width: 0.0, height: 0.0)
            
            self.btnHouseRent.shadowColor = UIColor(named: "primaryGrey")!
            self.btnHouseRent.masksToBounds = false
            self.btnHouseRent.backgroundColor = .clear
            self.btnHouseRent.cornerRadius = 8.0
            self.btnHouseRent.borderWidth = 1.0
            self.btnHouseRent.borderColor = UIColor(named: "primaryGrey")
            self.btnHouseRent.setImage(UIImage.init(named: "key_inactive"), for: .normal)
            
        }else{
            self.haveProperty = "Rent"
            self.btnHouseRent.backgroundColor = .white
            self.btnHouseRent.cornerRadius = 8.0
            self.btnHouseRent.borderWidth = 1.0
            self.btnHouseRent.borderColor = UIColor(named: "primaryBlue")
            self.btnHouseRent.masksToBounds = true
            self.btnHouseRent.shadowColor = UIColor(named: "primaryBlue")!
            self.btnHouseRent.shadowRadius = 2.0
            self.btnHouseRent.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.btnHouseRent.setImage(UIImage.init(named: "key_active"), for: .normal)
            
            self.btnOwnHouse.shadowColor = UIColor(named: "primaryGrey")!
            self.btnOwnHouse.masksToBounds = false
            self.btnOwnHouse.backgroundColor = .clear
            self.btnOwnHouse.cornerRadius = 8.0
            self.btnOwnHouse.borderWidth = 1.0
            self.btnOwnHouse.setImage(UIImage.init(named: "home_inacitve"), for: .normal)
            self.btnOwnHouse.borderColor = UIColor(named: "primaryGrey")
            
        }
        
    }
    
    @IBAction func actionVehicle(_ sender: UIButton){
        
        if sender.tag == 2 {
            self.haveVehicle = "YES"
            self.btnOwnCar.backgroundColor = .white
            self.btnOwnCar.cornerRadius = 8.0
            self.btnOwnCar.borderWidth = 1.0
            self.btnOwnCar.borderColor = UIColor(named: "primaryBlue")
            self.btnOwnCar.masksToBounds = true
            self.btnOwnCar.shadowColor = UIColor(named: "primaryBlue")!
            self.btnOwnCar.shadowRadius = 2.0
            self.btnOwnCar.setImage(UIImage.init(named: "havecar_active"), for: .normal)
            self.btnOwnCar.shadowOffset = CGSize(width: 0.0, height: 0.0)
            
            self.btnRentCar.shadowColor = UIColor(named: "primaryGrey")!
            self.btnRentCar.masksToBounds = false
            self.btnRentCar.backgroundColor = .clear
            self.btnRentCar.cornerRadius = 8.0
            self.btnRentCar.borderWidth = 1.0
            self.btnRentCar.borderColor = UIColor(named: "primaryGrey")
            self.btnRentCar.setImage(UIImage.init(named: "dontcar"), for: .normal)
            
            
        }else{
            self.haveVehicle = "NO"
            self.btnRentCar.backgroundColor = .white
            self.btnRentCar.cornerRadius = 8.0
            self.btnRentCar.borderWidth = 1.0
            self.btnRentCar.borderColor = UIColor(named: "primaryBlue")
            self.btnRentCar.masksToBounds = true
            self.btnRentCar.shadowColor = UIColor(named: "primaryBlue")!
            self.btnRentCar.shadowRadius = 2.0
            self.btnRentCar.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.btnRentCar.setImage(UIImage.init(named: "dontcar_Active"), for: .normal)
            
            self.btnOwnCar.shadowColor = UIColor(named: "primaryGrey")!
            self.btnOwnCar.masksToBounds = false
            self.btnOwnCar.backgroundColor = .clear
            self.btnOwnCar.cornerRadius = 8.0
            self.btnOwnCar.borderWidth = 1.0
            self.btnOwnCar.setImage(UIImage.init(named: "havecar_Inactive"), for: .normal)
            self.btnOwnCar.borderColor = UIColor(named: "primaryGrey")
            
        }
        
    }
    
    func isActive() {
        self.bgView.backgroundColor = .white
        self.bgView.cornerRadius = 8.0
        self.bgView.borderWidth = 1.0
        self.bgView.borderColor = UIColor(named: "primaryBlue")
        self.categoryLbl.textColor = UIColor(named: "primaryBlue")
        self.bgView.masksToBounds = true
        self.bgView.shadowColor = UIColor(named: "primaryBlue")!
        self.bgView.shadowRadius = 2.0
        self.bgView.shadowOffset = CGSize(width: 0.0, height: 0.0)
        // self.shadowOpacity = 0.5
        self.bgView.masksToBounds = false
        let ogImg = self.categoryImg.image?.withRenderingMode(.alwaysOriginal)
        self.categoryImg.image = ogImg
        self.iconLabelFA.textColor = UIColor(named: "primaryBlue")
    }
    
    func isInactive() {
        self.bgView.backgroundColor = .clear
        self.bgView.cornerRadius = 8.0
        self.bgView.borderWidth = 1.0
        self.bgView.borderColor = UIColor(named: "primaryGrey")
        self.categoryLbl.textColor = .black
        let tempImg = self.categoryImg.image?.withRenderingMode(.alwaysTemplate)
        self.categoryImg.image = tempImg
        self.categoryImg.tintColor = .black
        self.iconLabelFA.textColor = .black
    }
    
    
    func updateOtherInfo (){
        self.view.endEditing(true)
        if self.isValidFields() {
            updateProfileServiceCall()
        }
    }
    
    func updateProfileServiceCall() {
        
        self.txtStatus.text = txtStatus.text?.trim()
        self.txtChild.text = self.txtChild.text?.trim()
        
        // self.showLoader()
        let parameter:[String:Any] = [
            "update_profile":1,
            "active_tab":"other",
            "marital_status": self.txtStatus.text ?? "",
            "children":self.txtChild.text ?? "",
            "ownhome_or_rent":self.haveProperty,
            "owncar_or_not":self.haveVehicle
            
        ]
        let loginService = LoginService()
        loginService.updateProfile(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                
            }
            self.removLoader()
        }
    }
    
    func getProfileServiceCall() {
        //self.showLoader()
        let parameter:[String:Any] = [
            "":""
        ]
        
        let loginService = LoginService()
        loginService.getUserDetail(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.txtStatus.text = responseData.data.maritalStatus.capitalized
                    self.txtChild.text = responseData.data.children
                    
                    if responseData.data.property == "OWN" {
                        
                        self.haveProperty = "OWN"
                        self.btnOwnHouse.backgroundColor = .white
                        self.btnOwnHouse.cornerRadius = 8.0
                        self.btnOwnHouse.borderWidth = 1.0
                        self.btnOwnHouse.borderColor = UIColor(named: "primaryBlue")
                        self.btnOwnHouse.masksToBounds = true
                        self.btnOwnHouse.shadowColor = UIColor(named: "primaryBlue")!
                        self.btnOwnHouse.shadowRadius = 2.0
                        self.btnOwnHouse.setImage(UIImage.init(named: "ownActive"), for: .normal)
                        self.btnOwnHouse.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        
                        self.btnHouseRent.shadowColor = UIColor(named: "primaryGrey")!
                        self.btnHouseRent.masksToBounds = false
                        self.btnHouseRent.backgroundColor = .clear
                        self.btnHouseRent.cornerRadius = 8.0
                        self.btnHouseRent.borderWidth = 1.0
                        self.btnHouseRent.borderColor = UIColor(named: "primaryGrey")
                        self.btnHouseRent.setImage(UIImage.init(named: "key_inactive"), for: .normal)
                        
                    } else if responseData.data.property == "RENT" {
                        self.haveProperty = "Rent"
                        self.btnHouseRent.backgroundColor = .white
                        self.btnHouseRent.cornerRadius = 8.0
                        self.btnHouseRent.borderWidth = 1.0
                        self.btnHouseRent.borderColor = UIColor(named: "primaryBlue")
                        self.btnHouseRent.masksToBounds = true
                        self.btnHouseRent.shadowColor = UIColor(named: "primaryBlue")!
                        self.btnHouseRent.shadowRadius = 2.0
                        self.btnHouseRent.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        self.btnHouseRent.setImage(UIImage.init(named: "key_active"), for: .normal)
                        
                        self.btnOwnHouse.shadowColor = UIColor(named: "primaryGrey")!
                        self.btnOwnHouse.masksToBounds = false
                        self.btnOwnHouse.backgroundColor = .clear
                        self.btnOwnHouse.cornerRadius = 8.0
                        self.btnOwnHouse.borderWidth = 1.0
                        self.btnOwnHouse.setImage(UIImage.init(named: "home_inacitve"), for: .normal)
                        self.btnOwnHouse.borderColor = UIColor(named: "primaryGrey")
                    }else{
                        
                    }
                    
                    
                    if responseData.data.ownCar == "YES" {
                        self.haveVehicle = "YES"
                        self.btnOwnCar.backgroundColor = .white
                        self.btnOwnCar.cornerRadius = 8.0
                        self.btnOwnCar.borderWidth = 1.0
                        self.btnOwnCar.borderColor = UIColor(named: "primaryBlue")
                        self.btnOwnCar.masksToBounds = true
                        self.btnOwnCar.shadowColor = UIColor(named: "primaryBlue")!
                        self.btnOwnCar.shadowRadius = 2.0
                        self.btnOwnCar.setImage(UIImage.init(named: "havecar_active"), for: .normal)
                        self.btnOwnCar.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        
                        self.btnRentCar.shadowColor = UIColor(named: "primaryGrey")!
                        self.btnRentCar.masksToBounds = false
                        self.btnRentCar.backgroundColor = .clear
                        self.btnRentCar.cornerRadius = 8.0
                        self.btnRentCar.borderWidth = 1.0
                        self.btnRentCar.borderColor = UIColor(named: "primaryGrey")
                        self.btnRentCar.setImage(UIImage.init(named: "dontcar"), for: .normal)
                        
                    } else if responseData.data.ownCar == "NO" {
                        self.haveVehicle = "NO"
                        self.btnRentCar.backgroundColor = .white
                        self.btnRentCar.cornerRadius = 8.0
                        self.btnRentCar.borderWidth = 1.0
                        self.btnRentCar.borderColor = UIColor(named: "primaryBlue")
                        self.btnRentCar.masksToBounds = true
                        self.btnRentCar.shadowColor = UIColor(named: "primaryBlue")!
                        self.btnRentCar.shadowRadius = 2.0
                        self.btnRentCar.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        self.btnRentCar.setImage(UIImage.init(named: "dontcar_Active"), for: .normal)
                        
                        self.btnOwnCar.shadowColor = UIColor(named: "primaryGrey")!
                        self.btnOwnCar.masksToBounds = false
                        self.btnOwnCar.backgroundColor = .clear
                        self.btnOwnCar.cornerRadius = 8.0
                        self.btnOwnCar.borderWidth = 1.0
                        self.btnOwnCar.setImage(UIImage.init(named: "havecar_Inactive"), for: .normal)
                        self.btnOwnCar.borderColor = UIColor(named: "primaryGrey")
                        
                    }else{
                        
                    }
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
}

//MARK: - UIPicker Delegates
extension OtherDetailsVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.text = accountTypes[row]
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedType = accountTypes[row]
    }
}


extension OtherDetailsVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        // Allow to remove character (Backspace)
//        if string == "" {
//            return true
//        }
//
//        // Block multiple dot
//        if (textField.text?.contains("."))! && string == "." {
//            return false
//        }
//
//
//        if (textField.text?.contains("."))! {
//            let limitDecimalPlace = 2
//            let decimalPlace = textField.text?.components(separatedBy: ".").last
//            if (decimalPlace?.count)! < limitDecimalPlace {
//                return true
//            }
//            else {
//                return false
//            }
//        }
//
        if textField == txtChild {
            let maxLength = 2
            let currentString: NSString = txtChild.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }
    
    func pickUP(_ textField: UITextField) {
        self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.selectRow(0, inComponent: 0, animated: true)
        self.selectedType = accountTypes[0]
        //        self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
        //        self.selectedCategory = self.mainCategoryModel?.data[0].categoryName ?? ""
        txtStatus.inputView = self.picker
        //
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        //
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtStatus.inputAccessoryView = toolBar
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUP(txtStatus)
    }
    
    @objc func selectClick() {
        
        self.txtStatus.text = self.selectedType
        txtStatus.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtStatus.resignFirstResponder()
    }
}
