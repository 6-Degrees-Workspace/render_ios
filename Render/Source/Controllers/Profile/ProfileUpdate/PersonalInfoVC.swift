import UIKit

class PersonalInfoVC: UIViewController {
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtOccupation: UITextField!
    @IBOutlet weak var txtEmp: UITextField!
    @IBOutlet weak var txtCitizenship: UITextField!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    
    var gender = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("updatePersonalInfo"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileServiceCall()
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            //self.bottomConst.constant = -35
        }else{
            // self.bottomConst.constant = 40
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        self.updatePersonalInfo()
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        
        self.txtFirstName.text = txtFirstName.text?.trim()
        self.txtLastName.text = self.txtLastName.text?.trim()
        self.txtEmail.text = self.txtEmail.text?.trim()
        self.txtPhoneNumber.text = txtPhoneNumber.text?.trim()
        self.txtOccupation.text = self.txtOccupation.text?.trim()
        self.txtEmp.text = self.txtEmp.text?.trim()
        self.txtCitizenship.text = self.txtCitizenship.text?.trim()
        let strText = txtEmail.text?.trim()
        
        if (txtFirstName.text?.isEmpty)! {
            message = "Please Enter First Name"
            isFound = false
        } else if (txtFirstName.text?.length ?? 0 < 3){
            message = "The First Name must be at least 3 characters in length"
            isFound = false
        } else if (txtLastName.text?.isEmpty)! {
            message = "Please Enter Last Name"
            isFound = false
        } else if (txtLastName.text?.length ?? 0 < 3){
            message = "The Last Name must be at least 3 characters in length"
            isFound = false
        } else if (txtEmail.text?.isEmpty)! {
            message = "Please Enter Email"
            isFound = false
        } else if (self.isValidEmail(strText!) == false ) {
            message = "Invalid email address"
            isFound = false
        } else if (txtPhoneNumber.text?.isEmpty)! {
            message = "Please Enter Phone Number"
            isFound = false
        } else if (txtOccupation.text?.isEmpty)! {
            message = "Please Enter Occupation"
            isFound = false
        } else if (txtOccupation.text?.length ?? 0 < 3){
            message = "The Occupation must be at least 3 characters in length"
            isFound = false
        }else if (txtEmp.text?.isEmpty)! {
            message = "Please Enter Employer Name"
            isFound = false
        } else if (txtEmp.text?.length ?? 0 < 3){
            message = "The Employer must be at least 3 characters in length"
            isFound = false
        }else if (txtCitizenship.text?.isEmpty)! {
            message = "Please Enter Citizenship"
            isFound = false
        } else if (txtCitizenship.text?.length ?? 0 < 3){
            message = "The Citizenship must be at least 3 characters in length"
            isFound = false
        }
        
        if !isFound {
            // Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    
    func updatePersonalInfo (){
        self.view.endEditing(true)
        if self.isValidFields() {
            updateProfileServiceCall()
        }
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    @IBAction func actionMale(_ sender: Any) {
        self.gender = "male"
        self.maleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        //self.maleBtn.titleLabel?.textColor = UIColor.white
        
        self.femaleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        // self.femaleBtn.titleLabel?.textColor = UIColor.lightGray
    }
    
    
    @IBAction func actionFemale(_ sender: Any) {
        self.gender = "female"
        self.femaleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        //self.femaleBtn.titleLabel?.textColor = UIColor.white
        
        self.maleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        // self.maleBtn.titleLabel?.textColor = UIColor.lightGray
    }
    
    
    func updateProfileServiceCall() {
        // self.showLoader()
        
        self.txtFirstName.text = txtFirstName.text?.trim()
        self.txtLastName.text = self.txtLastName.text?.trim()
        self.txtEmail.text = self.txtEmail.text?.trim()
        self.txtPhoneNumber.text = txtPhoneNumber.text?.trim()
        self.txtOccupation.text = self.txtOccupation.text?.trim()
        self.txtEmp.text = self.txtEmp.text?.trim()
        self.txtCitizenship.text = self.txtCitizenship.text?.trim()
        
        let parameter:[String:Any] = [
            
            "update_profile":1,
            "active_tab":"personal",
            "first_name": self.txtFirstName.text ?? "",
            "last_name": self.txtLastName.text ?? "",
            "gender": self.gender,
            "occupation": self.txtOccupation.text ?? "",
            "employer": self.txtEmp.text ?? "",
            "citizenship": self.txtCitizenship.text ?? "",
            "email": self.txtEmail.text ?? "",
            
        ]
        
        let loginService = LoginService()
        loginService.updateProfile(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                
            }
            self.removLoader()
        }
    }
    
    func getProfileServiceCall() {
        //self.showLoader()
        let parameter:[String:Any] = [
            "":""
        ]
        
        let loginService = LoginService()
        loginService.getUserDetail(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    self.txtFirstName.text = responseData.data.firstName.capitalized ?? ""
                    self.txtLastName.text = responseData.data.lastName.capitalized ?? ""
                    self.txtEmail.text = responseData.data.userEmail ?? ""
                    self.txtPhoneNumber.text = responseData.data.userMobile ?? ""
                    self.txtOccupation.text = responseData.data.occupation.capitalized ?? ""
                    self.txtEmp.text = responseData.data.employer.capitalized ?? ""
                    self.txtCitizenship.text = responseData.data.citizenship.capitalized ?? ""
                    
                    if responseData.data.gender == "male" || responseData.data.gender == "Male"{
                        self.gender = "male"
                        self.maleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
                        //self.maleBtn.titleLabel?.textColor = UIColor.white
                        
                        self.femaleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
                        //self.femaleBtn.titleLabel?.textColor = UIColor.lightGray
                    } else if responseData.data.gender == "female" || responseData.data.gender == "Female" {
                        self.gender = "female"
                        self.femaleBtn.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
                        // self.femaleBtn.titleLabel?.textColor = UIColor.white
                        
                        self.maleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
                        //  self.maleBtn.titleLabel?.textColor = UIColor.lightGray
                    }else{
                        self.femaleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
                        // self.femaleBtn.titleLabel?.textColor = UIColor.lightGray
                        
                        self.maleBtn.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
                        //self.maleBtn.titleLabel?.textColor = UIColor.lightGray
                    }
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}


extension PersonalInfoVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters : CharacterSet
        if textField == txtFirstName || textField == txtLastName || textField == txtOccupation || textField == txtEmp || textField == txtCitizenship {
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_letter).inverted
        }else{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_special_letter).inverted
        }
        
        let components = string.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        
        if textField == txtFirstName {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtFirstName.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
                //return true
                
            } else {
                return false
            }
        }
        
        if textField == txtLastName {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtLastName.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
                //return true
            }else{
                return false
            }
        }
        
        if textField == txtOccupation {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtOccupation.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
            
        }
        
        if textField == txtEmail {
            if string == filtered {
                let maxLength = 35
                let currentString: NSString = txtEmail.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
        }
        
        if textField == txtEmp {
            let maxLength = 25
            let currentString: NSString = txtEmp.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtCitizenship {
            let maxLength = 25
            let currentString: NSString = txtCitizenship.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        return true
    }
}
