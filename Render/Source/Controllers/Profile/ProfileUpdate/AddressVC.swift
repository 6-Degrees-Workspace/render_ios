import UIKit

class AddressVC: UIViewController {
    
    @IBOutlet weak var txtAddress1: UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("updateAddress"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileServiceCall()
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            //self.bottomConst.constant = -35
        }else{
            // self.bottomConst.constant = 40
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        self.updateAddress()
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        
        self.txtAddress1.text = txtAddress1.text?.trim()
        self.txtAddress2.text = self.txtAddress2.text?.trim()
        self.txtCity.text = self.txtCity.text?.trim()
        self.txtState.text = txtState.text?.trim()
        self.txtZipCode.text = self.txtZipCode.text?.trim()
        
        if (txtAddress1.text?.isEmpty)! {
            message = "Please Enter Address Line 1"
            isFound = false
        } else if (txtAddress1.text?.length ?? 0 < 3){
            message = "The  Address Line 1 must be at least 3 characters in length"
            isFound = false
        } else if (txtAddress2.text?.isEmpty)! {
            message = "Please Enter Address Line 2"
            isFound = false
        } else if (txtAddress2.text?.length ?? 0 < 3){
            message = "The  Address Line 2 must be at least 3 characters in length"
            isFound = false
        } else if (txtCity.text?.isEmpty)! {
            message = "Please Enter City Name"
            isFound = false
        } else if (txtCity.text?.length ?? 0 < 3){
            message = "The City must be at least 3 characters in length"
            isFound = false
        } else if (txtState.text?.isEmpty)! {
            message = "Please Enter State Name"
            isFound = false
        } else if (txtState.text?.length ?? 0 < 3){
            message = "The State must be at least 3 characters in length"
            isFound = false
        }else if (txtZipCode.text?.isEmpty)! {
            message = "Please Enter Zipcode"
            isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    func updateAddress (){
        self.view.endEditing(true)
        if self.isValidFields() {
            updateProfileServiceCall()
        }
    }
    
    func updateProfileServiceCall() {
        
        self.txtAddress1.text = txtAddress1.text?.trim()
        self.txtAddress2.text = self.txtAddress2.text?.trim()
        self.txtCity.text = self.txtCity.text?.trim()
        self.txtState.text = txtState.text?.trim()
        self.txtZipCode.text = self.txtZipCode.text?.trim()
        
        //self.showLoader()
        let parameter:[String:Any] = [
            "update_profile":1,
            "active_tab":"address",
            "address_line1":self.txtAddress1.text ?? "",
            "address_line2":self.txtAddress2.text ?? "",
            "city":self.txtCity.text ?? "",
            "state":self.txtState.text ?? "",
            "zipcode":self.txtZipCode.text ?? ""
            
        ]
        let loginService = LoginService()
        loginService.updateProfile(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                
            }
            self.removLoader()
        }
    }
    
    func getProfileServiceCall() {
        //self.showLoader()
        let parameter:[String:Any] = [
            "":""
        ]
        
        let loginService = LoginService()
        loginService.getUserDetail(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    self.txtAddress1.text = responseData.data.addressLine1.capitalized ?? ""
                    self.txtAddress2.text = responseData.data.addressLine2.capitalized ?? ""
                    self.txtCity.text = responseData.data.city.capitalized ?? ""
                    self.txtState.text = responseData.data.state.capitalized ?? ""
                    self.txtZipCode.text = responseData.data.zipCode ?? ""
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}


extension AddressVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters : CharacterSet
        if textField == txtCity || textField == txtState {
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_letter).inverted
        }else{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_advSpecial_letter).inverted
        }
        
        let components = string.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        
        
        if textField == txtAddress1 {
            if string == filtered {
                let maxLength = 50
                let currentString: NSString = txtAddress1.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
        }
        
        if textField == txtAddress2 {
            if string == filtered {
                let maxLength = 50
                let currentString: NSString = txtAddress2.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
        }
        
        if textField == txtCity {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtCity.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
            
        }
        
        if textField == txtState {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtState.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
        }
        
        if textField == txtZipCode {
            let maxLength = 5
            let currentString: NSString = txtZipCode.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        return true
    }
    
}
