//
//  AccountInfoVC.swift
//  Render
//
//  Created by Anis Agwan on 06/12/21.
//

import UIKit
import Photos
import Alamofire
import SDWebImage

class AccountInfoVC: BaseVC {
    
    // IBOutlets
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var genderImgView: UIImageView!
    @IBOutlet weak var btnProfile : UIButton!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var bankTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bankLinkedTable: UITableView!
    @IBOutlet weak var lblOccupation: UILabel!
    @IBOutlet weak var lblEmployer: UILabel!
    @IBOutlet weak var lblCitizenship: UILabel!
    @IBOutlet weak var lblPhoneNo: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblZipCode: UILabel!
    @IBOutlet weak var lblMarital: UILabel!
    @IBOutlet weak var lblChildren: UILabel!
    @IBOutlet weak var lblProperty: UILabel!
    @IBOutlet weak var lblVehicle: UILabel!
    @IBOutlet weak var btnEditProfile : UIButton!
    @IBOutlet weak var btnCamera : UIButton!
    @IBOutlet weak var personalInfoContainer: UIView!
    @IBOutlet weak var addressContainer: UIView!
    @IBOutlet weak var otherDetailsContainer: UIView!
    @IBOutlet weak var btnPersonalInfoProfile : UIButton!
    @IBOutlet weak var btnAddressProfile : UIButton!
    @IBOutlet weak var btnOtherDetailsProfile : UIButton!
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    let imagePicker = UIImagePickerController()
    var userDetailsModel : UserDetailsModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroller.contentInsetAdjustmentBehavior = .never
        self.personalInfoContainer.isHidden = false
        self.addressContainer.isHidden = true
        self.otherDetailsContainer.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileServiceCall()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.bankLinkedTable.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewDidLayoutSubviews() {
        
        if self.personalInfoContainer.isHidden == false && self.addressContainer.isHidden == true && self.otherDetailsContainer.isHidden == true{
            scroller.contentSize = CGSize(width: 325,height:1000)
            self.bottomConst.constant = 600
        }else if self.personalInfoContainer.isHidden == true && self.addressContainer.isHidden == false && self.otherDetailsContainer.isHidden == true{
            scroller.contentSize = CGSize(width: 325,height:800)
            self.bottomConst.constant = 320
        }else if self.personalInfoContainer.isHidden == true && self.addressContainer.isHidden == true && self.otherDetailsContainer.isHidden == false{
            scroller.contentSize = CGSize(width: 325,height:800)
            self.bottomConst.constant = 450
        }else{
            scroller.contentSize = CGSize(width: 325,height:800)
            self.bottomConst.constant = 450
        }
        scroller.isScrollEnabled = true
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newValue = change?[.newKey] {
                let newSize = newValue as! CGSize
                self.bankTableHeightConstraint.constant = newSize.height
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    @IBAction func actionEditBtn(_ sender: Any) {
        
        let objVC: EditProfileVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: EditProfileVC.nameOfClass)
        objVC.userDetails = self.userDetailsModel
        //self.navigationController?.pushViewController(objVC, animated: true)
        self.present(objVC, animated: true)
    }
    
    @IBAction func actionProfile(_ sender: UIButton){
        DispatchQueue.main.async {
            self.OpenGalleyandCamra()
        }
    }
    
    @IBAction func actionAddProfile(_ sender: UIButton){
        if sender.tag == 0 {
            self.btnPersonalInfoProfile.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            self.btnAddressProfile.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnOtherDetailsProfile.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.personalInfoContainer.isHidden = false
            self.addressContainer.isHidden = true
            self.otherDetailsContainer.isHidden = true
            
        } else if sender.tag == 1 {
            
            self.btnPersonalInfoProfile.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnAddressProfile.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            self.btnOtherDetailsProfile.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.personalInfoContainer.isHidden = true
            self.addressContainer.isHidden = false
            self.otherDetailsContainer.isHidden = true
        }else{
            
            self.btnPersonalInfoProfile.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnAddressProfile.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnOtherDetailsProfile.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            
            self.personalInfoContainer.isHidden = true
            self.addressContainer.isHidden = true
            self.otherDetailsContainer.isHidden = false
        }
    }
    
    
    @IBAction func updateProfilePressed(_ sender: UIButton) {
        
        if self.personalInfoContainer.isHidden == false && self.addressContainer.isHidden == true && self.otherDetailsContainer.isHidden == true{
            NotificationCenter.default.post(name: Notification.Name("updatePersonalInfo"), object: nil, userInfo: nil)
        }else if self.personalInfoContainer.isHidden == true && self.addressContainer.isHidden == false && self.otherDetailsContainer.isHidden == true{
            NotificationCenter.default.post(name: Notification.Name("updateAddress"), object: nil, userInfo: nil)
        }else if self.personalInfoContainer.isHidden == true && self.addressContainer.isHidden == true && self.otherDetailsContainer.isHidden == false{
            NotificationCenter.default.post(name: Notification.Name("updateOtherInfo"), object: nil, userInfo: nil)
        }
    }
    
    
}


extension AccountInfoVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.bankLinkedTable.dequeueReusableCell(withIdentifier: "bankAccCell", for: indexPath)
        
        return cell
    }
    
    
}


// MARK: - UIImagePickerControllerDelegate Methods
@available(iOS 13.0, *)
extension AccountInfoVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[.editedImage] as? UIImage {
            //self.btnProfile.setImage(editedImage, for: .normal)
            self.imgProfile.image = editedImage
            self.uploadProfileData(image: editedImage)
        } else if let originalImage = info[.originalImage] as? UIImage {
            //self.btnProfile.setImage(originalImage, for: .normal)
            self.imgProfile.image = originalImage
            self.uploadProfileData(image: originalImage)
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    func openCamera(){
        DispatchQueue.main.async {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func openGallary() {
        
        DispatchQueue.main.async {
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image"]
            self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    //Camra and Gallery
    func OpenGalleyandCamra(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.selectImageFromCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.checkPhotoLibraryPermission()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel){
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectImageFromCamera() //to Access Camera
    {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            
            let alert = UIAlertController(title: "Unable to access the Camera",
                                          message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        }
        else if (authStatus == AVAuthorizationStatus.notDetermined) {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    }
                }
            })
        } else {
            self.openCamera()
        }
        
    }
    
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            self.openGallary()
        case .denied, .restricted :
            //handle denied status
            let alert = UIAlertController(title: "Unable to access the Gallary",
                                          message: "To enable access, go to Settings > Privacy > Gallary and turn on Gallary access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            self.present(alert, animated: true, completion: nil)
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized:
                    self.openGallary()
                    break
                case .denied, .restricted: break
                case .notDetermined: break
                case .limited:
                    break
                @unknown default:
                    break
                }
            }
        case .limited:
            break
        @unknown default:
            break
        }
    }
    
    func uploadProfileData (image: UIImage) {
        self.showLoader()
        //let SERVICE_URL_ONBOARD = "http://renderweb.wsisrdev.com/"
        //Development
        
        let token = ApplicationPreference.getToken() ?? ""
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Authorization" : token
        ]
        //  print(SERVICE_URL_ONBOARD+APIEndPoint.uploadProfilePicture)
        AF.upload(multipartFormData: { multipartFormData in
            
            let imageData: Data = (image.pngData()! as NSData) as Data
            multipartFormData.append(imageData, withName: "userProfilePic", fileName: "file.png", mimeType: "image/png")
        }, to: Config.shared.baseURL+Constants.EndPoint.uploadProfilePhoto.rawValue, method: .post , headers: headers)
            .responseJSON(completionHandler: { (responseData) in
                
                if let err = responseData.error{
                    print(err)
                    self.removLoader()
                    return
                }
                
                print("Succesfully uploaded")
                let json = responseData.data
                
                if let jsondata = try? JSONSerialization.jsonObject(with: responseData.data!) as? [String:Any]{
                    print("Success ---- \(jsondata) ")
                    
                    if let data = jsondata["data"] as? [String:Any]{
                        let url = data["url"]
                        //print(url)
                        DispatchQueue.global(qos: .background).sync {
                            self.imgProfile.image = image
                            self.imgProfile.sd_setImage(with: URL(string: url as! String), placeholderImage: UIImage(named: ""))
                        }
                    }
                    //                    let status = jsondata["status"]
                    //                    let message = jsondata["message"]
                    self.removLoader()
                }
                //print(json)
                if (json != nil) {
                    print(json ?? "")
                }else{
                    self.removLoader()
                    print("Something went wrong")
                    //Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            })
    }
}

extension AccountInfoVC{
    
    func getProfileServiceCall() {
        self.showLoader()
        let parameter:[String:Any] = [
            "":""
        ]
        
        let loginService = LoginService()
        loginService.getUserDetail(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.userDetailsModel = responseData
                    DispatchQueue.global(qos: .background).async {
                        if responseData.data.gender == "female"{
                            self.imgProfile.sd_setImage(with: URL(string: responseData.data.userProfilePic), placeholderImage: UIImage(named: "FemaleProfileAvatar"))
                        }else{
                            self.imgProfile.sd_setImage(with: URL(string: responseData.data.userProfilePic), placeholderImage: UIImage(named: "profileAvatar"))
                        }
                    }
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}

