import UIKit
import Photos
import Alamofire
import SDWebImage

class AccountInfoDetailsVC: BaseVC {
    
    // IBOutlets
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var genderImgView: UIImageView!
    @IBOutlet weak var btnProfile : UIButton!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var bankTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bankLinkedTable: UITableView!
    @IBOutlet weak var lblOccupation: UILabel!
    @IBOutlet weak var lblEmployer: UILabel!
    @IBOutlet weak var lblCitizenship: UILabel!
    @IBOutlet weak var lblPhoneNo: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblZipCode: UILabel!
    @IBOutlet weak var lblMarital: UILabel!
    @IBOutlet weak var lblChildren: UILabel!
    @IBOutlet weak var lblProperty: UILabel!
    @IBOutlet weak var lblVehicle: UILabel!
    @IBOutlet weak var btnEditProfile : UIButton!
    
    let imagePicker = UIImagePickerController()
    var userDetailsModel : UserDetailsModel?
    var getUserAllBankAccountsModel : GetUserAllBankAccountsModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnEditProfile.setImage(UIImage.init(named: "editBtnIcon"), for: .normal)
        let defaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "SavedPerson") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(UserData.self, from: savedPerson) {
                //print(loadedPerson.name)
                lblFullName.text = loadedPerson.fullName
                lblEmailAddress.text = loadedPerson.userEmail
                lblGender.text = loadedPerson.gender
                DispatchQueue.global(qos: .background).async {
                    //  self.btnProfile.sd_setImage(with: URL(string: loadedPerson.userProfilePic), for: .normal, placeholderImage: UIImage(named: "ic_profile"))
                    self.imgProfile.sd_setImage(with: URL(string: loadedPerson.userProfilePic), placeholderImage: UIImage(named: ""))
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUserAllBankAccounts()
        self.getProfileServiceCall()
        self.bankLinkedTable.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.bankLinkedTable.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newValue = change?[.newKey] {
                let newSize = newValue as! CGSize
                self.bankTableHeightConstraint.constant = newSize.height
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let objVC: EditProfileVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: EditProfileVC.nameOfClass)
        objVC.userDetails = self.userDetailsModel
        //self.navigationController?.pushViewController(objVC, animated: true)
        self.present(objVC, animated: true)
    }
    
    @IBAction func actionEditBtn(_ sender: Any) {
        
        let objVC: AccountInfoVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.profile, viewControllerName: AccountInfoVC.nameOfClass)
        //objVC.userDetails = self.userDetailsModel
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func actionProfile(_ sender: UIButton){
        DispatchQueue.main.async {
            self.OpenGalleyandCamra()
        }
    }
    
}


extension AccountInfoDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getUserAllBankAccountsModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = self.bankLinkedTable.dequeueReusableCell(withIdentifier: "bankAccCell", for: indexPath)
        let cell = bankLinkedTable.dequeueReusableCell(withIdentifier: "bankAccCell", for: indexPath) as! BankTableCell
        let objDict = self.getUserAllBankAccountsModel?.data?[indexPath.row]
        cell.bankName.text = objDict?.institutionName
        if objDict?.isPlaidReauthorized == "0"{
            cell.btnReauthorize.isHidden = true
        }else{
            cell.btnReauthorize.isHidden = false
        }
        cell.bankDetails.text = objDict?.bankAccountID
        cell.btnDelete.tag =  Int((objDict?.bankAccountID)!)!
        cell.btnDelete.addTarget(self, action: #selector(self.actionDelete(_:)), for: UIControl.Event.touchUpInside)
        cell.btnReauthorize.tag =  Int((objDict?.bankAccountID)!)!
        cell.btnReauthorize.addTarget(self, action: #selector(self.actionReauthorize(_:)), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    @IBAction func actionDelete(_ sender: UIButton){
        
        let alert = UIAlertController(title: "Render", message: "Are you sure to delete this item?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {_ in
            
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: {_ in
            self.deleteBankServiceCall(bankId: String(sender.tag))
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionReauthorize(_ sender: UIButton){
        self.reauthorizeAccountServiceCall(bankId: String(sender.tag))
    }
}


// MARK: - UIImagePickerControllerDelegate Methods
@available(iOS 13.0, *)
extension AccountInfoDetailsVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[.editedImage] as? UIImage {
            //self.btnProfile.setImage(editedImage, for: .normal)
            self.imgProfile.image = editedImage
            self.uploadProfileData(image: editedImage)
        } else if let originalImage = info[.originalImage] as? UIImage {
            //self.btnProfile.setImage(originalImage, for: .normal)
            self.imgProfile.image = originalImage
            self.uploadProfileData(image: originalImage)
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    func openCamera(){
        DispatchQueue.main.async {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func openGallary() {
        
        DispatchQueue.main.async {
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image"]
            self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    //Camra and Gallery
    func OpenGalleyandCamra(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.selectImageFromCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.checkPhotoLibraryPermission()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel){
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectImageFromCamera() //to Access Camera
    {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            
            let alert = UIAlertController(title: "Unable to access the Camera",
                                          message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        }
        else if (authStatus == AVAuthorizationStatus.notDetermined) {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    }
                }
            })
        } else {
            self.openCamera()
        }
        
    }
    
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            self.openGallary()
        case .denied, .restricted :
            //handle denied status
            let alert = UIAlertController(title: "Unable to access the Gallary",
                                          message: "To enable access, go to Settings > Privacy > Gallary and turn on Gallary access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            self.present(alert, animated: true, completion: nil)
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized:
                    self.openGallary()
                    break
                case .denied, .restricted: break
                case .notDetermined: break
                case .limited:
                    break
                @unknown default:
                    break
                }
            }
        case .limited:
            break
        @unknown default:
            break
        }
    }
    
    func uploadProfileData (image: UIImage) {
        self.showLoader()
        //let SERVICE_URL_ONBOARD = "http://renderweb.wsisrdev.com/"
        //Development
        
        let token = ApplicationPreference.getToken() ?? ""
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Authorization" : token
        ]
        //  print(SERVICE_URL_ONBOARD+APIEndPoint.uploadProfilePicture)
        AF.upload(multipartFormData: { multipartFormData in
            
            let imageData: Data = (image.pngData()! as NSData) as Data
            multipartFormData.append(imageData, withName: "userProfilePic", fileName: "file.png", mimeType: "image/png")
        }, to: Config.shared.baseURL+Constants.EndPoint.uploadProfilePhoto.rawValue, method: .post , headers: headers)
            .responseJSON(completionHandler: { (responseData) in
                
                if let err = responseData.error{
                    print(err)
                    self.removLoader()
                    return
                }
                self.removLoader()
                print("Succesfully uploaded")
                let json = responseData.data
                if let jsondata = try? JSONSerialization.jsonObject(with: responseData.data!) as? [String:Any]{
                    print("Success ---- \(jsondata) ")
                    
                    if let data = jsondata["data"] as? [String:Any]{
                        let url = data["url"]
                        // print(url)
                        DispatchQueue.global(qos: .background).async {
                            self.imgProfile.sd_setImage(with: URL(string: url as! String), placeholderImage: UIImage(named: "avatarSettings"))
                        }
                        
                    }
                    //let status = jsondata["status"]
                    //let message = jsondata["message"]
                }
                if (json != nil) {
                    print(json ?? "")
                }else{
                    self.removLoader()
                    print("Something went wrong")
                    //Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            })
    }
}

extension AccountInfoDetailsVC{
    
    func getUserAllBankAccounts(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "":""
        ]
        
        let plaidService = PlaidService()
        plaidService.getUserAllBankAccounts(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.getUserAllBankAccountsModel = responseData
                    self.bankLinkedTable.reloadData()
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func getProfileServiceCall() {
        self.showLoader()
        let parameter:[String:Any] = [
            "":""
        ]
        
        let loginService = LoginService()
        loginService.getUserDetail(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.userDetailsModel = responseData
                    self.lblFullName.text = responseData.data.fullName.capitalized ?? "NA"
                    self.lblEmailAddress.text = responseData.data.userEmail ?? "NA"
                    self.lblGender.text = responseData.data.gender.capitalized ?? "NA"
                    self.lblOccupation.text = responseData.data.occupation.capitalized ?? "NA"
                    self.lblEmployer.text = responseData.data.employer.capitalized ?? "NA"
                    self.lblCitizenship.text = responseData.data.citizenship.capitalized ?? "NA"
                    self.lblPhoneNo.text = responseData.data.userMobile ?? "NA"
                    self.lblAddress1.text = responseData.data.addressLine1.capitalized ?? "NA"
                    self.lblAddress2.text = responseData.data.addressLine2.capitalized ?? "NA"
                    self.lblCity.text = responseData.data.city.capitalized ?? "NA"
                    self.lblState.text = responseData.data.state.capitalized ?? "NA"
                    self.lblZipCode.text = responseData.data.zipCode ?? "NA"
                    self.lblMarital.text = responseData.data.maritalStatus.capitalized ?? "NA"
                    self.lblChildren.text = responseData.data.children ?? "NA"
                    self.lblProperty.text = responseData.data.property.capitalized ?? "NA"
                    self.lblVehicle.text = responseData.data.ownCar.capitalized ?? "NA"
                    
                    DispatchQueue.global(qos: .background).async {
                        if responseData.data.gender == "female"{
                            self.imgProfile.sd_setImage(with: URL(string: responseData.data.userProfilePic), placeholderImage: UIImage(named: "FemaleProfileAvatar"))
                        }else{
                            self.imgProfile.sd_setImage(with: URL(string: responseData.data.userProfilePic), placeholderImage: UIImage(named: "profileAvatar"))
                        }
                    }
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
    func deleteBankServiceCall(bankId: String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "bank_account_id": bankId
        ]
        
        let plaidService = PlaidService()
        plaidService.removeBankAccount(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.getUserAllBankAccounts()
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func reauthorizeAccountServiceCall(bankId: String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "bank_account_id": bankId
        ]
        let plaidService = PlaidService()
        plaidService.reauthorizeAccount(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.getUserAllBankAccounts()
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
}
