import UIKit
import Photos
import Alamofire

class UploadPhotoVC: BaseVC {
    
    @IBOutlet weak var btnProfile : UIButton!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    let imagePicker = UIImagePickerController()
    var gender : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if gender == "female"{
            imgProfile.image = UIImage(named: "FemaleProfileAvatar")
        }else{
            imgProfile.image = UIImage(named: "profileAvatar")
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionProfile(_ sender: UIButton){
        DispatchQueue.main.async {
            self.OpenGalleyandCamra()
        }
    }
    
    @IBAction func actionNext(_ sender: UIButton){
        //Check mobile number for user send Dashboard or sign up VC
        let objVC : CreatePinVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: CreatePinVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func actionSkip(_ sender: UIButton){
        //Check mobile number for user send Dashboard or sign up VC
        let objVC : CreatePinVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: CreatePinVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}

// MARK: - UIImagePickerControllerDelegate Methods
@available(iOS 13.0, *)
extension UploadPhotoVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[.editedImage] as? UIImage {
            //self.btnProfile.setImage(editedImage, for: .normal)
            self.imgProfile.image = editedImage
            self.uploadProfileData(image: editedImage)
        } else if let originalImage = info[.originalImage] as? UIImage {
            //self.btnProfile.setImage(originalImage, for: .normal)
            self.imgProfile.image = originalImage
            self.uploadProfileData(image: originalImage)
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    func openCamera(){
        DispatchQueue.main.async {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func openGallary() {
        
        DispatchQueue.main.async {
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image"]
            self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    //Camra and Gallery
    func OpenGalleyandCamra(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.selectImageFromCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.checkPhotoLibraryPermission()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel){
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectImageFromCamera() //to Access Camera
    {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            
            let alert = UIAlertController(title: "Unable to access the Camera",
                                          message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        }
        else if (authStatus == AVAuthorizationStatus.notDetermined) {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    }
                }
            })
        } else {
            self.openCamera()
        }
        
    }
    
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            self.openGallary()
        case .denied, .restricted :
            //handle denied status
            let alert = UIAlertController(title: "Unable to access the Gallary",
                                          message: "To enable access, go to Settings > Privacy > Gallary and turn on Gallary access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            self.present(alert, animated: true, completion: nil)
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized:
                    self.openGallary()
                    break
                case .denied, .restricted: break
                case .notDetermined: break
                case .limited:
                    break
                @unknown default:
                    break
                }
            }
        case .limited:
            break
        @unknown default:
            break
        }
    }
    
    func uploadProfileData (image: UIImage) {
        self.showLoader()
        //let SERVICE_URL_ONBOARD = "http://renderweb.wsisrdev.com/"
        //Development
        
        let token = ApplicationPreference.getToken() ?? ""
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Authorization" : token
        ]
        //  print(SERVICE_URL_ONBOARD+APIEndPoint.uploadProfilePicture)
        AF.upload(multipartFormData: { multipartFormData in
            
            let imageData: Data = (image.pngData()! as NSData) as Data
            multipartFormData.append(imageData, withName: "userProfilePic", fileName: "file.png", mimeType: "image/png")
        }, to: Config.shared.baseURL+Constants.EndPoint.uploadProfilePhoto.rawValue, method: .post , headers: headers)
            .responseJSON(completionHandler: { (responseData) in
                
                if let err = responseData.error{
                    print(err)
                    self.removLoader()
                    return
                }
                self.removLoader()
                print("Succesfully uploaded")
                self.nextButton.isEnabled = true
                self.nextButton.backgroundColor = UIColor(named: "buttonColor")
                let json = responseData.data
                
                if let jsondata = try? JSONSerialization.jsonObject(with: responseData.data!) as? [String:Any]{
                    print("Success ---- \(jsondata) ")
                    
                    if let data = jsondata["data"] as? [String:Any]{
                        let url = data["url"]
                        print(url)
                        DispatchQueue.global(qos: .background).async {
                            self.imgProfile.sd_setImage(with: URL(string: url as! String), placeholderImage: UIImage(named: "avatarSettings"))
                        }

                    }
                    let status = jsondata["status"]
                    let message = jsondata["message"]
                    
                    
                }
                
                if (json != nil) {
                    print(json ?? "")
                }else{
                    self.removLoader()
                    print("Something went wrong")
                    //Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            })
    }
}



extension UploadPhotoVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == txtMobileNumber {
//            let maxLength = 10
//            let currentString: NSString = txtMobileNumber.text! as NSString
//            let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//
//        }
        return true
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print(textField.text)
//        if txtMobileNumber.text != "" {
//            self.nextButton.isEnabled = true
//            self.nextButton.backgroundColor = UIColor(named: "buttonColor")
//        }
    }
    
  

}
