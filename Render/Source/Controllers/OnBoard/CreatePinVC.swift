import UIKit

class CreatePinVC: BaseVC {
    
    //MARK: - Variables
    var pin: [Int] = []
    let numbers: [String] = ["1","2","3","4","5","6","7","8","9"]
    
    //MARK: - IBOutlets
    @IBOutlet weak var forgotPinBtn: UIButton!
    @IBOutlet weak var forgotPinLbl: UILabel!
    @IBOutlet weak var numpadCollection: UICollectionView!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func navigateToConfirmPin(pinTransfer: [Int]) {
        self.createPinServiceCall(pin: pinTransfer)
        //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmPinVC") as! ConfirmPinVC
        //        vc.pin = pinTransfer
        //        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func forgotPinAction(_ sender: UIButton) {
    }
    
}

//MARK: - Number Pad UICollectionView
extension CreatePinVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    // Collection general functions
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if indexPath.section == 1 {
            if indexPath.item == 1 {
                //print("BACK SPACE")
                if !(pin.isEmpty) {
                    self.pin.removeLast()
                } else {
                    //print("NOTHING INSIDE")
                }
            } else {
                if pin.count >= 4 {
                    //print("CAN NOT ADD MORE")
                } else if pin.count == 3 {
                    //print("JUST added the last pin")
                    self.pin.append(indexPath.item)
                    navigateToConfirmPin(pinTransfer: self.pin)
                    
                } else {
                    self.pin.append(indexPath.item)
                }
            }
        } else {
            if pin.count >= 4 {
                //print("CAN NOT ADD MORE")
            } else if pin.count == 3 {
                //print("JUST added the last pin")
                self.pin.append(indexPath.item+1)
                navigateToConfirmPin(pinTransfer: self.pin)
                
            } else {
                print("\(indexPath.item+1)")
                self.pin.append(indexPath.item+1)
            }
        }
        print("PIN: \(pin)")
        numpadCollection.reloadData()
    }
    
    //Header collection funcs
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = numpadCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "NumPadHeader", for: indexPath) as! NumpadHeaderCollectionView
        
        header.highlightPIN(pinCount: pin.count)
        //header.backgroundColor = .white
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 1 {
            return .zero
        }
        
        let height = numpadCollection.frame.height * 0.2
        
        return .init(width: numpadCollection.frame.width, height: height)
    }
    
    // Cells collection funcs
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return 2
        }
        return numbers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 1 {
            if indexPath.item == 0 {
                let cell = numpadCollection.dequeueReusableCell(withReuseIdentifier: "NumPadCell", for: indexPath) as! NumPadCell
                cell.configureCell(num: "0")
                cell.backgroundColor = UIColor(named: "NumPadButtonColor")
                
                return cell
            } else {
                let backspace = numpadCollection.dequeueReusableCell(withReuseIdentifier: "BackspaceCell", for: indexPath) as! BackspaceNumPadCell
                
                return backspace
            }
        }
        
        let cell = numpadCollection.dequeueReusableCell(withReuseIdentifier: "NumPadCell", for: indexPath) as! NumPadCell
        cell.configureCell(num: numbers[indexPath.item])
        cell.backgroundColor = UIColor(named: "NumPadButtonColor")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let edgePadding = numpadCollection.frame.width * 0.13
        let interSpacing = numpadCollection.frame.width * 0.1
        let cellWidth = (numpadCollection.frame.width - 2*edgePadding - 2*interSpacing) / 3
        
        return .init(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 1 {
            let edgePadding = numpadCollection.frame.width * 0.13
            let interSpacing = numpadCollection.frame.width * 0.1
            let cellWidth = (numpadCollection.frame.width - 2*edgePadding - 2*interSpacing) / 3
            
            let leftPadding = (numpadCollection.frame.width) / 2 - cellWidth / 2
            return .init(top: 0, left: leftPadding, bottom: 0, right: edgePadding)
        }
        
        let edgePadding = numpadCollection.frame.width * 0.15
        //let interSpacing = numpadCollection.frame.width * 0.1
        return .init(top: 20, left: edgePadding, bottom: 20, right: edgePadding)
    }
    
}

extension CreatePinVC {
    
    // MARK: - Service Calls
    func createPinServiceCall(pin: [Int]) {
        
        let securityCodeArray = pin.map { String($0) }
        let pinCode = securityCodeArray.joined(separator: "")
        print("pinCode",pinCode)
        
        self.showLoader()
        let parameter:[String:Any] = [
            "security_code": pinCode
        ]
        let signupService = SignupService()
        signupService.createPin(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    let objVC : ConfirmPinVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: ConfirmPinVC.nameOfClass)
                    objVC.pin = pin
                    self.navigationController?.pushViewController(objVC, animated: true)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}

