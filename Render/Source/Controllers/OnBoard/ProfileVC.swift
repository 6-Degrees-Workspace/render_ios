import UIKit

class ProfileVC: BaseVC {
    @IBOutlet weak var firstNameTxtFld : UITextField!
    @IBOutlet weak var lastNameTxtFld: UITextField!
    @IBOutlet weak var btnMale : UIButton!
    @IBOutlet weak var btnFemale : UIButton!
    
    var gender : String = ""
    var name : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        // Do any additional setup after loading the view.
    }
    
    func setInitialData(){
        
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.firstNameTxtFld.text = firstNameTxtFld.text?.trim()
        self.lastNameTxtFld.text = lastNameTxtFld.text?.trim()
        
        if (firstNameTxtFld.text?.isEmpty)! {
            message = "Please enter first name."
            isFound = false
        }else if (firstNameTxtFld.text?.length ?? 0 < 3){
            message = "The first name must be at least 3 characters in length"
            isFound = false
        }else if (lastNameTxtFld.text?.isEmpty)! {
            message = "Please enter last name"
            isFound = false
        }else if (lastNameTxtFld.text?.length ?? 0 < 3){
            message = "The last name must be at least 3 characters in length"
            isFound = false
        } else if (self.gender.length == 0 ) {
                message = "Please select gender"
                isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func actionMale(_ sender: UIButton){
        self.gender = "male"
        self.btnMale.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        self.btnMale.titleLabel?.textColor = UIColor.white
        
        self.btnFemale.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        self.btnFemale.titleLabel?.textColor = UIColor.lightGray
    }
    
    @IBAction func actionFemale(_ sender: UIButton){
        self.gender = "female"
        self.btnFemale.backgroundColor = UIColor(red: 0/255.0, green: 140/255.0, blue: 248/255.0, alpha: 1.0)
        self.btnFemale.titleLabel?.textColor = UIColor.white
        
        self.btnMale.backgroundColor = UIColor(red: 248/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)
        self.btnMale.titleLabel?.textColor = UIColor.lightGray
        
    }
    
    @IBAction func actionNext(_ sender: UIButton){
        if isValidFields(){
            self.updateProfileServiceCall()
        }
        
    }
    
    // MARK: - Service Calls
    func updateProfileServiceCall() {
        self.showLoader()
        let parameter:[String:Any] = [
            "update_profile":0,
            "first_name": self.firstNameTxtFld.text ?? "",
            "last_name": self.lastNameTxtFld.text ?? "",
            "gender":self.gender
        ]
        let loginService = LoginService()
        loginService.updateProfile(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    //Check mobile number for user send Dashboard or sign up VC
                    let objVC : UploadPhotoVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: UploadPhotoVC.nameOfClass)
                    objVC.gender = self.gender
                    self.navigationController?.pushViewController(objVC, animated: true)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                // self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
                let objVC : VerifyOTPVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: VerifyOTPVC.nameOfClass)
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            self.removLoader()
        }
    }
}




//MARK:- Action Method
@available(iOS 13.0, *)
extension ProfileVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == firstNameTxtFld {
            let maxLength = 25
            let currentString: NSString = firstNameTxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        if textField == lastNameTxtFld {
            let maxLength = 25
            let currentString: NSString = lastNameTxtFld.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else{
            return true
        }
    }
    
}

