//
//  ConfirmPinViewController.swift
//  Rendr-test
//
//  Created by Anis Agwan on 30/11/21.
//

import UIKit

class ConfirmPinViewController: UIViewController {
    
    //MARK: - Variables
    var pin: [Int] = []
    var confirmPIN: [Int] = []
    let numbers: [String] = ["1","2","3","4","5","6","7","8","9"]
    
    
    //MARK: - IBOutlets
    @IBOutlet weak var numpadCollection: UICollectionView!
    @IBOutlet weak var donotSharePINLabel: UILabel!
    
    //MARK: - View Lifecyle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true)
        
    }
    
    // Function to check the pin and confirmPin
    func matchingPins(pin: [Int], confirm: [Int]) {
        if pin != confirmPIN {
            donotSharePINLabel.text = "Pins do not match"
            donotSharePINLabel.textColor = .red
        } else {
            // CALL THE API HERE
            print("PINS match")
        }
    }

}


//MARK: - Number Pad UICollectionView
extension ConfirmPinViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    // Collection general functions
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if indexPath.section == 1 {
            if indexPath.item == 1 {
                print("BACK SPACE")
                if !(confirmPIN.isEmpty) {
                    self.confirmPIN.removeLast()
                    self.donotSharePINLabel.text = "Do not share this with anyone"
                    self.donotSharePINLabel.textColor = UIColor(named: "donotShareOTPlabelColor")
                } else {
                    print("NOTHING INSIDE")
                }
            } else if confirmPIN.count == 3 {
                print("JUST added the last pin")
                self.confirmPIN.append(indexPath.item)
                //navigateToConfirmPin(pinTransfer: self.pin)
                self.matchingPins(pin: self.pin, confirm: self.confirmPIN)
                
            } else {
                if confirmPIN.count >= 4 {
                    print("CAN NOT ADD MORE")
                } else {
                    self.confirmPIN.append(indexPath.item)
                }
            }
        } else {
            if confirmPIN.count >= 4 {
                print("CAN NOT ADD MORE")
            } else if confirmPIN.count == 3 {
                print("JUST added the last pin")
                self.confirmPIN.append(indexPath.item+1)
                //navigateToConfirmPin(pinTransfer: self.pin)
                self.matchingPins(pin: self.pin, confirm: self.confirmPIN)
                
                
            } else {
                print("\(indexPath.item+1)")
                self.confirmPIN.append(indexPath.item+1)
            }
        }
        print("PIN: \(confirmPIN)")
        numpadCollection.reloadData()
    }
    
    //Header collection funcs
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = numpadCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "NumPadHeader", for: indexPath) as! NumpadHeaderCollectionView
        
        header.highlightPIN(pinCount: confirmPIN.count)
        //header.backgroundColor = .white
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 1 {
            return .zero
        }
        
        let height = numpadCollection.frame.height * 0.2
        
        return .init(width: numpadCollection.frame.width, height: height)
    }
    
    // Cells collection funcs
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return 2
        }
        return numbers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 1 {
            if indexPath.item == 0 {
                let cell = numpadCollection.dequeueReusableCell(withReuseIdentifier: "NumPadCell", for: indexPath) as! NumPadCell
                cell.configureCell(num: "0")
                cell.backgroundColor = UIColor(named: "NumPadButtonColor")
                
                return cell
            } else {
                let backspace = numpadCollection.dequeueReusableCell(withReuseIdentifier: "BackspaceCell", for: indexPath) as! BackspaceNumPadCell
                
                return backspace
            }
        }
        
        let cell = numpadCollection.dequeueReusableCell(withReuseIdentifier: "NumPadCell", for: indexPath) as! NumPadCell
        cell.configureCell(num: numbers[indexPath.item])
        cell.backgroundColor = UIColor(named: "NumPadButtonColor")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let edgePadding = numpadCollection.frame.width * 0.13
        let interSpacing = numpadCollection.frame.width * 0.1
        let cellWidth = (numpadCollection.frame.width - 2*edgePadding - 2*interSpacing) / 3
        
        return .init(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 1 {
            let edgePadding = numpadCollection.frame.width * 0.13
            let interSpacing = numpadCollection.frame.width * 0.1
            let cellWidth = (numpadCollection.frame.width - 2*edgePadding - 2*interSpacing) / 3
            
            let leftPadding = (numpadCollection.frame.width) / 2 - cellWidth / 2
            return .init(top: 0, left: leftPadding, bottom: 0, right: edgePadding)
        }
        
        let edgePadding = numpadCollection.frame.width * 0.15
        //let interSpacing = numpadCollection.frame.width * 0.1
        
        return .init(top: 10, left: edgePadding, bottom: 10, right: edgePadding)
    }
}
