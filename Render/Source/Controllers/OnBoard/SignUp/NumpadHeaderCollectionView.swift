//
//  NumpadHeaderCollectionView.swift
//  Rendr-test
//
//  Created by Anis Agwan on 25/11/21.
//

import UIKit

class NumpadHeaderCollectionView: UICollectionReusableView {
    @IBOutlet weak var pin1: UIView!
    @IBOutlet weak var pin2: UIView!
    @IBOutlet weak var pin3: UIView!
    @IBOutlet weak var pin4: UIView!
    
    func highlightPIN(pinCount: Int) {
        print(pinCount)
        switch pinCount {
        case 0:
            pin1.backgroundColor = UIColor(named: "emptyPinColor")
            pin2.backgroundColor = UIColor(named: "emptyPinColor")
            pin3.backgroundColor = UIColor(named: "emptyPinColor")
            pin4.backgroundColor = UIColor(named: "emptyPinColor")
        case 1:
            pin1.backgroundColor = UIColor(named: "filledPinColor")
            pin2.backgroundColor = UIColor(named: "emptyPinColor")
            pin3.backgroundColor = UIColor(named: "emptyPinColor")
            pin4.backgroundColor = UIColor(named: "emptyPinColor")
        case 2:
            pin1.backgroundColor = UIColor(named: "filledPinColor")
            pin2.backgroundColor = UIColor(named: "filledPinColor")
            pin3.backgroundColor = UIColor(named: "emptyPinColor")
            pin4.backgroundColor = UIColor(named: "emptyPinColor")
        case 3:
            pin1.backgroundColor = UIColor(named: "filledPinColor")
            pin2.backgroundColor = UIColor(named: "filledPinColor")
            pin3.backgroundColor = UIColor(named: "filledPinColor")
            pin4.backgroundColor = UIColor(named: "emptyPinColor")
        case 4:
            pin1.backgroundColor = UIColor(named: "filledPinColor")
            pin2.backgroundColor = UIColor(named: "filledPinColor")
            pin3.backgroundColor = UIColor(named: "filledPinColor")
            pin4.backgroundColor = UIColor(named: "filledPinColor")
        default:
            print("NO NUMBER")
        }
    }
    
}
