//
//  NumPadCell.swift
//  Rendr-test
//
//  Created by Anis Agwan on 25/11/21.
//

import UIKit

class NumPadCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var numberLabel: UILabel!
    
    //MARK: - Cell Lifecycle
    override var isHighlighted: Bool {
        didSet {
            //print("is Highlighted? \(isHighlighted)")
            backgroundColor = isHighlighted ? UIColor(named: "filledPinColor") : UIColor(named: "NumPadButtonColor")
            numberLabel.textColor = isHighlighted ? .white : .black
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = self.frame.width / 2
    }
    
    //MARK: - Functions
    func configureCell(num: String) {
        numberLabel.text = num
    }
    
}
