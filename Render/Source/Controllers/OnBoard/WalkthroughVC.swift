import UIKit

class WalkthroughVC: UIViewController {
    
    //@IBOutlet weak var scrollView: UIScrollView!
    //@IBOutlet weak var imgView: UIImageView!
    //@IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var onboardCollectionPage: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var topConst: NSLayoutConstraint!
    
    let pages: [WalkthroughPageModel] = [
        WalkthroughPageModel(imageName: "onboard_img_1", bgColor: "onboardBGColor1", title: "Manage your personal finance simply", description: "There are many features to manage your all personal finances."),
        WalkthroughPageModel(imageName: "onboard_img_2", bgColor: "onboardBGColor2", title: "Track all your financial records at one place", description: "There are many features to manage your all personal finances."),
        WalkthroughPageModel(imageName: "onboard_img_3", bgColor: "onboardBGColor1", title: "Get best planning suggestions by AI tool", description: "There are many features to manage your all personal finances.")
    ]
    
    var walkthroughbgColors: [String] = ["onboardBGColor1", "onboardBGColor2", "onboardBGColor1"]
    var walkthroughTitles: [String] = ["Manage your personal finance simply", "Track all your financial records at one place", "Get best planning suggestions by AI tool"]
    //var walkthroughDesc: [String] = ["There are many features to manage your all personal finances.", "There is many features to manage your all personal finances.", "There is many features to manage your all personal finances."]
    var counter: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            bottomConst.constant = 15
            topConst.constant = 0
        }else{
            //topConst.constant = 0
            bottomConst.constant = 34
        }
        
   
        if ApplicationPreference.getUsercompleteWalkthrough() == true {
            
            if ApplicationPreference.getUsercompleteProfile() == true {
                
                // let objVC: DashboardTabBarController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.dashboard, viewControllerName: DashboardTabBarController.nameOfClass)
                // self.navigationController?.pushViewController(objVC, animated: true)
                
                let objVC : EnterPinVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: EnterPinVC.nameOfClass)
                self.navigationController?.pushViewController(objVC, animated: true)
                
            } else{
                
                let objVC : LoginVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: LoginVC.nameOfClass)
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
        
 
        self.pageControl.numberOfPages = pages.count
        
    }
    
    @IBAction func pageChanged(_ sender: Any) {
        
        let pc = sender as! UIPageControl
        
        onboardCollectionPage.scrollToItem(at: IndexPath(item: pc.currentPage, section: 0), at: .centeredHorizontally, animated: true)
        
    }
    
    @IBAction func closeOnboard(_ sender: Any) {
        print("Closing")
        ApplicationPreference.saveUsercompleteWalkthrough(userInfo: true)
        let objVC : LoginVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: LoginVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
}

extension WalkthroughVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = onboardCollectionPage.dequeueReusableCell(withReuseIdentifier: "WalkthroughCell", for: indexPath) as! WalkthroughCell
        
        //let cell = onboardCollectionPage.dequeueReusableCell(withReuseIdentifier: "OnboardCell", for: indexPath)
        if indexPath.row == 1{
            let color1 = hexStringToUIColor(hex: "#65FBDA")
            cell.progressView.backgroundColor = color1

        }else{
//            let color2 = hexStringToUIColor(hex: "#117DF1")
//            cell.progressView.backgroundColor = color2//UIColor(red: 60, green: 165, blue: 254, alpha: 1.00)
        }
        cell.backgroundColor = UIColor(named: pages[indexPath.row].bgColor)
        cell.configureCell(page: pages[indexPath.row], indexPath: indexPath)
        cell.getStartedBtnAction = {
            ApplicationPreference.saveUsercompleteWalkthrough(userInfo: true)
            let objVC : LoginVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: LoginVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.onboardCollectionPage.frame.width, height: self.onboardCollectionPage.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.view.backgroundColor = UIColor(named: pages[pageControl.currentPage].bgColor)
        self.onboardCollectionPage.backgroundColor = UIColor(named: pages[pageControl.currentPage].bgColor)
    }
    
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        self.view.backgroundColor = UIColor(named: pages[pageControl.currentPage].bgColor)
        self.onboardCollectionPage.backgroundColor = UIColor(named: pages[pageControl.currentPage].bgColor)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

