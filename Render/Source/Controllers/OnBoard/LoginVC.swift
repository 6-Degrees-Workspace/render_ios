import UIKit

class LoginVC: BaseVC {
    
    //@IBOutlet weak var mobileNoTextField: UITextField!
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var txtMobileNumber : UITextField!
    @IBOutlet weak var nextButton: UIButton!
    var isForgot : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //mobileNoTextField.layer.borderWidth = 0.5
        //mobileNoTextField.layer.borderColor = (UIColor(named: "textFieldBorderColor") as! CGColor)
        placeholderView.layer.borderWidth = 1.0
        placeholderView.layer.borderColor = UIColor(named: "textFieldBorderColor")?.cgColor
        //placeholderView.layer.cornerRadius = 10.0
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtMobileNumber.text = txtMobileNumber.text?.trim()
        
        if (txtMobileNumber.text?.isEmpty)! {
            message = "Please enter the mobile number."
            isFound = false
        }else if (self.isValidPhone(phone: txtMobileNumber.text!) == false) {
            message = "Please enter the vaild mobile number."
            isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    
    @IBAction func actionSendOTP(_ sender: UIButton){
        
        if isValidFields() {
            self.sendOtpForLoginServiceCall()
            
            /*   let param = ["mobile_no":self.txtMobileNumber.text!] as [String : Any]
             APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.login, andParam: param, showHud: true, changeBaseUrl: true) { (responseDict) in
             print("password-->",responseDict)
             Helper.UI {
             //  if let responseBody = responseDict["responseBody"] as? [String: Any]{
             var msg = ""
             if let errorMessage = responseDict["errorMessage"] as? String {
             msg = errorMessage
             Helper.sharedInstance.showToast(isError: false, title: msg)
             self.dismiss(animated: true)
             } else if let successMessage = responseDict["successMessage"] as? String {
             msg = successMessage
             Helper.sharedInstance.showToast(isError: false, title: msg)
             }else{
             print("Something went wrong")
             //Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
             }
             }
             }*/
        }
    }
    
    // MARK: - Service Calls
    func sendOtpForLoginServiceCall() {
        self.showLoader()
        let parameter:[String:Any] = [
            "mobile_no": self.txtMobileNumber.text ?? ""
        ]
        let loginService = LoginService()
        loginService.sendOTPForLogin(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    var screenName = ""
                    print("responseData.data.isPinExist",responseData.data)
                    ApplicationPreference.saveToken(data: responseData.data.token)
                    //ApplicationPreference.saveUserData(data: responseData.data)
                    //Helper.sharedInstance.showToast(isError: true, title: responseData.data.otp)
                    
                    let encoder = JSONEncoder()
                    if let encoded = try? encoder.encode(responseData.data) {
                        let defaults = UserDefaults.standard
                        defaults.set(encoded, forKey: "SavedPerson")
                    }
                    
                    if self.isForgot == true {
                        screenName = "CreatePinVC"
                    }else{
                        if responseData.data.fullName != "" && responseData.data.isPinExist == "1" {
                            screenName = "DashboardVC"
                        }else if responseData.data.fullName == "" {
                            screenName = "ProfileVC"
                        }/*else if responseData.data.userProfilePic == "" {
                          let objVC : UploadPhotoVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: UploadPhotoVC.nameOfClass)
                          self.navigationController?.pushViewController(objVC, animated: true)
                          }*/else if responseData.data.isPinExist == "" {
                              screenName = "CreatePinVC"
                          }else{
                              screenName = "EnterPinVC"
                          }
                    }
                    
                    let objVC : VerifyOTPVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: VerifyOTPVC.nameOfClass)
                    objVC.otp = responseData.data.otp
                    objVC.passScreen = screenName
                    objVC.phone = self.txtMobileNumber.text ?? ""
                    self.navigationController?.pushViewController(objVC, animated: true)
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}

extension LoginVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMobileNumber {
            let maxLength = 10
            let currentString: NSString = txtMobileNumber.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print(textField.text)
        if txtMobileNumber.text != "" {
            self.nextButton.isEnabled = true
            self.nextButton.backgroundColor = UIColor(named: "buttonColor")
        }
    }
    
    func isValidPhone(phone: String) -> Bool {
        
        let phoneRegex = "^((0091)|(\\+91)|0?)[6789]{1}\\d{9}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }

}
