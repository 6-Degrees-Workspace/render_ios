import UIKit

class AddNetIncomeVC: BaseVC {
    
    var selectedCategory: String = "Select Category"
    var liabilityCat:[String] = [
        "Income",
        "Expenses"
    ]
    
    var picker: UIPickerView!
    @IBOutlet weak var addIncomeContainer: UIView!
    @IBOutlet weak var addExpenceContainer: UIView!
    @IBOutlet weak var addLiabiltityTxtFld: UITextField!
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var btnAdd: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfAddLoader(notification:)), name: Notification.Name("AddLoader"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfRemoveLoader(notification:)), name: Notification.Name("RemoveLoader"), object: nil)
        self.hideContainers()
    }
    
    override func viewDidLayoutSubviews() {
        scroller.isScrollEnabled = true
    }
    
    @IBAction func AddingIncomeVCPressed(_ sender: UIButton) {
        if selectedCategory != "Select Category" {
            if selectedCategory == "Income" {
                NotificationCenter.default.post(name: Notification.Name("AddIncome"), object: nil, userInfo: nil)
            }else{
                NotificationCenter.default.post(name: Notification.Name("AddExpense"), object: nil, userInfo: nil)
            }
        }
    }
    
    @objc func methodOfAddLoader(notification: Notification) {
        self.showLoader()
    }
    
    @objc func methodOfRemoveLoader(notification: Notification) {
        self.showLoader()
    }
}

extension AddNetIncomeVC: UITextFieldDelegate {
    func pickUP(_ textField: UITextField) {
        self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.selectRow(0, inComponent: 0, animated: true)
        //self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
        self.selectedCategory = self.liabilityCat[0] //self.mainCategoryModel?.data[0].categoryName ?? ""
        addLiabiltityTxtFld.inputView = self.picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        addLiabiltityTxtFld.inputAccessoryView = toolBar
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUP(addLiabiltityTxtFld)
    }
    
    
    @objc func selectClick() {
        // print(mainCategoryID)
        // self.getSubCategoriesServiceCall(categoryId: self.mainCategoryID)
        
        self.addLiabiltityTxtFld.text = selectedCategory
        
        if self.selectedCategory == "Income" {
            btnAdd.setTitle("Add", for: .normal)
            self.addIncomeContainer.isHidden = false
            self.addExpenceContainer.isHidden = true
        }else if self.selectedCategory == "Expenses" {
            btnAdd.setTitle("Next", for: .normal)
            self.addIncomeContainer.isHidden = true
            self.addExpenceContainer.isHidden = false
        }else {
            self.hideContainers()
        }
        //self.addPersonalContainer.isHidden = true
        addLiabiltityTxtFld.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        addLiabiltityTxtFld.resignFirstResponder()
    }
    
    func hideContainers() {
        self.addIncomeContainer.isHidden = true
        self.addExpenceContainer.isHidden = true        
    }
}


extension AddNetIncomeVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.liabilityCat.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        //let objDict = self.mainCategoryModel?.data[row]
        label.text = self.liabilityCat[row] //objDict?.categoryName//assetCategory[row]
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedCategory = self.liabilityCat[row]
    }
    
}

