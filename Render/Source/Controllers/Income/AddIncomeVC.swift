import UIKit

class AddIncomeVC: BaseVC {
    
    var selectedIncomeType: String = ""
    var selectedIncomeTypeId: String = ""
    var picker: UIPickerView!
    var getAllIncomeTypesData : GetAllIncomeTypesData?
    var mainCategoryModel : MainCategoryModel?
    var update: Bool = false
    var incomeData: IncomeDatum?
    var categoryId: String = ""
    var selectedCategory: String = ""
    var addDefault: Bool = false
    
    @IBOutlet weak var txtIncomeType: UITextField!
    @IBOutlet weak var txtEmployee: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtPayFreq: UITextField!
    @IBOutlet weak var txtEmployerName: UITextField!
    @IBOutlet weak var btnAddIncome: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if addDefault {
            self.txtIncomeType.isEnabled = false
            self.txtIncomeType.text = selectedCategory
        }
        
        if update {
            self.txtIncomeType.isEnabled = false
            txtIncomeType.textColor = UIColor(named: "textGrayColor")
            if incomeData?.incomeAmount.length ?? 0 > 9{
                self.txtAmount.text = incomeData?.incomeAmount.replacingOccurrences(of: ".00", with: "")
            }else{
                self.txtAmount.text = incomeData?.incomeAmount
            }
            self.btnAddIncome.setTitle("Update Income", for: .normal)
            self.txtIncomeType.text = incomeData?.categoryName
            self.txtEmployee.text = incomeData?.employeeName
            //self.txtAmount.text = incomeData?.incomeAmount
            self.txtPayFreq.text = incomeData?.payFrequency
            self.txtEmployerName.text = incomeData?.employerName
            
        }
        //self.getAllIncomeTypeServiceCall()
        self.getAllCategoriesServiceCall()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("AddIncome"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        if self.isValidFields() {
            self.addIncomeServiceCall()
        }
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtIncomeType.text = txtIncomeType.text?.trim()
        self.txtEmployee.text = self.txtEmployee.text?.trim()
        self.txtEmployerName.text = self.txtEmployerName.text?.trim()
        self.txtPayFreq.text = self.txtPayFreq.text?.trim()
        self.txtAmount.text = self.txtAmount.text?.trim()
        
        if (txtIncomeType.text?.isEmpty)! {
            message = "Please Select Income Type"
            isFound = false
        } else if (txtEmployee.text?.isEmpty)! {
            message = "Please Enter Employee Name"
            isFound = false
        } else if (txtEmployee.text?.length ?? 0 < 3){
            message = "The Employee Name must be at least 3 characters in length"
            isFound = false
        } else if (txtEmployerName.text?.isEmpty)! {
            message = "Please Enter Employer Name"
            isFound = false
        } else if (txtEmployerName.text?.length ?? 0 < 3){
            message = "The Employer Name must be at least 3 characters in length"
            isFound = false
        } else if (txtPayFreq.text?.isEmpty)! {
            message = "Please Enter Pay Freq"
            isFound = false
        }else if (txtPayFreq.text?.length ?? 0 < 3){
            message = "Pay Freq must be at least 3 characters in length"
            isFound = false
        } else if (txtAmount.text?.isEmpty)! {
            message = "Please Enter Amount"
            isFound = false
        }else if (txtAmount.text == "0" || txtAmount.text == "00" || txtAmount.text == "000" || txtAmount.text == "0000" || txtAmount.text == "00000" || txtAmount.text == "000000") {
            message = "Please Enter Vaild Amount"
            isFound = false
        } else if (txtAmount.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        }
        
        
        
        if !isFound {
           // Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func actionAddIncome(_ sender: Any) {
        self.view.endEditing(true)
        
        
        if self.btnAddIncome.titleLabel?.text == "Update Income"{
            if self.isValidFields() {
                self.updateIncomeServiceCall()
            }
        }else{
            if self.isValidFields() {
                self.addIncomeServiceCall()
            }
        }
    }
    
}


//MARK: - UIPicker Delegates
extension AddIncomeVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.mainCategoryModel?.data.count ?? 0
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.text = self.mainCategoryModel?.data[row].categoryName
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedIncomeType = self.mainCategoryModel?.data[row].categoryName ?? ""
        self.selectedIncomeTypeId = self.mainCategoryModel?.data[row].categoryCode ?? ""
    }
}


extension AddIncomeVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters : CharacterSet
        if textField == txtEmployee || textField == txtPayFreq || textField == txtEmployerName  {
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_letter).inverted
        }else{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_special_letter).inverted
        }
        
        let components = string.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        
        
        // Allow to remove character (Backspace)
        if textField == txtAmount{
            if string == "" {
                return true
            }
            
            // Block multiple dot
            if (textField.text?.contains("."))! && string == "." {
                return false
            }
            
            
            if (textField.text?.contains("."))! {
                let limitDecimalPlace = 2
                let decimalPlace = textField.text?.components(separatedBy: ".").last
                if (decimalPlace?.count)! < limitDecimalPlace {
                    return true
                }
                else {
                    return false
                }
            }
        }
          
        if textField == txtEmployee {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtEmployee.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
        }
        
        if textField == txtAmount {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtAmount.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtPayFreq {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtPayFreq.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
           
            
        }
        
        if textField == txtEmployerName {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtEmployerName.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
        }
        return true
    }
    
    func pickUP(_ textField: UITextField) {
        
        self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.selectRow(0, inComponent: 0, animated: true)
        //self.selectedIncomeType = self.getAllIncomeTypesData?.incomeTypes[0].incomeTypeName ?? ""
        self.selectedIncomeType = self.mainCategoryModel?.data[0].categoryName ?? ""
        self.selectedIncomeTypeId = self.mainCategoryModel?.data[0].categoryCode ?? ""
        // self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
        // self.selectedCategory = self.mainCategoryModel?.data[0].categoryName ?? ""
        txtIncomeType.inputView = self.picker
        //
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        //
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtIncomeType.inputAccessoryView = toolBar
        
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtIncomeType{
            self.pickUP(txtIncomeType)
        }
    }
    
    @objc func selectClick() {
        
        self.txtIncomeType.text = self.selectedIncomeType
        txtIncomeType.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtIncomeType.resignFirstResponder()
    }
    
    @objc func selectTimeTypeClick() {
        
        self.txtIncomeType.text = self.selectedIncomeType
        txtIncomeType.resignFirstResponder()
    }
    
    @objc func cancelTimeTypeClick() {
        txtIncomeType.resignFirstResponder()
    }
}


//MARK: - Service Calls
extension AddIncomeVC {
    
    func getAllIncomeTypeServiceCall() {

        let parameter:[String:Any] = [
            "": "",
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.getAllIncomeTypes(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.getAllIncomeTypesData = responseData
                    //self.navigationController?.popViewController(animated: false)
                    //self.showAlert(message: responseData.message)
                }else{
                    //self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            
            NotificationCenter.default.post(name: Notification.Name("RemoveLoader"), object: nil, userInfo: nil)
            //self.removLoader()
            //self.loadingView.isHidden = true
        }
    }
    
    func updateIncomeServiceCall() {

        self.showLoader()
        let parameter:[String:Any] = [
            "income_id": incomeData?.incomeID ?? "",
            "employer_name": self.txtEmployerName.text ?? "",
            "employee_name":self.txtEmployee.text ?? "",
            "income_amount": self.txtAmount.text ?? "",
            "pay_frequency": self.txtPayFreq.text ?? "",
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.editIncome(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            
            self.removLoader()
        }
        
    }
    
    
    func addIncomeServiceCall() {

        self.txtIncomeType.text = txtIncomeType.text?.trim()
        self.txtEmployee.text = self.txtEmployee.text?.trim()
        self.txtEmployerName.text = self.txtEmployerName.text?.trim()
        self.txtPayFreq.text = self.txtPayFreq.text?.trim()
        self.txtAmount.text = self.txtAmount.text?.trim()
        
        //NotificationCenter.default.post(name: Notification.Name("AddLoader"), object: nil, userInfo: nil)
        self.showLoader()
        let parameter:[String:Any] = [
            
            "category_code": self.selectedIncomeTypeId,
            "employer_name": self.txtEmployerName.text ?? "",
            "employee_name": self.txtEmployee.text ?? "",
            "pay_frequency": self.txtPayFreq.text ?? "",
            "income_amount": self.txtAmount.text ?? ""
     
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.addIncome(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            
           // NotificationCenter.default.post(name: Notification.Name("RemoveLoader"), object: nil, userInfo: nil)
            self.removLoader()
            //self.loadingView.isHidden = true
        }
    }
    
    // MARK: - Get All Categories Service Call
    func getAllCategoriesServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "type": "INCOME",
        ]
        let netWorthService = NetWorthService()
        netWorthService.getMasterCategories(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.mainCategoryModel = responseData
                    //self.netIncomeTable.reloadData()
                    //self.assetCategoryPicker.reloadAllComponents()
                    //print(responseData.data[1].categoryID)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}

