import UIKit
import Alamofire

class EssentialExpensesVC: BaseVC {
    
    @IBOutlet weak var netIncomeTable: UITableView!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnAddExpenses: UIButton!
    @IBOutlet weak var lblNavTitle: UILabel!
    
    var getAllExpenseTypesModel : GetAllExpenseTypesModel?
    var categoryId : String = ""
    var selectedIndex:IndexPath?
    var selectedExpenseType: String = ""
    var update: Bool = false
    var expenseData: ExpenseDatum?
    //var categoryId: String = ""
    var selectedCategory: String = ""
    var selectedCategoryID: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblNavTitle.text = selectedCategoryID
        if update {
            if expenseData?.expenseAmount.length ?? 0 > 9{
                self.txtAmount.text = expenseData?.expenseAmount.replacingOccurrences(of: ".00", with: "")
            }else{
                self.txtAmount.text = expenseData?.expenseAmount
            }
            
            //netIncomeTable.isUserInteractionEnabled = false
            self.btnAddExpenses.setTitle("Update Expenses", for: .normal)
            //self.txtAmount.text = expenseData?.expenseAmount
        }
        self.getAllExpenseTypesServiceCall()
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtAmount.text = txtAmount.text?.trim()
        
        if (txtAmount.text?.isEmpty)! {
            message = "Please Enter Amount"
            isFound = false
        }else if (selectedExpenseType == "") {
            message = "Please Select Essential Expense Type"
            isFound = false
        }else if (txtAmount.text == "0" || txtAmount.text == "00" || txtAmount.text == "000" || txtAmount.text == "0000" || txtAmount.text == "00000" || txtAmount.text == "000000") {
            message = "Please Enter Vaild Amount"
            isFound = false
        }else if (txtAmount.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        }
        
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func actionAddExpenses(_ sender: Any) {
        //self.view.endEditing(true)
        
        if self.btnAddExpenses.titleLabel?.text == "Update Expenses"{
           // if self.isValidFields() {
                self.updateExpenseTypesServiceCall()
           // }
        }else{
            if self.isValidFields() {
                self.addExpenseTypesServiceCall()
            }
        }
//        if update {
//            if self.isValidFields() {
//                self.updateExpenseTypesServiceCall()
//            }
//        }else{
//            if self.isValidFields() {
//                self.addExpenseTypesServiceCall()
//            }
//        }
    }
    
    // MARK: - Get All Categories Service Call
    func getAllExpenseTypesServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id": categoryId,
        ]
        let netIncomeService = NetIncomeService()
        netIncomeService.getAllExpenseTypes(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.getAllExpenseTypesModel = responseData
                    self.netIncomeTable.reloadData()
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
    // MARK: - Get All Categories Service Call
    func addExpenseTypesServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id":categoryId,
            "expense_type": selectedExpenseType,
            "expense_amount": txtAmount.text ?? ""
        ]
        let netIncomeService = NetIncomeService()
        netIncomeService.addExpense(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    if responseData.status == 200{
                        let objVC: DashboardTabBarController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.dashboard, viewControllerName: DashboardTabBarController.nameOfClass)
                        objVC.selectedIndex = 2
                        self.navigationController?.pushViewController(objVC, animated: false)
                        
                        NotificationCenter.default.post(name: Notification.Name("Expense"), object: nil, userInfo: nil)
                    }
                    //self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func updateExpenseTypesServiceCall (){
        self.showLoader()
        var expenseTypeId : String = ""
        if (selectedExpenseType == "") {
            expenseTypeId = expenseData?.expenseTypeId ?? ""
        }else{
            expenseTypeId = selectedExpenseType
        }
        
        let parameter:[String:Any] = [
            "expense_amount": txtAmount.text ?? "",
            "expense_type": expenseTypeId,
            "expense_id": expenseData?.expenseID ?? ""
        ]
        let netIncomeService = NetIncomeService()
        netIncomeService.editExpense(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    if responseData.status == 200{
                        NotificationCenter.default.post(name: Notification.Name("Expense"), object: nil, userInfo: nil)
                        print("responseData",responseData)
                        // Helper.sharedInstance.showToast(isError: true, title: "Expense update successfully")
                        let objVC: DashboardTabBarController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.dashboard, viewControllerName: DashboardTabBarController.nameOfClass)
                        objVC.selectedIndex = 2
                        self.navigationController?.pushViewController(objVC, animated: false)
                        NotificationCenter.default.post(name: Notification.Name("Expense"), object: nil, userInfo: nil)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}

//MARK: - UITableView
extension EssentialExpensesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getAllExpenseTypesModel?.data.expenseTypes.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return UITableView.automaticDimension
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = netIncomeTable.dequeueReusableCell(withIdentifier: "netIncomeCell", for: indexPath) as! NetWorthTableViewCell
        
        let objDict = self.getAllExpenseTypesModel?.data.expenseTypes[indexPath.row]
        cell.lblExName.text = objDict?.expenseTypeName
        cell.logoEx.sd_setImage(with: URL(string: objDict?.expenseTypeImage ?? ""), placeholderImage: UIImage(named: "placeholder"))
        if (selectedIndex == indexPath) {
            cell.imageExName.image = UIImage(named: "radio_select")
        } else {
            cell.imageExName.image = UIImage(named: "radio_unselect")
        }
        
        if update {
            if expenseData?.expenseType == objDict?.expenseTypeName{
                cell.imageExName.image = UIImage(named: "radio_select")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if update {
        //
        //        }else{
        // tableView.deselectRowAtIndexPath(indexPath, animated: true)
        // let row = indexPath.row
        // println(countryArray[row])
        update = false
        let objDict = self.getAllExpenseTypesModel?.data.expenseTypes[indexPath.row]
        selectedIndex = indexPath
        selectedExpenseType = objDict?.expenseTypeID ?? ""
        netIncomeTable.reloadData()
        //}
    }
    
    
}


extension EssentialExpensesVC: UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // Allow to remove character (Backspace)
        if string == "" {
            return true
        }
        
        // Block multiple dot
        if (textField.text?.contains("."))! && string == "." {
            return false
        }
        
        if (textField.text?.contains("."))! {
            let limitDecimalPlace = 2
            let decimalPlace = textField.text?.components(separatedBy: ".").last
            if (decimalPlace?.count)! < limitDecimalPlace {
                return true
            }
            else {
                return false
            }
        }
        
        if textField == txtAmount {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtAmount.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }
    
}
