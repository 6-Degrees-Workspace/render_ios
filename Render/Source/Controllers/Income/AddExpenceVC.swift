import UIKit

class AddExpenceVC: BaseVC {
    
    @IBOutlet weak var netIncomeTable: UITableView!
    
    var mainCategoryModel : MainCategoryModel?
    var selectedIndex:IndexPath?
    var selectedExpenseType: String = ""
    var selectedType: String = ""
    var selectedCategory: String = ""
    var addDefault: Bool = false
    var selectedIncomeTypeId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if addDefault {
            print("selectedIncomeTypeId",selectedIncomeTypeId)
            print("selectedCategory",selectedCategory)
            selectedType = selectedCategory
            selectedExpenseType = selectedIncomeTypeId
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("AddExpense"), object: nil)
        self.getAllCategoriesServiceCall()
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        if self.isValidFields() {
            
            let objVC: EssentialExpensesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: EssentialExpensesVC.nameOfClass)
            objVC.categoryId = selectedExpenseType
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func AddingIncomeVCPressed(_ sender: UIButton) {
        if self.isValidFields() {
            let objVC: EssentialExpensesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: EssentialExpensesVC.nameOfClass)
            objVC.categoryId = selectedExpenseType
            objVC.selectedCategoryID = selectedType
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func isValidFields()->Bool{
        
        //print("datePicker", datePicker.date)
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        
        if (selectedExpenseType == "") {
            message = "Please selected expense type."
            isFound = false
        }
        
        
        if !isFound {
           // Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
}



//MARK: - UITableView
extension AddExpenceVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mainCategoryModel?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return UITableView.automaticDimension
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = netIncomeTable.dequeueReusableCell(withIdentifier: "netIncomeCell", for: indexPath) as! NetWorthTableViewCell
        let objDict = self.mainCategoryModel?.data[indexPath.row]
        cell.lblExName.text = objDict?.categoryName
        cell.imageExLogoName.sd_setImage(with: URL(string: objDict?.categoryImageUrl ?? ""), placeholderImage: UIImage(named: "placeholder"))
        
        if (selectedIndex == indexPath) {
            cell.imageExName.image = UIImage(named: "radio_select")
        } else {
            cell.imageExName.image = UIImage(named: "radio_unselect")
        }
        
        if addDefault {
            if selectedCategory == objDict?.categoryName{
                cell.imageExName.image = UIImage(named: "radio_select")
            }
            
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath
        netIncomeTable.reloadData()
        
        let objDict = self.mainCategoryModel?.data[indexPath.row]
        selectedExpenseType = objDict?.categoryID ?? ""
        selectedType = objDict?.categoryName ?? ""
        
    }
    
    
    // MARK: - Get All Categories Service Call
    func getAllCategoriesServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "type": "EXPENSE",
        ]
        let netWorthService = NetWorthService()
        netWorthService.getMasterCategories(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.mainCategoryModel = responseData
                    self.netIncomeTable.reloadData()
                    //self.assetCategoryPicker.reloadAllComponents()
                    //print(responseData.data[1].categoryID)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}



