//
//  IncomeVC.swift
//  Render
//
//  Created by Anis Agwan on 10/12/21.
//

import UIKit

class IncomeVC: BaseVC {
    
    @IBOutlet weak var tableHeaderLbl: UILabel!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var incomeDifference: UILabel!
    @IBOutlet weak var incomeAmt: UILabel!
    @IBOutlet weak var incomeSmallHdr: UILabel!
    @IBOutlet weak var incomeTable: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var btnAddView: UIView!
    
    var navTitle: String = ""
    var categoryId: String = ""
    var categoryName: String = ""
    var categoryCode: String = ""
    var getAllIncomeDetailByCategoryModel : GetAllIncomeDetailByCategoryModel?
    var getAllExpenseDetailByCategoryModel : GetAllExpenseDetailByCategoryModel?
    var getAllPlaidExpenseDetailByCategoryModel : GetAllPlaidExpenseDetailByCategoryModel?
    var getAllPlaidIncomeDetailByCategoryModel : GetAllPlaidIncomeDetailByCategoryModel?
    var incomeSubCategory: [String] = [ ]
    var DicAsset = [String:[IncomeDatum]]()
    var DicExpens = [String:[ExpenseDatum]]()
    var expensePlaidData = [String:[ExpensePlaidData]]()
    var incomePlaidData = [String:[IncomePlaidData]]()
    var data: String = "Manual"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lbl_NavigationTitle.text = navTitle
        switch navTitle {
        case "Income":
            self.getAllIncomeDetailByCategoryServiceCall()
        case "Expense":
            self.lbl_NavigationTitle.text =  categoryName
            self.getAllIncomeDetailByCategoryForExpenseServiceCall()
        default: break
        }
    }
    
    @IBAction func dataChanged(sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            btnAddView.isHidden = false
            data = "Manual"
            NSLog("manual selected")
            switch navTitle {
            case "Income":
                self.getAllIncomeDetailByCategoryServiceCall()
            case "Expense":
                self.lbl_NavigationTitle.text =  categoryName
                self.getAllIncomeDetailByCategoryForExpenseServiceCall()
            default: break
            }
        case 1:
            btnAddView.isHidden = true
            data = "Plaid"
            NSLog("Plaid selected")
            switch navTitle {
            case "Income":
                //self.getAllIncomeDetailByCategoryServiceCall()
                self.getAllPlaidIncomeDetailByCategoryServiceCall()
            case "Expense":
                self.lbl_NavigationTitle.text =  categoryName
                self.getAllPlaidExpenseDetailByCategoryServiceCall()
            default: break
            }
        default:
            break;
        }
    }
    
}

//MARK: - TableView
//MARK: - TableView for Stocks and more
extension IncomeVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch navTitle {
        case "Income":
            return 0
        case "Expense":
            return 0
        default: break
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if data == "Manual"{
            switch navTitle {
            case "Income":
                if #available(iOS 14.0, *) {
                    let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
                    let label = UILabel(frame: CGRect(x:10, y:10, width:tableView.frame.size.width, height:18))
                    label.font = UIFont.systemFont(ofSize: 18)
                    label.text = incomeSubCategory[section]
                    self.lbl_NavigationTitle.text = incomeSubCategory[section]
                    view.addSubview(label);
                    view.backgroundColor = UIColor.white;
                    return view
                } else {
                    let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
                    let label = UILabel(frame: CGRect(x:10, y:0, width:tableView.frame.size.width, height:18))
                    label.font = UIFont.systemFont(ofSize: 18)
                    //label.text = incomeSubCategory[section]
                    self.lbl_NavigationTitle.text = incomeSubCategory[section]
                    view.addSubview(label);
                    view.backgroundColor = UIColor.white;
                    return view
                }
            case "Expense":
                return nil
                
            default: break
            }

        }else{
            switch navTitle {
            case "Income":
                if #available(iOS 14.0, *) {
                    let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
                    let label = UILabel(frame: CGRect(x:10, y:10, width:tableView.frame.size.width, height:18))
                    label.font = UIFont.systemFont(ofSize: 18)
                    label.text = incomeSubCategory[section]
                    if categoryCode == "OTHER" {
                        self.lbl_NavigationTitle.text = "Other"
                    }else{
                        self.lbl_NavigationTitle.text = incomeSubCategory[section]
                    }
                    view.addSubview(label);
                    view.backgroundColor = UIColor.white;
                    return view
                } else {
                    let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
                    let label = UILabel(frame: CGRect(x:10, y:0, width:tableView.frame.size.width, height:18))
                    label.font = UIFont.systemFont(ofSize: 18)
                    if categoryCode == "OTHER" {
                        self.lbl_NavigationTitle.text = "Other"
                    }else{
                        self.lbl_NavigationTitle.text = incomeSubCategory[section]
                    }
                  
                    view.addSubview(label);
                    view.backgroundColor = UIColor.white;
                    return view
                }
            case "Expense":
                return nil
                
            default: break
            }

        }
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if data == "Manual"{
            switch navTitle {
            case "Income":
                return self.DicAsset.keys.count
            case "Expense":
                return self.DicExpens.keys.count
            default: break
            }
        }else{
            switch navTitle {
            case "Income":
                //return self.incomePlaidData.keys.count
                return 1
            case "Expense":
                return self.expensePlaidData.keys.count
            default: break
            }
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data == "Manual"{
            switch navTitle {
            case "Income":
                let cate = incomeSubCategory[section]
                return self.DicAsset[cate]?.count ?? 0
            case "Expense":
                let cate = incomeSubCategory[section]
                return self.DicExpens[cate]?.count ?? 0
            default: break
            }
        }else{
            switch navTitle {
            case "Income":
//                let cate = incomeSubCategory[section]
//                return self.incomePlaidData[cate]?.count ?? 0
                return self.getAllPlaidIncomeDetailByCategoryModel?.data.incomeData.count ?? 0
            case "Expense":
                let cate = incomeSubCategory[section]
                return self.expensePlaidData[cate]?.count ?? 0
            default: break
            }
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = incomeTable.dequeueReusableCell(withIdentifier: "incomeCell", for: indexPath) as! IncomeTableCell
        
        if data == "Manual"{
            switch navTitle {
            case "Income":
                
                cell.btnDelete.isHidden = false
                cell.btnShowDelete.isHidden = false
                
                let key = incomeSubCategory[indexPath.section]
                let category = self.DicAsset[key]
                let objDict = category?[indexPath.row]
              
                cell.incomeName.text = objDict?.categoryName
                cell.incomeAmt.text = "\(objDict!.incomeAmount)".convertDoubleToCurrency()//"$"+"\(objDict!.incomeAmount)"
                cell.incomeImg.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
                cell.incomeDate.text = DateTimeUtils.sharedInstance.getSeenAtDateFromString(strDate: objDict!.incomeDate)

                let assetId : Int = Int (objDict?.incomeID ?? "") ?? 0
                cell.btnDelete.tag =  assetId
                cell.btnDelete.addTarget(self, action: #selector(self.actionDelete(_:)), for: UIControl.Event.touchUpInside)
                return cell
            case "Expense":
                
                cell.btnDelete.isHidden = false
                cell.btnShowDelete.isHidden = false
                
                let key = incomeSubCategory[indexPath.section]
                let category = self.DicExpens[key]
                let objDict = category?[indexPath.row]
              
                cell.incomeName.text = objDict?.expenseType
                cell.incomeAmt.text = "\(objDict!.expenseAmount)".convertDoubleToCurrency()//"$"+"\(objDict!.expenseAmount)"
                cell.incomeImg.sd_setImage(with: URL(string: objDict?.expenseTypeImage ?? ""), placeholderImage: UIImage(named: "placeholder"))
                cell.incomeDate.text = DateTimeUtils.sharedInstance.getSeenAtDateFromString(strDate: objDict!.expenseDate)
                
                let assetId : Int = Int (objDict?.expenseID ?? "") ?? 0
                cell.btnDelete.tag =  assetId
                cell.btnDelete.addTarget(self, action: #selector(self.actionDelete(_:)), for: UIControl.Event.touchUpInside)
                return cell
            default: break
            }
            
        }else{
            switch navTitle {
            case "Income":
                
                cell.btnDelete.isHidden = true
                cell.btnShowDelete.isHidden = true
                
//                let key = incomeSubCategory[indexPath.section]
//                let category = self.incomePlaidData[key]
//                let objDict = category?[indexPath.row]
                let objDict = self.getAllPlaidIncomeDetailByCategoryModel?.data.incomeData[indexPath.row]
                
                cell.incomeName.text = objDict?.categoryName
                cell.incomeAmt.text = "\(objDict!.incomeAmount)".convertDoubleToCurrency()//"$"+"\(objDict!.incomeAmount)"
                cell.incomeImg.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
                cell.incomeDate.text = DateTimeUtils.sharedInstance.getSeenAtDateFromString(strDate: objDict!.incomeDate)

//                let assetId : Int = Int (objDict?.incomeID ?? "") ?? 0
//                cell.btnDelete.tag =  assetId
//                cell.btnDelete.addTarget(self, action: #selector(self.actionDelete(_:)), for: UIControl.Event.touchUpInside)
                return cell
            case "Expense":
                
                cell.btnDelete.isHidden = true
                cell.btnShowDelete.isHidden = true
                
                let key = incomeSubCategory[indexPath.section]
                let category = self.expensePlaidData[key]
                let objDict = category?[indexPath.row]
                
                cell.incomeName.text = objDict?.expenseType
                cell.incomeAmt.text = "\(objDict!.expenseAmount)".convertDoubleToCurrency()//"$"+"\(objDict!.expenseAmount)"
                cell.incomeImg.sd_setImage(with: URL(string: objDict?.expenseTypeImage ?? ""), placeholderImage: UIImage(named: "placeholder"))
                cell.incomeDate.text = DateTimeUtils.sharedInstance.getSeenAtDateFromString(strDate: objDict!.expenseDate)
               
                return cell
            default: break
            }
        }
      
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if data == "Manual"{
            switch navTitle {
            case "Income":
                let key = incomeSubCategory[indexPath.section]
                let category = self.DicAsset[key]
                let objDict = category?[indexPath.row]
                let objVC: AddIncomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: AddIncomeVC.nameOfClass)
                objVC.update = true
                //objVC.selectedCategory = assetCategory
                objVC.incomeData = objDict
                objVC.categoryId = categoryId
                self.navigationController?.pushViewController(objVC, animated: true)
            case "Expense":
                let key = incomeSubCategory[indexPath.section]
                let category = self.DicExpens[key]
                let objDict = category?[indexPath.row]
                let objVC: EssentialExpensesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: EssentialExpensesVC.nameOfClass)
                objVC.update = true
                //objVC.selectedCategory = assetCategory
                objVC.expenseData = objDict
                objVC.categoryId = categoryId
                self.navigationController?.pushViewController(objVC, animated: true)
            default: break
            }
        }
    }
    
    @IBAction func actionAdd(_ sender: UIButton){
        
        switch navTitle {
        case "Income":
            let objVC: AddIncomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: AddIncomeVC.nameOfClass)
            objVC.addDefault = true
            objVC.selectedCategory = categoryName
            objVC.selectedIncomeTypeId = categoryCode
            self.navigationController?.pushViewController(objVC, animated: true)
        case "Expense":
            let objVC: AddExpenceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: AddExpenceVC.nameOfClass)
            objVC.addDefault = true
            objVC.selectedCategory = categoryName
            objVC.selectedIncomeTypeId = categoryId
            self.navigationController?.pushViewController(objVC, animated: true)
        default: break
        }
        
    }
    
    @IBAction func actionDelete(_ sender: UIButton){
        
        switch navTitle {
        case "Income":
            let alert = UIAlertController(title: "Render", message: "Are you sure to delete this item?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {_ in
                
            }))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: {_ in
                self.deleteIncomeServiceCall(incomeId: String(sender.tag))
            }))
            self.present(alert, animated: true, completion: nil)
        case "Expense":
            
            
            let alert = UIAlertController(title: "Render", message: "Are you sure to delete this item?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {_ in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: {_ in
                self.deleteExpenseServiceCall(expenseId: String(sender.tag))
            }))
            self.present(alert, animated: true, completion: nil)
        default: break
        }
    }
}

extension IncomeVC {
    
    func getAllIncomeDetailByCategoryForExpenseServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id": categoryId
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.getAllExpenseDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.incomeAmt.text = "\(responseData.data.plaidAndManual)".convertDoubleToCurrency()
                    
                    self.getAllExpenseDetailByCategoryModel = responseData
                    self.DicExpens = Dictionary(grouping: ((((self.getAllExpenseDetailByCategoryModel?.data.expenseData)!)))) { $0.categoryName }
                    self.getAllExpenseDetailByCategoryModel?.data.expenseData.forEach { subcategoryName in
                        self.incomeSubCategory.append(subcategoryName.categoryName)
                    }
                    
                    self.incomeSubCategory = NSOrderedSet(array: self.incomeSubCategory).map({ $0 as! String })
                    self.incomeTable.reloadData()
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
    func getAllIncomeDetailByCategoryServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id": categoryId
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.getAllIncomeDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.incomeAmt.text = "\(responseData.data.plaidAndManual)".convertDoubleToCurrency()
                    self.getAllIncomeDetailByCategoryModel = responseData
                    self.DicAsset = Dictionary(grouping: ((self.getAllIncomeDetailByCategoryModel?.data.incomeData)!)) { $0.categoryName }
                    self.getAllIncomeDetailByCategoryModel?.data.incomeData.forEach { subcategoryName in
                        self.incomeSubCategory.append(subcategoryName.categoryName)
                    }
                    self.incomeSubCategory = NSOrderedSet(array: self.incomeSubCategory).map({ $0 as! String })
                    self.incomeTable.reloadData()
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func deleteIncomeServiceCall(incomeId: String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "income_id": incomeId
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.deleteIncome(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.showAlert(message: responseData.message)
                    self.getAllIncomeDetailByCategoryServiceCall()
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func deleteExpenseServiceCall(expenseId: String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "expense_id": expenseId
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.deleteExpense(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.showAlert(message: responseData.message)
                    self.getAllIncomeDetailByCategoryForExpenseServiceCall()
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func getAllPlaidIncomeDetailByCategoryServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id": categoryId
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.getAllPlaidIncomeDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.incomeAmt.text = "\(responseData.data.totalIncomes)".convertDoubleToCurrency()
                    self.getAllPlaidIncomeDetailByCategoryModel = responseData
//                    self.incomePlaidData = Dictionary(grouping: ((self.getAllPlaidIncomeDetailByCategoryModel?.data.incomeData)!)) { $0.categoryName }
//                    self.getAllPlaidIncomeDetailByCategoryModel?.data.incomeData.forEach { subcategoryName in
//                        self.incomeSubCategory.append(subcategoryName.categoryName)
//                    }
//                    self.incomeSubCategory = NSOrderedSet(array: self.incomeSubCategory).map({ $0 as! String })
                    self.incomeTable.reloadData()
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func getAllPlaidExpenseDetailByCategoryServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_id": categoryId
        ]
        
        let netIncomeService = NetIncomeService()
        netIncomeService.getAllPlaidExpenseDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.incomeAmt.text = "\(responseData.data.totalExpenses)".convertDoubleToCurrency()
                    self.getAllPlaidExpenseDetailByCategoryModel = responseData
                    self.expensePlaidData = Dictionary(grouping: ((self.getAllPlaidExpenseDetailByCategoryModel?.data.expenseData)!)) { $0.categoryName }
                    
                    self.getAllPlaidExpenseDetailByCategoryModel?.data.expenseData.forEach { subcategoryName in
                        self.incomeSubCategory.append(subcategoryName.categoryName)
                    }
                    self.incomeSubCategory = NSOrderedSet(array: self.incomeSubCategory).map({ $0 as! String })
                    self.incomeTable.reloadData()
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}

