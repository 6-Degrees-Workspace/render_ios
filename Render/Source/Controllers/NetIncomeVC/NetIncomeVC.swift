
import UIKit
import Charts

class NetIncomeVC: UIViewController {
    
    // Variables
    var viewToDisplay = "Income"
    var income: [String] = []
    var incomeValues: [Double] = []
    var expenses: [String] = []
    var expenseValues: [Double] = []
    var colors: [UIColor?] = [
        UIColor(named: "chartColor1"),
        UIColor(named: "chartColor2"),
        UIColor(named: "chartColor3"),
        UIColor(named: "chartColor4"),
        UIColor(named: "chartColor5"),
        UIColor(named: "chartColor6")
    ]
    
    var getAllIncomesModel : GetAllIncomesModel?
    var getAllExpensesModel : GetAllExpensesModel?
    var startDate = Date()
    var endDate = Date()
    // IBOutlets
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var incomeButton: UIButton!
    @IBOutlet weak var expensesButton: UIButton!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var netIncomeTable: UITableView!
    @IBOutlet weak var netIncomeTableHeight: NSLayoutConstraint!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    @IBOutlet weak var segmentedControlFilter: UISegmentedControl!
    
    //View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewToDisplay = "Income"
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Expense"), object: nil)
        if #available(iOS 13.4, *) {
            self.startDateTextField.datePicker(target: self, doneAction: #selector(startDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: "")
            self.endDateTextField.datePicker(target: self, doneAction: #selector(endDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: "")
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.endDateTextField.text = DateTimeUtils.sharedInstance.getCurrentsDate()
        self.startDateTextField.text = DateTimeUtils.sharedInstance.thirtyDaysBeforeToday()
        
        startDate = DateTimeUtils.sharedInstance.convertDate(inputDate: self.startDateTextField.text ?? "")
        endDate = DateTimeUtils.sharedInstance.convertDate(inputDate: self.endDateTextField.text ?? "")
        
        
        self.netIncomeTable.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        if self.viewToDisplay == "Income" {
            incomeButton.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            expensesButton.setTitleColor(UIColor(named: "primaryGrey"), for: .normal)
            self.viewToDisplay = "Income"
            self.expensesButton.titleLabel?.textColor = UIColor(named: "primaryGrey")
            //self.createChart(dataPoints: income, values: incomeValues, title: viewToDisplay)
            self.netIncomeTable.reloadData()
            self.getAllIncomes(days: "30")
            print(self.viewToDisplay)
        }else{
            incomeButton.setTitleColor(UIColor(named: "primaryGrey"), for: .normal)
            expensesButton.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            self.viewToDisplay = "Expense"
            self.incomeButton.titleLabel?.textColor = UIColor(named: "primaryGrey")
            //self.createChart(dataPoints: income, values: incomeValues, title: viewToDisplay)
            self.expensesButton.titleLabel?.textColor = UIColor(named: "primaryBlue")
            self.netIncomeTable.reloadData()
            print(self.viewToDisplay)
            self.getAllExpense(days: "30")
        }
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        self.netIncomeTable.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    //        self.netIncomeTable.reloadData()
    //    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.netIncomeTable.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newvalue = change?[.newKey] {
                let newsize = newvalue as! CGSize
                self.tableHeight.constant = newsize.height
            }
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.viewToDisplay = "Expense"
    }
    
    // IBActions
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        if sender.tag == 0 {
            incomeButton.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            expensesButton.setTitleColor(UIColor(named: "primaryGrey"), for: .normal)
            self.viewToDisplay = "Income"
            self.expensesButton.titleLabel?.textColor = UIColor(named: "primaryGrey")
            //self.createChart(dataPoints: assets, values: assetValues, title: viewToDisplay)
            self.netIncomeTable.reloadData()
            print(self.viewToDisplay)
            segmentedControlFilter.selectedSegmentIndex = 0
            self.getAllIncomes(days: "30")
            //self.getAllAssetsWithOutDate()
        } else if sender.tag == 1 {
            incomeButton.setTitleColor(UIColor(named: "primaryGrey"), for: .normal)
            expensesButton.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            self.viewToDisplay = "Expense"
            self.incomeButton.titleLabel?.textColor = UIColor(named: "primaryGrey")
            //self.createChart(dataPoints: liabilites, values: liabilitesValues, title: viewToDisplay)
            self.expensesButton.titleLabel?.textColor = UIColor(named: "primaryBlue")
            self.netIncomeTable.reloadData()
            print(self.viewToDisplay)
            segmentedControlFilter.selectedSegmentIndex = 0
            self.getAllExpense(days: "30")
            //self.getAllLiabilitiesWithOutDate()
        }
    }
    
    // IBActions
    @IBAction func buttonAddPressed(_ sender: UIButton) {
        
        if self.viewToDisplay == "Income" {
            let objVC: AddIncomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: AddIncomeVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        } else {
            let objVC: AddExpenceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: AddExpenceVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    
    @objc func startDoneAction() {
        if let datePickerView = self.startDateTextField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let dateString = dateFormatter.string(from: datePickerView.date)
            startDate = datePickerView.date
            print("DATE: \(dateString)")
            self.startDateTextField.text = "\(dateString)"
            //DateTimeUtils.sharedInstance.formatFancyDate2(date: datePickerView.date) ?? ""
            print(datePickerView.date)
            print(dateString)
            self.startDateTextField.resignFirstResponder()
            
            let FromDateGreater = DateTimeUtils.sharedInstance.describeComparison(startDate: self.startDate, endDate: self.endDate)
            if FromDateGreater == "FromDateGreater"{
                self.endDateTextField.text = DateTimeUtils.sharedInstance.getCurrentsDate()
                self.startDateTextField.text = DateTimeUtils.sharedInstance.thirtyDaysBeforeToday()
                return
            }else{
                // self.btnToDate.setTitle(selectedDate, for: .normal)
            }
            
            if self.viewToDisplay == "Income" {
                self.getAllIncomes(days: "30")
            }else{
                self.getAllExpense(days: "30")
            }
        }
    }
    
    @objc func endDoneAction() {
        if let datePickerView = self.endDateTextField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let dateString = dateFormatter.string(from: datePickerView.date)
            endDate = datePickerView.date
            print("DATE: \(dateString)")
            self.endDateTextField.text = "\(dateString)"//DateTimeUtils.sharedInstance.formatFancyDate1(date: datePickerView.date) ?? ""
            print(datePickerView.date)
            print(dateString)
            
            self.endDateTextField.resignFirstResponder()
            
            
            print("startDate",startDate)
            print("endDate",endDate)
            let FromDateGreater = DateTimeUtils.sharedInstance.describeComparison(startDate: self.startDate, endDate: self.endDate)
            if FromDateGreater == "FromDateGreater"{
                self.endDateTextField.text = DateTimeUtils.sharedInstance.getCurrentsDate()
                self.startDateTextField.text = DateTimeUtils.sharedInstance.thirtyDaysBeforeToday()
                
                return
            }else{
                // self.btnToDate.setTitle(selectedDate, for: .normal)
            }
            
            
            if self.viewToDisplay == "Income" {
                self.getAllIncomes(days: "30")
            }else{
                self.getAllExpense(days: "30")
            }
        }
    }
    
    @objc func cancelAction() {
        self.startDateTextField.resignFirstResponder()
        self.endDateTextField.resignFirstResponder()
    }
    
    
    @IBAction func dataChanged(sender: UISegmentedControl) {
        print(segmentedControlFilter.selectedSegmentIndex)
        
        if self.viewToDisplay == "Income" {
            switch segmentedControlFilter.selectedSegmentIndex
            {
            case 0:
                self.getAllIncomes(days: "30")
            case 1:
                self.getAllIncomes(days: "60")
            case 2:
                self.getAllIncomes(days: "90")
            default:
                break;
            }
        }else{
            switch segmentedControlFilter.selectedSegmentIndex
            {
            case 0:
                self.getAllExpense(days: "30")
            case 1:
                self.getAllExpense(days: "60")
            case 2:
                self.getAllExpense(days: "90")
            default:
                break;
            }
        }
   
    }
    
}


extension NetIncomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewToDisplay == "Income" {
            return self.getAllIncomesModel?.data.catData.count ?? 0
        } else {
            return self.getAllExpensesModel?.data.catData.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = netIncomeTable.dequeueReusableCell(withIdentifier: "netIncomeCell", for: indexPath) as! NetWorthTableViewCell
        
        if self.viewToDisplay == "Income" {
            let objDict = self.getAllIncomesModel?.data.catData[indexPath.row]
            cell.cellName.text = objDict?.categoryName
            //cell.cellValue.text = "$\(objDict?.incomeValueOriginal ?? "")"
            cell.cellImage.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
            
            if let assetCurrentValue = "\(objDict!.incomeValueOriginal)".first {
                if assetCurrentValue == "-"{
                    cell.cellValue.text = "\(objDict!.incomeValueOriginal)".replacingOccurrences(of: "-", with: "-$")
                }else{
                    cell.cellValue.text = "$"+"\(objDict!.incomeValueOriginal)"
                }
            }
            
        } else {
            let objDict = getAllExpensesModel?.data.catData[indexPath.row]
            cell.cellName.text = objDict?.categoryName
            //cell.cellValue.text = "$\(objDict?.incomeValueOriginal ?? "")"
            cell.cellImage.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
            
            if let assetCurrentValue = "\(objDict!.expenseValueOriginal)".first {
                if assetCurrentValue == "-"{
                    cell.cellValue.text = "\(objDict!.expenseValueOriginal)".replacingOccurrences(of: "-", with: "-$")
                }else{
                    cell.cellValue.text = "$"+"\(objDict!.expenseValueOriginal)"
                }
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.viewToDisplay == "Income" {
            
            let objDict = self.getAllIncomesModel?.data.catData[indexPath.row]
            let objVC: IncomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: IncomeVC.nameOfClass)
            objVC.navTitle = "Income"
            objVC.categoryId = objDict?.categoryID ?? ""
            objVC.categoryName = objDict?.categoryName ?? ""
            objVC.categoryCode = objDict?.categoryCode ?? ""
            self.navigationController?.pushViewController(objVC, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
            
        } else {
            let objDict = self.getAllExpensesModel?.data.catData[indexPath.row]
            let objVC: IncomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.income, viewControllerName: IncomeVC.nameOfClass)
            objVC.navTitle = "Expense"
            objVC.categoryId = objDict?.categoryID ?? ""
            objVC.categoryName = objDict?.categoryName ?? ""
            objVC.categoryCode = objDict?.categoryCode ?? ""
            self.navigationController?.pushViewController(objVC, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
            
        }
    }
}

//MARK: - Pie Chart Creation
extension NetIncomeVC {
    func createChart(dataPoints: [String], values: [Double], title: String, totalvalue: String) {
        
        pieChartView.rotationAngle = 0
        pieChartView.rotationEnabled = false
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.drawSlicesUnderHoleEnabled = false
        //pieChartView.legend.enabled = false
        pieChartView.isUserInteractionEnabled = false
        
        let largeNumber = (Double(values.reduce(.zero, +)))
        //        let numberFormatter = NumberFormatter()
        //        numberFormatter.numberStyle = .decimal
        //        let formattedNumber = numberFormatter.string(from: NSNumber(value:largeNumber))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let chartAttributesOne: [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: UIFont(name: "FilsonPro-Regular", size: 14.0)!,NSAttributedString.Key.paragraphStyle: paragraphStyle]
        let firstString = NSMutableAttributedString(string: "\(title) Value\n", attributes: chartAttributesOne)
        let chartAttributesTwo: [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: UIFont(name: "FilsonPro-Bold", size: 16.0)!,NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        //        var value : String = "\(String(format: "%.2f", largeNumber))".currencyFormatting()
        //        if let assetCurrentValue = "\(value)".first {
        //            if assetCurrentValue == "-"{
        //                value = value.replacingOccurrences(of: "-", with: "-$")
        //            }else{
        //                value = "$"+"\(value)"
        //            }
        //        }
        
        var value : String = ""
        if let assetCurrentValue = "\(totalvalue)".first {
            if assetCurrentValue == "-"{
                value = totalvalue.replacingOccurrences(of: "-", with: "-$")
            }else{
                value = "$"+"\(totalvalue)"
            }
        }
        
        
        let secondString = NSMutableAttributedString(string: "\(value)", attributes: chartAttributesTwo)
        let string = NSMutableAttributedString()
        string.append(firstString)
        string.append(secondString)
        pieChartView.centerAttributedText = string
        //pieChartView.centerText = "Assets Value \(Int(assetValues.reduce(.zero, +)))"
        pieChartView.transparentCircleColor = UIColor.red
        pieChartView.holeRadiusPercent = 0.8
        
        // 1. Set ChartDataEntry
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            if values[i] > 0{
                let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i], data: dataPoints[i] as AnyObject)
                dataEntries.append(dataEntry)
            }
        }
        
        // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = self.colors as! [NSUIColor]
        
        // 3. Set ChartData
        let pieChartdata = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        format.locale = Locale(identifier: "en_US")
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartdata.setValueFormatter(formatter)
        pieChartdata.setValueTextColor(NSUIColor.clear)
        
        // 4. Assign to chart's data
        pieChartView.data = pieChartdata
        //pieChartView.usePercentValuesEnabled = true
        
    }
}

extension NetIncomeVC {
    
    func getAllIncomes(days:String) {
        
        self.showLoader()
        let netIncomeService = NetIncomeService()
        let parameter:[String:Any] = [
            "days":days
//            "start_date": DateTimeUtils.sharedInstance.convertDateFormat(inputDate: self.startDateTextField.text ?? ""),
//            "end_date":DateTimeUtils.sharedInstance.convertDateFormat(inputDate: self.endDateTextField.text ?? "")
        ]
        netIncomeService.getAllIncomes(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.message.length > 0 {
                        self.showAlert(message: responseData.message)
                    }
                    self.viewToDisplay = "Income"
                    self.getAllIncomesModel = responseData
                    self.netIncomeTable.reloadData()
                    
                    self.income.removeAll()
                    self.incomeValues.removeAll()
                    
                    self.getAllIncomesModel?.data.catData.forEach { obj in
                        let value : Double = Double(obj.incomeValueOriginal) ?? 0.0
                        self.incomeValues.append(value)
                        self.income.append(obj.categoryName)
                    }
                    
                    self.income = NSOrderedSet(array: self.income).map({ $0 as! String })
                    
                    self.createChart(dataPoints: self.income, values: self.incomeValues, title: self.viewToDisplay, totalvalue: responseData.data.incomesTotalValueOriginal)
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func getAllExpense(days:String) {
        
        self.showLoader()
        let netIncomeService = NetIncomeService()
        let parameter:[String:Any] = [
            "days":days
//            "start_date": DateTimeUtils.sharedInstance.convertDateFormat(inputDate: self.startDateTextField.text ?? ""),
//            "end_date":DateTimeUtils.sharedInstance.convertDateFormat(inputDate: self.endDateTextField.text ?? "")
        ]
        netIncomeService.getAllExpenses(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.message.length > 0 {
                        self.showAlert(message: responseData.message)
                    }
                    self.viewToDisplay = "Expense"
                    self.getAllExpensesModel = responseData
                    self.netIncomeTable.reloadData()
                    
                    self.expenses.removeAll()
                    self.expenseValues.removeAll()
                    
                    self.getAllExpensesModel?.data.catData.forEach { obj in
                        let value : Double = Double(obj.expenseValueOriginal) ?? 0.0
                        self.expenseValues.append(value)
                        self.expenses.append(obj.categoryName)
                    }
                    
                    self.expenses = NSOrderedSet(array: self.expenses).map({ $0 as! String })
                    self.createChart(dataPoints: self.expenses, values: self.expenseValues, title: self.viewToDisplay, totalvalue: responseData.data.expensesTotalValueOriginal)
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
}

extension NetIncomeVC: UITextFieldDelegate {
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if #available(iOS 13.4, *) {
            
            self.startDateTextField.datePicker(target: self, doneAction: #selector(startDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: textField.text)
            self.endDateTextField.datePicker(target: self, doneAction: #selector(endDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: textField.text)
            
        } else {
            // Fallback on earlier versions
        }
        
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
}
