//
//  NetWorthVC.swift
//  Render
//
//  Created by Anis Agwan on 01/12/21.
//

import UIKit
import Charts

class NetWorthVC: UIViewController {
    
    // Variables
    var viewToDisplay: String = "Assets"
    //var assets: [String] = ["Cash and Financial Account Balances", "Real Estate Assets", "Personal Possessions"]
    // var assetValues: [Double] = [7000, 5000, 3000]
    var assets: [String] = []
    var assetValues: [Double] = []
    var liabilites: [String] = []
    var liabilitesValues: [Double] = []
    var colors: [UIColor?] = [
        UIColor(named: "chartColor1"),
        UIColor(named: "chartColor2"),
        UIColor(named: "chartColor3"),
        UIColor(named: "chartColor4"),
        UIColor(named: "chartColor5"),
        UIColor(named: "chartColor6")
    ]
    
    //IBOutlets
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var assetsButton: UIButton!
    @IBOutlet weak var liabilityButton: UIButton!
    @IBOutlet weak var netWorthTableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var segmentedControlFilter: UISegmentedControl!
    
    var assetModel : AssetModel?
    var liabilitiesModel : LiabilitiesModel?
    var startDate = Date()
    var endDate = Date()
    
    // View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.4, *) {
            self.startDateTextField.datePicker(target: self, doneAction: #selector(startDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: "")
            self.endDateTextField.datePicker(target: self, doneAction: #selector(endDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: "")
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.endDateTextField.text = DateTimeUtils.sharedInstance.getCurrentsDate()
        self.startDateTextField.text = DateTimeUtils.sharedInstance.thirtyDaysBeforeToday()
        
        startDate = DateTimeUtils.sharedInstance.convertDate(inputDate: self.startDateTextField.text ?? "")
        endDate = DateTimeUtils.sharedInstance.convertDate(inputDate: self.endDateTextField.text ?? "")
        
        self.netWorthTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        if self.viewToDisplay == "Assets" {
            self.assetsButton.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            self.netWorthTableView.reloadData()
            //self.createChart(dataPoints: assets, values: assetValues, title: viewToDisplay)
            //self.getAllAssetsWithOutDate()
            self.getAllAssetServiceCall(days: "0")
        }else{
            assetsButton.setTitleColor(UIColor(named: "primaryGrey"), for: .normal)
            liabilityButton.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            self.viewToDisplay = "Liabilities"
            self.assetsButton.titleLabel?.textColor = UIColor(named: "primaryGrey")
            //self.createChart(dataPoints: liabilites, values: liabilitesValues, title: viewToDisplay)
            self.liabilityButton.titleLabel?.textColor = UIColor(named: "primaryBlue")
            self.netWorthTableView.reloadData()
            print(self.viewToDisplay)
            //self.getAllLiabilitiesWithOutDate()
            self.getAllLiabilitiesServiceCall(days: "0")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.netWorthTableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    @objc func cancelAction() {
        self.startDateTextField.resignFirstResponder()
        self.endDateTextField.resignFirstResponder()
    }
    
    @objc func startDoneAction() {
        if let datePickerView = self.startDateTextField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let dateString = dateFormatter.string(from: datePickerView.date)
            startDate = datePickerView.date
            print("DATE: \(dateString)")
            self.startDateTextField.text = "\(dateString)"
            //DateTimeUtils.sharedInstance.formatFancyDate2(date: datePickerView.date) ?? ""
            print(datePickerView.date)
            print(dateString)
            self.startDateTextField.resignFirstResponder()
            
            let FromDateGreater = DateTimeUtils.sharedInstance.describeComparison(startDate: self.startDate, endDate: self.endDate)
            if FromDateGreater == "FromDateGreater"{
                self.endDateTextField.text = DateTimeUtils.sharedInstance.getCurrentsDate()
                self.startDateTextField.text = DateTimeUtils.sharedInstance.thirtyDaysBeforeToday()
                return
            }else{
                // self.btnToDate.setTitle(selectedDate, for: .normal)
            }
            
            if self.viewToDisplay == "Assets" {
                self.getAllAssetServiceCall(days: "0")
            }else{
                self.getAllLiabilitiesServiceCall(days: "0")
            }
        }
    }
    
    @objc func endDoneAction() {
        if let datePickerView = self.endDateTextField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let dateString = dateFormatter.string(from: datePickerView.date)
            endDate = datePickerView.date
            print("DATE: \(dateString)")
            self.endDateTextField.text = "\(dateString)"//DateTimeUtils.sharedInstance.formatFancyDate1(date: datePickerView.date) ?? ""
            print(datePickerView.date)
            print(dateString)
            
            self.endDateTextField.resignFirstResponder()
            
            
            print("startDate",startDate)
            print("endDate",endDate)
            let FromDateGreater = DateTimeUtils.sharedInstance.describeComparison(startDate: self.startDate, endDate: self.endDate)
            if FromDateGreater == "FromDateGreater"{
                self.endDateTextField.text = DateTimeUtils.sharedInstance.getCurrentsDate()
                self.startDateTextField.text = DateTimeUtils.sharedInstance.thirtyDaysBeforeToday()
                
                return
            }else{
                // self.btnToDate.setTitle(selectedDate, for: .normal)
            }
            
            
            if self.viewToDisplay == "Assets" {
                self.getAllAssetServiceCall(days: "0")
            }else{
                self.getAllLiabilitiesServiceCall(days: "0")
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newValue = change?[.newKey] {
                let newSize = newValue as! CGSize
                self.tableHeight.constant = newSize.height
            }
        }
    }
    
    // IBActions
    @IBAction func buttonPressed(_ sender: UIButton) {
        if sender.tag == 0 {
            self.endDateTextField.text = DateTimeUtils.sharedInstance.getCurrentsDate()
            self.startDateTextField.text = DateTimeUtils.sharedInstance.thirtyDaysBeforeToday()
            
            assetsButton.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            liabilityButton.setTitleColor(UIColor(named: "primaryGrey"), for: .normal)
            self.viewToDisplay = "Assets"
            self.liabilityButton.titleLabel?.textColor = UIColor(named: "primaryGrey")
            //self.createChart(dataPoints: assets, values: assetValues, title: viewToDisplay)
            self.netWorthTableView.reloadData()
            print(self.viewToDisplay)
            //self.getAllAssetsWithOutDate()
            segmentedControlFilter.selectedSegmentIndex = 0
            self.getAllAssetServiceCall(days: "0")
        } else if sender.tag == 1 {
            self.endDateTextField.text = DateTimeUtils.sharedInstance.getCurrentsDate()
            self.startDateTextField.text = DateTimeUtils.sharedInstance.thirtyDaysBeforeToday()
            
            assetsButton.setTitleColor(UIColor(named: "primaryGrey"), for: .normal)
            liabilityButton.setTitleColor(UIColor(named: "primaryBlue"), for: .normal)
            self.viewToDisplay = "Liabilities"
            self.assetsButton.titleLabel?.textColor = UIColor(named: "primaryGrey")
            //self.createChart(dataPoints: liabilites, values: liabilitesValues, title: viewToDisplay)
            self.liabilityButton.titleLabel?.textColor = UIColor(named: "primaryBlue")
            self.netWorthTableView.reloadData()
            print(self.viewToDisplay)
            //self.getAllLiabilitiesWithOutDate()
            segmentedControlFilter.selectedSegmentIndex = 0
            self.getAllLiabilitiesServiceCall(days: "0")
        }
    }
    
    @IBAction func addNetWorth(_ sender: UIButton) {
        if self.viewToDisplay == "Assets" {
            let objVC: AddAssetViewController = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.assets, viewControllerName: AddAssetViewController.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        } else if self.viewToDisplay == "Liabilities" {
            let objVC: AddingLiabilitiesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.liabilities, viewControllerName: AddingLiabilitiesVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        } else {
            print("NOTHING")
        }
    }
    
    @IBAction func dataChanged(sender: UISegmentedControl) {
        print(segmentedControlFilter.selectedSegmentIndex)
        
        if self.viewToDisplay == "Assets" {
            switch segmentedControlFilter.selectedSegmentIndex
            {
            case 0:
                self.getAllAssetServiceCall(days: "0")
            case 1:
                self.getAllAssetServiceCall(days: "30")
            case 2:
                self.getAllAssetServiceCall(days: "60")
            case 3:
                self.getAllAssetServiceCall(days: "90")
            default:
                break;
            }
        }else{
            switch segmentedControlFilter.selectedSegmentIndex
            {
            case 0:
                self.getAllLiabilitiesServiceCall(days: "0")
            case 1:
                self.getAllLiabilitiesServiceCall(days: "30")
            case 2:
                self.getAllLiabilitiesServiceCall(days: "60")
            case 3:
                self.getAllLiabilitiesServiceCall(days: "90")
            default:
                break;
            }
        }
      
    }
}

//MARK: - UITableView
extension NetWorthVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewToDisplay == "Assets" {
            //return self.assets.count
            return self.assetModel?.data.catData.count ?? 0
        } else {
            // return self.liabilites.count
            return self.liabilitiesModel?.data.catData.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = netWorthTableView.dequeueReusableCell(withIdentifier: "netWorthCell", for: indexPath) as! NetWorthTableViewCell
        
        if self.viewToDisplay == "Assets" {
            let objDict = self.assetModel?.data.catData[indexPath.row]
            cell.cellName.text = objDict?.categoryName
            //cell.cellValue.text = "$\(objDict?.assetCurrentValueOrginal ?? "")"
            cell.cellImage.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
            
            if let assetCurrentValueOrginal = "\(objDict!.assetCurrentValueOrginal)".first {
                if assetCurrentValueOrginal == "-"{
                    cell.cellValue.text = "\(objDict!.assetCurrentValueOrginal)".replacingOccurrences(of: "-", with: "-$")
                }else{
                    cell.cellValue.text = "$"+"\(objDict!.assetCurrentValueOrginal)"
                }
            }
            //"$"+"\(String(format: "%.2f", responseData.data.netWorth))"
            //
            //            if objDict?.categoryName == "Personal Possessions"{
            //                cell.cellImage.image = UIImage(named: "otherActive")
            //            }else if objDict?.categoryName == "Real Estate Assets"{
            //                cell.cellImage.image = UIImage(named: "propertyImage")
            //            }else if objDict?.categoryName == "Cash and Financial Account Balances"{
            //                cell.cellImage.image = UIImage(named: "cashImage")
            //            } else {
            //                cell.cellImage.image = UIImage(named: "otherActive")
            //            }
        } else {
            cell.cellName.text = self.liabilites[indexPath.item]
            //cell.cellValue.text = "$\(self.liabilitesValues[indexPath.item])"
            //cell.iconLabelFA.font = UIFont(name: "Font Awesome 6 Free Solid", size: 22)
            let objDict = self.liabilitiesModel?.data.catData[indexPath.row]
            cell.cellName.text = objDict?.categoryName
            cell.cellImage.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
            
            if let assetCurrentValueOrginal = "\(self.liabilitesValues[indexPath.item])".first {
                if assetCurrentValueOrginal == "-"{
                    cell.cellValue.text = "\(String(format: "%.2f", self.liabilitesValues[indexPath.item]))".replacingOccurrences(of: "-", with: "-$")
                }else{
                    cell.cellValue.text = "$"+"\(objDict?.categoryWiseTotalLiabilitiesValueOriginal ?? "")"//"$"+"\(self.liabilitesValues[indexPath.item])" //String(format: "%.2f", self.liabilitesValues[indexPath.item])
                }
            }
            
            //            if objDict?.categoryName == "Loan"{
            //                cell.cellImage.image = UIImage(named: "loansActive")
            //            }else  if objDict?.categoryName == "Credit Card"{
            //                cell.cellImage.image = UIImage(named: "creditCardActive")
            //            } else {
            //                cell.cellImage.image = UIImage(named: "otherActive")
            //            }
            
            
            //            let str = objDict?.categoryIconHexCode ?? ""
            //
            //            if let num = Int(str, radix: 16) {
            //                if let uniCode = UnicodeScalar(num) {
            //                    print("NUM: \(num)")
            //                    print("UniCOde: \(uniCode)")
            //                    cell.iconLabelFA.text = "\(uniCode)"
            //                } else {
            //                    print("Cant convert")
            //                }
            //            } else {
            //                cell.iconLabelFA.text = "0"
            //            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.viewToDisplay == "Liabilities" {
            
            let objDict = self.liabilitiesModel?.data.catData[indexPath.row]
            
            if objDict?.categoryName == "Loan" {
                let objVC: AllLoansVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.liabilities, viewControllerName: AllLoansVC.nameOfClass)
                objVC.navTitle = "Loan"
                objVC.liabilitiesTotalValue = "$\(self.liabilitesValues[indexPath.item])"
                objVC.categoryId = objDict?.categoryName ?? ""//objDict?.categoryID ?? ""
                objVC.libCategory = objDict?.categoryName ?? ""
                objVC.catLiabilitiesData = objDict
                self.navigationController?.pushViewController(objVC, animated: true)
                tableView.deselectRow(at: indexPath, animated: true)
            } else if objDict?.categoryName == "Insurance" {
                let objVC: AllLoansVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.liabilities, viewControllerName: AllLoansVC.nameOfClass)
                objVC.navTitle = "Insurance"
                objVC.categoryId = objDict?.categoryID ?? ""
                self.navigationController?.pushViewController(objVC, animated: true)
                tableView.deselectRow(at: indexPath, animated: true)
            }else if objDict?.categoryName == "Credit Card" {
                let objVC: AllLoansVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.liabilities, viewControllerName: AllLoansVC.nameOfClass)
                objVC.navTitle = "Credit Card"
                objVC.liabilitiesTotalValue = "$\(self.liabilitesValues[indexPath.item])"
                objVC.categoryId = objDict?.categoryName ?? ""//objDict?.categoryID ?? ""
                objVC.libCategory = objDict?.categoryName ?? ""
                objVC.catLiabilitiesData = objDict
                self.navigationController?.pushViewController(objVC, animated: true)
                tableView.deselectRow(at: indexPath, animated: true)
            } else {
                print("All good")
            }
        } else {
            //let cell = assets[indexPath.row]
            //Cash and Financial Account Balances
            //Real Estate Assets
            //Personal Possessions
            let objDict = self.assetModel?.data.catData[indexPath.row]
            if objDict?.categoryName == "Cash and Financial Account Balances" {
                let objVC: AssetVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.assets, viewControllerName: AssetVC.nameOfClass)
                objVC.navTitle = "Cash and Financial Account Balances"
                objVC.categoryId = objDict?.categoryID ?? ""
                objVC.assetCategory = objDict?.categoryName ?? ""
                objVC.catDatum = objDict
                self.navigationController?.pushViewController(objVC, animated: true)
                tableView.deselectRow(at: indexPath, animated: true)
            } else if objDict?.categoryName == "Real Estate Assets" {
                let objVC: AssetVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.assets, viewControllerName: AssetVC.nameOfClass)
                objVC.navTitle = "Real Estate Assets"
                objVC.assetCategory = objDict?.categoryName ?? ""
                objVC.categoryId = objDict?.categoryID ?? ""
                self.navigationController?.pushViewController(objVC, animated: true)
                tableView.deselectRow(at: indexPath, animated: true)
            } else if objDict?.categoryName == "Personal Possessions" {
                let objVC: AssetVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.assets, viewControllerName: AssetVC.nameOfClass)
                objVC.navTitle = "Personal Possessions"
                objVC.assetCategory = objDict?.categoryName ?? ""
                objVC.categoryId = objDict?.categoryID ?? ""
                self.navigationController?.pushViewController(objVC, animated: true)
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
}

//MARK: - Pie Chart Creation
extension NetWorthVC {
    func createChart(dataPoints: [String], values: [Double], title: String, totalvalue: String) {
        // Customize and Update the chart here
        
        pieChartView.rotationAngle = 0
        pieChartView.rotationEnabled = false
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.drawSlicesUnderHoleEnabled = false
        //pieChartView.legend.enabled = false
        pieChartView.isUserInteractionEnabled = false
        
        let largeNumber = (Double(values.reduce(.zero, +)))
        //        let numberFormatter = NumberFormatter()
        //        numberFormatter.numberStyle = .decimal
        //        let formattedNumber = numberFormatter.string(from: NSNumber(value:largeNumber))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let chartAttributesOne: [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: UIFont(name: "FilsonPro-Regular", size: 14.0)!,NSAttributedString.Key.paragraphStyle: paragraphStyle]
        let firstString = NSMutableAttributedString(string: "\(title) Value\n", attributes: chartAttributesOne)
        let chartAttributesTwo: [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: UIFont(name: "FilsonPro-Bold", size: 16.0)!,NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        //        var value : String = "\(String(format: "%.2f", largeNumber))".currencyFormatting()
        //        if let assetCurrentValueOrginal = "\(value)".first {
        //            if assetCurrentValueOrginal == "-"{
        //                value = value.replacingOccurrences(of: "-", with: "-$")
        //            }else{
        //                value = "$"+"\(value)"
        //            }
        //        }
        
        var value : String = ""
        if let assetCurrentValueOrginal = "\(totalvalue)".first {
            if assetCurrentValueOrginal == "-"{
                value = totalvalue.replacingOccurrences(of: "-", with: "-$")
            }else{
                value = "$"+"\(totalvalue)"
            }
        }
        
        
        let secondString = NSMutableAttributedString(string: "\(value)", attributes: chartAttributesTwo)
        let string = NSMutableAttributedString()
        string.append(firstString)
        string.append(secondString)
        pieChartView.centerAttributedText = string
        //pieChartView.centerText = "Assets Value \(Int(assetValues.reduce(.zero, +)))"
        pieChartView.transparentCircleColor = UIColor.red
        pieChartView.holeRadiusPercent = 0.8
        
        // 1. Set ChartDataEntry
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            if values[i] > 0{
                let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i], data: dataPoints[i] as AnyObject)
                dataEntries.append(dataEntry)
            }
        }
        
        // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = self.colors as! [NSUIColor]
        
        // 3. Set ChartData
        let pieChartdata = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        format.locale = Locale(identifier: "en_US")
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartdata.setValueFormatter(formatter)
        pieChartdata.setValueTextColor(NSUIColor.clear)
        
        // 4. Assign to chart's data
        pieChartView.data = pieChartdata
        //pieChartView.usePercentValuesEnabled = true
        
    }
}

// MARK: - API Call
extension NetWorthVC {
    
    // MARK: - Get All Categories Service Call
    func getAllCategoriesServiceCall(){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "type": "ASSET",
        ]
        let netWorthService = NetWorthService()
        netWorthService.getAllCategories(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    print(responseData.data)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    // MARK: - Get All Liabilities
    func getAllLiabilitiesServiceCall(days:String) {
        
        self.showLoader()
        let netWorthService = NetWorthService()
        let parameter:[String:Any] = [
            
            "days":days
//            "start_date": DateTimeUtils.sharedInstance.convertDateFormat(inputDate: self.startDateTextField.text ?? ""),
//            "end_date":DateTimeUtils.sharedInstance.convertDateFormat(inputDate: self.endDateTextField.text ?? "")
        ]
        netWorthService.getAllLiabilities(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.message.length > 0 {
                        self.showAlert(message: responseData.message)
                    }
                    self.viewToDisplay = "Liabilities"
                    self.liabilitiesModel = responseData
                    self.netWorthTableView.reloadData()
                    
                    self.liabilites.removeAll()
                    self.liabilitesValues.removeAll()
                    
                    self.liabilitiesModel?.data.catData.forEach { obj in
                        let value : Double = Double(obj.categoryWiseTotalLiabilitiesValueOriginal) ?? 0.0
                        self.liabilitesValues.append(value)
                        self.liabilites.append(obj.categoryName)
                    }
                    
                    self.liabilites = NSOrderedSet(array: self.liabilites).map({ $0 as! String })
                    
                    self.createChart(dataPoints: self.liabilites, values: self.liabilitesValues, title: self.viewToDisplay, totalvalue: responseData.data.liabilitiesTotalValueOriginal)
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    // MARK: - Get All Assets
    
    func getAllAssetServiceCall(days:String) {
        
        self.showLoader()
        let parameter:[String:Any] = [
            "days":days
//            "start_date": DateTimeUtils.sharedInstance.convertDateFormat(inputDate: self.startDateTextField.text ?? ""),
//            "end_date":DateTimeUtils.sharedInstance.convertDateFormat(inputDate: self.endDateTextField.text ?? "")
        ]
        let netWorthService = NetWorthService()
        netWorthService.getAllAssets(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.message.length > 0 {
                        self.showAlert(message: responseData.message)
                    }
                    
                    self.viewToDisplay = "Assets"
                    self.assetModel = responseData
                    
                    self.assets.removeAll()
                    self.assetValues.removeAll()
                    
                    self.assetModel?.data.catData.forEach { obj in
                        let value : Double = Double(obj.assetCurrentValueOrginal) ?? 0.0
                        self.assetValues.append(value)
                        self.assets.append(obj.categoryName)
                    }
                    
                    self.assets = NSOrderedSet(array: self.assets).map({ $0 as! String })
                    self.netWorthTableView.reloadData()
                    self.createChart(dataPoints: self.assets, values: self.assetValues, title: self.viewToDisplay, totalvalue: responseData.data.assetsTotalValueOrginal)
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let responseData):
                print(responseData)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
}

extension NetWorthVC: UITextFieldDelegate {
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print(DateTimeUtils.sharedInstance.convertDate(inputDate: textField.text ?? ""))
        if #available(iOS 13.4, *) {
            self.startDateTextField.datePicker(target: self, doneAction: #selector(startDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: textField.text)
            self.endDateTextField.datePicker(target: self, doneAction: #selector(endDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: textField.text)
        } else {
            // Fallback on earlier versions
        }
        
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
}
