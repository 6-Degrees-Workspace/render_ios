//
//  NetWorthTableViewCell.swift
//  Render
//
//  Created by Anis Agwan on 01/12/21.
//

import UIKit

class NetWorthTableViewCell: UITableViewCell {

    // IBOutlets
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellPercent: UILabel!
    @IBOutlet weak var cellValue: UILabel!
    @IBOutlet weak var iconLabelFA: UILabel!
    //
    @IBOutlet weak var lblExName: UILabel!
    @IBOutlet weak var imageExLogoName: UIImageView!
    @IBOutlet weak var imageExName: UIImageView!
    @IBOutlet weak var logoEx: UIImageView!
    //
    @IBOutlet weak var profileLogo: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
