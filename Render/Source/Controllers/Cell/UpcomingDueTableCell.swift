//
//  UpcomingDueTableCell.swift
//  Render
//
//  Created by Anis Agwan on 23/12/21.
//

import UIKit

class UpcomingDueTableCell: UITableViewCell {

    @IBOutlet weak var cellImg: UIImageView!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellDueDate: UILabel!
    @IBOutlet weak var cellValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
