import UIKit

class CreditCardTableCell: UITableViewCell {

    //IbOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cardHolderName: UILabel!
    @IBOutlet weak var cardTypeImg: UIImageView!
    @IBOutlet weak var cardNumberLbl: UILabel!
    @IBOutlet weak var smallView: UIView!
    @IBOutlet weak var unbilledAmt: UILabel!
    @IBOutlet weak var cardAmt: UILabel!
  
    //View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
