import UIKit

class AllLoansTableCell: UITableViewCell {

    //IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblMonthlyEMI: UILabel!
    @IBOutlet weak var lbltenure: UILabel!
    @IBOutlet weak var smallView: UIView!
    @IBOutlet weak var loanImgView: UIImageView!
    @IBOutlet weak var loanName: UILabel!
    @IBOutlet weak var lblView: UIView!
    @IBOutlet weak var lblInsurance: UILabel!
    
    
    override func awakeFromNib() {
        
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
