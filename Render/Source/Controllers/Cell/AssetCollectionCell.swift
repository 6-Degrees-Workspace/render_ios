//
//  AssetCollectionCell.swift
//  Render
//
//  Created by Anis Agwan on 09/12/21.
//

import UIKit

class AssetCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var assetImage: UIImageView!
    @IBOutlet weak var assetSubName: UILabel!
    @IBOutlet weak var assetName: UILabel!
    @IBOutlet weak var assetCurrentValue: UILabel!
}
