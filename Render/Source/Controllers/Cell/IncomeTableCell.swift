//
//  IncomeTableCell.swift
//  Render
//
//  Created by Anis Agwan on 10/12/21.
//

import UIKit

class IncomeTableCell: UITableViewCell {

    @IBOutlet weak var incomeDifference: UILabel!
    @IBOutlet weak var incomeAmt: UILabel!
    @IBOutlet weak var incomeDate: UILabel!
    @IBOutlet weak var incomeName: UILabel!
    @IBOutlet weak var incomeImg: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnShowDelete: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
