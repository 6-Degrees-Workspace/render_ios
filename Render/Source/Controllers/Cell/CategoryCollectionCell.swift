//
//  CategoryCollectionCell.swift
//  Render
//
//  Created by Anis Agwan on 09/12/21.
//

import UIKit

class CategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var iconLabelFA: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.isInactive()
    }
    
    func isActive() {
        self.bgView.backgroundColor = .white
        self.bgView.cornerRadius = 8.0
        self.bgView.borderWidth = 1.0
        self.bgView.borderColor = UIColor(named: "primaryBlue")
        self.categoryLbl.textColor = UIColor(named: "primaryBlue")
        self.bgView.masksToBounds = true
        self.bgView.shadowColor = UIColor(named: "primaryBlue")!
        self.bgView.shadowRadius = 2.0
        self.bgView.shadowOffset = CGSize(width: 0.0, height: 0.0)
//        self.shadowOpacity = 0.5
        self.bgView.masksToBounds = false
        let ogImg = self.categoryImg.image?.withRenderingMode(.alwaysOriginal)
        self.categoryImg.image = ogImg
        self.iconLabelFA.textColor = UIColor(named: "primaryBlue")
    }
    
    func isInactive() {
        self.bgView.backgroundColor = .clear
        self.bgView.cornerRadius = 8.0
        self.bgView.borderWidth = 1.0
        self.bgView.borderColor = UIColor(named: "primaryGrey")
        self.categoryLbl.textColor = .black
        let tempImg = self.categoryImg.image?.withRenderingMode(.alwaysTemplate)
        self.categoryImg.image = tempImg
        self.categoryImg.tintColor = .black
        self.iconLabelFA.textColor = .black
    }
    
    
    func isDisableActive() {
        self.bgView.backgroundColor = UIColor(named: "lightGray")
        self.bgView.cornerRadius = 8.0
        self.bgView.borderWidth = 1.0
        self.bgView.borderColor = UIColor(named: "primaryBlue")
        self.categoryLbl.textColor =  UIColor(named: "textGrayColor")//.black
        self.bgView.masksToBounds = true
        self.bgView.shadowColor = UIColor(named: "primaryBlue")!
        self.bgView.shadowRadius = 2.0
        self.bgView.shadowOffset = CGSize(width: 0.0, height: 0.0)
//        self.shadowOpacity = 0.5
        self.bgView.masksToBounds = false
        let ogImg = self.categoryImg.image?.withRenderingMode(.alwaysOriginal)
        self.categoryImg.image = ogImg
        self.iconLabelFA.textColor = UIColor(named: "primaryBlue")
    }
    
}
