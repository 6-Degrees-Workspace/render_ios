import UIKit

class BankTableCell: UITableViewCell {

    @IBOutlet weak var bankImg: UIImageView!
    @IBOutlet weak var bankName: UILabel!
    @IBOutlet weak var bankDetails: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnReauthorize: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
