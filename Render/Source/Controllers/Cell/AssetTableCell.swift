//
//  AssetTableCell.swift
//  Render
//
//  Created by Anis Agwan on 09/12/21.
//

import UIKit

class AssetTableCell: UITableViewCell {

    @IBOutlet weak var assetImg: UIImageView!
    @IBOutlet weak var assetName: UILabel!
    @IBOutlet weak var assetDate: UILabel!
    @IBOutlet weak var assetAmt: UILabel!
    @IBOutlet weak var assetDifference: UILabel!
    @IBOutlet weak var lblIcon: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnShowDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
