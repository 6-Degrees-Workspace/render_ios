//
//  HelpSupportTableCell.swift
//  Render
//
//  Created by Anis Agwan on 23/12/21.
//

import UIKit

class HelpSupportTableCell: UITableViewCell {

    @IBOutlet weak var helpLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
