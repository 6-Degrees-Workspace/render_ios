//
//  FAQsCell.swift
//  Render
//
//  Created by Apple on 28/04/22.
//

import UIKit

class FAQsCell: UITableViewCell {
    
    @IBOutlet weak var lblFAQsDes: UILabel!
    @IBOutlet weak var lblFAQsTitle: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
