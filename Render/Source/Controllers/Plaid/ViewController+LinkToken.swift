//
//  ViewController+LinkToken.swift
//  LinkDemo-Swift
//
//  Copyright © 2020 Plaid Inc. All rights reserved.
//

import LinkKit

extension MyProfileVC {
    
    func createLinkTokenConfiguration() -> LinkTokenConfiguration {
#warning("Replace <#GENERATED_LINK_TOKEN#> below with your link_token")
        // In your production application replace the hardcoded linkToken below with code that fetches an link_token
        // from your backend server which in turn retrieves it securely from Plaid, for details please refer to
        // https://plaid.com/docs/#create-link-token
        
        let linkToken = AppDelegate.sharedApp().token
        
        // With custom configuration using a link_token
        var linkConfiguration = LinkTokenConfiguration(token: linkToken) { success in
            print("public-token: \(success.publicToken) metadata: \(success.metadata)")
            self.createAccessTokenServiceCall(publicToken: success.publicToken, institutionId: success.metadata.institution.id, institutionName: success.metadata.institution.name)
            
        }
        linkConfiguration.onExit = { exit in
            if let error = exit.error {
                print("exit with \(error)\n\(exit.metadata)")
            } else {
                print("exit with \(exit.metadata)")
            }
        }
        return linkConfiguration
    }
    
    // MARK: Start Plaid Link using a Link token
    // For details please see https://plaid.com/docs/#create-link-token
    func presentPlaidLinkUsingLinkToken() {
        let linkConfiguration = createLinkTokenConfiguration()
        let result = Plaid.create(linkConfiguration)
        switch result {
        case .failure(let error):
            print("Unable to create Plaid handler due to: \(error)")
        case .success(let handler):
            handler.open(presentUsing: .viewController(self))
            linkHandler = handler
        }
    }
    
    func createAccessTokenServiceCall(publicToken: String, institutionId: String, institutionName: String) {
        
        self.showLoader()
        let plaidService = PlaidService()
        let parameter:[String:Any] = [
            "public_token": publicToken,//"public-sandbox-645eebb9-cdaf-4ab9-8b70-fbd23469275c",
            "institution_id": institutionId,//"ins_5",
            "institution_name": institutionName//"Bank Name"
        ]
        
        plaidService.createAccessToken(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                        self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
}
