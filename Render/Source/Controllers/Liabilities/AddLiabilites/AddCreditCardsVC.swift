import UIKit

class AddCreditCardsVC: BaseVC {
    
    var loanTypes: [String] = ["Auto Loan", "Mortgage Loan","Student Loan","Personal Loan","Other"]
    var selectedType: String = ""
    var picker: UIPickerView!
    //var asset: AssetDatum?
    var update: Bool = false
    var frameSize: CGRect?
    var safe: CGRect?
    var getCreditCardData : LiabilityData?
    
    //IBOutlets
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var txtCCLimit: UITextField!
    @IBOutlet weak var txtOutCredit: UITextField!
    @IBOutlet weak var txtCurrentBalance: UITextField!
    @IBOutlet weak var txtAvailableCredit: UITextField!
    @IBOutlet weak var txtCardNo: UITextField!
    @IBOutlet weak var txtCardName: UITextField!
    @IBOutlet weak var txtInsiName: UITextField!
    @IBOutlet weak var btnAddAsset: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.getAllLiabilityDetailByCategoryServiceCall()
        if update {
            
            if getCreditCardData?.cardCurrentOutstanding.length ?? 0 > 9{
                self.txtCurrentBalance.text = getCreditCardData?.cardCurrentOutstanding.replacingOccurrences(of: ".00", with: "")
            }else{
                self.txtCurrentBalance.text = getCreditCardData?.cardCurrentOutstanding
            }
            
            //self.txtCurrentBalance.text = getCreditCardData?.cardCurrentOutstanding
            self.txtAvailableCredit.text = getCreditCardData?.availableLimit
            self.txtInsiName.text = getCreditCardData?.institutionName
            self.txtCardName.text = getCreditCardData?.cardName
            self.txtCardNo.text = getCreditCardData?.cardNumber
        }
        // add observer in controller(s) where you want to receive data
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("AddCreditCard"), object: nil)
        
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        self.addCCService()
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        //self.txtCCLimit.text = txtCCLimit.text?.trim()
        self.txtCurrentBalance.text = self.txtCurrentBalance.text?.trim()
        self.txtAvailableCredit.text = self.txtAvailableCredit.text?.trim()
        self.txtInsiName.text = self.txtInsiName.text?.trim()
        self.txtCardName.text = self.txtCardName.text?.trim()
        self.txtCardNo.text = self.txtCardNo.text?.trim()
        
        if (txtCurrentBalance.text?.isEmpty)! {
            message = "Please Enter Current Balance"
            isFound = false
        } else if (txtAvailableCredit.text?.isEmpty)! {
            message = "Please Enter Available Credit Amount"
            isFound = false
        } else if (txtInsiName.text?.isEmpty)! {
            message = "Please Enter Institution Name"
            isFound = false
        } else if (txtInsiName.text?.length ?? 0 < 3){
            message = "The Institution Name  must be at least 3 characters in length"
            isFound = false
        } else if (txtCardName.text?.isEmpty)! {
            message = "Please Enter Name on Card"
            isFound = false
        } else if (txtCardName.text?.length ?? 0 < 3){
            message = "The Card Name  must be at least 3 characters in length"
            isFound = false
        } else if (txtCardNo.text?.isEmpty)! {
            message = "Please Enter Card Number"
            isFound = false
        } else if (txtCardNo.text?.length ?? 0 < 16){
            message = "The Card Number must be at least 16 digits in length"
            isFound = false
        } else if (txtCurrentBalance.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        } else if (txtAvailableCredit.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func actionAddLoans(_ sender: Any) {
        self.view.endEditing(true)
        if update {
            if self.isValidFields() {
                self.updateCCServiceCall()
            }
        }else{
            if self.isValidFields() {
                self.addCCServiceCall()
            }
        }
    }
    
    func addCCService (){
        self.view.endEditing(true)
        
        if update {
            if self.isValidFields() {
                self.updateCCServiceCall()
            }
        }else{
            if self.isValidFields() {
                self.addCCServiceCall()
            }
        }
    }
    
}


extension AddCreditCardsVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtCurrentBalance || textField == txtAvailableCredit{
            // Allow to remove character (Backspace)
            if string == "" {
                return true
            }
            
            // Block multiple dot
            if (textField.text?.contains("."))! && string == "." {
                return false
            }
            
            
            if (textField.text?.contains("."))! {
                let limitDecimalPlace = 2
                let decimalPlace = textField.text?.components(separatedBy: ".").last
                if (decimalPlace?.count)! < limitDecimalPlace {
                    return true
                }
                else {
                    return false
                }
            }
            
        }
        
        let allowedCharacters : CharacterSet
        if textField == txtCardName || textField == txtInsiName{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_letter).inverted
        }else{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_special_letter).inverted
        }
        
        let components = string.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        
        
        if textField == txtCurrentBalance {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtCurrentBalance.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtAvailableCredit {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtAvailableCredit.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtInsiName {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtInsiName.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
          
            
        }
        
        if textField == txtCardName {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtCardName.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
        }
        
        if textField == txtCCLimit {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtCCLimit.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtOutCredit {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtOutCredit.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtCardNo {
            let maxLength = 16
            let currentString: NSString = txtCardNo.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }
}

//MARK: - UIPicker Delegates
extension AddCreditCardsVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return loanTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.text = loanTypes[row]
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedType = loanTypes[row]
    }
}

//MARK: - Service Calls
extension AddCreditCardsVC {
    
    func addCCServiceCall() {
        
        self.txtCurrentBalance.text = txtCurrentBalance.text?.trim()
        self.txtAvailableCredit.text = self.txtAvailableCredit.text?.trim()
        self.txtInsiName.text = self.txtInsiName.text?.trim()
        self.txtCardName.text = self.txtCardName.text?.trim()
        self.txtCardNo.text = self.txtCardNo.text?.trim()
        NotificationCenter.default.post(name: Notification.Name("AddLoader"), object: nil, userInfo: nil)
        
        //        self.showLoader()
        let parameter:[String:Any] = [
            "card_name": self.txtCardName.text ?? "",
            "institution_name":self.txtInsiName.text ?? "",
            "card_number":self.txtCardNo.text ?? "",
            "card_current_outstanding":self.txtCurrentBalance.text ?? "",
            "available_limit":self.txtAvailableCredit.text ?? "",
            "category_code":"CREDITCARD"
            
        ]
        
        let liabilitieService = LiabilitieService()
        liabilitieService.addCCLiabilities(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            NotificationCenter.default.post(name: Notification.Name("RemoveLoader"), object: nil, userInfo: nil)
            //self.removLoader()
        }
    }
    
    func getAllLiabilityDetailByCategoryServiceCall(){
        
        //self.showLoader()
        let parameter:[String:Any] = [
            "category_code": "9"
        ]
        
        let netWorthService = NetWorthService()
        netWorthService.getAllLiabilityDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    //self.liabilityDetailByCategoryModel = responseData
                    //self.liabilityTableView.reloadData()
                    //self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    func updateCCServiceCall() {
        
        //self.showLoader()
        let parameter:[String:Any] = [
            "category_code":"CREDITCARD",
            "liability_id":getCreditCardData?.liabilityID ?? "",
            "card_name": self.txtCardName.text ?? "",
            "institution_name":self.txtInsiName.text ?? "",
            "card_number":self.txtCardNo.text ?? "",
            "card_current_outstanding":self.txtCurrentBalance.text ?? "",
            "available_limit":self.txtAvailableCredit.text ?? ""
            
        ]
        
        let liabilitieService = LiabilitieService()
        liabilitieService.updateLiability(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            //self.removLoader()
        }
        
    }
    
}
