
import UIKit

class AddLoansVC: BaseVC {
    
    var loanTypes: [String] = ["Auto Loan", "Mortgage Loan","Student Loan","Personal Loan","Other"]
    var loanTime: [String] = ["Day", "Month","Year"]
    var selectedType: String = ""
    var selectedTypeId: String = ""
    var picker: UIPickerView!
    var pickerTime: UIPickerView!
    var selectedLoanTime: String = ""
    //var asset: AssetDatum?
    var update: Bool = false
    var frameSize: CGRect?
    var safe: CGRect?
    var loanTypeModel: LoanTypeModel?
    var liabilityDatum: LiabilityDatum?
    
    //IBOutlets
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var txtLoanName: UITextField!
    @IBOutlet weak var txtLoanType: UITextField!
    @IBOutlet weak var txtInstitutionName: UITextField!
    @IBOutlet weak var txtLoanAccountNumber: UITextField!
    @IBOutlet weak var txtPrincipalAmount: UITextField!
    @IBOutlet weak var txtCurrentOutstanding: UITextField!
    @IBOutlet weak var txtLoanTerm: UITextField!
    @IBOutlet weak var txtLoanTime: UITextField!
    @IBOutlet weak var txtLoanDate: UITextField!
    @IBOutlet weak var txtMonthlyPayment: UITextField!
    @IBOutlet weak var txtInterestRate: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if update {
            
            if liabilityDatum?.principleAmount.length ?? 0 > 9{
                self.txtPrincipalAmount.text = liabilityDatum?.principleAmount.replacingOccurrences(of: ".00", with: "")
            }else{
                self.txtPrincipalAmount.text = liabilityDatum?.principleAmount
            }
            
            if liabilityDatum?.currentOutstandingAmount.length ?? 0 > 9{
                self.txtCurrentOutstanding.text = liabilityDatum?.currentOutstandingAmount.replacingOccurrences(of: ".00", with: "")
            }else{
                self.txtCurrentOutstanding.text = liabilityDatum?.currentOutstandingAmount
            }
            
            if liabilityDatum?.monthlyPayment.length ?? 0 > 9{
                self.txtMonthlyPayment.text = liabilityDatum?.monthlyPayment.replacingOccurrences(of: ".00", with: "")
            }else{
                self.txtMonthlyPayment.text = liabilityDatum?.monthlyPayment
            }
            
            self.txtLoanName.text = liabilityDatum?.loanName
            self.txtLoanType.text = liabilityDatum?.loanType
            self.txtInstitutionName.text = liabilityDatum?.institutionName
            self.txtLoanAccountNumber.text = liabilityDatum?.loanAccountNumber
            self.txtLoanDate.text = liabilityDatum?.liabilityDate
            self.txtInterestRate.text = liabilityDatum?.intrestRate
            self.txtInterestRate.isEnabled = false
            self.txtLoanType.isEnabled = false
            self.txtLoanDate.isEnabled = false
            
            txtInterestRate.textColor = UIColor(named: "textGrayColor")
            txtLoanType.textColor = UIColor(named: "textGrayColor")
            txtLoanDate.textColor = UIColor(named: "textGrayColor")
            
            let loanTerm = liabilityDatum?.loanTerm.components(separatedBy: " ")
            if loanTerm?.count ?? 0 > 1{
                self.txtLoanTerm.text =  loanTerm?[0]
                self.txtLoanTime.text = loanTerm?[1]
            }
        }
        
        if #available(iOS 13.4, *) {
            self.txtLoanDate.datePicker(target: self, doneAction: #selector(startDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date, setDate: "")
            //self.endDateTextField.datePicker(target: self, doneAction: #selector(endDoneAction), cancelACtion: #selector(cancelAction), datePickerMode: .date)
        } else {
            // Fallback on earlier versions
        }
        
        self.getAllLoanTypesServiceCall()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("AddLoan"), object: nil)
        
    }
    
    @objc func cancelAction() {
        self.txtLoanDate.resignFirstResponder()
    }
    
    @objc func startDoneAction() {
        if let datePickerView = self.txtLoanDate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "d MMM, yyyy"
            let dateString = dateFormatter.string(from: datePickerView.date)
            print("DATE: \(dateString)")
            self.txtLoanDate.text = DateTimeUtils.sharedInstance.formatFancyDate1(date: datePickerView.date) ?? ""
            print(datePickerView.date)
            print(dateString)
            self.txtLoanDate.resignFirstResponder()
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        self.addLoanService()
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtLoanName.text = txtLoanName.text?.trim()
        self.txtLoanType.text = self.txtLoanType.text?.trim()
        self.txtInstitutionName.text = self.txtInstitutionName.text?.trim()
        self.txtLoanAccountNumber.text = self.txtLoanAccountNumber.text?.trim()
        self.txtPrincipalAmount.text = self.txtPrincipalAmount.text?.trim()
        self.txtCurrentOutstanding.text = self.txtCurrentOutstanding.text?.trim()
        self.txtLoanTerm.text = self.txtLoanTerm.text?.trim()
        self.txtLoanDate.text = self.txtLoanDate.text?.trim()
        self.txtMonthlyPayment.text = self.txtMonthlyPayment.text?.trim()
        self.txtInterestRate.text = self.txtInterestRate.text?.trim()
        self.txtLoanTime.text = self.txtLoanTime.text?.trim()
        
        if (txtLoanName.text?.isEmpty)! {
            message = "Please Enter Loan Name"
            isFound = false
        } else if (txtLoanName.text?.length ?? 0 < 3){
            message = "The Loan Name must be at least 3 characters in length"
            isFound = false
        } else if (txtLoanType.text?.isEmpty)! {
            message = "Please Select  Loan Type"
            isFound = false
        } else if (txtInstitutionName.text?.isEmpty)! {
            message = "Please Enter Institution Name"
            isFound = false
        } else if (txtInstitutionName.text?.length ?? 0 < 3){
            message = "The Institution Name must be at least 3 characters in length"
            isFound = false
        } else if (txtLoanAccountNumber.text?.isEmpty)! {
            message = "Please Enter Loan Account Number"
            isFound = false
        } else if (txtLoanAccountNumber.text?.length ?? 0 < 10){
            message = "The Loan Account Number  must be at least 10 digits in length"
            isFound = false
        } else if (txtPrincipalAmount.text?.isEmpty)! {
            message = "Please Enter Original Loan Amount"
            isFound = false
        } else if (txtCurrentOutstanding.text?.isEmpty)! {
            message = "Please Enter Current Balance"
            isFound = false
        } else if (txtMonthlyPayment.text?.isEmpty)! {
            message = "Please Enter Monthly Payment"
            isFound = false
        }else if (txtInterestRate.text?.isEmpty)! {
            message = "Please Enter Interest Rate"
            isFound = false
        }else if (txtLoanDate.text?.isEmpty)! {
            message = "Please Enter Loan Date"
            isFound = false
        } else if (txtLoanTerm.text?.isEmpty)! {
            message = "Please Enter Loan Term "
            isFound = false
        } else if (txtLoanTime.text?.isEmpty)! {
            message = "Please Select Day/ Month/ Year"
            isFound = false
        }
        else if (txtPrincipalAmount.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        }else if (txtCurrentOutstanding.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Amount"
            isFound = false
        }else if (txtLoanTerm.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Loan term"
            isFound = false
        }else if (txtInterestRate.text?.toInt() ?? 0 <= 0) {
            message = "Please Enter Vaild Interest Rate"
            isFound = false
        }
        
        if !isFound {
            //Helper.sharedInstance.showToast(isError: true, title: message)
            self.showAlert(message: message)
        }
        return isFound
    }
    
    @IBAction func actionAddLoans(_ sender: Any) {
        self.view.endEditing(true)
        
        if update {
            if self.isValidFields() {
                self.updateLoanServiceCall()
            }
        }else{
            if self.isValidFields() {
                self.addLoanServiceCall()
            }
        }
        
    }
    
    func addLoanService (){
        self.view.endEditing(true)
        if update {
            if self.isValidFields() {
                self.updateLoanServiceCall()
            }
        }else{
            if self.isValidFields() {
                self.addLoanServiceCall()
            }
        }
    }
}


extension AddLoansVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtPrincipalAmount || textField == txtCurrentOutstanding || textField == txtMonthlyPayment || textField == txtInterestRate{
            // Allow to remove character (Backspace)
            if string == "" {
                return true
            }
            
            // Block multiple dot
            if (textField.text?.contains("."))! && string == "." {
                return false
            }
            
            
            if (textField.text?.contains("."))! {
                let limitDecimalPlace = 2
                let decimalPlace = textField.text?.components(separatedBy: ".").last
                if (decimalPlace?.count)! < limitDecimalPlace {
                    return true
                }
                else {
                    return false
                }
            }

        }
        
        let allowedCharacters : CharacterSet
        if textField == txtLoanName || textField == txtInstitutionName{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_letter).inverted
        }else{
            allowedCharacters = CharacterSet(charactersIn:AppConstant.capital_small_number_special_letter).inverted
        }
        
        let components = string.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        
        if textField == txtLoanName {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtLoanName.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }
            
        }
        
        if textField == txtLoanType {
            let maxLength = 25
            let currentString: NSString = txtLoanType.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtInstitutionName {
            
            if string == filtered {
                let maxLength = 25
                let currentString: NSString = txtInstitutionName.text! as NSString
                let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                return false
            }            
        }
        
        if textField == txtLoanAccountNumber {
            let maxLength = 16
            let currentString: NSString = txtLoanAccountNumber.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtPrincipalAmount {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtPrincipalAmount.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtCurrentOutstanding {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtCurrentOutstanding.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtMonthlyPayment {
            let maxLength = AppConstant.maxLength
            let currentString: NSString = txtMonthlyPayment.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtInterestRate {
            
            let allowedCharacters = CharacterSet(charactersIn:"1234567890.").inverted
            
            let components = string.components(separatedBy: allowedCharacters)
            let filtered = components.joined(separator: "")
            
            if string == filtered {
                
                return true
                
            } else {
                
                return false
            }
        }else if textField == txtInterestRate {
            let maxLength = 3
            let currentString: NSString = txtInterestRate.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == txtLoanTerm {
            let maxLength = 3
            let currentString: NSString = txtLoanTerm.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        
        return true
    }
    
    func pickUP(_ textField: UITextField) {
        
        if textField == txtLoanType {
            self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.picker.delegate = self
            self.picker.dataSource = self
            self.picker.selectRow(0, inComponent: 0, animated: true)
            self.selectedType = loanTypes[0]
            self.selectedTypeId = self.loanTypeModel?.data.loanTypes[0].loanTypeID ?? ""
            //        self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
            //        self.selectedCategory = self.mainCategoryModel?.data[0].categoryName ?? ""
            txtLoanType.inputView = self.picker
            //
            let toolBar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.sizeToFit()
            //
            // Adding Button ToolBar
            let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
            
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            txtLoanType.inputAccessoryView = toolBar
        }else if textField == txtLoanTime  {
            self.pickerTime = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerTime.delegate = self
            self.pickerTime.dataSource = self
            self.pickerTime.selectRow(0, inComponent: 0, animated: true)
            //self.selectedType = loanTime[0]
            self.selectedLoanTime = self.loanTime[0]
            // self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
            // self.selectedCategory = self.mainCategoryModel?.data[0].categoryName ?? ""
            txtLoanTime.inputView = self.pickerTime
            let toolBar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.sizeToFit()
            // Adding Button ToolBar
            let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectTimeTypeClick))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTimeTypeClick))
            
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            txtLoanTime.inputAccessoryView = toolBar
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtLoanType {
            self.pickUP(txtLoanType)
        }else if textField == txtLoanTime {
            self.pickUP(txtLoanTime)
        }
        
    }
    
    @objc func selectClick() {
        
        self.txtLoanType.text = self.selectedType
        txtLoanType.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtLoanType.resignFirstResponder()
    }
    
    @objc func selectTimeTypeClick() {
        
        self.txtLoanTime.text = self.selectedLoanTime
        txtLoanTime.resignFirstResponder()
    }
    
    @objc func cancelTimeTypeClick() {
        txtLoanTime.resignFirstResponder()
    }
}

//MARK: - UIPicker Delegates
extension AddLoansVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.picker{
            return self.loanTypeModel?.data.loanTypes.count ?? 0
        }else{
            return self.loanTime.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        
        if pickerView == self.picker{
            var label: UILabel
            
            if let view = view as? UILabel {
                label = view
            } else {
                label = UILabel()
            }
            
            
            label.text = self.loanTypeModel?.data.loanTypes[row].loanTypeName
            label.textAlignment = .center
            label.font = UIFont(name: "Filson Pro", size: 18)
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            
            return label
        }else{
            var label: UILabel
            
            if let view = view as? UILabel {
                label = view
            } else {
                label = UILabel()
            }
            
            label.text = self.loanTime[row]
            label.textAlignment = .center
            label.font = UIFont(name: "Filson Pro", size: 18)
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            
            return label
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.picker{
            self.selectedType = self.loanTypeModel?.data.loanTypes[row].loanTypeName ?? ""
            self.selectedTypeId = self.loanTypeModel?.data.loanTypes[row].loanTypeID ?? ""
        }else{
            self.selectedLoanTime = self.loanTime[row]
            
        }
    }
}

//MARK: - Service Calls
extension AddLoansVC {
    func addLoanServiceCall() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            NotificationCenter.default.post(name: Notification.Name("RemoveLoader"), object: nil, userInfo: nil)
        }
        
        self.txtLoanName.text = txtLoanName.text?.trim()
        self.txtLoanType.text = self.txtLoanType.text?.trim()
        self.txtInstitutionName.text = self.txtInstitutionName.text?.trim()
        self.txtLoanAccountNumber.text = self.txtLoanAccountNumber.text?.trim()
        self.txtPrincipalAmount.text = self.txtPrincipalAmount.text?.trim()
        self.txtCurrentOutstanding.text = self.txtCurrentOutstanding.text?.trim()
        self.txtLoanTerm.text = self.txtLoanTerm.text?.trim()
        self.txtLoanDate.text = self.txtLoanDate.text?.trim()
        self.txtMonthlyPayment.text = self.txtMonthlyPayment.text?.trim()
        self.txtInterestRate.text = self.txtInterestRate.text?.trim()
        
        NotificationCenter.default.post(name: Notification.Name("AddLoader"), object: nil, userInfo: nil)
        //self.showLoader()
        let parameter:[String:Any] = [
            
            "monthly_payment":txtMonthlyPayment.text ?? "",
            "intrest_rate":txtInterestRate.text ?? "",//"5.5", //(%)
            "original_loan_date": self.txtLoanDate.text ?? "",//"12/1/2021", //(m/d/Y)
            "loan_name": self.txtLoanName.text ?? "",
            "loan_type": self.selectedTypeId,//self.txtLoanType.text ?? "",
            "institution_name":self.txtInstitutionName.text ?? "",
            "loan_account_number":self.txtLoanAccountNumber.text ?? "",
            "principle_amount":self.txtPrincipalAmount.text ?? "",
            "current_outstanding_amount":self.txtCurrentOutstanding.text ?? "",
            "loan_term":self.txtLoanTerm.text ?? "",
            "loan_term_period":self.selectedLoanTime.lowercased(),
            "category_code":"LOAN"
            
        ]
        
        let liabilitieService = LiabilitieService()
        liabilitieService.addLoanLiabilities(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            
            NotificationCenter.default.post(name: Notification.Name("RemoveLoader"), object: nil, userInfo: nil)
            self.removLoader()
        }
    }
    
    func getAllLoanTypesServiceCall(){
        
        //self.showLoader()
        let parameter:[String:Any] = [
            "category_id": "9"
        ]
        
        let liabilitieService = LiabilitieService()
        liabilitieService.getAllLoanTypes(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.loanTypeModel = responseData
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
    func updateLoanServiceCall() {
        
        //self.showLoader()
        let parameter:[String:Any] = [
            
            "liability_id":liabilityDatum?.liabilityID ?? "",
            "monthly_payment":txtMonthlyPayment.text ?? "",
            "intrest_rate":txtInterestRate.text ?? "",//"5.5", //(%)
            "original_loan_date": self.txtLoanDate.text ?? "",//"12/1/2021", //(m/d/Y)
            "loan_name": self.txtLoanName.text ?? "",
            "loan_type": self.txtLoanType.text ?? "",
            "institution_name":self.txtInstitutionName.text ?? "",
            "loan_account_number":self.txtLoanAccountNumber.text ?? "",
            "principle_amount":self.txtPrincipalAmount.text ?? "",
            "current_outstanding_amount":self.txtCurrentOutstanding.text ?? "",
            "loan_term":self.txtLoanTerm.text ?? "",
            "loan_term_period":self.txtLoanTime.text!.lowercased(),
            "category_code":"LOAN"            
        ]
        print("para",parameter)
        
        let liabilitieService = LiabilitieService()
        liabilitieService.updateLoanLiability(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    if responseData.status == 200{
                        self.navigationController?.popViewController(animated: false)
                    }
                    self.showAlert(message: responseData.message)
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
        
    }
}

