import UIKit
import Foundation

class AllLoansVC: BaseVC {
    
    var navTitle: String = ""
    var categoryId: String = ""
    var assetDetailByCategoryModel : AssetDetailByCategoryModel?
    var assetSubCategory: [String] = [ ]
    var libCategory: String = ""
    var liabilitiesTotalValue: String = ""
    var catLiabilitiesData: CatLiabilitiesData?
    var dataArray = NSMutableArray()
    var DicAsset = [String:[LiabilityDatum]]()
    var dicPlaidLiability = [String:[LiabilityPlaidData]]()
    var getCreditCardPlaidModel : GetCreditCardPlaidModel?
    var liabilityDetailByCategoryModel : LiabilityDetailByCategoryModel?
    var getAllPlaidLiabilityDetailByCategoryModel : GetAllPlaidLiabilityDetailByCategoryModel?
    var getCreditCardModel : GetCreditCardModel?
    var data: String = "Manual"
    
    // IBOutlets
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var lblAssetAmt: UILabel!
    @IBOutlet weak var lblAssetdifference: UILabel!
    @IBOutlet weak var assetTable: UITableView!
    @IBOutlet weak var lblAssetSmallHdr: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    //@IBOutlet weak var assetCollection: UICollectionView!
    @IBOutlet weak var btnAddView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let assetCurrentValue = "\(liabilitiesTotalValue)".first {
            if assetCurrentValue == "-"{
                self.lblAssetAmt.text = "\(liabilitiesTotalValue)".replacingOccurrences(of: "-", with: "-$")
            }else{
                self.lblAssetAmt.text = "\(liabilitiesTotalValue)"
            }
        }
        //self.lblAssetAmt.text = "\(liabilitiesTotalValue)".convertDoubleToCurrency()
        assetTable.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lbl_NavigationTitle.text = navTitle
        //self.lblAssetAmt.text = catLiabilitiesData.d
        switch navTitle {
        case "Loan":
            //self.getAllAssetDetailByCategoryServiceCall()
            self.getAllLiabilityDetailByCategoryServiceCall(categoryName: "LOAN")
            assetTable.isHidden = false
            
        case "Credit Card":
            self.getAllLiabilityDetailByCategoryServiceCall_CC(categoryName: "CREDITCARD")
            assetTable.isHidden = false
            
        default:
            assetTable.isHidden = true
            
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //self.assetData.removeAll()
    }
    
    @IBAction func actionAddAsset(_ sender: Any) {
        
        switch navTitle {
        case "Loan":
            //            let key = assetSubCategory[indexPath.section]
            //            let category = self.DicAsset[key]
            //            let objDict = category?[indexPath.row]
            
            let objVC: AddingLiabilitiesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.liabilities, viewControllerName: AddingLiabilitiesVC.nameOfClass)
            // objVC.liabilityDatum = objDict
            objVC.selectedCategory = "Loan"
            objVC.addDefault = true
            //objVC.update = true
            self.navigationController?.pushViewController(objVC, animated: true)
        case "Credit Card":
            //let objDict = self.getCreditCardModel?.data.liabilityData[indexPath.row]
            let objVC: AddingLiabilitiesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.liabilities, viewControllerName: AddingLiabilitiesVC.nameOfClass)
            //objVC.getCreditCardData = objDict
            objVC.selectedCategory = "Credit Card"
            //objVC.update = true
            objVC.addDefault = true
            self.navigationController?.pushViewController(objVC, animated: true)
        default: break
            
        }
    }
    
    @IBAction func dataChanged(sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            btnAddView.isHidden = false
            data = "Manual"
            NSLog("manual selected")
            switch navTitle {
            case "Loan":
                self.getAllLiabilityDetailByCategoryServiceCall(categoryName: "LOAN")
                assetTable.isHidden = false
            case "Credit Card":
                self.getAllLiabilityDetailByCategoryServiceCall_CC(categoryName: "CREDITCARD")
                assetTable.isHidden = false
            default:
                assetTable.isHidden = true
            }
            
        case 1:
            btnAddView.isHidden = true
            data = "Plaid"
            NSLog("Plaid selected")
            switch navTitle {
            case "Loan":
                self.getAllPlaidLiabilityDetailByCategoryServiceCall(categoryName: "LOAN")
                assetTable.isHidden = false
            case "Credit Card":
                self.getAllPlaidLiabilityDetailByCategoryServiceCall_CC(categoryName: "CREDITCARD")
                assetTable.isHidden = false
            default:
                assetTable.isHidden = true
            }
        default:
            break;
        }
    }
    
}

//MARK: - TableView for Stocks and more
extension AllLoansVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch navTitle {
        case "Loan":
            
            let view = UIView(frame: CGRect(x:0, y:10, width:tableView.frame.size.width, height:18))
            let label = UILabel(frame: CGRect(x:10, y:10, width:tableView.frame.size.width, height:18))
            label.font = UIFont.systemFont(ofSize: 18)
            label.text = assetSubCategory[section]
            view.addSubview(label);
            view.backgroundColor = UIColor.white;
            return view
            
        case "Credit Card":
            
            let view = UIView(frame: CGRect(x:0, y:10, width:tableView.frame.size.width, height:18))
            let label = UILabel(frame: CGRect(x:10, y:10, width:tableView.frame.size.width, height:18))
            label.font = UIFont.systemFont(ofSize: 18)
            label.text = "Credit Card"
            view.addSubview(label);
            view.backgroundColor = UIColor.white;
            return view
            
        default:
            let view = UIView(frame: CGRect(x:0, y:10, width:tableView.frame.size.width, height:18))
            let label = UILabel(frame: CGRect(x:10, y:0, width:tableView.frame.size.width, height:18))
            label.font = UIFont.systemFont(ofSize: 18)
            label.text = ""
            view.addSubview(label);
            view.backgroundColor = UIColor.white;
            return view
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch navTitle {
        case "Loan":
            if data == "Manual"{
                return self.DicAsset.keys.count
            }else{
                return self.dicPlaidLiability.keys.count
            }
            
        case "Credit Card":
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch navTitle {
        case "Loan":
            if data == "Manual"{
                let cate = assetSubCategory[section]
                return self.DicAsset[cate]?.count ?? 0
            }else{
                let cate = assetSubCategory[section]
                return self.dicPlaidLiability[cate]?.count ?? 0
            }
        case "Credit Card":
            if data == "Manual"{
                return self.getCreditCardModel?.data.liabilityData.count ?? 0
            }else{
                return self.getCreditCardPlaidModel?.data.liabilityData.count ?? 0
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = assetTable.dequeueReusableCell(withIdentifier: "assetCell", for: indexPath) as! AssetTableCell
        
        switch navTitle {
        case "Loan":
            
            if data == "Manual"{
                
                let key = assetSubCategory[indexPath.section]
                let category = self.DicAsset[key]
                let objDict = category?[indexPath.row]
                
                cell.assetDate.isHidden = false
                cell.btnDelete.isHidden = false
                cell.btnShowDelete.isHidden = false
                
                cell.assetName.text = objDict?.loanName
                cell.assetAmt.text = "\(objDict!.currentOutstandingAmount)".convertDoubleToCurrency()
                cell.assetDate.text = DateTimeUtils.sharedInstance.getSeenAtDateFromString(strDate: objDict!.liabilityDate)
                cell.assetImg.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
                cell.assetDifference.isHidden = true
                
                let assetId : Int = Int (objDict?.liabilityID ?? "") ?? 0
                cell.btnDelete.tag =  assetId
                cell.btnDelete.addTarget(self, action: #selector(self.actionDelete(_:)), for: UIControl.Event.touchUpInside)
                
            }else{
                let key = assetSubCategory[indexPath.section]
                let category = self.dicPlaidLiability[key]
                let objDict = category?[indexPath.row]
                
                cell.assetDate.isHidden = true
                cell.btnDelete.isHidden = true
                cell.btnShowDelete.isHidden = true
                
                cell.assetName.text = objDict?.categoryName
                cell.assetAmt.text = "\(objDict!.currentOutstandingAmount)".convertDoubleToCurrency()
                cell.assetImg.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
                
            }
            
        case "Credit Card":
            
            if data == "Manual"{
                
                cell.assetDate.isHidden = false
                cell.btnDelete.isHidden = false
                cell.btnShowDelete.isHidden = false
                
                let objDict = self.getCreditCardModel?.data.liabilityData[indexPath.row]
                cell.assetName.text = objDict?.cardName
                cell.assetDate.text = DateTimeUtils.sharedInstance.getSeenAtDateFromString(strDate: objDict!.liabilityDate)
                cell.assetAmt.text = "\(objDict!.cardCurrentOutstanding)".convertDoubleToCurrency()
                cell.assetDifference.isHidden = true
                cell.assetImg.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
                
                let assetId : Int = Int (objDict?.liabilityID ?? "") ?? 0
                cell.btnDelete.tag =  assetId
                cell.btnDelete.addTarget(self, action: #selector(self.actionDelete(_:)), for: UIControl.Event.touchUpInside)
                cell.lblIcon.font = UIFont(name: "Font Awesome 6 Free Solid", size: 22)
                cell.lblIcon.text = "\u{f09d}"
                return cell
            }else{
                let objDict = self.getCreditCardPlaidModel?.data.liabilityData[indexPath.row]
                cell.assetName.text = objDict?.categoryName
                
                cell.assetDate.isHidden = true
                cell.btnDelete.isHidden = true
                cell.btnShowDelete.isHidden = true
                
                cell.assetAmt.text = "\(objDict!.currentOutstandingAmount)".convertDoubleToCurrency()
                cell.assetDifference.isHidden = true
                cell.assetImg.sd_setImage(with: URL(string: objDict?.categoryImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
                return cell
            }
            
        default:
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if data == "Manual"{
            switch navTitle {
            case "Loan":
                let key = assetSubCategory[indexPath.section]
                let category = self.DicAsset[key]
                let objDict = category?[indexPath.row]
                
                let objVC: AddingLiabilitiesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.liabilities, viewControllerName: AddingLiabilitiesVC.nameOfClass)
                objVC.liabilityDatum = objDict
                objVC.selectedCategory = "Loan"
                objVC.update = true
                self.navigationController?.pushViewController(objVC, animated: true)
            case "Credit Card":
                let objDict = self.getCreditCardModel?.data.liabilityData[indexPath.row]
                let objVC: AddingLiabilitiesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.liabilities, viewControllerName: AddingLiabilitiesVC.nameOfClass)
                objVC.getCreditCardData = objDict
                objVC.selectedCategory = "Credit Card"
                objVC.update = true
                self.navigationController?.pushViewController(objVC, animated: true)
            default: break
                
            }
        }
     
    }
    
    @IBAction func actionDelete(_ sender: UIButton){
        
        let alert = UIAlertController(title: "Render", message: "Are you sure to delete this item?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {_ in
            
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: {_ in
            self.deleteLiabilityServiceCall(liability_id: String(sender.tag))
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension AllLoansVC {
    
    
    func getAllLiabilityDetailByCategoryServiceCall(categoryName:String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_code": categoryName
        ]
        
        let netWorthService = NetWorthService()
        netWorthService.getAllLiabilityDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    switch self.navTitle {
                    case "Loan":
                        self.assetSubCategory.removeAll()
                        self.liabilityDetailByCategoryModel = responseData
                        self.lblAssetAmt.text = "\(responseData.data.totalLiabilities)".convertDoubleToCurrency()
                        self.DicAsset = Dictionary(grouping: ((self.liabilityDetailByCategoryModel?.data.liabilityData)!)) { $0.loanType }
                        self.liabilityDetailByCategoryModel?.data.liabilityData.forEach { subcategoryName in
                            self.assetSubCategory.append(subcategoryName.loanType)
                        }
                        self.assetSubCategory = NSOrderedSet(array: self.assetSubCategory).map({ $0 as! String })
                        self.assetTable.reloadData()
                    case "Credit Card":
                        self.assetTable.reloadData()
                        
                    default: break
                    }
                    //  self.showAlert(message: responseData.message)
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
        
    }
    
    func getAllLiabilityDetailByCategoryServiceCall_CC(categoryName:String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_code": categoryName
        ]
        
        let liabilitieService = LiabilitieService()
        liabilitieService.getAllLiabilityDetailByCategoryCreditCard(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.lblAssetAmt.text = "\(responseData.data.totalLiabilities)".convertDoubleToCurrency()
                    
                    self.getCreditCardModel = responseData
                    self.assetTable.reloadData()
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
    func deleteLiabilityServiceCall(liability_id: String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "liability_id": liability_id
        ]
        
        let liabilitieService = LiabilitieService()
        liabilitieService.deleteLiabilitie(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    switch self.navTitle {
                    case "Loan":
                        //self.getAllAssetDetailByCategoryServiceCall()
                        self.getAllLiabilityDetailByCategoryServiceCall(categoryName: "LOAN")
                    case "Credit Card":
                        self.getAllLiabilityDetailByCategoryServiceCall_CC(categoryName: "CREDITCARD")
                    default: break
                    }
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
    func getAllPlaidLiabilityDetailByCategoryServiceCall(categoryName:String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_code": categoryName
        ]
        
        let liabilitieService = LiabilitieService()
        liabilitieService.getAllPlaidLiabilityDetailByCategory(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    
                    switch self.navTitle {
                    case "Loan":
                        self.assetSubCategory.removeAll()
                        self.getAllPlaidLiabilityDetailByCategoryModel = responseData
                        self.lblAssetAmt.text = "\(responseData.data.totalLiabilities)".convertDoubleToCurrency()
                        
                        self.dicPlaidLiability = Dictionary(grouping: ((self.getAllPlaidLiabilityDetailByCategoryModel?.data.liabilityData)!)) { $0.loanType }
                        
                        self.getAllPlaidLiabilityDetailByCategoryModel?.data.liabilityData.forEach { subcategoryName in
                            self.assetSubCategory.append(subcategoryName.loanType)
                        }
                        
                        self.assetSubCategory = NSOrderedSet(array: self.assetSubCategory).map({ $0 as! String })
                        self.assetTable.reloadData()
                    case "Credit Card": break
                        //  self.assetTable.reloadData()
                    default: break
                    }
                    //  self.showAlert(message: responseData.message)
                    
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
    
    
    func getAllPlaidLiabilityDetailByCategoryServiceCall_CC(categoryName:String){
        
        self.showLoader()
        let parameter:[String:Any] = [
            "category_code": categoryName
        ]
        
        let liabilitieService = LiabilitieService()
        liabilitieService.getAllPlaidLiabilityDetailByCategoryCreditCard(parameters: parameter){ (result,code) in
            switch result{
            case .success(let responseData):
                if code == 200 {
                    self.lblAssetAmt.text = "\(responseData.data.totalLiabilities)".convertDoubleToCurrency()
                    self.getCreditCardPlaidModel = responseData
                    self.assetTable.reloadData()
                }else{
                    self.showAlert(message: responseData.message)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(message: "We could not process your request at this time. Please try again or contact your Advisor for assistance.")
            }
            self.removLoader()
        }
    }
}




