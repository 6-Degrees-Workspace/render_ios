import UIKit

class AddingLiabilitiesVC: BaseVC {
    
    var selectedCategory: String = "Select Category"
    var liabilityCat:[String] = [
        "Loan",
        "Credit Card"
    ]
    
    var frameSize: CGRect = UIScreen.main.bounds
    var picker: UIPickerView!
    var getCreditCardData : LiabilityData?
    var catLiabilitiesData: CatLiabilitiesData?
    var liabilityDatum: LiabilityDatum?
    var update: Bool = false
    var addDefault: Bool = false
    
    @IBOutlet weak var addLoanContainer: UIView!
    @IBOutlet weak var addCCContainer: UIView!
    @IBOutlet weak var addLiabiltityTxtFld: UITextField!
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var btnAddLiabilities: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfAddLoader(notification:)), name: Notification.Name("AddLoader"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfRemoveLoader(notification:)), name: Notification.Name("RemoveLoader"), object: nil)
        self.hideContainers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if addDefault {
            self.btnAddLiabilities.setTitle("Add Liability", for: .normal)
            self.addLiabiltityTxtFld.text = selectedCategory
            addLiabiltityTxtFld.isEnabled = false
            if self.selectedCategory == "Loan" {
                self.addCCContainer.isHidden = true
                self.addLoanContainer.isHidden = false
            }else if self.selectedCategory == "Credit Card" {
                self.addCCContainer.isHidden = false
                self.addLoanContainer.isHidden = true
            }else {
                self.hideContainers()
            }
        }
        
        if update {
            addLiabiltityTxtFld.textColor = UIColor(named: "textGrayColor")
        
            self.addLiabiltityTxtFld.text = selectedCategory
            self.btnAddLiabilities.setTitle("Update Liability", for: .normal)
            addLiabiltityTxtFld.isEnabled = false
            if self.selectedCategory == "Loan" {
                self.addCCContainer.isHidden = true
                self.addLoanContainer.isHidden = false
            }else if self.selectedCategory == "Credit Card" {
                self.addCCContainer.isHidden = false
                self.addLoanContainer.isHidden = true
            }else {
                self.hideContainers()
            }
        }else{
            self.btnAddLiabilities.setTitle("Add Liability", for: .normal)
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        if UIScreen.main.sizeType == .iPhone4 || UIScreen.main.sizeType == .iPhone5 || UIScreen.main.sizeType == .iPhone6 || UIScreen.main.sizeType == .iPhone6Plus{
            if  selectedCategory == "Credit Card" {
                self.bottomConst.constant = -20
                //scroller.contentSize = CGSize(width: 325,height:800)
            }else{
                self.bottomConst.constant = 100
                scroller.contentSize = CGSize(width: 325,height:800)
            }
        }else{
            self.bottomConst.constant = -80
        }
        scroller.isScrollEnabled = true
        
    }
    
    @IBAction func AddingLiabilitiesVCPressed(_ sender: UIButton) {
        if selectedCategory != "Select Category" {
            if  selectedCategory == "Credit Card" {
                NotificationCenter.default.post(name: Notification.Name("AddCreditCard"), object: nil, userInfo: nil)
            }else{
                NotificationCenter.default.post(name: Notification.Name("AddLoan"), object: nil, userInfo: nil)
            }
        }
    }
    
    @objc func methodOfAddLoader(notification: Notification) {
        self.showLoader()
    }
    
    @objc func methodOfRemoveLoader(notification: Notification) {
        self.removLoader()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "toLoans":
            if let accountVC = segue.destination as? AddLoansVC {
                accountVC.liabilityDatum = liabilityDatum
                accountVC.update = update
                accountVC.frameSize = self.frameSize
                accountVC.safe = self.view.safeAreaLayoutGuide.layoutFrame
            }
        case "toCreditCard":
            if let cdVC = segue.destination as? AddCreditCardsVC {
                cdVC.getCreditCardData = getCreditCardData
                cdVC.update = update
                cdVC.frameSize = frameSize
            }
            
        default:
            print("None")
        }
        
        if segue.identifier == "toAccounts" {
            
        }
    }
}

extension AddingLiabilitiesVC: UITextFieldDelegate {
    func pickUP(_ textField: UITextField) {
        self.picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.selectRow(0, inComponent: 0, animated: true)
        //self.mainCategoryID = self.mainCategoryModel?.data[0].categoryID ?? ""
        self.selectedCategory = self.liabilityCat[0] //self.mainCategoryModel?.data[0].categoryName ?? ""
        addLiabiltityTxtFld.inputView = self.picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        addLiabiltityTxtFld.inputAccessoryView = toolBar
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUP(addLiabiltityTxtFld)
    }
    
    
    @objc func selectClick() {
        
        self.addLiabiltityTxtFld.text = selectedCategory
        if self.selectedCategory == "Loan" {
            self.addCCContainer.isHidden = true
            self.addLoanContainer.isHidden = false
        }else if self.selectedCategory == "Credit Card" {
            self.addCCContainer.isHidden = false
            self.addLoanContainer.isHidden = true
        }else {
            self.hideContainers()
        }
        //self.addPersonalContainer.isHidden = true
        addLiabiltityTxtFld.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        addLiabiltityTxtFld.resignFirstResponder()
    }
    
    func hideContainers() {
        self.addCCContainer.isHidden = true
        self.addLoanContainer.isHidden = true
    }
}


extension AddingLiabilitiesVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.liabilityCat.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        //let objDict = self.mainCategoryModel?.data[row]
        label.text = self.liabilityCat[row] //objDict?.categoryName//assetCategory[row]
        label.textAlignment = .center
        label.font = UIFont(name: "Filson Pro", size: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedCategory = self.liabilityCat[row]
    }
    
}
