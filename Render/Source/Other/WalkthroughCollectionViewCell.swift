import UIKit

class WalkthroughCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var cellView: UIView!
    
    static let identifier = "OnboardingCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(page: WalkthroughPageModel) {
        self.cellView.backgroundColor = UIColor(named: page.bgColor)
        self.imgView.image = UIImage(named: page.imageName)
        self.titleLabel.text = page.title
        self.textView.text = page.description
    }
    

}
