import UIKit

class WalkthroughCell: UICollectionViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var getStartedBtn: UIButton!
    
    
    var getStartedBtnAction: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.getStartedBtn.addTarget(self, action: #selector(getStartedBtnTapped), for: .touchUpInside)
    }
    
    
    @objc func getStartedBtnTapped() {
        self.getStartedBtnAction?()
    }
    
    func configureCell(page: WalkthroughPageModel, indexPath: IndexPath) {
        titleLbl.text = page.title
        descriptionLbl.text = page.description
        imgView.image = UIImage(named: page.imageName)
        print(Float((indexPath.row + 1)) / 3.0)
        progressView.progress = Float((indexPath.row + 1)) / 3.0
    }
    
}
