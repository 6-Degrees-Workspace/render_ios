import Foundation

// MARK: - Welcome
struct LiabilityDetailByLiabilityIdModel: Codable {
    let status: Int
    let message: String
    let data: LiabilityDetailByLiabilityIdData
}

// MARK: - DataClass
struct LiabilityDetailByLiabilityIdData: Codable {
    let liabilityData: [liabilityDatas]
}

// MARK: - LiabilityDatum
struct liabilityDatas: Codable {
    let remainingAmount, intrestRate, liabilityName, liabilityID: String
    let loanAmount, monthlyEmi, loanTenure: String
    let principleAmount: Int
    let liabilityDatumDescription, emiDate: String

    enum CodingKeys: String, CodingKey {
        case remainingAmount = "remaining_amount"
        case intrestRate = "intrest_rate"
        case liabilityName = "liability_name"
        case liabilityID = "liability_id"
        case loanAmount = "loan_amount"
        case monthlyEmi = "monthly_emi"
        case loanTenure = "loan_tenure"
        case principleAmount = "principle_amount"
        case liabilityDatumDescription = "description"
        case emiDate = "emi_date"
    }
}
