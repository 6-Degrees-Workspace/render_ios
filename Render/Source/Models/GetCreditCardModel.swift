import Foundation

// MARK: - Welcome
struct GetCreditCardModel: Codable {
    let status: Int
    let message: String
    let data: GetCreditCardData
}

// MARK: - DataClass
struct GetCreditCardData: Codable {
    let categoryWiseTotalLiabilitiesValue: String
    let plaidLiabilitiesValues: String
    let totalLiabilities: String
    let liabilityData: [LiabilityData]

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalLiabilitiesValue = "category_wise_total_liabilities_value"
        case plaidLiabilitiesValues = "plaid_liabilities_values"
        case totalLiabilities = "total_liabilities"
        case liabilityData
    }
}

// MARK: - LiabilityDatum
struct LiabilityData: Codable {
    let institutionName, liabilityDate, cardName, categoryID: String
    let categoryImageURL: String
    let categoryCode, availableLimit, categoryIconHexcode, categoryName: String
    let liabilityID, cardCurrentOutstanding, cardNumber: String

    enum CodingKeys: String, CodingKey {
        case institutionName = "institution_name"
        case liabilityDate = "liability_date"
        case cardName = "card_name"
        case categoryID = "category_id"
        case categoryImageURL = "category_image_url"
        case categoryCode = "category_code"
        case availableLimit = "available_limit"
        case categoryIconHexcode = "category_icon_hexcode"
        case categoryName = "category_name"
        case liabilityID = "liability_id"
        case cardCurrentOutstanding = "card_current_outstanding"
        case cardNumber = "card_number"
    }
}
