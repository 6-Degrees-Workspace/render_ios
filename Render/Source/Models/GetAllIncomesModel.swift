import Foundation

// MARK: - Welcome
struct GetAllIncomesModel: Codable {
    let status: Int
    let message: String
    let data: GetAllIncomesData
}

// MARK: - DataClass
struct GetAllIncomesData: Codable {
    let catData: [IncomeData]
    let incomesTotalValue, incomesTotalValueOriginal: String

    enum CodingKeys: String, CodingKey {
        case catData
        case incomesTotalValue = "incomes_total_value"
        case incomesTotalValueOriginal = "incomes_total_value_original"
    }
}

// MARK: - CatDatum
struct IncomeData: Codable {
    let categoryID, categoryCode, categoryName, categoryIconHexCode: String
    let categoryImageURL: String
    let incomeValue, incomeValueOriginal: String

    enum CodingKeys: String, CodingKey {
        case categoryID = "category_id"
        case categoryCode = "category_code"
        case categoryName = "category_name"
        case categoryIconHexCode = "category_icon_hex_code"
        case categoryImageURL = "category_image_url"
        case incomeValue = "income_value"
        case incomeValueOriginal = "income_value_original"
    }
}
