// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct GetAllPlaidIncomeDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: GetAllPlaidIncomeDetailData
}

// MARK: - DataClass
struct GetAllPlaidIncomeDetailData: Codable {
    let categoryWiseTotalManualIncomes, categoryWiseTotalPlaidIncomes, totalIncomes: String
    let incomeData: [IncomePlaidData]

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalManualIncomes = "category_wise_total_manual_incomes"
        case categoryWiseTotalPlaidIncomes = "category_wise_total_plaid_incomes"
        case totalIncomes = "total_incomes"
        case incomeData
    }
}

// MARK: - IncomeDatum
struct IncomePlaidData: Codable {
    let incomeAmount, incomeDate, categoryID, categoryName: String
    let categoryCode, categoryIcon: String
    let categoryImageURL: String

    enum CodingKeys: String, CodingKey {
        case incomeAmount = "income_amount"
        case incomeDate = "income_date"
        case categoryID = "category_id"
        case categoryName = "category_name"
        case categoryCode = "category_code"
        case categoryIcon = "category_icon"
        case categoryImageURL = "category_image_url"
    }
}
