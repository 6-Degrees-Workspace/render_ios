import Foundation

// MARK: - Welcome
struct GetDashboardDataModel: Codable {
    let status: Int
    let message: String
    let data: GetDashboardData
}

// MARK: - DataClass
struct GetDashboardData: Codable {
    let totalExpenseValue, totalLiabilityValue, totalAssetValue, totalIncomeValue: String
    let upArrow, netWorth, downArrow, netIncome: String

    enum CodingKeys: String, CodingKey {
        case totalExpenseValue, totalLiabilityValue, totalAssetValue, totalIncomeValue, upArrow
        case netWorth = "net_worth"
        case downArrow
        case netIncome = "net_income"
    }
}
