import Foundation

// MARK: - Welcome
struct LiabilitiesCreditCardModel: Codable {
    let status: Int
    let message: String
    let data: DataCreditCard
}

// MARK: - DataClass
struct DataCreditCard: Codable {
    let cardHolderName, cardNumber, cardIntrest, liabilityName: String
    let liabilityID, bankName, dataDescription, cardDueDate: String
    let cardAmount: String

    enum CodingKeys: String, CodingKey {
        case cardHolderName = "card_holder_name"
        case cardNumber = "card_number"
        case cardIntrest = "card_intrest"
        case liabilityName = "liability_name"
        case liabilityID = "liability_id"
        case bankName = "bank_name"
        case dataDescription = "description"
        case cardDueDate = "card_due_date"
        case cardAmount = "card_amount"
    }
}
