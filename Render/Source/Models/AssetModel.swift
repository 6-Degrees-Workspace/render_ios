import Foundation

// MARK: - Welcome
struct AssetModel: Codable {
    let status: Int
    let message: String
    let data: DataGetAsset
}

// MARK: - DataClass
struct DataGetAsset: Codable {
    let assetsTotalValue: String
    let assetsTotalValueOrginal:String
    let catData: [CatDatum]

    enum CodingKeys: String, CodingKey {
        case assetsTotalValue = "assets_total_value"
        case assetsTotalValueOrginal = "assets_total_value_original"
        case catData
    }
}

// MARK: - CatDatum
struct CatDatum: Codable {
    let categoryIconHexCode, categoryName, assetCurrentValue, assetCurrentValueOrginal, categoryID: String
    let categoryImageURL: String

    enum CodingKeys: String, CodingKey {
        case categoryIconHexCode = "category_icon_hex_code"
        case categoryName = "category_name"
        case assetCurrentValue = "asset_current_value"
        case categoryID = "category_id"
        case categoryImageURL = "category_image_url"
        case assetCurrentValueOrginal = "asset_current_value_orginal"
        
    }
}
