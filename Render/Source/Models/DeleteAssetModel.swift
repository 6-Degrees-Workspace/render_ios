import Foundation

// MARK: - Welcome
struct DeleteAssetModel: Codable {
    let status: Int
    let message: String
}
