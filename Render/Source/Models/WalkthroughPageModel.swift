import Foundation

struct WalkthroughPageModel {
    let imageName: String
    let bgColor: String
    let title: String
    let description: String
}

