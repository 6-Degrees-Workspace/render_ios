import Foundation

// MARK: - Welcome
struct AssetAddModel: Codable {
    let status: Int
    let message: String
    //let data: DataAsset
}

// MARK: - DataClass
struct DataAsset: Codable {
    let assetDate, assetID, assetName, assetType: String
    let categoryName, institutionName, assetCurrentValue, accountNumber: String
    
    enum CodingKeys: String, CodingKey {
        case assetDate = "asset_date"
        case assetID = "asset_id"
        case assetName = "asset_name"
        case assetType = "asset_type"
        case categoryName = "category_name"
        case institutionName = "institution_name"
        case assetCurrentValue = "asset_current_value"
        case accountNumber = "account_number"
    }
}
