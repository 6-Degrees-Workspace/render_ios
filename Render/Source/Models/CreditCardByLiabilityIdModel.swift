import Foundation

// MARK: - Welcome
struct CreditCardByLiabilityIdModel: Codable {
    let status: Int
    let message: String
    let data: CreditCardByLiabilityData
}

// MARK: - DataClass
struct CreditCardByLiabilityData: Codable {
    let liabilityData: [LiabilityDatas]
}

// MARK: - LiabilityDatum
struct LiabilityDatas: Codable {
    let cardHolderName, cardNumber, liabilityName, liabilityID: String
    let liabilityDatumDescription, cardStatus, cardAmount: String

    enum CodingKeys: String, CodingKey {
        case cardHolderName = "card_holder_name"
        case cardNumber = "card_number"
        case liabilityName = "liability_name"
        case liabilityID = "liability_id"
        case liabilityDatumDescription = "description"
        case cardStatus = "card_status"
        case cardAmount = "card_amount"
    }
}
