import Foundation


//MARK: -  Home viewModel
class HomeModel: NSObject {

    var alertId : String?
    var alertType : String?
    var imageUrl : String?
    var occurenceTimeStamp : Int?
    var deviceName : String?
    var deviceId : String?
    var locationName : String?
    var locationId : String?
    var acknowledged : Bool?
    var bookMarked : Bool?
    var isCorrect : Bool?
    var comment : String?
    var videoUrl : String?
    var device : String?
    var smokeValue : Int?
   
    
    init(alertId : String?,alertType: String?,imageUrl: String?,occurenceTimeStamp: Int?,deviceName: String?,deviceId: String?,locationName: String?,locationId: String?,acknowledged: Bool?,bookMarked: Bool,isCorrect: Bool,comment: String?,videoUrl: String?,device: String?,smokeValue: Int?) {
        
        if let alertId = alertId {
            self.alertId = alertId
        }
        
        if let alertType = alertType {
            self.alertType = alertType
        }
      
        if let imageUrl = imageUrl {
            self.imageUrl = imageUrl
        }
        
        if let occurenceTimeStamp = occurenceTimeStamp {
            self.occurenceTimeStamp = occurenceTimeStamp
        }
        
        if let deviceName = deviceName {
            self.deviceName = deviceName
        }
        
        if let deviceId = deviceId {
            self.deviceId = deviceId
        }
        
        if let locationName = locationName {
            self.locationName = locationName
        }
        
        if let locationId = locationId {
            self.locationId = locationId
        }
        
        if let acknowledged = acknowledged {
            self.acknowledged = acknowledged
        }
        
        self.acknowledged = acknowledged
        
        self.bookMarked = bookMarked
        
        self.isCorrect = isCorrect
        
        if let comment = comment {
            self.comment = comment
        }
        
        if let videoUrl = videoUrl {
            self.videoUrl = videoUrl
        }
        
        if let device = device {
            self.device = device
        }
        
        if let smokeValue = smokeValue {
            self.smokeValue = smokeValue
        }
        
    }
}

//MARK: -  AlertType viewModel
class AlertTypeModel: NSObject{
    var name : String?
    var id : Int?
    var isSelected : Bool?
    
    
    init(id: Int?, name: String?, isSelected:Bool?) {
        
        if let id = id {
            self.id = id
        }
        
        if let name = name {
            self.name = name
        }
      
        self.isSelected = isSelected

        
    }
}


//MARK: -  Location viewModel
class LocationModel: NSObject{
    var name : String?
    var id : String?
    var isSelected : Bool?
    
    
    init(id: String?, name: String?, isSelected:Bool?) {
        
        if let id = id {
            self.id = id
        }
        
        if let name = name {
            self.name = name
        }
      
        self.isSelected = isSelected

        
    }
}



//MARK: -  Device viewModel
class DeviceModel: NSObject {

    var id : String?
    var name : String?
    var connected : Bool?
    var locationId : String?
    var locationName : String?
    var macAddress : String?
    var firmwareVersion : String?
    var wifiName : String?
    var subscriptionId : String?
    var availableFeatures : [String]?
    var subscriptionExpiryDate : String?
    var currentUserPermission : String?
    var deviceType : String?
    var subscriptionStatus : String?
    var disabled : Bool?
    
    var devicePermissions = [DevicePermissions]()
   
    
    init(id : String?,name: String?,connected: Bool?,locationId: String?,locationName: String?,macAddress: String?,firmwareVersion: String?,wifiName: String?,subscriptionId: String?,availableFeatures: [String]?,subscriptionExpiryDate: String?,currentUserPermission: String?,devicePermissions: [[String:Any]]?,deviceType: String?,subscriptionStatus: String?,disabled: Bool?) {
        
        if let id = id {
            self.id = id
        }
        
        if let name = name {
            self.name = name
        }
      
        self.connected = connected
        
        if let locationId = locationId {
            self.locationId = locationId
        }
        
        if let locationName = locationName {
            self.locationName = locationName
        }
        
        if let macAddress = macAddress {
            self.macAddress = macAddress
        }
        
        if let firmwareVersion = firmwareVersion {
            self.firmwareVersion = firmwareVersion
        }
        
        if let wifiName = wifiName {
            self.wifiName = wifiName
        }
        
        if let subscriptionId = subscriptionId {
            self.subscriptionId = subscriptionId
        }
     
        if let availableFeatures = availableFeatures {
            self.availableFeatures = availableFeatures
        }
        
        if let subscriptionExpiryDate = subscriptionExpiryDate {
            self.subscriptionExpiryDate = subscriptionExpiryDate
        }
        
        if let currentUserPermission = currentUserPermission {
            self.currentUserPermission = currentUserPermission
        }
    
        if let deviceType = deviceType {
            self.deviceType = deviceType
        }
        
        if let subscriptionStatus = subscriptionStatus {
            self.subscriptionStatus = subscriptionStatus
        }

        for obj in devicePermissions ?? []{
            let objData = DevicePermissions.init(userEmail: obj["userEmail"] as? String, permission: obj["permission"] as? String)
            self.devicePermissions.append(objData)
        }
        
        self.disabled = disabled
        
    }
}

//MARK: -  Device Permissions viewModel
class DevicePermissions: NSObject {
    var userEmail : String?
    var permission : String?
    
    init(userEmail: String?, permission: String?) {
        
        if let userEmail = userEmail {
            self.userEmail = userEmail
        }
        
        if let permission = permission {
            self.permission = permission
        }
    }
}
