import Foundation

// MARK: - Welcome
struct UserModel: Codable {
    let status: Int
    let message: String
}
