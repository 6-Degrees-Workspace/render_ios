import Foundation

// MARK: - Welcome
struct AddCCModel: Codable {
    let status: Int
    let message: String
    let data: AddCCData
}

// MARK: - DataClass
struct AddCCData: Codable {
    let cardNumber, liabilityName, liabilityID, cardName: String
    let availableLimit, institutionName, cardCurrentOutstanding: String

    enum CodingKeys: String, CodingKey {
        case cardNumber = "card_number"
        case liabilityName = "liability_name"
        case liabilityID = "liability_id"
        case cardName = "card_name"
        case availableLimit = "available_limit"
        case institutionName = "institution_name"
        case cardCurrentOutstanding = "card_current_outstanding"
    }
}
