import Foundation

// MARK: - Welcome
struct AssetAllTypeModel: Codable {
    let status: Int
    let message: String
    let data: AssetTypeData
}

// MARK: - DataClass
struct AssetTypeData: Codable {
    let categoryID: String
    let assetTypes: [AssetType]

    enum CodingKeys: String, CodingKey {
        case categoryID = "category_id"
        case assetTypes
    }
}

// MARK: - AssetType
struct AssetType: Codable {
    let assetTypeID, assetTypeName, sequence: String

    enum CodingKeys: String, CodingKey {
        case assetTypeID = "asset_type_id"
        case assetTypeName = "asset_type_name"
        case sequence
    }
}
