import Foundation

// MARK: - Welcome
struct GetAllIncomeDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: GetAllIncomeDetailByCategoryData
}

// MARK: - DataClass
struct GetAllIncomeDetailByCategoryData: Codable {
    let incomeData: [IncomeDatum]
    let categoryWiseTotalIncomeValue: String
    let plaidAndManual: String
    let plaidIncomesValue: String

    enum CodingKeys: String, CodingKey {
        case incomeData
        case categoryWiseTotalIncomeValue = "category_wise_total_income_value"
        case plaidAndManual = "total_incomes"
        case plaidIncomesValue = "plaid_incomes_value"
        
        
    }
}

// MARK: - IncomeDatum
struct IncomeDatum: Codable {
    let categoryImageURL: String
    let categoryID, payFrequency, incomeID, categoryName: String
    let incomeDate, categoryCode, incomeAmount, categoryIcon: String
    let employerName, employeeName: String

    enum CodingKeys: String, CodingKey {
        case categoryImageURL = "category_image_url"
        case categoryID = "category_id"
        case payFrequency = "pay_frequency"
        case incomeID = "income_id"
        case categoryName = "category_name"
        case incomeDate = "income_date"
        case categoryCode = "category_code"
        case incomeAmount = "income_amount"
        case categoryIcon = "category_icon"
        case employerName = "employer_name"
        case employeeName = "employee_name"
    }
}
