import Foundation

// MARK: - Welcome
struct GetUserAllBankAccountsModel: Codable {
    let status: Int
    let message: String
    let data: [GetUserAllBankAccountsData]?
}

// MARK: - Datum
struct GetUserAllBankAccountsData: Codable {
    let institutionID, bankAccountID, isPlaidReauthorized, institutionName: String

    enum CodingKeys: String, CodingKey {
        case institutionID = "institution_id"
        case bankAccountID = "bank_account_id"
        case institutionName = "institution_name"
        case isPlaidReauthorized = "is_plaid_reauthorized"
    }
}
