import Foundation

// MARK: - Welcome
struct LogoutModel: Codable {
    let status: Int
    let message: String
}
