import Foundation

// MARK: - Welcome
struct CreateAccessTokenModel: Codable {
    let status: Int
    let message: String
}
