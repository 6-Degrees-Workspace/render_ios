import Foundation

// MARK: - Welcome
struct AssetAddPersonalModel: Codable {
    let status: Int
    let message: String
    //let data: AssetAddPersonalData
}

// MARK: - DataClass
struct AssetAddPersonalData: Codable {
    let assetID, assetType, categoryName, assetCurrentValue: String
    let assetDate, assetName: String

    enum CodingKeys: String, CodingKey {
        case assetID = "asset_id"
        case assetType = "asset_type"
        case categoryName = "category_name"
        case assetCurrentValue = "asset_current_value"
        case assetDate = "asset_date"
        case assetName = "asset_name"
    }
}
