import Foundation

// MARK: - Welcome
struct addAssetHomePaidoffModel: Codable {
    let status: Int
    let message: String
    //let data: addAssetHomePaidoffData
}

// MARK: - DataClass
struct addAssetHomePaidoffData: Codable {
    let assetID, assetCurrentValue, categoryName, assetDate: String
    let assetName: String

    enum CodingKeys: String, CodingKey {
        case assetID = "asset_id"
        case assetCurrentValue = "asset_current_value"
        case categoryName = "category_name"
        case assetDate = "asset_date"
        case assetName = "asset_name"
    }
}
