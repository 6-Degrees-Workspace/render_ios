import Foundation

// MARK: - Welcome
struct LiabilitiesModel: Codable {
    let status: Int
    let message: String
    let data: LiabilitiesData
}

// MARK: - DataClass
struct LiabilitiesData: Codable {
    let liabilitiesTotalValue: String
    let liabilitiesTotalValueOriginal: String
    let catData: [CatLiabilitiesData]

    enum CodingKeys: String, CodingKey {
        case liabilitiesTotalValue = "liabilities_total_value"
        case liabilitiesTotalValueOriginal = "liabilities_total_value_original"
        case catData
    }
}

// MARK: - CatDatum
struct CatLiabilitiesData: Codable {
    let categoryWiseTotalLiabilitiesValue, categoryIconHexCode, categoryName, categoryID: String
    let categoryCode: String
    let categoryImageURL: String
    let categoryWiseTotalLiabilitiesValueOriginal: String
    

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalLiabilitiesValue = "category_wise_total_liabilities_value"
        case categoryIconHexCode = "category_icon_hex_code"
        case categoryName = "category_name"
        case categoryID = "category_id"
        case categoryCode = "category_code"
        case categoryImageURL = "category_image_url"
        case categoryWiseTotalLiabilitiesValueOriginal = "category_wise_total_liabilities_value_original"
        
    }
}
