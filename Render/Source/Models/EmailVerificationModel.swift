import Foundation

// MARK: - Welcome
struct EmailVerificationModel: Codable {
    let status: Int
    let message: String
}
