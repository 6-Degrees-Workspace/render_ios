import Foundation

// MARK: - Welcome
struct MainCategoryModel: Codable {
    let status: Int
    let message: String
    let data: [DataMainCategory]
}

// MARK: - Datum
struct DataMainCategory: Codable {
    let categoryIcon, categoryName, categoryID, categoryCode: String
    let categoryIconHex,categoryImageUrl: String

    enum CodingKeys: String, CodingKey {
        case categoryIcon = "category_icon"
        case categoryName = "category_name"
        case categoryID = "category_id"
        case categoryCode = "category_code"
        case categoryIconHex = "category_icon_hex"
        case categoryImageUrl = "category_image_url"
        
    }
}
