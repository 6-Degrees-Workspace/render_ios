import Foundation

// MARK: - Welcome
struct CreateLinkTokenModel: Codable {
    let status: Int
    let message: String
    let data: CreateLinkTokenData
}

// MARK: - DataClass
struct CreateLinkTokenData: Codable {
    let linkToken, requestID: String
    //let expiration: String

    enum CodingKeys: String, CodingKey {
        case linkToken = "link_token"
        case requestID = "request_id"
        //case expiration
    }
}
