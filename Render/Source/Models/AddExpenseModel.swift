import Foundation

// MARK: - Welcome
struct AddExpenseModel: Codable {
    let status: Int
    let message: String
    //let data: AddExpenseData
}

// MARK: - DataClass
struct AddExpenseData: Codable {
    let expenseCategoryName, expenseType, expenseAmount, expenseID: String?

    enum CodingKeys: String, CodingKey {
        case expenseCategoryName = "expense_category_name"
        case expenseType = "expense_type"
        case expenseAmount = "expense_amount"
        case expenseID = "expense_id"
    }
}
