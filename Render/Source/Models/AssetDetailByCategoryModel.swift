import Foundation

// MARK: - Welcome
struct AssetDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: AssetDetail
}

// MARK: - DataClass
struct AssetDetail: Codable {
    let categoryWiseTotalAssetsValue: String
    let plaidAndManual: String
    let plaidAssetsValue: String
    let assetData: [AssetDatum]

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalAssetsValue = "category_wise_total_assets_value"
        case plaidAndManual = "total_assets"
        case plaidAssetsValue = "plaid_assets_value"
        
        case assetData
    }
}

// MARK: - AssetDatum
struct AssetDatum: Codable {
    let assetDate, assetID, subcategoryName, assetName: String
    let assetType: String?
    let subcategoryIcon: String
    let institutionName: String?
    let assetCurrentValue: String?
    let accountNumber: String?
    let assetPurchasePrice: String?
    let equity: String?
    let subcategoryImageUrl: String?
    

    enum CodingKeys: String, CodingKey {
        case assetDate = "asset_date"
        case assetID = "asset_id"
        case subcategoryName = "subcategory_name"
        case assetName = "asset_name"
        case assetType = "asset_type"
        case subcategoryIcon = "subcategory_icon"
        case institutionName = "institution_name"
        case assetCurrentValue = "asset_current_value"
        case accountNumber = "account_number"
        case assetPurchasePrice = "asset_purchase_price"
        case equity = "equity"
        case subcategoryImageUrl = "subcategory_image_url"
        
        
    }
}



// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)
/*
import Foundation

// MARK: - Welcome
struct AssetDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: AssetDetail
}

// MARK: - DataClass
struct AssetDetail: Codable {
    let categoryWiseTotalAssetsValue: String
    let assetData: [AssetDatum]

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalAssetsValue = "category_wise_total_assets_value"
        case assetData
    }
}

// MARK: - AssetDatum
struct AssetDatum: Codable {
    let assetID, assetCurrentValue, assetDate, subcategoryName: String
    let assetName: String

    enum CodingKeys: String, CodingKey {
        case assetID = "asset_id"
        case assetCurrentValue = "asset_current_value"
        case assetDate = "asset_date"
        case subcategoryName = "subcategory_name"
        case assetName = "asset_name"
    }
}
*/

/*import Foundation

// MARK: - Welcome
struct AssetDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: AssetDetail
}

// MARK: - DataClass
struct AssetDetail: Codable {
    let catData: [CategorytData]
    let categoryID, categoryWiseTotalAssetsValue: String

    enum CodingKeys: String, CodingKey {
        case catData
        case categoryID = "category_id"
        case categoryWiseTotalAssetsValue = "category_wise_total_assets_value"
    }
}

// MARK: - CatDatum
struct CategorytData: Codable {
    let assetID, assetName, tenurePercentage, catDatumDescription: String
    let assetCurrentValue, assetValueType, assetDate: String

    enum CodingKeys: String, CodingKey {
        case assetID = "asset_id"
        case assetName = "asset_name"
        case tenurePercentage = "tenure_percentage"
        case catDatumDescription = "description"
        case assetCurrentValue = "asset_current_value"
        case assetValueType = "asset_value_type"
        case assetDate = "asset_date"
    }
}
*/

