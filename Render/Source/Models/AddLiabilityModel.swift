import Foundation

// MARK: - Welcome
struct AddLiabilityModel: Codable {
    let status: Int
    let message: String
    //let data: DataAddLiability
}

// MARK: - DataClass
struct DataAddLiability: Codable {
    let outstandingAmount, liabilityName, liabilityID, principleAmount: String
    let loanTye, loanName, institutionName, loanAccountNumber: String

    enum CodingKeys: String, CodingKey {
        case outstandingAmount = "outstanding_amount"
        case liabilityName = "liability_name"
        case liabilityID = "liability_id"
        case principleAmount = "principle_amount"
        case loanTye = "loan_tye"
        case loanName = "loan_name"
        case institutionName = "institution_name"
        case loanAccountNumber = "loan_account_number"
    }
}
