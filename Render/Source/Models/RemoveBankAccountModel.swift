import Foundation

// MARK: - Welcome
struct RemoveBankAccountModel: Codable {
    let status: Int
    let message: String
}


struct ReauthorizeAccountModel: Codable {
    let status: Int
    let message: String
}

