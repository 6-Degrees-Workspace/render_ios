import Foundation

// MARK: - Welcome
struct LoanTypeModel: Codable {
    let status: Int
    let message: String
    let data: LoanTypeData
}

// MARK: - DataClass
struct LoanTypeData: Codable {
    let categoryID: String
    let loanTypes: [LoanType]

    enum CodingKeys: String, CodingKey {
        case categoryID = "category_id"
        case loanTypes
    }
}

// MARK: - LoanType
struct LoanType: Codable {
    let sequence, loanTypeName, loanTypeID, loanTypeCode: String

    enum CodingKeys: String, CodingKey {
        case sequence
        case loanTypeName = "loan_type_name"
        case loanTypeID = "loan_type_id"
        case loanTypeCode = "loan_type_code"
    }
}
