import Foundation

// MARK: - Welcome
struct LoginResponseModel: Codable {
    let status: Int
    let message: String
    let data: UserData
}

// MARK: - DataClass
struct UserData: Codable {
    let userEmail: String
    let userProfilePic: String
    let citizenship, addressLine2, userMobile, employer: String
    let property, isVerifyEmail, addressLine1, otp: String
    let city, lastName, children, ownCar: String
    let maritalStatus, state, gender, occupation: String
    let fullName, zipCode, token, isPinExist: String
    let firstName: String

    enum CodingKeys: String, CodingKey {
        case userEmail, userProfilePic, citizenship
        case addressLine2 = "address_line2"
        case userMobile, employer, property
        case isVerifyEmail = "is_verify_email"
        case addressLine1 = "address_line1"
        case otp = "OTP"
        case city
        case lastName = "last_name"
        case children
        case ownCar = "own_car"
        case maritalStatus = "marital_status"
        case state, gender, occupation
        case fullName = "full_name"
        case zipCode = "zip_code"
        case token
        case isPinExist = "is_pin_exist"
        case firstName = "first_name"
    }
}

/*
import Foundation

// MARK: - Welcome
struct LoginResponseModel: Codable {
    //"status" : 200, Exit user
    //"status" : 201, New user
    
//isPinExist = "1"
//isPinExist = "0"
    
    let status: Int
    let message: String
    let data: UserData
}

// MARK: - DataClass
struct UserData: Codable {
    let userEmail, gender, userProfilePic, otp: String
    let isPinExist, lastName, fullName, token: String
    let firstName: String

    enum CodingKeys: String, CodingKey {
        case userEmail, gender, userProfilePic
        case otp = "OTP"
        case isPinExist = "is_pin_exist"
        case lastName = "last_name"
        case fullName = "full_name"
        case token
        case firstName = "first_name"
    }
}

*/
