import Foundation

// MARK: - Welcome
struct SubCategoryModel: Codable {
    let status: Int
    let message: String
    let data: subDataClass
}

// MARK: - DataClass
struct subDataClass: Codable {
    let categoryID: String
    let subcatData: [SubcatDatum]

    enum CodingKeys: String, CodingKey {
        case categoryID = "category_id"
        case subcatData
    }
}

// MARK: - SubcatDatum
struct SubcatDatum: Codable {
    let subCategoryID, subCategoryCode, subCategoryName, subCategoryIcon: String
    let subCategoryImageUrl, subCategoryIconHexCode: String

    enum CodingKeys: String, CodingKey {
        case subCategoryID = "sub_category_id"
        case subCategoryCode = "sub_category_code"
        case subCategoryName = "sub_category_name"
        case subCategoryIcon = "sub_category_icon"
        case subCategoryIconHexCode = "sub_category_icon_hex_code"
        case subCategoryImageUrl = "sub_category_image_url"
        
    }
}
