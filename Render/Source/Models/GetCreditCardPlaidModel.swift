import Foundation

// MARK: - Welcome
struct GetCreditCardPlaidModel: Codable {
    let status: Int
    let message: String
    let data: GetCreditCardPlaid
}

// MARK: - DataClass
struct GetCreditCardPlaid: Codable {
    let categoryWiseTotalManualLiabilities, totalLiabilities, categoryWiseTotalPlaidLiabilities: String
    let liabilityData: [LiabilityCCPlaidData]

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalManualLiabilities = "category_wise_total_manual_liabilities"
        case totalLiabilities = "total_liabilities"
        case categoryWiseTotalPlaidLiabilities = "category_wise_total_plaid_liabilities"
        case liabilityData
    }
}

// MARK: - LiabilityDatum
struct LiabilityCCPlaidData: Codable {
    let categoryIconHexcode, cardNumber, categoryID: String
    let availableLimit: JSONNull?
    let categoryName, categoryCode: String
    let currentOutstandingAmount: Int
    let institutionName: String
    let categoryImageURL: String

    enum CodingKeys: String, CodingKey {
        case categoryIconHexcode = "category_icon_hexcode"
        case cardNumber = "card_number"
        case categoryID = "category_id"
        case availableLimit = "available_limit"
        case categoryName = "category_name"
        case categoryCode = "category_code"
        case currentOutstandingAmount = "current_outstanding_amount"
        case institutionName = "institution_name"
        case categoryImageURL = "category_image_url"
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
