import Foundation

// MARK: - Welcome
struct GetAllIncomeTypesModel: Codable {
    let status: Int
    let message: String
    let data: GetAllIncomeTypesData
}

// MARK: - DataClass
struct GetAllIncomeTypesData: Codable {
    let incomeTypes: [IncomeType]
}

// MARK: - IncomeType
struct IncomeType: Codable {
    let sequence, incomeTypeID, incomeTypeName, incomeTypeCode: String

    enum CodingKeys: String, CodingKey {
        case sequence
        case incomeTypeID = "income_type_id"
        case incomeTypeName = "income_type_name"
        case incomeTypeCode = "income_type_code"
    }
}
