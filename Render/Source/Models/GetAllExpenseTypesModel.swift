import Foundation

// MARK: - Welcome
struct GetAllExpenseTypesModel: Codable {
    let status: Int
    let message: String
    let data: GetAllExpenseTypesData
}

// MARK: - DataClass
struct GetAllExpenseTypesData: Codable {
    let expenseTypes: [ExpenseType]
    let categoryID: String

    enum CodingKeys: String, CodingKey {
        case expenseTypes
        case categoryID = "category_id"
    }
}

// MARK: - ExpenseType
struct ExpenseType: Codable {
    let sequence, expenseTypeCode, expenseTypeID, expenseTypeName: String
    let expenseTypeImage: String?

    enum CodingKeys: String, CodingKey {
        case sequence
        case expenseTypeCode = "expense_type_code"
        case expenseTypeID = "expense_type_id"
        case expenseTypeName = "expense_type_name"
        case expenseTypeImage = "expense_type_image"
    }
}
