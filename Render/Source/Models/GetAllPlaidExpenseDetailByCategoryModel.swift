import Foundation

// MARK: - Welcome
struct GetAllPlaidExpenseDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: GetAllPlaidExpenseDetailData
}

// MARK: - DataClass
struct GetAllPlaidExpenseDetailData: Codable {
    let categoryWiseTotalManualExpenses, categoryWiseTotalPlaidExpenses, totalExpenses: String
    let expenseData: [ExpensePlaidData]

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalManualExpenses = "category_wise_total_manual_expenses"
        case categoryWiseTotalPlaidExpenses = "category_wise_total_plaid_expenses"
        case totalExpenses = "total_expenses"
        case expenseData
    }
}

// MARK: - ExpenseDatum
struct ExpensePlaidData: Codable {
    let expenseType: String
    let expenseTypeImage: String
    let expenseAmount, expenseDate, categoryID, categoryName: String
    let categoryCode, categoryIcon: String
    let categoryImageURL: String

    enum CodingKeys: String, CodingKey {
        case expenseType = "expense_type"
        case expenseTypeImage = "expense_type_image"
        case expenseAmount = "expense_amount"
        case expenseDate = "expense_date"
        case categoryID = "category_id"
        case categoryName = "category_name"
        case categoryCode = "category_code"
        case categoryIcon = "category_icon"
        case categoryImageURL = "category_image_url"
    }
}
