import Foundation

// MARK: - Welcome
struct GetAllExpenseDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: GetAllExpenseDetailByCategoryData
}

// MARK: - DataClass
struct GetAllExpenseDetailByCategoryData: Codable {
    let expenseData: [ExpenseDatum]
    let categoryWiseTotalExpenseValue: String
    let plaidAndManual: String
    let plaidExpensesValue: String

    enum CodingKeys: String, CodingKey {
        case expenseData
        case categoryWiseTotalExpenseValue = "category_wise_total_expense_value"
        case plaidExpensesValue = "plaid_expenses_value"
        case plaidAndManual = "total_expenses"
    }
}

// MARK: - ExpenseDatum
struct ExpenseDatum: Codable {
    let expenseTypeImage: String
    let expenseID, expenseType, categoryID, categoryName: String
    let categoryCode, categoryIcon, expenseAmount, expenseDate: String
    let categoryImageURL: String
    let expenseTypeId: String
    

    enum CodingKeys: String, CodingKey {
        case expenseTypeImage = "expense_type_image"
        case expenseID = "expense_id"
        case expenseType = "expense_type"
        case categoryID = "category_id"
        case categoryName = "category_name"
        case categoryCode = "category_code"
        case categoryIcon = "category_icon"
        case expenseAmount = "expense_amount"
        case expenseDate = "expense_date"
        case categoryImageURL = "category_image_url"
        case expenseTypeId = "expense_type_id"
        
    }
}
