import Foundation

// MARK: - Welcome
struct VerifyModel: Codable {
    let status: Int
    let message: String
}
