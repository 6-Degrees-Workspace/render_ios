import Foundation

// MARK: - Welcome
struct CategoryModel: Codable {
    let status: Int
    let message: String
    let data: [Datum]
}

// MARK: - Datum
struct Datum: Codable {
    let categoryIcon, categoryName, categoryID, categoryIconHex: String

    enum CodingKeys: String, CodingKey {
        case categoryIcon = "category_icon"
        case categoryName = "category_name"
        case categoryID = "category_id"
        case categoryIconHex = "category_icon_hex"
    }
}
