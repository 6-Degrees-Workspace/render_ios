import Foundation

// MARK: - Welcome
struct EditExpenseModel: Codable {
    let status: Int
    let message: String
    //let data: EditExpenseData
}

// MARK: - DataClass
struct EditExpenseData: Codable {
    let expenseCategoryName, expenseType, expenseAmount, expenseID: String

    enum CodingKeys: String, CodingKey {
        case expenseCategoryName = "expense_category_name"
        case expenseType = "expense_type"
        case expenseAmount = "expense_amount"
        case expenseID = "expense_id"
    }
}
