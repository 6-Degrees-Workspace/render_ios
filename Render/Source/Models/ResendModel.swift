import Foundation

// MARK: - Welcome
struct ResendModel: Codable {
    let status: Int
    let message: String
    let data: DataOTP
}

// MARK: - DataClass
struct DataOTP: Codable {
    let otp: String

    enum CodingKeys: String, CodingKey {
        case otp = "OTP"
    }
}
