import Foundation

// MARK: - Welcome
struct UpdateAssetModel: Codable {
    let status: Int
    let message: String
   // let data: UpdateAssetData
}

// MARK: - DataClass
struct UpdateAssetData: Codable {
    let assetID, categoryName, assetCurrentValue: String
    let assetName: String

    enum CodingKeys: String, CodingKey {
        case assetID = "asset_id"
        //case assetType = "asset_type"
        case categoryName = "category_name"
        case assetCurrentValue = "asset_current_value"
        //case assetDate = "asset_date"
        case assetName = "asset_name"
    }
}
