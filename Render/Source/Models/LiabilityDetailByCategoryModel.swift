import Foundation

// MARK: - Welcome
struct LiabilityDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: LiabilityDetails
}

// MARK: - DataClass
struct LiabilityDetails: Codable {
    let categoryWiseTotalLiabilitiesValue: String
    let plaidLiabilitiesValues: String
    let totalLiabilities: String
    let liabilityData: [LiabilityDatum]

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalLiabilitiesValue = "category_wise_total_liabilities_value"
        case plaidLiabilitiesValues = "plaid_liabilities_values"
        case totalLiabilities = "total_liabilities"
        case liabilityData
    }
}

// MARK: - LiabilityDatum
struct LiabilityDatum: Codable {
    let institutionName, liabilityDate, loanTypeIcon, loanAccountNumber: String
    let loanType, categoryID, originalLoanDate: String
    let categoryImageURL: String
    let currentOutstandingAmount, categoryCode, principleAmount, categoryIconHexcode: String
    let categoryName, liabilityID, intrestRate, loanTerm: String
    let monthlyPayment, loanName: String

    enum CodingKeys: String, CodingKey {
        case institutionName = "institution_name"
        case liabilityDate = "liability_date"
        case loanTypeIcon = "loan_type_icon"
        case loanAccountNumber = "loan_account_number"
        case loanType = "loan_type"
        case categoryID = "category_id"
        case originalLoanDate = "original_loan_date"
        case categoryImageURL = "category_image_url"
        case currentOutstandingAmount = "current_outstanding_amount"
        case categoryCode = "category_code"
        case principleAmount = "principle_amount"
        case categoryIconHexcode = "category_icon_hexcode"
        case categoryName = "category_name"
        case liabilityID = "liability_id"
        case intrestRate = "intrest_rate"
        case loanTerm = "loan_term"
        case monthlyPayment = "monthly_payment"
        case loanName = "loan_name"
    }
}
