import Foundation

// MARK: - Welcome
struct CreditCardModel: Codable {
    let status: Int
    let message: String
    let data: CreditCardData
}

// MARK: - DataClass
struct CreditCardData: Codable {
    let liabilityData: [LiabilityCreditCardData]
}

// MARK: - LiabilityDatum
struct LiabilityCreditCardData: Codable {
    let cardNumber, cardAmount, cardStatus, liabilityName: String
    let cardHolderName, liabilityID: String

    enum CodingKeys: String, CodingKey {
        case cardNumber = "card_number"
        case cardAmount = "card_amount"
        case cardStatus = "card_status"
        case liabilityName = "liability_name"
        case cardHolderName = "card_holder_name"
        case liabilityID = "liability_id"
    }
}
