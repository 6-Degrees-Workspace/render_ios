import Foundation

// MARK: - Welcome
struct AssetAddCDModel: Codable {
    let status: Int
    let message: String
    //let data: DataAssetAddCD
}

// MARK: - DataClass
struct DataAssetAddCD: Codable {
    let assetDate, assetID, assetName, categoryName: String
    let institutionName, assetCurrentValue, accountNumber: String

    enum CodingKeys: String, CodingKey {
        case assetDate = "asset_date"
        case assetID = "asset_id"
        case assetName = "asset_name"
        case categoryName = "category_name"
        case institutionName = "institution_name"
        case assetCurrentValue = "asset_current_value"
        case accountNumber = "account_number"
    }
}
