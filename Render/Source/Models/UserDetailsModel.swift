import Foundation

// MARK: - Welcome
struct UserDetailsModel: Codable {
    let status: Int
    let message: String
    let data: UserDetailsData
}

// MARK: - DataClass
struct UserDetailsData: Codable {
    let userEmail: String
    let userProfilePic: String
    let citizenship, addressLine2, userMobile, employer: String
    let property, isVerifyEmail, addressLine1, city: String
    let lastName, children, ownCar, maritalStatus: String
    let state, gender, occupation, fullName: String
    let zipCode, firstName: String

    enum CodingKeys: String, CodingKey {
        case userEmail, userProfilePic, citizenship
        case addressLine2 = "address_line2"
        case userMobile, employer, property
        case isVerifyEmail = "is_verify_email"
        case addressLine1 = "address_line1"
        case city
        case lastName = "last_name"
        case children
        case ownCar = "own_car"
        case maritalStatus = "marital_status"
        case state, gender, occupation
        case fullName = "full_name"
        case zipCode = "zip_code"
        case firstName = "first_name"
    }
}
