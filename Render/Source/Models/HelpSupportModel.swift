//
//  HelpSupportModel.swift
//  Render
//
//  Created by Apple on 28/04/22.
//

import Foundation



struct HelpSupportModel: Codable {
    let status: Int
    let message: String
    var data: [GetAllStaticPage]?
}

struct GetAllStaticPage: Codable {
    let pagesId :String
    let pagesTitle :String
    let pagesCode :String
    let pagesDesc :String
    
    enum CodingKeys: String, CodingKey {
        case pagesId, pagesTitle, pagesCode, pagesDesc
    }
}

struct FAQsModel: Codable {
    let status: Int
    let message: String
    var data: [GetAllFAQs]?
}

struct GetAllFAQs: Codable {
    let faqId :String
    let faqTitle :String
    let faqDesc :String
    
    enum CodingKeys: String, CodingKey {
        case faqId, faqTitle, faqDesc
    }
}
