import Foundation

// MARK: - Welcome
struct GetAllPlaidLiabilityDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: GetAllPlaidLiabilityDetailData
}

// MARK: - DataClass
struct GetAllPlaidLiabilityDetailData: Codable {
    let categoryWiseTotalManualLiabilities, totalLiabilities, categoryWiseTotalPlaidLiabilities: String
    let liabilityData: [LiabilityPlaidData]

    enum CodingKeys: String, CodingKey {
        case categoryWiseTotalManualLiabilities = "category_wise_total_manual_liabilities"
        case totalLiabilities = "total_liabilities"
        case categoryWiseTotalPlaidLiabilities = "category_wise_total_plaid_liabilities"
        case liabilityData
    }
}

// MARK: - LiabilityDatum
struct LiabilityPlaidData: Codable {
    let categoryIconHexcode, categoryID, categoryCode, categoryName: String
    let loanType, loanAccountMask, institutionName, loanName: String
    let currentOutstandingAmount: Double
    let categoryImageURL: String

    enum CodingKeys: String, CodingKey {
        case categoryIconHexcode = "category_icon_hexcode"
        case categoryID = "category_id"
        case categoryCode = "category_code"
        case categoryName = "category_name"
        case loanType = "loan_type"
        case loanAccountMask = "loan_account_mask"
        case institutionName = "institution_name"
        case loanName = "loan_name"
        case currentOutstandingAmount = "current_outstanding_amount"
        case categoryImageURL = "category_image_url"
    }
}
