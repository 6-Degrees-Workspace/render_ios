import Foundation

// MARK: - Welcome
struct GetAllPlaidAssetDetailByCategoryModel: Codable {
    let status: Int
    let message: String
    let data: GetAllPlaidAssetDetailData
}

// MARK: - DataClass
struct GetAllPlaidAssetDetailData: Codable {
    let assetData: [AssetPlaidData]
    let categoryWiseTotalManualAssets, totalAssets, categoryWiseTotalPlaidAssets: String

    enum CodingKeys: String, CodingKey {
        case assetData
        case categoryWiseTotalManualAssets = "category_wise_total_manual_assets"
        case totalAssets = "total_assets"
        case categoryWiseTotalPlaidAssets = "category_wise_total_plaid_assets"
    }
}

// MARK: - AssetDatum
struct AssetPlaidData: Codable {
    let subcategoryName, subcategoryIcon, assetName, accountMask: String
    let assetType: String?
    let parentCategoryID, subcategoryCode, institutionName: String
    let assetCurrentValue: Int
    let subcategoryImageURL: String
    let subcategoryID: String

    enum CodingKeys: String, CodingKey {
        case subcategoryName = "subcategory_name"
        case subcategoryIcon = "subcategory_icon"
        case assetName = "asset_name"
        case accountMask = "account_mask"
        case assetType = "asset_type"
        case parentCategoryID = "parent_category_id"
        case subcategoryCode = "subcategory_code"
        case institutionName = "institution_name"
        case assetCurrentValue = "asset_current_value"
        case subcategoryImageURL = "subcategory_image_url"
        case subcategoryID = "subcategory_id"
    }
}
