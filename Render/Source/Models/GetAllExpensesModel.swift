import Foundation

// MARK: - Welcome
struct GetAllExpensesModel: Codable {
    let status: Int
    let message: String
    let data: GetAllExpensesData
}

// MARK: - DataClass
struct GetAllExpensesData: Codable {
    let catData: [GetCatData]
    let expensesTotalValueOriginal, expensesTotalValue: String

    enum CodingKeys: String, CodingKey {
        case catData
        case expensesTotalValueOriginal = "expenses_total_value_original"
        case expensesTotalValue = "expenses_total_value"
    }
}

// MARK: - CatDatum
struct GetCatData: Codable {
    let expenseValue, categoryID, categoryCode, categoryName: String
    let categoryIconHexCode: String
    let categoryImageURL: String
    let expenseValueOriginal: String

    enum CodingKeys: String, CodingKey {
        case expenseValue = "expense_value"
        case categoryID = "category_id"
        case categoryCode = "category_code"
        case categoryName = "category_name"
        case categoryIconHexCode = "category_icon_hex_code"
        case categoryImageURL = "category_image_url"
        case expenseValueOriginal = "expense_value_original"
    }
}
