import Foundation

// MARK: - Welcome
struct UpdateIncomeModel: Codable {
    let status: Int
    let message: String
    //let data: UpdateIncomeData
}

// MARK: - DataClass
struct UpdateIncomeData: Codable {
    let incomeID, incomeAmount, employeeName, employerName: String
    let payFrequency: String

    enum CodingKeys: String, CodingKey {
        case incomeID = "income_id"
        case incomeAmount = "income_amount"
        case employeeName = "employee_name"
        case employerName = "employer_name"
        case payFrequency = "pay_frequency"
    }
}
