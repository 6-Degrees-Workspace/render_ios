import Foundation
import UIKit

struct LiabilitieService {
    
    
    func addLoanLiabilities(parameters:[String:Any], complition:@escaping( Result<AddLiabilityModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addLiability.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AddLiabilityModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func addCCLiabilities(parameters:[String:Any], complition:@escaping( Result<AddCCModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addLiability.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AddCCModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllLiabilityDetailByCategoryCreditCard(parameters:[String:Any], complition:@escaping( Result<GetCreditCardModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllLiabilityDetailByCategory.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<GetCreditCardModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func getAllLoanTypes(parameters:[String:Any], complition:@escaping( Result<LoanTypeModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllLoanTypes.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, parameters: parameters, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<LoanTypeModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func deleteLiabilitie(parameters:[String:Any], complition:@escaping( Result<DeleteAssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.deleteLiability.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<DeleteAssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func updateLiability(parameters:[String:Any], complition:@escaping( Result<UpdateCCModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.updateLiability.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UpdateCCModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func updateLoanLiability(parameters:[String:Any], complition:@escaping( Result<UpdateCCModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.updateLiability.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UpdateCCModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllPlaidLiabilityDetailByCategory(parameters:[String:Any], complition:@escaping( Result<GetAllPlaidLiabilityDetailByCategoryModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllPlaidLiabilityDetailByCategory.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<GetAllPlaidLiabilityDetailByCategoryModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func getAllPlaidLiabilityDetailByCategoryCreditCard(parameters:[String:Any], complition:@escaping( Result<GetCreditCardPlaidModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllPlaidLiabilityDetailByCategory.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<GetCreditCardPlaidModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
}
