import Foundation
import UIKit


struct SignupService {
    
    func uploadProfilePhoto(parameters:[String:Any], complition:@escaping( Result<UserModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.uploadProfilePhoto.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UserModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func createPin(parameters:[String:Any], complition:@escaping( Result<UserModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.createPin.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UserModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func verifyPin(parameters:[String:Any], complition:@escaping( Result<UserModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.verifyPin.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UserModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func checkPin(complition:@escaping( Result<VerifyModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.checkPin.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<VerifyModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
}

