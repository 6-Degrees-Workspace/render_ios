import Foundation
import UIKit


struct LoginService {
    
    func sendOTPForLogin(parameters:[String:Any], complition:@escaping( Result<LoginResponseModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "device-id":"ee2ad5ad3bd09c6c",
                                       "device-type":"iPhone",
                                       "device-fcm-id":"123",
                                       "version":"12"]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.generateOTP.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<LoginResponseModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func verifyOTPForLogin(parameters:[String:Any], complition:@escaping( Result<VerifyModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.verifyOTP.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<VerifyModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func resendOTPForLogin(complition:@escaping( Result<ResendModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.resendOTP.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<ResendModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func updateProfile(parameters:[String:Any], complition:@escaping( Result<UserModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.updateProfile.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UserModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func logout(complition:@escaping( Result<LogoutModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.logout.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<LogoutModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func emailVerification(parameters:[String:Any], complition:@escaping( Result<EmailVerificationModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.verifyEmail.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<EmailVerificationModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getUserDetail(parameters:[String:Any], complition:@escaping( Result<UserDetailsModel, Error>,_ statusCode: Int) -> Void){
        
        
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getUserDetail.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UserDetailsModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
        
    }
    
    func shootEmailforVerification(parameters:[String:Any], complition:@escaping( Result<EmailVerificationModel, Error>,_ statusCode: Int) -> Void){
        
        
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.shootEmailforVerification.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<EmailVerificationModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
}
