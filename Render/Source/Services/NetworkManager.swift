import UIKit
import Foundation
import Alamofire

protocol NetworkManagerProtocol {
    func loadDataWithModel<T:Decodable>(request: RequestProtocol, complition:@escaping( Result<T, Error>,Int) -> Void)
}

protocol RequestProtocol {
    var url : String { get }
    var methodType : HttpMethod { get }
    var parameters : [String: Any]? { get }
    var headers : [String: String]? { get }
    var encoding : ParameterEncoding {get}
}

extension RequestProtocol {
    var url : String { return "" }
    var methodType : HttpMethod { return .get }
    var parameters : [String: Any]? { return nil }
    var headers : [String: String]? { return nil }
    var encoding : ParameterEncoding {return .default}
}

struct AlmofireNetworkManager: NetworkManagerProtocol {
    func loadDataWithModel<T:Decodable>(request: RequestProtocol, complition: @escaping (Result<T, Error>,Int) -> Void) {
        print("Requset URL --------->\(request.url)")
        print("Requset Method Type --------->\(request.methodType)")
        print("Requset Encoding --------->\(request.encoding)")
        print("Requset Parameters --------->\(String(describing: request.parameters))")
        print("Requset Headers --------->\(String(describing: request.headers))")
        AF.createRequest(requestProtocol: request).response { response in
            if let error = response.error {
                return complition(.failure(error),response.response?.statusCode ?? 0)
            }
//            print("HTTP Response Status Code ---- \(response.response?.statusCode) ")
            if let data = response.data {
                do {
                    // To do
                    // Need to make the model dynamic
                    if let jsondata = try? JSONSerialization.jsonObject(with: data) as? [String:Any]{
                       // print("Success ---- \(jsondata) ")
                        
                        let status = jsondata["status"]
                        let message = jsondata["message"]
                 
                        if status as! Int == 401 && message as! String == "Your session is expired, please login again"{
                            NotificationCenter.default.post(name: Notification.Name("Logout"), object: nil, userInfo: nil)
                            return
                        }
                        
                    }
                    if let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers),
                       let jsonData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted) {
                        print("Success Json ------ \(String(decoding: jsonData, as: UTF8.self))")
                        print(jsonData)
                    } else {
                        print("json data malformed")
                    }
                    
                    
                    let model = try JSONDecoder().decode(T.self, from: data)
                    print(model)
                    return complition(.success(model),response.response?.statusCode ?? 0)
//                } catch let DecodingError.dataCorrupted(context) {
//                    print(context)
//                } catch let DecodingError.keyNotFound(key, context) {
//                    print("Key '\(key)' not found:", cNJBUIB N BY7 8GHVUTFGXDS4M  W5WEI6  sti9fv65ryontext.debugDescription)
//                    print("codingPath:", context.codingPath)
//                } catch let DecodingError.valueNotFound(value, context) {
//                    print("Value '\(value)' not found:", context.debugDescription)
//                    print("codingPath:", context.codingPath)
//                } catch let DecodingError.typeMismatch(type, context)  {
//                    print("Type '\(type)' mismatch:", context.debugDescription)
//                    print("codingPath:", context.codingPath)
                } catch {
                    print("error: ", error)
                    return complition(.failure(error),response.response?.statusCode ?? 0)
                    //(.failure(CustomError(errorMessage: "We could not process your request at this time. Please try again or contact your Advisor for assistance.", statusCode: 999)))
                }
            }
        }
    }
}

protocol JsonResposeProtocol {
}

struct JsonArrayResponse : JsonResposeProtocol {
    var data : [[String:Any]]
}

struct JsonDictResponse : JsonResposeProtocol {
    var data : [String:Any]
}

enum HttpMethod: String {
    case post
    case get
    case delete
    case put
}

extension HttpMethod {
    var almofire: Alamofire.HTTPMethod {
        return Alamofire.HTTPMethod(rawValue: self.rawValue.uppercased())
    }
}

enum ParameterEncoding: Int {
    case `default`
    case json
    case query
}

extension ParameterEncoding {
    var almofire: Alamofire.ParameterEncoding {
        switch self {
        case .default:
            return URLEncoding.default
        case .json:
            return JSONEncoding.default
        case .query:
            return URLEncoding.queryString
        }
    }
}

extension Alamofire.Session {
    func createRequest(requestProtocol: RequestProtocol) -> DataRequest {
        var headers: HTTPHeaders?
        if let headerDict = requestProtocol.headers {
            headers = HTTPHeaders(headerDict)
        }
        return request(requestProtocol.url, method: requestProtocol.methodType.almofire, parameters: requestProtocol.parameters, encoding: requestProtocol.encoding.almofire, headers: headers, interceptor: nil, requestModifier: nil)
    }
}

struct Requester: RequestProtocol {
    var url : String
    var methodType : HttpMethod
    var parameters : [String: Any]?
    var headers : [String: String]?
    var encoding : ParameterEncoding
}

struct CustomError:Error{
    var errorMessage: String
    var statusCode : Int

}
