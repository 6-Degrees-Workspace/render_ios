import Foundation
import UIKit

struct AssetService {
    
    //Asset----
    func getAllAssets(parameters:[String:Any], complition:@escaping( Result<AssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllAssets.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post ,parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllAssetsWithOutDate(complition:@escaping( Result<AssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllAssets.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post , headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllAssetDetailByCategory(parameters:[String:Any], complition:@escaping( Result<AssetDetailByCategoryModel, Error>,_ statusCode: Int) -> Void){
        
        
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllAssetDetailByCategory.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post , parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetDetailByCategoryModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
        
    }
    
    func getAllAssetType(parameters:[String:Any], complition:@escaping( Result<AssetAllTypeModel, Error>,_ statusCode: Int) -> Void) {
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllAssetTypes.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get ,parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetAllTypeModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func updateAsset(parameters:[String:Any], complition:@escaping( Result<UpdateAssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.updateAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UpdateAssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func addAssetSavingAccount(parameters:[String:Any], complition:@escaping( Result<AssetAddModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetAddModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func addAssetCD(parameters:[String:Any], complition:@escaping( Result<AssetAddCDModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetAddCDModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func addAssetInvestment(parameters:[String:Any], complition:@escaping( Result<AssetAddInvestmentModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetAddInvestmentModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    
    func addAssetPersonal(parameters:[String:Any], complition:@escaping( Result<AssetAddPersonalModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetAddPersonalModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func addAssetHomeEquity(parameters:[String:Any], complition:@escaping( Result<addAssetHomeEquityModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<addAssetHomeEquityModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func addAssetHomePaidoff(parameters:[String:Any], complition:@escaping( Result<addAssetHomePaidoffModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<addAssetHomePaidoffModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func deleteAsset(parameters:[String:Any], complition:@escaping( Result<DeleteAssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.deleteAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<DeleteAssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func updateAssetHomePaidoff(parameters:[String:Any], complition:@escaping( Result<UpdateAssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.updateAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UpdateAssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func updateAssetHomeEquity(parameters:[String:Any], complition:@escaping( Result<UpdateAssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.updateAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UpdateAssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func updateCDAsset(parameters:[String:Any], complition:@escaping( Result<UpdateCDModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.updateAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UpdateCDModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
}

