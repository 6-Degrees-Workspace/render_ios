//
//  HelpSupportService.swift
//  Render
//
//  Created by Apple on 28/04/22.
//

import Foundation

struct HelpSupportService {
    
    func getAllStaticPages(complition:@escaping( Result<HelpSupportModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllStaticPages.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<HelpSupportModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllFaqs(complition:@escaping( Result<FAQsModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllFaqs.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<FAQsModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
}

