import Foundation
import UIKit


struct PlaidService {
    
    func createLinkToken(complition:@escaping( Result<CreateLinkTokenModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.createLinkToken.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<CreateLinkTokenModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func createAccessToken(parameters:[String:Any], complition:@escaping( Result<CreateAccessTokenModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.createAccessToken.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<CreateAccessTokenModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getUserAllBankAccounts(parameters:[String:Any], complition:@escaping( Result<GetUserAllBankAccountsModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getUserAllBankAccounts.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<GetUserAllBankAccountsModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func removeBankAccount(parameters:[String:Any], complition:@escaping( Result<RemoveBankAccountModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.removeBankAccount.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<RemoveBankAccountModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func reauthorizeAccount(parameters:[String:Any], complition:@escaping( Result<ReauthorizeAccountModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.reauthorizeAccount.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<ReauthorizeAccountModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
}
