import Foundation
import UIKit


struct HomeService {
    
    func getDashboardData(parameters:[String:Any], complition:@escaping( Result<GetDashboardDataModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getDashboardData.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, parameters: parameters, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<GetDashboardDataModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
}
