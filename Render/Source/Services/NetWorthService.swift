import Foundation
import UIKit

struct NetWorthService {
    
    //GetAllCategories----
    func getMasterCategories(parameters:[String:Any], complition:@escaping( Result<MainCategoryModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getMasterCategories.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, parameters: parameters, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<MainCategoryModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllSubcategories(parameters:[String:Any], complition:@escaping( Result<SubCategoryModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllSubcategories.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, parameters: parameters, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<SubCategoryModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllCategories(parameters:[String:Any], complition:@escaping( Result<CategoryModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllCategories.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, parameters: parameters, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<CategoryModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    //Liabilities----
    
    func getAllLiabilities(parameters:[String:Any], complition:@escaping( Result<LiabilitiesModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllLiabilities.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<LiabilitiesModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllLiabilitiesWithOutDate(complition:@escaping( Result<LiabilitiesModel, Error>,_ statusCode: Int) -> Void){
        
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllLiabilities.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<LiabilitiesModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllLiabilityDetailByCategory(parameters:[String:Any], complition:@escaping( Result<LiabilityDetailByCategoryModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllLiabilityDetailByCategory.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<LiabilityDetailByCategoryModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func getAllLiabilityDetailByCategoryForCreditCardServiceCall(parameters:[String:Any], complition:@escaping( Result<CreditCardModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllLiabilityDetailByCategory.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<CreditCardModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllLiabilityDetailByLiability(parameters:[String:Any], complition:@escaping( Result<LiabilityDetailByLiabilityIdModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getLiabilityDetailByLiabilityId.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, parameters: parameters, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<LiabilityDetailByLiabilityIdModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getLiabilityDetailByServiceLiabilityIDForCreditCard(parameters:[String:Any], complition:@escaping( Result<CreditCardByLiabilityIdModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getLiabilityDetailByLiabilityId.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .get, parameters: parameters, headers: headers, encoding: .default)
        almofireManager.loadDataWithModel(request: request) { (result : Result<CreditCardByLiabilityIdModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    
    func addLiabilitiesCreditCard(parameters:[String:Any], complition:@escaping( Result<LiabilitiesCreditCardModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addLiability.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<LiabilitiesCreditCardModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func addLiabilities(parameters:[String:Any], complition:@escaping( Result<AddLiabilityModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addLiability.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AddLiabilityModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    //Asset----
    func getAllAssets(parameters:[String:Any], complition:@escaping( Result<AssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        // let deviceid = UserDefaults.standard.value(forKey: "DeviceId") as? String ?? ""
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllAssets.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                //                print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllAssetsWithOutDate(complition:@escaping( Result<AssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllAssets.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post , headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    func getAllAssetDetailByCategory(parameters:[String:Any], complition:@escaping( Result<AssetDetailByCategoryModel, Error>,_ statusCode: Int) -> Void){
        
        
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllAssetDetailByCategory.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post , parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetDetailByCategoryModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
        
    }
    
    
    func updateAsset(parameters:[String:Any], complition:@escaping( Result<UpdateAssetModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.updateAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<UpdateAssetModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func addAsset(parameters:[String:Any], complition:@escaping( Result<AssetAddModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.addAsset.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .json)
        almofireManager.loadDataWithModel(request: request) { (result : Result<AssetAddModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }
    
    
    func getAllPlaidAssetDetailByCategory(parameters:[String:Any], complition:@escaping( Result<GetAllPlaidAssetDetailByCategoryModel, Error>,_ statusCode: Int) -> Void){
        let almofireManager = AlmofireNetworkManager()
        let headers:[String:String] = ["Content-Type":"application/json",
                                       "authorization":ApplicationPreference.getToken() ?? ""]
        
        let urlString = Config.shared.baseURL + Constants.EndPoint.getAllPlaidAssetDetailByCategory.rawValue
        let request: RequestProtocol = Requester(url: urlString , methodType: .post, parameters: parameters, headers: headers, encoding: .query)
        almofireManager.loadDataWithModel(request: request) { (result : Result<GetAllPlaidAssetDetailByCategoryModel,Error>, statusCode:Int) in
            switch result{
            case .success(let model):
                // print("Success -- \(model)")
                return complition(.success(model), statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                return complition(.failure(error), statusCode)
            }
        }
    }

    
}
