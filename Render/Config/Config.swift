import Foundation
enum Environment: String {
    
    case qa = "QA"
    case prod = "Prod"
    
    static var infoPlist: NSDictionary! {
        let plistPath = Bundle.main.path(forResource: "Info", ofType: "plist")
        return NSDictionary(contentsOfFile: plistPath!)
    }
    
    static var env: Environment {
        guard let appEnv = infoPlist["AppEnvironment"] as? String else {
            return .prod
        }
        return Environment(rawValue: appEnv) ?? Environment.prod
    }
    
}

struct Config: Decodable {
    let baseURL: String
    let environment: String
    static let shared = Config(Environment.env)
    private init(_ env: Environment) {
        let decoder = PropertyListDecoder()
        guard let plistURL = Bundle.main.url(forResource: "Config_\(env.rawValue)", withExtension: "plist"), let data = try? Data(contentsOf: plistURL) else {
            fatalError("Plist not found")
        }
        do {
            self = try decoder.decode(Config.self, from: data)
        }
        catch {
            fatalError("Unable to decode config \(error.localizedDescription)")
        }
    }
}

